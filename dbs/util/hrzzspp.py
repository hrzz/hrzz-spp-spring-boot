#!/usr/bin/env python
# -*- coding: UTF-8 -*-


def csv2sql(From,Table,Fields):

   file   = From
   inattr = Fields
   table  = Table
   
   f = open(file)
   
   delimiter = ","
   i     = 0
   index = []
   data  = []
   type  = []
   attr  = []
   attr  = inattr.split(",")
   
   for j,e in enumerate(attr):

       type.append(e[e.index("~") + 1:])
       attr[j] = e[:e.index("~")]
   
   for line in f:
        if (i == 0):
           index = line.split(delimiter)
           i = 1
        else:
           data        = line.split(delimiter)
           format_data = ""
           field       = ""
   
           for k,z in enumerate(attr):
               
               if (z[0] != "#"):
                field = data[index.index(z)]
               else:
                field = z[1:]
   
               if (type[k] == "%S"):
                   field = "'" + field + "'"
   
               if (k == 0):
                 format_data += field
               else:
                 format_data = format_data + "," + field
   
   
           print "insert into %s values (%s);" % (table,format_data) 
            
   f.close()


def printHelp():
   print "Usage: %s [-f|--fromfile=csv_file -t|--table=dbs_table -c|--fields=IME_%S,PREZIME_%D,OIB_%S"

if __name__ == "__main__":

   import getopt
   import sys

   From  = None
   Table = None
   Fields = None

   opts, args = getopt.getopt(sys.argv[1:], "f:t:c:", ["fromfile=", "table=" "fields="])
   for a, o in opts:
      if (a == "-f" or a == "--fromfile"):
         From = o
      elif (a == "-t" or a == "--table"):
         Table = o
      elif (a == "-c" or a == "--fields"):
         Fields = o


   if (not From or not Table or not Fields):
        printHelp()
        sys.exit(1)

   
   csv2sql(From,Table,Fields)
