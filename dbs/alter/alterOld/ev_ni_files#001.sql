CREATE TABLE `ev_ni_files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(45) NOT NULL DEFAULT 'ev_narativno_izvjesce',
  `ref_id` int(11) NOT NULL,
  `ev_ni_type` varchar(45) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_blob` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ev_narativno_izvjesce_idx` (`ref_id`),
  KEY `i_evnif_file_name` (`file_name`),
  KEY `i_evnif_ref_table_id` (`ref_table`,`ref_id`),
  KEY `i_evnif_ev_ni_type` (`ev_ni_type`),
  CONSTRAINT `fk_ev_narativno_izvjesce` FOREIGN KEY (`ref_id`) REFERENCES `ev_narativno_izvjesce` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB;

