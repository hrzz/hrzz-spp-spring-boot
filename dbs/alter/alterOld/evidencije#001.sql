create TABLE `hrzzspp`.`status_evidencija` (
  `id` INT(11) NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));
  
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('1', 'Neaktivno');
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('2', 'Aktivno');
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('3', 'Zaključano');
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('4', 'Isporučeno');
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('5', 'Prihvaćeno');
INSERT INTO `hrzzspp`.`status_evidencija` (`id`, `naziv`) VALUES ('6', 'Odbijeno');

create TABLE `hrzzspp`.`evidencija` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) NOT NULL,
  `opis` VARCHAR(45) NOT NULL,
  `naziv_xhtml` VARCHAR(45) NOT NULL,
  `status_prk` INT(1) default 1,
  `komentar_prk` VARCHAR(500) NOT NULL,
  `status_id` INT(1) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_period_idx` (`period_id` ASC),
  INDEX `fk_stat_ev_idx` (`status_id` ASC),
   CONSTRAINT `fk_status_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `hrzzspp`.`status_evidencija` (`id`),
  CONSTRAINT `fk_period_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);
   
    
--Tablica 'evidencija' povećanje polja opis na 200
ALTER TABLE `hrzzspp`.`evidencija` 
CHANGE COLUMN `opis` `opis` VARCHAR(200) NOT NULL ;
