/*TABLICA 1 : Rezultati"*/
ALTER TABLE `hrzzspp`.`ev_rezultati` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 2 : Točke provjere"*/
ALTER TABLE `hrzzspp`.`ev_tocke_provjere` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 3 : Osoblje*/
ALTER TABLE `hrzzspp`.`ev_osoblje` 
DROP COLUMN `status_prk`,
DROP COLUMN `komentar_prk`;

ALTER TABLE `hrzzspp`.`ev_osoblje_br_osoba` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 4 : Istraživačka grupa*/
ALTER TABLE `hrzzspp`.`ev_istrazivacka_skupina` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 5 : Usavršavanje*/
ALTER TABLE `hrzzspp`.`ev_usavrsavanje` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 6 : Broj doktorskih disertacija proizašlih s projekta*/
ALTER TABLE `hrzzspp`.`ev_dok_disertacije` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 7 : Popis doktorskih disertacija i diplomskih/magistarskih radova*/
ALTER TABLE `hrzzspp`.`ev_popis_radova` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `poveznica`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 8 : Popis znanstvenih radova proizašlih iz aktivnosti na projektu*/
ALTER TABLE `hrzzspp`.`ev_popis_zn_radova` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `poveznica`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 9 : Popis ostalih objavljenih publikacija proizašlih iz aktivnosti na projektu*/
ALTER TABLE `hrzzspp`.`ev_popis_publikacija` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `poveznica`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 10 : Popis diseminacijskih aktivnosti*/
ALTER TABLE `hrzzspp`.`ev_popis_dis_aktivnosti` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 11 : Popis svih patentnih prijava, žigova, industrijskih dizajna i dr.*/
ALTER TABLE `hrzzspp`.`ev_patenata` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 12 : Novoostvarene suradnje*/
ALTER TABLE `hrzzspp`.`ev_suradnje` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 13 : Prijave na inozemne fondove*/
ALTER TABLE `hrzzspp`.`ev_fondova` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;


/*TABLICA 14 : Ostalo*/
ALTER TABLE `hrzzspp`.`ev_ostalo` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

/*TABLICA 15 : Utjecaj projekta*/
ALTER TABLE `hrzzspp`.`ev_dobrobit` 
ADD COLUMN `komentar_prk` VARCHAR(500) NULL DEFAULT NULL AFTER `period_id`,
ADD COLUMN `status_prk` INT(10) NOT NULL DEFAULT 1 AFTER `komentar_prk`;

