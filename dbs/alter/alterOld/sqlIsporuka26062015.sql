update projects set summary_hr = "";

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_narativno_izvjesce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `pp_odstupanje` VARCHAR(600),
  `pp_objasnjenje` VARCHAR(600),
  `pp_nova_pitanja` VARCHAR(600),
  `pp_preporuke` VARCHAR(600),
  `pp_potpora` VARCHAR(600),
  `ig_aktivnosti` VARCHAR(600),
  `ig_promjena` VARCHAR(600),
  `os_prijetnje` VARCHAR(600),
  `os_poduzimanja` VARCHAR(600),
  `datum_mjesto` VARCHAR(60),
  `kreiran_pdf` INT(1),
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_evni_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

alter table projects add column celnik_ustanove varchar(60);
alter table projects add column poveznica_mreza varchar(100);