CREATE TABLE `hrzzspp`.`users_roles_periods` (
  `period_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`period_id`, `user_id`, `role_id`),
  INDEX `fk_urp_r_id_idx` (`role_id` ASC),
  INDEX `fk_urp_u_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_urp_rp_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_urp_ro_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `hrzzspp`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_urp_us_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `hrzzspp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;



DELETE FROM `hrzzspp`.`evaluator_izvjesce` WHERE id > 0;

ALTER TABLE `hrzzspp`.`evaluator_izvjesce` 
DROP FOREIGN KEY `fk_project_id`,
DROP FOREIGN KEY `fk_user_id`;
ALTER TABLE `hrzzspp`.`evaluator_izvjesce` 
CHANGE COLUMN `project_id` `period_id` INT(11) NOT NULL ;
ALTER TABLE `hrzzspp`.`evaluator_izvjesce` 
ADD CONSTRAINT `fk_ei_period_id`
  FOREIGN KEY (`period_id`)
  REFERENCES `hrzzspp`.`reporting_periods` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_ei_user_id`
  FOREIGN KEY (`user_id`)
  REFERENCES `hrzzspp`.`users` (`id`)
  ON DELETE NO ACTION
  ON UPDATE NO ACTION;