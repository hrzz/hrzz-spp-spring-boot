INSERT INTO `hrzzspp`.`roles` (`name`, `description`) VALUES ('EVL', 'Evaluator');

CREATE TABLE if not exists `users_roles_projects` (
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`role_id`,`user_id`),
  CONSTRAINT `fk_urp_u_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_urp_r_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_urp_p_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table ev_rezultati add column status_prk int (1);

alter table ev_rezultati add column komentar_prk varchar(500);