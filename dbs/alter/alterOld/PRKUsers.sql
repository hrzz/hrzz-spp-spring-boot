UPDATE `hrzzspp`.`users` SET `username`='prirodne.znanosti@hrzz.hr' WHERE `id`='99997';
UPDATE `hrzzspp`.`users` SET `username`='drustvene.znanosti@hrzz.hr' WHERE `id`='99996';
UPDATE `hrzzspp`.`users` SET `username`='biomedicina@hrzz.hr' WHERE `id`='99995';
INSERT INTO `hrzzspp`.`users` (`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('99994', 'biotehnicke.znanosti@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');
INSERT INTO `hrzzspp`.`users` (`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('99993', 'tehnicke.znanosti@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');
INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('1', '99993');
INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('1', '99994');
INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('99994', 'Ivana', 'Stanić', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');
INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('99993', 'Amina', 'Othman', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');

ALTER TABLE `hrzzspp`.`major_scientific_fields` 
ADD COLUMN `mail` VARCHAR(45) NULL AFTER `modified_at`;

UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='biotehnicke.znanosti@hrzz.hr' WHERE `id`='2';
UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='biomedicina@hrzz.hr' WHERE `id`='1';
UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='drustvene.znanosti@hrzz.hr' WHERE `id`='3';
UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='humanisticke.znanosti@hrzz.hr' WHERE `id`='4';
UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='prirodne.znanosti@hrzz.hr' WHERE `id`='5';
UPDATE `hrzzspp`.`major_scientific_fields` SET `mail`='tehnicke.znanosti@hrzz.hr' WHERE `id`='6';
