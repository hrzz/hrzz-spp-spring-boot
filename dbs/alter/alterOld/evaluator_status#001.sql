CREATE TABLE `hrzzspp`.`evaluator_status` (
  `id` INT NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  `ikona` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

ALTER TABLE `hrzzspp`.`evaluator_status` 
CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

