alter table reporting_periods drop column iznos;

alter table  ev_popis_zn_radova drop column naziv_casopisa;
alter table  ev_popis_zn_radova drop column broj_casopisa;
alter table  ev_popis_zn_radova drop column godiste_casopisa;
alter table  ev_popis_zn_radova drop column autori;

alter table ev_popis_dis_aktivnosti modify naziv varchar(500);
alter table ev_popis_zn_radova modify naziv varchar(500);

INSERT INTO `sifarnik` VALUES (76,'ev_fondova.status',1,'P','Prihvaćen');
INSERT INTO `sifarnik` VALUES (77,'ev_fondova.status',2,'O','Odbijen');
INSERT INTO `sifarnik` VALUES (78,'ev_fondova.status',3,'N','Nepoznat');

INSERT INTO `ev_sifarnik` VALUES (76,'ev_fondova','status',1,'P','Prihvaćen');
INSERT INTO `ev_sifarnik` VALUES (77,'ev_fondova','status',2,'O','Odbijen');
INSERT INTO `ev_sifarnik` VALUES (78,'ev_fondova','status',3,'N','Nepoznat');
