ALTER TABLE hrzzspp.ev_rezultati
  DROP COLUMN rok_dovrsetka;
  
ALTER TABLE hrzzspp.ev_rezultati
  ADD COLUMN poveznica varchar(256);
  
ALTER TABLE hrzzspp.ev_tocke_provjere
  DROP COLUMN rok;
  
DROP TABLE IF EXISTS `hrzzspp`.`ev_suradnje`;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_suradnje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redni_broj` INT NOT NULL,
  `vrsta_suradnje` VARCHAR(20) NULL,
  `institucija_suradnje` VARCHAR(200) NULL,
  `ime_i_prezime_suradnje` VARCHAR(100) NULL,
  `opis` VARCHAR(256) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evs_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evs_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `hrzzspp`.`ev_ostalo`;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_ostalo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_nematerijalnih` INT NULL,
  `naziv_nematerijalnih` VARCHAR(100) NULL,
  `opis_nematerijalnih` VARCHAR(100) NULL,
  `broj_proizvoda` INT NULL,
  `naziv_proizvoda` VARCHAR(100) NULL,
  `opis_proizvoda` VARCHAR(100) NULL,
  `project_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evu_project_idx` (`project_id` ASC),
  CONSTRAINT `fk_evu_project`
    FOREIGN KEY (`project_id`)
    REFERENCES `hrzzspp`.`projects` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

DROP TABLE IF EXISTS `hrzzspp`.`ev_osoblje`;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_osoblje` (
  `id` INT NOT NULL,
  `radno_mjesto` VARCHAR(45) NOT NULL,
  `broj_zena` INT NOT NULL,
  `broj_muskaraca` INT NOT NULL,
  `broj_u_priv_sektoru` INT NOT NULL,
  `broj_u_jav_sektoru` INT NOT NULL,
  `broj_u_sveucilistu` INT NOT NULL,
  `broj_u_inozemstvu` INT NOT NULL,
  `broj_nezaposlenih` INT NOT NULL,
  `project_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
	UNIQUE INDEX `u_ev_osoblje_id_project` (`id`, `project_id`),
  CONSTRAINT `fk_ev_projekt`
    FOREIGN KEY (`project_id`)
    REFERENCES `hrzzspp`.`projects` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
