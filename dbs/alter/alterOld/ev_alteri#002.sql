alter table ev_popis_publikacija modify autori varchar(500);
alter table ev_popis_zn_radova modify autori varchar(500);

insert into roles (name,description) values ('PRK','Programski koordinator');
insert into roles (name,description) values ('VOD','Voditelj');

insert into users_roles (role_id,user_id) select 2, leader_id from projects;

INSERT INTO `hrzzspp`.`users` 
(`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, 
`is_credentials_expired`, `is_enabled`) 
VALUES ('99999', '99999', 'hrzzspp', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users_data` 
(`id`, `user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, 
`phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, 
`address_of_residence`, `zip_code`, `city`, `nationality`, `institution_tmp`) 
VALUES ('242', '99999', 'Zvonimir', 'Deković', 'dr. sc.', 'doktor/ica znanosti', 
'66', '051228692', 'prirodne.znanosti@hrzz.hr', '11111111111', 'm', '11111', 
'051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)', 'null');

insert into users_roles (role_id,user_id) values(1,99999);

alter table `ev_istrazivacka_skupina` modify `datum_ukljucivanja` date DEFAULT NULL;

INSERT INTO `sifarnik` VALUES (75,'ev_popis_zn_radova.kvartil',5,'A1','A1');
INSERT INTO `ev_sifarnik` VALUES (75,'ev_popis_zn_radova','kvartil',5,'A1','A1');
