ALTER TABLE `hrzzspp`.`status_evidencija` 
ADD COLUMN `ikona` VARCHAR(45) NOT NULL AFTER `naziv`;

UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-neaktivno' WHERE `id`='1';
UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-aktivno' WHERE `id`='2';
UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-zakljucano' WHERE `id`='3';
UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-isporuceno' WHERE `id`='4';
UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-prihvaceno' WHERE `id`='5';
UPDATE `hrzzspp`.`status_evidencija` SET `ikona`='ikona-odbijeno' WHERE `id`='6';