alter table ev_rezultati drop index u_br_cilja;
alter table ev_rezultati drop index u_br_rezultata;

alter table ev_rezultati add index u_br_cilja (`br_cilja` ASC);
alter table ev_rezultati add index u_br_rezultata (`br_rezultata` ASC);

alter table ev_usavrsavanje modify ime_prezime varchar(500);
alter table ev_popis_dis_aktivnosti modify ime_i_prezime varchar(500);
alter table ev_suradnje modify ime_i_prezime_suradnje varchar(500);
