CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_ostalo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NULL,
  `vrsta` CHAR(2) NULL,
  `naziv` VARCHAR(100) NULL,
  `opis` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evost_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evost_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;