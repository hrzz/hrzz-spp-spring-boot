CREATE TABLE `hrzzspp`.`evaluator_izvjesce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ciljevi_a`  INT ,
  `ciljevi_b` INT ,
  `ciljevi_c` INT ,
  `ciljevi_komentari` VARCHAR(1000) ,
  `implementacija_a` INT ,
  `implementacija_a_komentari` VARCHAR(1000) ,
  `implementacija_b` INT ,
  `implementacija_b_komentari` VARCHAR(1000) ,
  `evaluator_izvjescecol` VARCHAR(45) ,
  `diseminacija` INT ,
  `potpora` INT ,
  `komentari` VARCHAR(1000) ,
  `zav_ocjena_a` TINYINT(1) ,
  `zav_ocjena_b` TINYINT(1) ,
  `zav_ocjena_c` TINYINT(1) ,
  `zav_ocjena_d` TINYINT(1) ,
  `zav_ocjena_komentari` VARCHAR(1000) ,
  `primjenjivo_uspjesan` TINYINT(1) ,
  `primjenjivo_zanimljiv` TINYINT(1) ,
  `primjenjivo_znacajan` TINYINT(1) ,
  `primjenjivo_nacionalne` TINYINT(1) ,
  `primjenjivo_potencija` TINYINT(1) ,
  `primjenjivo_istrazivaci` TINYINT(1) ,
  `primjenjivo_sektor` TINYINT(1) ,
  `primjenjivo_drugo` TINYINT(1) ,
  `primjenjivo_komentari` VARCHAR(1000) ,
  `ucinak_d1` INT ,
  `ucinak_d2` INT ,
  `ucinak_d3` INT ,
  `ucinak_d4` INT ,
  `ucinak_komentari` VARCHAR(1000) ,
  `analiza_isplaceno` VARCHAR(45) ,
  `analiza_neutroseno` VARCHAR(45) ,
  `analiza_utroseno` VARCHAR(45) ,
  `analiza_procjena` VARCHAR(1000) ,
  `analiza_preporuke` VARCHAR(1000) ,
  `period_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_period_id_ei_idx` (`period_id` ASC),
  INDEX `fk_user_id_ei_idx` (`user_id` ASC),
  CONSTRAINT `fk_period_ei_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_ei_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `hrzzspp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);