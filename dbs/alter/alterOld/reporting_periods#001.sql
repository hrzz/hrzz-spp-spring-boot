ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD COLUMN `ordinal_number` INT(11) NOT NULL AFTER `project_id`;
ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD UNIQUE INDEX `u_rp_projord` (`project_id` ASC, `ordinal_number` ASC);
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
DROP FOREIGN KEY `fk_ppr_reporting_periods_id`;
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
DROP COLUMN `reporting_periods_id`,
DROP INDEX `fk_ppr_reporting_periods_id` ,
ADD INDEX `fk_ppr_reporting_periods_id_idx` (`period_id` ASC);
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
ADD CONSTRAINT `fk_ppr_reporting_periods_id`
  FOREIGN KEY (`period_id`)
  REFERENCES `hrzzspp`.`reporting_periods` (`id`);