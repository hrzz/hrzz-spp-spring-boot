CREATE TABLE `hrzzspp`.`arhiva_static_files` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NOT NULL,
  `file_name` VARCHAR(255) NOT NULL,
  `file_size` BIGINT(20) NOT NULL,
  `file_type` VARCHAR(100) NOT NULL,
  `file_blob` LONGBLOB NULL,
  `group` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Administrative form:', 'IPACSTOPIDD - Admin EN.pdf', '20000', 'application/pdf', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Application form:', 'ENGL_IPACSTOPIDD_15.9.2014.doc', '20000', 'application/msword', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Certificate of PhD:', ' ', '20000', ' ', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Letter of Intent:', 'IPACTOPIDD Letter of intent.pdf', '20000', 'application/pdf', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Work plan:', 'HRZZ-IP2014_Work Plan_form.docx', '20000', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Research budget form:', 'HRZZ_IP2014 IPACSTOPIDD engl.xlsx', '20000', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Institutional support form:', 'IPACSTOPIDD - Institutional support.pdf', '20000', 'application/pdf', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Other:', 'IPACSTOPIDD - Eticki29562.doc', '20000', 'application/msword', 'PROJECT_ENG');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Administrativni obrazac:', 'IPACSTOPIDD - Admin HR.pdf', '20000', 'application/pdf', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Prijavni obrazac:', 'HRV_IPACSTOPIDD_15.9.2014.doc', '20000', 'application/msword', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Potvrda o doktoratu:', ' ', '20000', ' ', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Pismo namjere:', 'IPACSTOPIDD Pismo namjere.pdf', '20000', 'application/pdf', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Radni plan:', 'HRZZ_IP_09_2014_Obrazac_Radni plan.docx', '20000', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Financijski plan:', 'HRZZ_IP_09_2014 IPACSTOPIDD hrv.xlsx', '20000', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Potpora ustanove:', 'IPACSTOPIDD - Potpora ustanova.pdf', '20000', 'application/pdf', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Ostalo:', 'IPASCTOPIDD - Etički.doc', '20000', 'application/msword', 'PROJECT_HRV');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Ugovor:', 'Ugovor o fin.pdf', '20000', 'application/pdf', 'UGOVOR');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Prilog 1:', 'Financijski plan.xls', '20000', 'application/vnd.ms-excel', 'UGOVOR');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Prilog 2:', 'Radni plan.doc', '20000', 'application/msword', 'UGOVOR');
INSERT INTO `hrzzspp`.`arhiva_static_files` (`title`, `file_name`, `file_size`, `file_type`, `group`) VALUES ('Prilog 3:', 'Izjava.pdf', '20000', 'application/pdf', 'UGOVOR');