alter table reporting_periods drop column iznos;

alter table  ev_popis_zn_radova drop column naziv_casopisa;
alter table  ev_popis_zn_radova drop column broj_casopisa;
alter table  ev_popis_zn_radova drop column godiste_casopisa;
alter table  ev_popis_zn_radova drop column autori;

alter table ev_popis_dis_aktivnosti modify naziv varchar(500);
alter table ev_popis_zn_radova modify naziv varchar(500);

INSERT INTO `sifarnik` VALUES (76,'ev_fondova.status',1,'P','Prihvaćen');
INSERT INTO `sifarnik` VALUES (77,'ev_fondova.status',2,'O','Odbijen');
INSERT INTO `sifarnik` VALUES (78,'ev_fondova.status',3,'N','Nepoznat');

INSERT INTO `ev_sifarnik` VALUES (76,'ev_fondova','status',1,'P','Prihvaćen');
INSERT INTO `ev_sifarnik` VALUES (77,'ev_fondova','status',2,'O','Odbijen');
INSERT INTO `ev_sifarnik` VALUES (78,'ev_fondova','status',3,'N','Nepoznat');

ALTER TABLE `ev_popis_publikacija` 
ADD COLUMN `poveznica` VARCHAR(255) NULL AFTER `period_id`;

ALTER TABLE `ev_popis_zn_radova` 
ADD COLUMN `poveznica` VARCHAR(255) NULL AFTER `period_id`;

ALTER TABLE `ev_popis_radova` 
ADD COLUMN `poveznica` VARCHAR(255) NULL AFTER `period_id`;

INSERT INTO `hrzzspp`.`users` 
(`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, 
`is_credentials_expired`, `is_enabled`) 
VALUES ('99998', 'humanisticke.znanosti@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users` 
(`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, 
`is_credentials_expired`, `is_enabled`) 
VALUES ('99997', 'prirodne.znanosti@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users` 
(`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, 
`is_credentials_expired`, `is_enabled`) 
VALUES ('99996', 'društvene.znanosti@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users` 
(`id`, `username`, `password`, `is_account_expired`, `is_account_locked`, 
`is_credentials_expired`, `is_enabled`) 
VALUES ('99995', 'biomedicina@hrzz.hr', 'hrzzspp', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users_data` 
(`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, 
`phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, 
`address_of_residence`, `zip_code`, `city`, `nationality`, `institution_tmp`) 
VALUES ('99998', 'Ines', 'Skelac', 'dr. sc.', 'doktor/ica znanosti', 
'66', '051228692', 'humanisticke.znanosti@hrzz.hr', '11111111111', 'z', '11111', 
'051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)', 'null');

INSERT INTO `hrzzspp`.`users_data` 
(`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, 
`phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, 
`address_of_residence`, `zip_code`, `city`, `nationality`, `institution_tmp`) 
VALUES ('99997', 'Amina', 'Othman', 'dr. sc.', 'doktor/ica znanosti', 
'66', '051228692', 'prirodne.znanosti@hrzz.hr', '11111111111', 'z', '11111', 
'051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)', 'null');

INSERT INTO `hrzzspp`.`users_data` 
(`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, 
`phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, 
`address_of_residence`, `zip_code`, `city`, `nationality`, `institution_tmp`) 
VALUES ('99996', 'Ana', 'Ravnić-Perfido', 'dr. sc.', 'doktor/ica znanosti', 
'66', '051228692', 'drustvene.znanosti@hrzz.hr', '11111111111', 'z', '11111', 
'051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)', 'null');

INSERT INTO `hrzzspp`.`users_data` 
(`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, 
`phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, 
`address_of_residence`, `zip_code`, `city`, `nationality`, `institution_tmp`) 
VALUES ('99995', 'Ivana', 'Stanić', 'dr. sc.', 'doktor/ica znanosti', 
'66', '051228692', 'biomedicina@hrzz.hr', '11111111111', 'z', '11111', 
'051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)', 'null');

insert into users_roles (role_id,user_id) values (1, 99998);
insert into users_roles (role_id,user_id) values (1, 99997);
insert into users_roles (role_id,user_id) values (1, 99996);
insert into users_roles (role_id,user_id) values (1, 99995);

update users u set username = (select email from users_data ud where ud.user_id=u.id)

alter table reporting_periods add approved_resources decimal(13,2) not null;