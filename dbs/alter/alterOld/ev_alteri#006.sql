alter table ev_popis_publikacija drop column stalni_identifikator;
alter table ev_popis_zn_radova drop column stalni_identifikator;

alter table reporting_periods add status char(1) not null;
update reporting_periods set status = '1';

INSERT INTO `sifarnik` VALUES (79,'ev_ostalo.vrsta',1,'NT','Nove tehnologije, metode, procesi, programi, softveri i sl. proizašli iz projekta');
INSERT INTO `sifarnik` VALUES (80,'ev_ostalo.vrsta',2,'NP','Novi proizvodi proizašli iz projekta');
INSERT INTO `sifarnik` VALUES (81,'ev_ostalo.vrsta',3,'OS','Ostalo');

INSERT INTO `ev_sifarnik` VALUES (79,'ev_ostalo','vrsta',1,'NT','Nove tehnologije, metode, procesi, programi, softveri i sl. proizašli iz projekta');
INSERT INTO `ev_sifarnik` VALUES (80,'ev_ostalo','vrsta',2,'NP','Novi proizvodi proizašli iz projekta');
INSERT INTO `ev_sifarnik` VALUES (81,'ev_ostalo','vrsta',3,'OS','Ostalo');

DROP TABLE IF EXISTS ev_ostalo;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_ostalo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NULL,
  `vrsta` CHAR(2) NULL,
  `naziv` VARCHAR(100) NULL,
  `opis` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evost_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evost_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_osoblje_br_osoba` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_osoba` INT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evosobr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evosobr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;