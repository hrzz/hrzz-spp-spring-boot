alter table reporting_periods
add column `status_id` INT(11) NOT NULL default 31

alter table reporting_periods
add CONSTRAINT `fk_rp_stat_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `hrzzspp`.`status_razdoblja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION

update status_razdoblja set role='VOD' where id between 31 and 36;

ALTER TABLE `hrzzspp`.`sukob_interesa` 
ADD COLUMN `tocke_sukoba` VARCHAR(200) NULL AFTER `vrijeme_promjene`;