ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD COLUMN `ukupna_sredstva` DECIMAL(13,2) NULL AFTER `status`,
ADD COLUMN `neutroseno` DECIMAL(13,2) NULL AFTER `ukupna_sredstva`,
ADD COLUMN `nenamjenski_utros` DECIMAL(13,2) NULL AFTER `neutroseno`;

INSERT INTO `sifarnik` VALUES (83,'reporting_periods.tip',1,'1','1.Izv. razdoblje');
INSERT INTO `sifarnik` VALUES (84,'reporting_periods.tip',2,'2','2.Izv. razdoblje');
INSERT INTO `sifarnik` VALUES (85,'reporting_periods.tip',3,'3','3.Izv. razdoblje');
INSERT INTO `sifarnik` VALUES (86,'reporting_periods.tip',4,'4','4.Izv. razdoblje');

INSERT INTO `ev_sifarnik` VALUES (83,'reporting_periods','tip',1,'1','1.Izv. razdoblje');
INSERT INTO `ev_sifarnik` VALUES (84,'reporting_periods','tip',2,'2','2.Izv. razdoblje');
INSERT INTO `ev_sifarnik` VALUES (85,'reporting_periods','tip',3,'3','3.Izv. razdoblje');
INSERT INTO `ev_sifarnik` VALUES (86,'reporting_periods','tip',4,'4','4.Izv. razdoblje');

ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD COLUMN tip INT(1) NOT NULL default 1;

create TABLE `hrzzspp`.`status_ikona` (
  `id` INT(11) NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  `ikona` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
  
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('1', 'Neaktivno','ikona-neaktivno');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('2', 'Aktivno','ikona-aktivno');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('3', 'Zaključano','ikona-zakljucano');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('4', 'Isporučeno','ikona-isporuceno');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('5', 'Prihvaćeno','ikona-prihvaceno');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('6', 'Odbijeno','ikona-odbijeno');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('7', 'Sukob interesa 1','ikona-suk-int1');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('8', 'Iks','ikona-iks');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('9', 'Upitnik','ikona-upitnik');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('10', 'Kvacica','ikona-kvacica');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('11', 'Usklicnik','ikona-usklicnik');
INSERT INTO `hrzzspp`.`status_ikona` (`id`, `naziv`,`ikona`) VALUES ('12', 'Minus','ikona-minus');

create TABLE `hrzzspp`.`status_razdoblja` (
  `id` INT(11) NOT NULL,
  `naziv` VARCHAR(45) NOT NULL,
  `status_ikona_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_si_sr_id`
    FOREIGN KEY (`status_ikona_id`)
    REFERENCES `hrzzspp`.`status_ikona` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
    
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('1', 'Prihvaćen EVL','1');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('2', 'Sukob interesa 1 EVL','7');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('3', 'Sukob interesa 2 EVL','8');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('4', 'Mogući sukob interesa EVL','9');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('5', 'Nije u sukobu interesa EVL','10');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('6', 'Unos evaluacija','2');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('7', 'Poslano izvješće EVL','4');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('8', 'Zaključan unos EVL','3');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('9', 'Prihvaćen EVL','5');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('10', 'Odbijen EVL','6');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('11', 'Neaktivan COV','1');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('12', 'Sukob interesa 1 COV','7');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('13', 'Sukob interesa 2 COV','8');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('14', 'Mogući sukob interesa COV','9');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('15', 'Nije u sukobu interesa COV','10');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('16', 'Unos vrednovanja','2');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('17', 'Zaključan unos COV','3');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('18', 'Prihvaćen COV','5');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('19', 'Odbijen COV','6');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('20', 'Neaktivan CUO','1');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('21', 'Sukob interesa 1 CUO','7');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('22', 'Sukob interesa 2 CUO','8');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('23', 'Mogući sukob interesa CUO','9');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('24', 'Nije u sukobu interesa CUO','10');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('25', 'Aktivan CUO','2');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('26', 'Prihvaćen CUO','5');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('27', 'Odbijen CUO','6');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('28', 'Neaktivne financije','1');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('29', 'Odobrene financije','5');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('30', 'Neodobrene financije','6');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('31', 'Unesen period','1');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('32', 'Unos evidencija','2');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('33', 'Zaključan unos evidencija','3');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('34', 'Poslano izvješće VOD','4');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('35', 'Prihvaćene evidencije','5');
INSERT INTO `hrzzspp`.`status_razdoblja` (`id`, `naziv`,`status_ikona_id`) VALUES ('36', 'Odbijene evidencije','6');

//tablica prati statuse EVL, COV i CUO usera po periodu
CREATE TABLE `hrzzspp`.`users_roles_periods_status` (
  `period_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `role_id` INT(11) NOT NULL,
  `status_id` INT(11) NOT NULL,
  `vrijeme_promjene` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`period_id`, `user_id`, `role_id`),
  INDEX `fk_urp_r_id_idx` (`role_id` ASC),
  INDEX `fk_urp_u_id_idx` (`user_id` ASC),
  CONSTRAINT `fk_urp_pe_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_urp_rl_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `hrzzspp`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_urp_usr_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `hrzzspp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_urp_st_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `hrzzspp`.`status_razdoblja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE `hrzzspp`.`vrednovanje_rez_izvjesce` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `zav_ocjena_a` TINYINT(5) NULL,
  `zav_ocjena_b` TINYINT(5) NULL,
  `zav_ocjena_c` TINYINT(5) NULL,
  `zav_ocjena_d` TINYINT(5) NULL,
  `komentar` VARCHAR(2000) NULL,
  `vrijeme_promjene` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `period_id` INT(5) NOT NULL,
  `user_id` INT(11) NOT NULL,
	PRIMARY KEY (`id`),
  CONSTRAINT `fk_vri_pe_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vri_usr_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `hrzzspp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


CREATE TABLE `hrzzspp`.`sukob_interesa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `uvjet_povjer_da` TINYINT(2) NULL,
  `uvjet_povjer_ne` TINYINT(2) NULL,
  `sud_utvrdeni_si` TINYINT(2) NULL,
  `sud_moguci_si` TINYINT(2) NULL,
  `sud_ne_si` TINYINT(2) NULL,
  `period_id` INT(20) NULL,
  `user_id` INT(20) NULL,
  `vrijeme_promjene` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`));
  CONSTRAINT `fk_vri_pe_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_vri_usr_id`
    FOREIGN KEY (`user_id`)
    REFERENCES `hrzzspp`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;