UPDATE `hrzzspp`.`sifarnik` SET `description`='Završno izv. razdoblje' WHERE `id`='86';
UPDATE `hrzzspp`.`ev_sifarnik` SET `description`='Završno izv. razdoblje' WHERE `id`='86';

INSERT INTO `hrzzspp`.`roles` (`id`, `name`, `description`) VALUES ('4', 'COV', 'Vrednovatelj');
INSERT INTO `hrzzspp`.`roles` (`id`, `name`, `description`) VALUES ('5', 'CUO', 'Upravni odbor');
INSERT INTO `hrzzspp`.`roles` (`id`, `name`, `description`) VALUES ('6', 'ADM', 'Admin');


INSERT INTO `hrzzspp`.`evaluator_status` (`id`, `naziv`, `ikona`) VALUES ('5', 'Unesen', 'ikona');

//dummy COV i CUO useri

INSERT INTO `hrzzspp`.`users` (`username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('COV1@hrzz.hr', 'COV1', 'N', 'N', 'N', 'Y');
INSERT INTO `hrzzspp`.`users` (`username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('COV2@hrzz.hr', 'COV2', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('100108', 'Vrednovatelj1', 'Prvi', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');
INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('100109', 'Vrednovatelj2', 'Drugi', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');

INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('4', '100108');
INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('4', '100109');

INSERT INTO `hrzzspp`.`users` (`username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('CUO1@hrzz.hr', 'CUO1', 'N', 'N', 'N', 'Y');
INSERT INTO `hrzzspp`.`users` (`username`, `password`, `is_account_expired`, `is_account_locked`, `is_credentials_expired`, `is_enabled`) VALUES ('CUO2@hrzz.hr', 'CUO2', 'N', 'N', 'N', 'Y');

INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('100110', 'Upravni1', 'Odbor1', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');
INSERT INTO `hrzzspp`.`users_data` (`user_id`, `name`, `surname`, `title`, `degree`, `institution_id`, `phone_number`, `email`, `oib`, `sex`, `date_of_birth`, `telephone_number`, `address_of_residence`, `zip_code`, `city`, `nationality`,`institution_tmp`) VALUES ('100111', 'Upravni2', 'Odbor2', 'dr. sc.', 'doktor/ica znanosti', '66', '051228692', 'biotehnicke.znanosti@hrzz.hr', '11111111111', 'z', '11111', '051228692', 'Opatija', '51410', 'Opatija', 'Croatia (Hrvatska)','Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet');

INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('5', '100110');
INSERT INTO `hrzzspp`.`users_roles` (`role_id`, `user_id`) VALUES ('5', '100111');

//radna tablica za prikaz trenutnog statusa pojedinog nivoa (evidencije, evaluacije, COV, COU i financiranje)
CREATE TABLE `hrzzspp`.`status_role_period` (
  `status_id` INT(11) NOT NULL,
  `role_id` INT(11) NOT NULL,
  `period_id` INT(11) NOT NULL,
  `vrijeme_promjene` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`role_id`, `period_id`),
  INDEX `fk_srp_p_id_idx` (`period_id` ASC),
  CONSTRAINT `fk_srp_st_id`
    FOREIGN KEY (`status_id`)
    REFERENCES `hrzzspp`.`status_razdoblja` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_srp_rl_id`
    FOREIGN KEY (`role_id`)
    REFERENCES `hrzzspp`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_srp_pe_id`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;
