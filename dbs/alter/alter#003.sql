alter table status_razdoblja add column role char(3);
update status_razdoblja set role='EVL' where id between 1 and 10;
update status_razdoblja set role='COV' where id between 11 and 19;
update status_razdoblja set role='CUO' where id between 20 and 27;
update status_razdoblja set role='FIN' where id between 28 and 30;
update status_razdoblja set role='FIN' where id between 31 and 36;