-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hrzzspp
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB-1ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `scientific_fields`
--

LOCK TABLES `scientific_fields` WRITE;
/*!40000 ALTER TABLE `scientific_fields` DISABLE KEYS */;
INSERT INTO `scientific_fields` VALUES (1,'Matematika','MAT',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (2,'Fizika','FIZ',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (3,'Geologija','GEOL',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (4,'Kemija','KEM',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (5,'Biologija','BIO',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (6,'Geofizika','GEOF',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (7,'Interdisciplinarne prirodne znanosti','IPZN',5,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (8,'Arhitektura i urbanizam','AIU',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (9,'Brodogradnja','BROD',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (10,'Elektrotehnika','ET',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (11,'Geodezija','GEOD',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (12,'Građevinarstvo','GRAD',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (13,'Grafička tehnologija','GRAF',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (14,'Kemijsko inženjerstvo','KING',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (15,'Metalurgija','MET',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (16,'Računarstvo','RAC',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (17,'Rudarstvo, nafta i geološko inženjerstvo','RNGI',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (18,'Strojarstvo','STR',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (19,'Tehnologija prometa i transport','TPT',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (20,'Tekstilna tehnologija','TEK',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (21,'Zrakoplovstvo, raketna i svemirska tehnika','ZRST',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (22,'Temeljne tehničke znanosti','TTZN',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (23,'Interdisciplinarne tehničke znanosti','ITZN',6,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (24,'Temeljne medicinske znanosti','TMZN',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (25,'Kliničke medicinske znanosti','KMZN',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (26,'Javno zdravstvo i zdravstvena zaštita','JZZZ',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (27,'Veterinarska medicina','VETM',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (28,'Dentalna medicina','DENM',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (29,'Farmacija','FARM',1,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (30,'Poljoprivreda (agronomija)','POLJ',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (31,'Šumarstvo','SUMA',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (32,'Drvna tehnologija','DRVT',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (33,'Biotehnologija','BIOT',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (34,'Prehrambena tehnologija','PREHT',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (35,'Interdisciplinarne biotehničke znanosti','IBZN',2,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (36,'Ekonomija','EKN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (37,'Pravo','PRAV',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (38,'Politologija','POL',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (39,'Informacijske i komunikacijske znanosti','IKZN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (40,'Sociologija','SOC',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (41,'Psihologija','PSIH',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (42,'Pedagogija','PED',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (43,'Edukacijsko-rehabilitacijske znanosti','ERZN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (44,'Logopedija','LOGO',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (45,'Kineziologija','KIN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (46,'Demografija','DEM',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (47,'Socijalne djelatnosti','SOCD',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (48,'Sigurnosne i obrambene znanosti','SOZN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (49,'Interdisciplinarne društvene znanosti','IDZN',3,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (50,'Filozofija','FILOZ',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (51,'Teologija','TEO',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (52,'Filologija','FILOL',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (53,'Povijest','POV',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (54,'Povijest umjetnosti','POVU',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (55,'Znanost o umjetnosti','ZNU',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (56,'Arheologija','ARH',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (57,'Etnologija i antropologija','EIA',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (58,'Religijske znanosti (interdisciplinarno polje)','RELZN',4,'2015-03-23 13:53:15',NULL);
INSERT INTO `scientific_fields` VALUES (59,'Interdisciplinarne humanističke znanosti','IHZN',4,'2015-03-23 13:53:15',NULL);
/*!40000 ALTER TABLE `scientific_fields` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12  9:59:10
