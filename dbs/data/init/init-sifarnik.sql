-- MySQL dump 10.13  Distrib 5.6.19, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: hrzzspp
-- ------------------------------------------------------
-- Server version	5.5.43-MariaDB-1ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `sifarnik`
--

LOCK TABLES `sifarnik` WRITE;
/*!40000 ALTER TABLE `sifarnik` DISABLE KEYS */;
INSERT INTO `sifarnik` VALUES (1,'ev_rezultati.oznaka_diseminacije',1,'JAV','Javno');
INSERT INTO `sifarnik` VALUES (2,'ev_rezultati.oznaka_diseminacije',2,'POV','Povjerljivo, samo za zaposlenike zaklade i vrednovatelje');
INSERT INTO `sifarnik` VALUES (3,'ev_rezultati.postignuto',1,'1','Da');
INSERT INTO `sifarnik` VALUES (4,'ev_rezultati.postignuto',2,'0','Ne');
INSERT INTO `sifarnik` VALUES (5,'ev_tocke_provjere.postignuto',1,'1','Da');
INSERT INTO `sifarnik` VALUES (6,'ev_tocke_provjere.postignuto',2,'0','Ne');
INSERT INTO `sifarnik` VALUES (7,'ev_osoblje.radno_mjesto',1,'VOD','Voditelj projekta');
INSERT INTO `sifarnik` VALUES (8,'ev_osoblje.radno_mjesto',2,'IIST','Iskusni istraživači');
INSERT INTO `sifarnik` VALUES (9,'ev_osoblje.radno_mjesto',3,'PDOK','Poslijedoktorandi');
INSERT INTO `sifarnik` VALUES (10,'ev_osoblje.radno_mjesto',4,'DOK','Doktorandi');
INSERT INTO `sifarnik` VALUES (11,'ev_osoblje.radno_mjesto',5,'STUD','Studenti preddiplomskog, diplomskog ili integriranog studija');
INSERT INTO `sifarnik` VALUES (12,'ev_osoblje.radno_mjesto',6,'TEH','Tehničari');
INSERT INTO `sifarnik` VALUES (13,'ev_osoblje.radno_mjesto',7,'DRUGO','Drugo');
INSERT INTO `sifarnik` VALUES (14,'ev_istrazivacka_skupina.titula',1,'PROF','Prof.');
INSERT INTO `sifarnik` VALUES (15,'ev_istrazivacka_skupina.titula',2,'DR','Dr.sc.');
INSERT INTO `sifarnik` VALUES (16,'ev_istrazivacka_skupina.titula',3,'G','g.');
INSERT INTO `sifarnik` VALUES (17,'ev_istrazivacka_skupina.titula',4,'GDJA','gđa.');
INSERT INTO `sifarnik` VALUES (18,'ev_istrazivacka_skupina.titula',5,'BACC','bacc.');
INSERT INTO `sifarnik` VALUES (19,'ev_istrazivacka_skupina.titula',6,'MAG','mag.');
INSERT INTO `sifarnik` VALUES (20,'ev_usavrsavanje.vrsta_aktivnosti',1,'TEC','tečaj');
INSERT INTO `sifarnik` VALUES (21,'ev_usavrsavanje.vrsta_aktivnosti',2,'RAD','radionica');
INSERT INTO `sifarnik` VALUES (22,'ev_usavrsavanje.vrsta_aktivnosti',3,'SEM','seminar');
INSERT INTO `sifarnik` VALUES (23,'ev_usavrsavanje.vrsta_aktivnosti',4,'LJS','ljetna škola');
INSERT INTO `sifarnik` VALUES (24,'ev_usavrsavanje.vrsta_aktivnosti',5,'ZSK','zimska škola');
INSERT INTO `sifarnik` VALUES (25,'ev_popis_zn_radova.recenzija',1,'DOM','domaća');
INSERT INTO `sifarnik` VALUES (26,'ev_popis_zn_radova.recenzija',2,'INT','međunarodna');
INSERT INTO `sifarnik` VALUES (27,'ev_popis_zn_radova.kvartil',1,'Q1','Q1');
INSERT INTO `sifarnik` VALUES (28,'ev_popis_zn_radova.kvartil',2,'Q2','Q2');
INSERT INTO `sifarnik` VALUES (29,'ev_popis_zn_radova.kvartil',3,'Q3','Q3');
INSERT INTO `sifarnik` VALUES (30,'ev_popis_zn_radova.kvartil',4,'Q4','Q4');
INSERT INTO `sifarnik` VALUES (31,'ev_popis_zn_radova.otvoreni_pristup',1,'1','Da');
INSERT INTO `sifarnik` VALUES (32,'ev_popis_zn_radova.otvoreni_pristup',2,'0','Ne');
INSERT INTO `sifarnik` VALUES (33,'ev_popis_publikacija.vrsta',1,'MON','monografija');
INSERT INTO `sifarnik` VALUES (34,'ev_popis_publikacija.vrsta',2,'RAD','rad u knjizi');
INSERT INTO `sifarnik` VALUES (35,'ev_popis_publikacija.vrsta',3,'POG','poglavlje u knjizi');
INSERT INTO `sifarnik` VALUES (36,'ev_popis_publikacija.vrsta',4,'UDZ','udžbenik');
INSERT INTO `sifarnik` VALUES (37,'ev_popis_publikacija.vrsta',5,'STR','stručni rad');
INSERT INTO `sifarnik` VALUES (38,'ev_popis_publikacija.vrsta',6,'PRE','pregledni rad');
INSERT INTO `sifarnik` VALUES (39,'ev_popis_publikacija.vrsta',7,'SNS','sudjelovanje na skupovima');
INSERT INTO `sifarnik` VALUES (40,'ev_popis_publikacija.vrsta',8,'OST','ostalo');
INSERT INTO `sifarnik` VALUES (41,'ev_popis_publikacija.otvoreni_pristup',1,'1','Da');
INSERT INTO `sifarnik` VALUES (42,'ev_popis_publikacija.otvoreni_pristup',2,'0','Ne');
INSERT INTO `sifarnik` VALUES (43,'ev_popis_dis_aktivnosti.vrsta',1,'KON','konferencije');
INSERT INTO `sifarnik` VALUES (44,'ev_popis_dis_aktivnosti.vrsta',2,'RAD','radionice');
INSERT INTO `sifarnik` VALUES (45,'ev_popis_dis_aktivnosti.vrsta',3,'WEB','mrežne stranice/aplikacije');
INSERT INTO `sifarnik` VALUES (46,'ev_popis_dis_aktivnosti.vrsta',4,'PZJ','priopćenje za javnost');
INSERT INTO `sifarnik` VALUES (47,'ev_popis_dis_aktivnosti.vrsta',5,'LET','letci');
INSERT INTO `sifarnik` VALUES (48,'ev_popis_dis_aktivnosti.vrsta',6,'CLA','članci objavljeni u popularnim medijima');
INSERT INTO `sifarnik` VALUES (49,'ev_popis_dis_aktivnosti.vrsta',7,'VID','video');
INSERT INTO `sifarnik` VALUES (50,'ev_popis_dis_aktivnosti.vrsta',8,'MIZ','medijska izvještavanja');
INSERT INTO `sifarnik` VALUES (51,'ev_popis_dis_aktivnosti.vrsta',9,'PRE','prezentacije');
INSERT INTO `sifarnik` VALUES (52,'ev_popis_dis_aktivnosti.vrsta',10,'IZL','izložbe');
INSERT INTO `sifarnik` VALUES (53,'ev_popis_dis_aktivnosti.vrsta',11,'RAS','rasprave');
INSERT INTO `sifarnik` VALUES (54,'ev_popis_dis_aktivnosti.vrsta',12,'INT','intervjui');
INSERT INTO `sifarnik` VALUES (55,'ev_popis_dis_aktivnosti.vrsta',13,'FILM','filmovi');
INSERT INTO `sifarnik` VALUES (56,'ev_popis_dis_aktivnosti.vrsta',14,'POST','posteri');
INSERT INTO `sifarnik` VALUES (57,'ev_popis_dis_aktivnosti.uloga',1,'PP','pozvano predavanje');
INSERT INTO `sifarnik` VALUES (58,'ev_popis_dis_aktivnosti.uloga',2,'IZL','naučno ili stručno izlaganje na konferenciji');
INSERT INTO `sifarnik` VALUES (59,'ev_popis_dis_aktivnosti.uloga',3,'ORG','organizatori');
INSERT INTO `sifarnik` VALUES (60,'ev_popis_dis_aktivnosti.uloga',4,'OST','ostali sudionici');
INSERT INTO `sifarnik` VALUES (61,'ev_popis_dis_aktivnosti.vrsta_publike',1,'ZNJ','znanstvena javnost');
INSERT INTO `sifarnik` VALUES (62,'ev_popis_dis_aktivnosti.vrsta_publike',2,'GOS','gospodarstvo');
INSERT INTO `sifarnik` VALUES (63,'ev_popis_dis_aktivnosti.vrsta_publike',3,'CIV','civilno društvo');
INSERT INTO `sifarnik` VALUES (64,'ev_popis_dis_aktivnosti.vrsta_publike',4,'KRE','kreatori politika');
INSERT INTO `sifarnik` VALUES (65,'ev_popis_dis_aktivnosti.vrsta_publike',5,'MED','mediji');
INSERT INTO `sifarnik` VALUES (66,'ev_popis_dis_aktivnosti.vrsta_publike',6,'OST','ostalo');
INSERT INTO `sifarnik` VALUES (67,'ev_patenata.vrsta',1,'PAT','Patent');
INSERT INTO `sifarnik` VALUES (68,'ev_patenata.vrsta',2,'ZIG','Žig');
INSERT INTO `sifarnik` VALUES (69,'ev_patenata.vrsta',3,'IND_DIZ','Industrijski dizajn');
INSERT INTO `sifarnik` VALUES (70,'ev_patenata.vrsta',4,'DRG','Drugo');
INSERT INTO `sifarnik` VALUES (71,'ev_patenata.povjerljivo',1,'1','Da');
INSERT INTO `sifarnik` VALUES (72,'ev_patenata.povjerljivo',2,'0','Ne');
INSERT INTO `sifarnik` VALUES (73,'ev_suradnje.vrsta_suradnje',1,'NAC','Nacionalna');
INSERT INTO `sifarnik` VALUES (74,'ev_suradnje.vrsta_suradnje',2,'MED','Međunarodna');
/*!40000 ALTER TABLE `sifarnik` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-06-03 16:48:28
