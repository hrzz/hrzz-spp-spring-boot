-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hrzzspp
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB-1ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `major_scientific_fields`
--

LOCK TABLES `major_scientific_fields` WRITE;
/*!40000 ALTER TABLE `major_scientific_fields` DISABLE KEYS */;
INSERT INTO `major_scientific_fields` VALUES (1,'Biomedicina i zdravstvo','BMIZ','2015-05-11 13:58:42',NULL);
INSERT INTO `major_scientific_fields` VALUES (2,'Biotehničke znanosti','BTZN','2015-03-23 13:22:09',NULL);
INSERT INTO `major_scientific_fields` VALUES (3,'Društvene znanosti','DZN','2015-03-23 13:22:09',NULL);
INSERT INTO `major_scientific_fields` VALUES (4,'Humanističke znanosti','HZN','2015-03-23 13:22:09',NULL);
INSERT INTO `major_scientific_fields` VALUES (5,'Prirodne znanosti','PZN','2015-03-23 13:22:09',NULL);
INSERT INTO `major_scientific_fields` VALUES (6,'Tehničke znanosti','TZN','2015-03-23 13:22:09',NULL);
INSERT INTO `major_scientific_fields` VALUES (7,'SVI','ALL','2015-03-23 13:22:09',NULL);
/*!40000 ALTER TABLE `major_scientific_fields` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12  9:59:10
