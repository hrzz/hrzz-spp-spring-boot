-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hrzzspp
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB-1ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `institutions`
--

LOCK TABLES `institutions` WRITE;
/*!40000 ALTER TABLE `institutions` DISABLE KEYS */;
INSERT INTO `institutions` VALUES (1,'Ekonomski institut Zagreb','Trg J.F.Kennedyja 7','Zagreb','10000',NULL,'+385-1-2362-212,+38512362200','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (2,'Hrvatska akademija znanosti i umjetnosti','Zrinski trg 11','Zagreb','10000',NULL,'++ 385 1 4698 256,+385-20-356-222,+385 1 4895 122,+385 1 4812 703','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (3,'Hrvatski geološki institut','Sachsova 2','Zagreb','10000',NULL,'++38616160888,0038516160745','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (4,'Hrvatski institut za povijest','Opatička 10','Zagreb','10000',NULL,'01 4851721,+385-1-4851-721','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (5,'Hrvatski šumarski institut','Cvjetno naselje 41','Jastrebarsko','10450',NULL,'+38516273000,016273000,098339065,016273000','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (6,'Hrvatski veterinarski institut','Savska cesta 143','Zagreb','10000',NULL,'+38516123665','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (7,'Institut \"Ruđer Bošković\"','Bijenička cesta 54','Zagreb','10000',NULL,'01 6131060,+38514561111,+385 1 4561 065,+385 (0)1 456 1139,+385 (0)1 4561-111,+38514571235,014560996,4561 046,++385 1 1456 1173,+385-1-4561-111,01-4561 111,0156118,+385 1 4561111,+385 1 4561 136,014561068,01-4560-943,0038514561111,385-1-230-4129,1 468 020','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (8,'Institut društvenih znanosti Ivo Pilar','Marulićev trg 19/I','Zagreb','10000',NULL,'01 4886823,4886 800,+385 1 4886 836','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (9,'Institut za antropologiju','Ljudevita Gaja 32','Zagreb','10000',NULL,'5535112,+385-1-5535100','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (10,'Institut za arheologiju','Ljudevita Gaja 32','Zagreb','10000',NULL,'++385/01/615 1908,016150250,0038516150250','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (11,'Institut za etnologiju i folkloristiku','Šubićeva 42','Zagreb','10000',NULL,'+385 1 4596-700','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (12,'Institut za filozofiju','Ulica grada Vukovara 54','Zagreb','10000',NULL,'01-4622320,6111532,01/6111532, 01/6111984','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (13,'Institut za fiziku','Bijenička cesta 46','Zagreb','10000',NULL,'01 469 88 44,+385(01)4698811,+38514698811','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (14,'Institut za hrvatski jezik i jezikoslovlje','Ulica Republike Austrije 16','Zagreb','10000',NULL,'+385-1-3783-888,00385 1 37 83 881','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (15,'Institut za medicinska istraživanja i medicinu rada','Ksaverska cesta 2','Zagreb','10000',NULL,'+3850104682555,+385-1-46-82-620,014682636','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (16,'Institut za oceanografiju i ribarstvo','Šetalište I.Meštrovića 63','Split','21000',NULL,'+385 21 408 002','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (17,'Institut za povijest umjetnosti','Ulica grada Vukovara 68','Zagreb','10000',NULL,'+ 385-1-6112-740,+385 1 6112 744,+385 16112 744,00385 1 6112744','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (18,'Institut za razvoj i međunarodne odnose','Ljudevita Farkaša Vukotinovića 2','Zagreb','10000',NULL,'01/4877-472','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (19,'Institut za turizam','Vrhovec 5','Zagreb','10000',NULL,'01 3909 666','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (20,'Ništa od navedenog','Mirogojska 8','Zagreb','10000',NULL,'+385-1-2826-222,+385-1-2826-222','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (21,'Ništa od navedenog 2','Trg Nikole Šubića Zrinskog 19','Zagreb','10000',NULL,'+38514873000','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (22,'Poljoprivredni institut Osijek','Južno predgrađe 17, p.p. 334','Osijek','31000',NULL,'031 515 521','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (23,'Staroslavenski institut','Demetrova 11','Zagreb','10000',NULL,'+385 1 222 00 25','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (24,'Sveučilište Josipa Jurja Strossmayera u Osijeku, Filozofski fakultet','Lorenza Jagera 9','Osijek','31000',NULL,'031 211 400','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (25,'Sveučilište Josipa Jurja Strossmayera u Osijeku, Građevinski fakultet Osijek','Drinska 16a','Osijek','31000',NULL,'031/540070','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (26,'Sveučilište Josipa Jurja Strossmayera u Osijeku, Odjel za kemiju','Cara Hadrijana 8/a','Osijek','31000',NULL,'+385-31-399-350','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (27,'Sveučilište Josipa Jurja Strossmayera u Osijeku, Poljoprivredni fakultet u Osijeku','Kralja Petra Svačića 1d','Osijek','31000',NULL,'031/554923,385 (0) 31 554 866,+ 385 31 554 832','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (28,'Sveučilište Josipa Jurja Strossmayera u Osijeku, Prehrambeno-tehnološki fakultet','Franje Kuhača 20','Osijek','31000',NULL,'031/ 224 - 300,0038531224300','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (29,'Sveučilište Josipa Jurja Strossmayera, Ekonomski fakultet u Osijeku','Gajev trg 7','Osijek','31000',NULL,'031224400','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (30,'Sveučilište Jurja Dobrile u Puli','Zagrebačka ulica 30','Pula','52100',NULL,'385 52 377 000,052 377 500,052377500','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (31,'Sveučilište u Rijeci','Trg braće Mažuranića 10','Rijeka','51000',NULL,'+385 51 584 553','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (32,'Sveučilište u Rijeci, Ekonomski fakultet','Ivana Filipovića 4','Rijeka','51000',NULL,'0038551355111,051355111,051 355111','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (33,'Sveučilište u Rijeci, Filozofski fakultet u Rijeci','Sveučilišna avenija 4','Rijeka','51000',NULL,'051/265-705,051 265 600,0915342299,051/265-600','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (34,'Sveučilište u Rijeci, Građevinski fakultet u Rijeci','Radmile Matejčić 3','Rijeka','51000',NULL,'051/265-955,051265993','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (35,'Sveučilište u Rijeci, Medicinski fakultet u Rijeci','Braće Branchetta 20','Rijeka','51000',NULL,'+051651233,051 651 170,051 651 260,+385-51-651-203','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (36,'Sveučilište u Rijeci, Odjel za biotehnologiju','Radmile Matejčić 2','Rijeka','51000',NULL,'385 51 584579','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (37,'Sveučilište u Rijeci, Odjel za matematiku','Radmile Matejčić 2','Rijeka','51000',NULL,'051-584-650','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (38,'Sveučilište u Rijeci, Pravni fakultet','Hahlić 6','Rijeka','51000',NULL,'051 359 500,051-359-541,051/359-527','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (39,'Sveučilište u Rijeci, Tehnički fakultet','Vukovarska 58','Rijeka','51000',NULL,'++385 51 651 491,051651444,+38551651444','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (40,'Sveučilište u Splitu Fakultet građevinarstva, arhitekture i geodezije','Matice hrvatske 15','Split','21000',NULL,'098432410,0038521303354','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (41,'Sveučilište u Splitu, Ekonomski fakultet','Cvite Fiskovića 5','Split','21000',NULL,'021/430683','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (42,'Sveučilište u Splitu, Fakultet elektrotehnike, strojarstva i brodogradnje','Ruđera Boškovića 32','Split','21000',NULL,'021305933,021305953,0038521305777','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (43,'Sveučilište u Splitu, Filozofski fakultet','Sinjska 2','Split','21000',NULL,'021541905','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (44,'Sveučilište u Splitu, Kemijsko-tehnološki fakultet','Teslina 10/V','Split','21000',NULL,'00385 21 329 420,021329454,++385 21 329432','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (45,'Sveučilište u Splitu, Kineziološki fakultet','Ulica Nikole Tesle 6','Split','21000',NULL,'021 302 440','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (46,'Sveučilište u Splitu, Medicinski fakultet','Šoltanska 2','Split','21000',NULL,'021 557880,++385 21 557 903,021557873,+385 21 557 809,021557923, -923,+385 21 557 903,021 557 904,021 557 903','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (47,'Sveučilište u Splitu, Prirodoslovno-matematički fakultet','Teslina 12','Split','21000',NULL,'+385 21 385 286,+385-21-385133','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (48,'Sveučilište u Zadru','Ul. Mihovila Pavlinovića bb','Zadar','23000',NULL,'0911695933,023/200-535','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (49,'Sveučilište u Zagrebu','Trg maršala Tita 14','Zagreb','10000',NULL,'(+385 1) 6414 392,+385 1 6414 351,016120077','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (50,'Sveučilište u Zagrebu, Agronomski fakultet','Svetošimunska cesta 25','Zagreb','10000',NULL,'+385 (0)1 239 3777,01-239-4010,00385996565223,+385 1 2393 777','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (51,'Sveučilište u Zagrebu, Arhitektonski fakultet','Fra Andrije Kačića Miošića 26','Zagreb','10000',NULL,'003851 4639265,+385 1 4826116','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (52,'Sveučilište u Zagrebu, Edukacijsko-rehabilitacijski fakultet','Borongajska cesta 83f','Zagreb','10000',NULL,'01 2457500','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (53,'Sveučilište u Zagrebu, Ekonomski fakultet u Zagrebu','Trg. J. F. Kennedya 6','Zagreb','10000',NULL,'01/2383136,01 238 3333,01 238 32 61,+385 98 380 204,01-238 3110,++385 01 2383 333','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (54,'Sveučilište u Zagrebu, Fakultet elektrotehnike i računarstva','Unska 3','Zagreb','10000',NULL,'01/6129915,016129620,385 1 6129999,01/6129796,+385 1 6129 999,+385-1-6129-656,0038516129748,0038598804328,+385 1 6129999,385 1 6129 911,+38516129999,+385 1 6129 798,01-6129999,016129999,016129999,+385 1 6129898,016129926,01 6129 999,+ 38516129642,01/6129-','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (55,'Sveučilište u Zagrebu, Fakultet kemijskog inženjerstva i tehnologije','Marulićev trg 19','Zagreb','10000',NULL,'00385 1 4597 213,+38514597281,+385 1 4597 281,+38514597120','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (56,'Sveučilište u Zagrebu, Fakultet organizacije i informatike','Pavlinska 2','Varaždin','42000',NULL,'+38542390891','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (57,'Sveučilište u Zagrebu, Fakultet strojarstva i brodogradnje','Ivana Lučića 5','Zagreb','10000',NULL,'++385 1 6168 103,+385(1)6168-226,00385 1 6168 432,+38516168434,016168356','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (58,'Sveučilište u Zagrebu, Farmaceutsko-biokemijski fakultet','Ante Kovačića 1','Zagreb','10000',NULL,'+ 385 1 48 17 108','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (59,'Sveučilište u Zagrebu, Filozofski fakultet','Ivana Lučića 3','Zagreb','10000',NULL,'+385915453849,01/6120121,++38516120059,+385 1 6120 111,01/6120216,0993735890,++ 385 16120147,0997102359,01 6120 157,6120120,6120007','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (60,'Sveučilište u Zagrebu, Geodetski fakultet','Fra Andrije Kačića Miošića 26','Zagreb','10000',NULL,'0038514639222,01 4639 279','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (61,'Sveučilište u Zagrebu, Građevinski fakultet','Fra Andrije Kačića Miošića 26','Zagreb','10000',NULL,'+385-1-4639-222','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (62,'Sveučilište u Zagrebu, Medicinski fakultet','Šalata 3/Šalata 12','Zagreb','10000',NULL,'01 4566 952,385 1 4566777,4590-260,+38514566777,4566 925,+385 1 4566 972,+385-1-2379-933,+ 385 14596-856,+385(0)14566944','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (63,'Sveučilište u Zagrebu, Muzička akademija','Gundulićeva 6','Zagreb','10000',NULL,'01/4810200','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (64,'Sveučilište u Zagrebu, Pravni fakultet u Zagrebu','Trg maršala Tita 14','Zagreb','10000',NULL,'01 4564 317,+38514564309,4564 321,+38514597500','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (65,'Sveučilište u Zagrebu, Prehrambeno-biotehnološki fakultet','Pierottijeva 6','Zagreb','10000',NULL,'01/4605037,+385-4605-000,01 4605 128','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (66,'Sveučilište u Zagrebu, Prirodoslovno-matematički fakultet','Horvatovac 102','Zagreb','10000',NULL,'+385 1 4605735,38514605777,+385 1 4877716,01-460-5792,014605777,01 4605541,+385-1-460-5780,+38514895455,01/4605 868,4606044,460551,014606012,4606 010,014606266,014605701,014605583,01 4606400,+385 1 4605555,+38514605586,+385 1 4606 356','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (67,'Sveučilište u Zagrebu, Rudarsko-geološko-naftni fakultet','Pierottijeva 6','Zagreb','10000',NULL,'(01) 553 5808','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (68,'Sveučilište u Zagrebu, Šumarski fakultet','Svetošimunska 25','Zagreb','10000',NULL,'01 235 2493','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (69,'Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet','Prilaz baruna Filipovića 28a','Zagreb','10000',NULL,'+38513712500,+385 1 4605020,3712510','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (70,'Sveučilište u Zagrebu, Učiteljski fakultet','Savska cesta 77','Zagreb','10000',NULL,'01 6327 327','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (71,'Sveučilište u Zagrebu, Veterinarski fakultet','Heinzelova 55','Zagreb','10000',NULL,'+38512390111,01 2390 111','2015-05-11 15:21:13',NULL);
INSERT INTO `institutions` VALUES (72,'Visoko gospodarsko učilište u Križevcima','Milislava Demerca 1','Križevci','48260',NULL,'048 617 954','2015-05-11 15:21:13',NULL);
/*!40000 ALTER TABLE `institutions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12  9:59:10
