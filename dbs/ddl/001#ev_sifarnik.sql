CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_sifarnik` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ref_table` VARCHAR(45) NOT NULL,
  `ref_column` VARCHAR(45) NOT NULL,
  `seq_no` INT NULL,
  `code` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `u_table_col_code` (`ref_table`, `ref_column`, `code`))  
ENGINE = InnoDB;
