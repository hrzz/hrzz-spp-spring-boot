CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_dok_disertacije` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_ukupan` INT NULL,
  `broj_fin_hrzz` INT NULL,
  `broj_izvan_zz` INT NULL,
  `broj_gospodarstvo` INT NULL,
  `broj_dvojnih` INT NULL,
  `broj_radova_i_patenata` INT NULL,
  `broj_izdavanje` INT NULL,
  `period_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evdd_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evdd_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;