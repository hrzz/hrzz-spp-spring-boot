CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_tocke_provjere` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br_cilja` VARCHAR(15) NOT NULL,
  `br_tocke_provjere` VARCHAR(15) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `zaduzena_osoba` VARCHAR(100) NOT NULL,
  `postignuto` CHAR(1) NOT NULL DEFAULT 0,
  `datum_realizacije` CHAR(7) NULL DEFAULT '00/0000',
  `komentari` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evtp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evtp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;