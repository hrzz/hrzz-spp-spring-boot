CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_patenata` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `vrsta` VARCHAR(45) NOT NULL,
  `povjerljivo` CHAR(1) NOT NULL,
  `datum_embarga` DATE NOT NULL,
  `referenca` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(100) NOT NULL,
  `prijavitelj` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;