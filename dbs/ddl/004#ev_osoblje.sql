CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_osoblje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `radno_mjesto` VARCHAR(45) NOT NULL,
  `broj_zena` INT NOT NULL,
  `broj_muskaraca` INT NOT NULL,
  `broj_u_priv_sektoru` INT NOT NULL,
  `broj_u_jav_sektoru` INT NOT NULL,
  `broj_u_sveucilistu` INT NOT NULL,
  `broj_u_inozemstvu` INT NOT NULL,
  `broj_nezaposlenih` INT NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evo_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evo_period`
    FOREIGN KEY (`id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;
