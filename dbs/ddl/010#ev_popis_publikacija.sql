CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_publikacija` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `vrsta` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(100) NOT NULL,
  `autori` VARCHAR(255) NULL,
  `naziv_casopisa` VARCHAR(100) NULL,
  `broj_casopisa` VARCHAR(45) NULL,
  `godiste_casopisa` INT NULL,
  `izdavac` VARCHAR(100) NULL,
  `mjesto_izdanja` VARCHAR(45) NULL,
  `relevantne_stranice` VARCHAR(45) NULL,
  `godina_objavljivanja` INT NULL,
  `stalni_identifikator` VARCHAR(255) NULL,
  `otvoreni_pristup` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;