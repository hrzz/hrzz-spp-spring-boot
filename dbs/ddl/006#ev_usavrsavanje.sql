CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_usavrsavanje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br` INT NOT NULL,
  `vrsta_aktivnosti` VARCHAR(45) NOT NULL,
  `ime_prezime` VARCHAR(25) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `razdoblje_od` DATE NOT NULL,
  `razdoblje_do` DATE NOT NULL,
  `ustanova` VARCHAR(255) NULL,
  `drzava` VARCHAR(100) NULL,
  `usvojena_znanja` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evu_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evu_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;