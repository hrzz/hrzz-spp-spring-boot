CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_dis_aktivnosti` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `vrsta` VARCHAR(45) NOT NULL,
  `ime_i_prezime` VARCHAR(25) NOT NULL,
  `uloga` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `datum_od` DATE NULL,
  `datum_do` DATE NULL,
  `mjesto` VARCHAR(45) NULL,
  `vrsta_publike` VARCHAR(45) NULL,
  `broj_sudionika` INT NULL,
  `obuhvacene_drzave` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpda_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpda_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;