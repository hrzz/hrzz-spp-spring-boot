DROP TABLE  `hrzzspp`.`ev_suradnje`;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_suradnje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redni_broj` INT NOT NULL,
  `vrsta_suradnje` VARCHAR(20) NULL,
  `institucija_suradnje` VARCHAR(200) NULL,
  `ime_i_prezime_suradnje` VARCHAR(100) NULL,
  `opis` VARCHAR(256) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evs_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evs_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;