CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_rezultati` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br_cilja` VARCHAR(15) NOT NULL,
  `br_rezultata` VARCHAR(15) NOT NULL,
  `naziv` TEXT NOT NULL,
  `zaduzena_osoba` VARCHAR(100) NOT NULL,
  `oznaka_diseminacije` CHAR(3) NOT NULL,
  `postignuto` CHAR(1) NOT NULL DEFAULT 0,
  `datum_realizacije` CHAR(7) NOT NULL DEFAULT '00/0000',
  `komentar` VARCHAR(100) NULL,
  `poveznica` varchar(256) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `u_br_cilja` (`br_cilja` ASC),
  INDEX `u_br_rezultata` (`br_rezultata` ASC),
  INDEX `i_fk_evr_period` (`period_id` ASC),
  CONSTRAINT `fk_evr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;