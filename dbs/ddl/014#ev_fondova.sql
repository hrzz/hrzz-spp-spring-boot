CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_fondova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_prijava` INT NOT NULL,
  `naziv_programa` VARCHAR(100) NOT NULL,
  `rok_prijave` DATE NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evf_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evf_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;