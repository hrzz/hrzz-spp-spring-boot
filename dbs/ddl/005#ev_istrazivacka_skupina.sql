CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_istrazivacka_skupina` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime_prezime` VARCHAR(25) NOT NULL,
  `titula` VARCHAR(45) NOT NULL,
  `godina_rodjenja` INT NOT NULL,
  `ustanova_zaposlenja` VARCHAR(50) NOT NULL,
  `datum_ukljucivanja` DATE NOT NULL,
  `uloga` VARCHAR(30) NULL,
  `godina_upisa_dok` INT NULL,
  `godina_stjecanja_dok` INT NULL,
  `pocetak_zaposlenja` DATE NULL,
  `trajanje_zaposlenja` INT NULL,
  `izvor_place` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evis_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evis_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;