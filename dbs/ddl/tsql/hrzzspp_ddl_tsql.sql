USE [master]
GO
/****** Object:  Database [hrzzspp]    Script Date: 27.05.2015. 10:44:51 ******/
CREATE DATABASE [hrzzspp]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'hrzzspp', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXP2014\MSSQL\DATA\hrzzspp.mdf' , SIZE = 4288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'hrzzspp_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXP2014\MSSQL\DATA\hrzzspp_log.ldf' , SIZE = 1072KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [hrzzspp] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [hrzzspp].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [hrzzspp] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [hrzzspp] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [hrzzspp] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [hrzzspp] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [hrzzspp] SET ARITHABORT OFF 
GO
ALTER DATABASE [hrzzspp] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [hrzzspp] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [hrzzspp] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [hrzzspp] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [hrzzspp] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [hrzzspp] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [hrzzspp] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [hrzzspp] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [hrzzspp] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [hrzzspp] SET  ENABLE_BROKER 
GO
ALTER DATABASE [hrzzspp] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [hrzzspp] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [hrzzspp] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [hrzzspp] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [hrzzspp] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [hrzzspp] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [hrzzspp] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [hrzzspp] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [hrzzspp] SET  MULTI_USER 
GO
ALTER DATABASE [hrzzspp] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [hrzzspp] SET DB_CHAINING OFF 
GO
ALTER DATABASE [hrzzspp] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [hrzzspp] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [hrzzspp] SET DELAYED_DURABILITY = DISABLED 
GO
USE [hrzzspp]
GO
/****** Object:  Schema [m2ss]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE SCHEMA [m2ss]
GO
/****** Object:  Table [dbo].[epp_dokumenti]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[epp_dokumenti](
	[id] [int] NOT NULL,
	[call_id] [int] NOT NULL,
	[doc_admin_form] [nvarchar](255) NULL,
	[doc_form_a] [nvarchar](255) NULL,
	[doc_form_b] [nvarchar](255) NULL,
	[doc_collaborator] [nvarchar](255) NULL,
	[doc_work_plan] [nvarchar](255) NULL,
	[doc_research] [nvarchar](255) NULL,
	[doc_institut] [nvarchar](255) NULL,
	[doc_other] [nvarchar](255) NULL,
	[doc_other2] [nvarchar](255) NULL,
	[doc_other3] [nvarchar](255) NULL,
	[doc_other4] [nvarchar](255) NULL,
	[doc_other5] [nvarchar](255) NULL,
	[doc_other6] [nvarchar](255) NULL,
	[doc_other7] [nvarchar](255) NULL,
	[doc_other8] [nvarchar](255) NULL,
	[doc_other9] [nvarchar](255) NULL,
	[doc_other10] [nvarchar](255) NULL,
	[doc_admin_form_hr] [nvarchar](255) NULL,
	[doc_form_a_hr] [nvarchar](255) NULL,
	[doc_form_b_hr] [nvarchar](255) NULL,
	[doc_collaborator_hr] [nvarchar](255) NULL,
	[doc_work_plan_hr] [nvarchar](255) NULL,
	[doc_research_hr] [nvarchar](255) NULL,
	[doc_institut_hr] [nvarchar](255) NULL,
	[doc_other_hr] [nvarchar](255) NULL,
	[doc_other2_hr] [nvarchar](255) NULL,
	[doc_other3_hr] [nvarchar](255) NULL,
	[doc_other4_hr] [nvarchar](255) NULL,
	[doc_other5_hr] [nvarchar](255) NULL,
	[doc_other6_hr] [nvarchar](255) NULL,
	[doc_other7_hr] [nvarchar](255) NULL,
	[doc_other8_hr] [nvarchar](255) NULL,
	[doc_other9_hr] [nvarchar](255) NULL,
	[doc_other10_hr] [nvarchar](255) NULL,
	[admin_doc_1] [nvarchar](255) NULL,
	[admin_doc_2] [nvarchar](255) NULL,
	[admin_doc_3] [nvarchar](255) NULL,
	[admin_doc_4] [nvarchar](255) NULL,
	[admin_doc_5] [nvarchar](255) NULL,
	[admin_doc_6] [nvarchar](255) NULL,
	[admin_doc_7] [nvarchar](255) NULL,
 CONSTRAINT [PK_epp_dokumenti_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[epp_korisnici]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[epp_korisnici](
	[id] [int] NOT NULL,
	[korisnik_id] [int] NOT NULL,
	[ime] [nvarchar](50) NULL,
	[prezime] [nvarchar](100) NULL,
	[email] [nvarchar](100) NULL,
	[lozinka] [nvarchar](50) NULL,
	[oib] [nvarchar](11) NULL,
	[titula] [nvarchar](50) NULL,
	[akademski_stupanj] [nvarchar](50) NULL,
	[spol] [nvarchar](1) NULL,
	[datum_rodjenja] [date] NULL,
	[telefon] [nvarchar](100) NULL,
	[adresa_prebivalista] [nvarchar](100) NULL,
	[postanski_broj] [nvarchar](5) NULL,
	[grad] [nvarchar](50) NULL,
	[drzavljanstvo] [nvarchar](50) NULL,
	[osobni_web] [nvarchar](255) NULL,
	[ustanova] [nvarchar](255) NULL,
	[celnik_ustanove] [nvarchar](255) NULL,
	[adresa_ustanove] [nvarchar](100) NULL,
	[grad_ustanove] [nvarchar](100) NULL,
	[postanski_broj_ustanove] [nvarchar](5) NULL,
	[oib_ustanove] [nvarchar](11) NULL,
	[banka_ustanove] [nvarchar](255) NULL,
	[iban_ustanove] [nvarchar](34) NULL,
	[telefon_ustanove] [nvarchar](100) NULL,
	[status_suradnik] [int] NOT NULL,
	[status_doktorand] [int] NOT NULL,
	[status_voditelj] [int] NOT NULL,
	[status_recenzent] [int] NOT NULL,
	[status_st_odbor] [int] NOT NULL,
	[status_evaluator] [int] NOT NULL,
 CONSTRAINT [PK_epp_korisnici_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[epp_projekti]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[epp_projekti](
	[id] [int] NOT NULL,
	[id_pp] [int] NOT NULL,
	[sifra_pp] [int] NOT NULL,
	[naziv_pp] [nvarchar](255) NULL,
	[vrsta_pp] [nvarchar](5) NULL,
	[nat_rok_pp] [nvarchar](12) NULL,
	[id_voditelj_pp] [int] NOT NULL,
	[akronim_pp] [nvarchar](50) NULL,
	[trajanje_pp] [nvarchar](10) NULL,
	[interdisc_pp] [nvarchar](5) NULL,
	[prijava_zn_podrucje_pp] [nvarchar](50) NULL,
	[zn_polje] [nvarchar](50) NULL,
	[so_podrucje_pp] [nvarchar](50) NULL,
	[web_pp] [nvarchar](255) NULL,
	[sazetak_hr_pp] [nvarchar](max) NULL,
	[sazetak_en_pp] [nvarchar](max) NULL,
	[zaduzeni_prk_pp] [nvarchar](100) NULL,
	[ustanova_pp] [nvarchar](255) NULL,
	[celnik_ustanove_pp] [nvarchar](100) NULL,
	[adresa_ustanove_pp] [nvarchar](100) NULL,
	[grad_ustanove_pp] [nvarchar](100) NULL,
	[postanski_broj_ustanove_pp] [nvarchar](5) NULL,
	[oib_ustanove_pp] [nvarchar](11) NULL,
	[telefon_ustanove_pp] [nvarchar](100) NULL,
 CONSTRAINT [PK_epp_projekti_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_dobrobit]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_dobrobit](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rast_zaposlenosti] [nchar](1) NULL,
	[razvoj_novih] [nchar](1) NULL,
	[tema_u_medijima] [nchar](1) NULL,
	[posjecenost_ustanova] [nchar](1) NULL,
	[poboljsanje_uvjeta] [nchar](1) NULL,
	[promjena_navika] [nchar](1) NULL,
	[utjecaj_na_politiku] [nchar](1) NULL,
	[novi_programi] [nchar](1) NULL,
	[utjecaj_na_zdrastvo] [nchar](1) NULL,
	[pojava_novog] [nchar](1) NULL,
	[ostalo] [nchar](1) NULL,
	[opis_ostalo] [nvarchar](100) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_dobrobit_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_dok_disertacije]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_dok_disertacije](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj_ukupan] [int] NULL,
	[broj_fin_hrzz] [int] NULL,
	[broj_izvan_zz] [int] NULL,
	[broj_gospodarstvo] [int] NULL,
	[broj_dvojnih] [int] NULL,
	[broj_radova_i_patenata] [int] NULL,
	[broj_izdavanje] [int] NULL,
	[period_id] [int] NULL,
 CONSTRAINT [PK_ev_dok_disertacije_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_fondova]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_fondova](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj_prijava] [int] NOT NULL,
	[naziv_programa] [nvarchar](100) NOT NULL,
	[rok_prijave] [date] NOT NULL,
	[status] [nvarchar](45) NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_fondova_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_istrazivacka_skupina]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_istrazivacka_skupina](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ime_prezime] [nvarchar](25) NOT NULL,
	[titula] [nvarchar](45) NOT NULL,
	[godina_rodjenja] [int] NOT NULL,
	[ustanova_zaposlenja] [nvarchar](50) NOT NULL,
	[datum_ukljucivanja] [date] NOT NULL,
	[uloga] [nvarchar](30) NULL,
	[godina_upisa_dok] [int] NULL,
	[godina_stjecanja_dok] [int] NULL,
	[pocetak_zaposlenja] [date] NULL,
	[trajanje_zaposlenja] [int] NULL,
	[izvor_place] [nvarchar](45) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_istrazivacka_skupina_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_osoblje]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_osoblje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[radno_mjesto] [nvarchar](45) NOT NULL,
	[broj_zena] [int] NOT NULL,
	[broj_muskaraca] [int] NOT NULL,
	[broj_u_priv_sektoru] [int] NOT NULL,
	[broj_u_jav_sektoru] [int] NOT NULL,
	[broj_u_sveucilistu] [int] NOT NULL,
	[broj_u_inozemstvu] [int] NOT NULL,
	[broj_nezaposlenih] [int] NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_osoblje_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_ostalo]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_ostalo](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj_nematerijalnih] [int] NULL,
	[naziv_nematerijalnih] [nvarchar](100) NULL,
	[opis_nematerijalnih] [nvarchar](100) NULL,
	[broj_proizvoda] [int] NULL,
	[naziv_proizvoda] [nvarchar](100) NULL,
	[opis_proizvoda] [nvarchar](100) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_ostalo_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_patenata]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_patenata](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[vrsta] [nvarchar](45) NOT NULL,
	[povjerljivo] [nchar](1) NOT NULL,
	[datum_embarga] [date] NOT NULL,
	[referenca] [nvarchar](45) NOT NULL,
	[naziv] [nvarchar](100) NOT NULL,
	[prijavitelj] [nvarchar](100) NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_patenata_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_popis_dis_aktivnosti]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_popis_dis_aktivnosti](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj] [int] NOT NULL,
	[vrsta] [nvarchar](45) NOT NULL,
	[ime_i_prezime] [nvarchar](25) NOT NULL,
	[uloga] [nvarchar](45) NOT NULL,
	[naziv] [nvarchar](255) NOT NULL,
	[datum_od] [date] NULL,
	[datum_do] [date] NULL,
	[mjesto] [nvarchar](45) NULL,
	[vrsta_publike] [nvarchar](45) NULL,
	[broj_sudionika] [int] NULL,
	[obuhvacene_drzave] [nvarchar](100) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_popis_dis_aktivnosti_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_popis_publikacija]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_popis_publikacija](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj] [int] NOT NULL,
	[vrsta] [nvarchar](45) NOT NULL,
	[naziv] [nvarchar](100) NOT NULL,
	[autori] [nvarchar](255) NULL,
	[naziv_casopisa] [nvarchar](100) NULL,
	[broj_casopisa] [nvarchar](45) NULL,
	[godiste_casopisa] [int] NULL,
	[izdavac] [nvarchar](100) NULL,
	[mjesto_izdanja] [nvarchar](45) NULL,
	[relevantne_stranice] [nvarchar](45) NULL,
	[godina_objavljivanja] [int] NULL,
	[stalni_identifikator] [nvarchar](255) NULL,
	[otvoreni_pristup] [nvarchar](45) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_popis_publikacija_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_popis_radova]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_popis_radova](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[autor] [nvarchar](100) NOT NULL,
	[naslov] [nvarchar](255) NOT NULL,
	[godina] [int] NOT NULL,
	[ustanova] [nvarchar](100) NOT NULL,
	[mentor] [nvarchar](100) NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_popis_radova_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_popis_zn_radova]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_popis_zn_radova](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj] [int] NOT NULL,
	[naziv] [nvarchar](100) NULL,
	[autori] [nvarchar](255) NULL,
	[naziv_casopisa] [nvarchar](100) NULL,
	[broj_casopisa] [nvarchar](45) NULL,
	[godiste_casopisa] [int] NULL,
	[izdavac] [nvarchar](100) NULL,
	[mjesto_izdanja] [nvarchar](45) NULL,
	[relevantne_stranice] [nvarchar](45) NULL,
	[godina_objavljivanja] [int] NULL,
	[recenzija] [nvarchar](45) NULL,
	[kvartil] [nvarchar](45) NULL,
	[stalni_identifikator] [nvarchar](255) NULL,
	[otvoreni_pristup] [nvarchar](45) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_popis_zn_radova_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_rezultati]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_rezultati](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[br_cilja] [nvarchar](15) NOT NULL,
	[br_rezultata] [nvarchar](15) NOT NULL,
	[naziv] [nvarchar](max) NOT NULL,
	[zaduzena_osoba] [nvarchar](100) NOT NULL,
	[oznaka_diseminacije] [nchar](3) NOT NULL,
	[rok_dovrsetka] [nchar](7) NOT NULL,
	[postignuto] [nchar](1) NOT NULL,
	[datum_realizacije] [nchar](7) NOT NULL,
	[komentar] [nvarchar](100) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_rezultati_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_sifarnik]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_sifarnik](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ref_table] [nvarchar](45) NOT NULL,
	[ref_column] [nvarchar](45) NOT NULL,
	[seq_no] [int] NULL,
	[code] [nvarchar](45) NOT NULL,
	[description] [nvarchar](100) NULL,
 CONSTRAINT [PK_ev_sifarnik_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_suradnje]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_suradnje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[broj_novih] [int] NOT NULL,
	[broj_nacionalnih] [int] NULL,
	[nacionalna_institucija] [nvarchar](100) NULL,
	[nacionalno_ime_i_prezime] [nvarchar](25) NULL,
	[broj_medjunarodnih] [int] NULL,
	[internacionalna_institucija] [nvarchar](100) NULL,
	[internacionalno_ime_i_prezime] [nvarchar](25) NULL,
	[opis] [nvarchar](50) NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_suradnje_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_tocke_provjere]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_tocke_provjere](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[br_cilja] [nvarchar](15) NOT NULL,
	[br_tocke_provjere] [nvarchar](15) NOT NULL,
	[naziv] [nvarchar](255) NOT NULL,
	[zaduzena_osoba] [nvarchar](100) NOT NULL,
	[rok] [nchar](7) NOT NULL,
	[postignuto] [nchar](1) NOT NULL,
	[datum_realizacije] [nchar](7) NULL,
	[komentari] [nvarchar](100) NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_tocke_provjere_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ev_usavrsavanje]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ev_usavrsavanje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[br] [int] NOT NULL,
	[vrsta_aktivnosti] [nvarchar](45) NOT NULL,
	[ime_prezime] [nvarchar](25) NOT NULL,
	[naziv] [nvarchar](255) NOT NULL,
	[razdoblje_od] [date] NOT NULL,
	[razdoblje_do] [date] NOT NULL,
	[ustanova] [nvarchar](255) NULL,
	[drzava] [nvarchar](100) NULL,
	[usvojena_znanja] [nvarchar](100) NOT NULL,
	[period_id] [int] NOT NULL,
 CONSTRAINT [PK_ev_usavrsavanje_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[institutions]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[institutions](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[address] [nvarchar](255) NULL,
	[city] [nvarchar](255) NULL,
	[zip_code] [nchar](5) NULL,
	[oib] [nchar](11) NULL,
	[phone] [nvarchar](255) NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_institutions_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[major_scientific_fields]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[major_scientific_fields](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[code] [nvarchar](5) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_major_scientific_fields_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[projects]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[projects](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](10) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[acronym] [nvarchar](100) NOT NULL,
	[tender_deadline] [nvarchar](10) NULL,
	[url] [nvarchar](255) NULL,
	[duration] [int] NULL,
	[summary_hr] [nvarchar](max) NULL,
	[summary_en] [nvarchar](max) NULL,
	[is_multidisc] [nchar](1) NULL,
	[scientific_field_id] [int] NULL,
	[major_scientific_field_id] [int] NOT NULL,
	[tender_type_id] [int] NOT NULL,
	[leader_id] [int] NOT NULL,
	[institution_id] [int] NOT NULL,
	[institution_ceo] [nvarchar](255) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_projects_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[projects_periods_roles]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[projects_periods_roles](
	[project_id] [int] NOT NULL,
	[period_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
	[reporting_periods_id] [int] NOT NULL,
 CONSTRAINT [PK_projects_periods_roles_project_id] PRIMARY KEY CLUSTERED 
(
	[project_id] ASC,
	[period_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[projects_periods_roles_users_status]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[projects_periods_roles_users_status](
	[project_id] [int] NOT NULL,
	[period_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[status_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_projects_periods_roles_users_status_project_id] PRIMARY KEY CLUSTERED 
(
	[project_id] ASC,
	[period_id] ASC,
	[role_id] ASC,
	[user_id] ASC,
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[projects_status]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[projects_status](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[description] [nvarchar](max) NULL,
 CONSTRAINT [PK_projects_status_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[reporting_periods]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[reporting_periods](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[project_id] [int] NOT NULL,
	[date_from] [datetime] NOT NULL,
	[date_to] [datetime] NOT NULL,
	[is_final] [nchar](1) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_reporting_periods_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[roles]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[description] [nvarchar](255) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_roles_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[scientific_fields]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[scientific_fields](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[code] [nvarchar](5) NOT NULL,
	[msf_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_scientific_fields_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tender_types]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tender_types](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](5) NOT NULL,
	[description] [nvarchar](255) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_tender_types_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [nvarchar](255) NOT NULL,
	[password] [nvarchar](255) NOT NULL,
	[is_account_expired] [nchar](1) NOT NULL,
	[is_account_locked] [nchar](1) NOT NULL,
	[is_credentials_expired] [nchar](1) NOT NULL,
	[is_enabled] [nchar](1) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_users_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users_data]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users_data](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NOT NULL,
	[name] [nvarchar](255) NOT NULL,
	[surname] [nvarchar](255) NOT NULL,
	[title] [nvarchar](50) NOT NULL,
	[degree] [nvarchar](100) NOT NULL,
	[institution_id] [int] NULL,
	[phone_number] [nvarchar](50) NOT NULL,
	[email] [nvarchar](255) NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
	[oib] [nvarchar](11) NOT NULL,
	[sex] [nvarchar](1) NOT NULL,
	[date_of_birth] [nvarchar](100) NOT NULL,
	[telephone_number] [nvarchar](100) NOT NULL,
	[address_of_residence] [nvarchar](100) NULL,
	[zip_code] [nvarchar](10) NOT NULL,
	[city] [nvarchar](100) NOT NULL,
	[nationality] [nvarchar](100) NOT NULL,
	[personal_web] [nvarchar](200) NULL,
	[institution_tmp] [nvarchar](255) NOT NULL,
 CONSTRAINT [PK_users_data_id] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users_projects_periods_roles]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users_projects_periods_roles](
	[user_id] [int] NOT NULL,
	[project_id] [int] NOT NULL,
	[period_id] [int] NOT NULL,
	[role_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_users_projects_periods_roles_user_id] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[project_id] ASC,
	[period_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[users_roles]    Script Date: 27.05.2015. 10:44:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[users_roles](
	[role_id] [int] NOT NULL,
	[user_id] [int] NOT NULL,
	[created_at] [datetime] NOT NULL,
	[modified_at] [datetime] NULL,
 CONSTRAINT [PK_users_roles_role_id] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC,
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Index [epp_dokumenti$u_call_id]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[epp_dokumenti] ADD  CONSTRAINT [epp_dokumenti$u_call_id] UNIQUE NONCLUSTERED 
(
	[call_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [epp_dokumenti$u_call_id] ON [dbo].[epp_dokumenti] DISABLE
GO
/****** Object:  Index [epp_korisnici$u_korisnik_id]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[epp_korisnici] ADD  CONSTRAINT [epp_korisnici$u_korisnik_id] UNIQUE NONCLUSTERED 
(
	[korisnik_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [epp_korisnici$u_korisnik_id] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_doktorand]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_doktorand] ON [dbo].[epp_korisnici]
(
	[status_doktorand] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_doktorand] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_evaluator]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_evaluator] ON [dbo].[epp_korisnici]
(
	[status_evaluator] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_evaluator] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_recenzent]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_recenzent] ON [dbo].[epp_korisnici]
(
	[status_recenzent] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_recenzent] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_st_odbor]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_st_odbor] ON [dbo].[epp_korisnici]
(
	[status_st_odbor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_st_odbor] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_suradnik]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_suradnik] ON [dbo].[epp_korisnici]
(
	[status_suradnik] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_suradnik] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [i_status_voditelj]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_status_voditelj] ON [dbo].[epp_korisnici]
(
	[status_voditelj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_status_voditelj] ON [dbo].[epp_korisnici] DISABLE
GO
/****** Object:  Index [epp_projekti$u_id_pp]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[epp_projekti] ADD  CONSTRAINT [epp_projekti$u_id_pp] UNIQUE NONCLUSTERED 
(
	[id_pp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [epp_projekti$u_id_pp] ON [dbo].[epp_projekti] DISABLE
GO
/****** Object:  Index [epp_projekti$u_sifra_pp]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[epp_projekti] ADD  CONSTRAINT [epp_projekti$u_sifra_pp] UNIQUE NONCLUSTERED 
(
	[sifra_pp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [epp_projekti$u_sifra_pp] ON [dbo].[epp_projekti] DISABLE
GO
/****** Object:  Index [i_id_voditelj_pp]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_id_voditelj_pp] ON [dbo].[epp_projekti]
(
	[id_voditelj_pp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_id_voditelj_pp] ON [dbo].[epp_projekti] DISABLE
GO
/****** Object:  Index [fk_evd_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evd_period_idx] ON [dbo].[ev_dobrobit]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evd_period_idx] ON [dbo].[ev_dobrobit] DISABLE
GO
/****** Object:  Index [fk_evdd_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evdd_period_idx] ON [dbo].[ev_dok_disertacije]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evdd_period_idx] ON [dbo].[ev_dok_disertacije] DISABLE
GO
/****** Object:  Index [fk_evf_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evf_period_idx] ON [dbo].[ev_fondova]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evf_period_idx] ON [dbo].[ev_fondova] DISABLE
GO
/****** Object:  Index [fk_evis_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evis_period_idx] ON [dbo].[ev_istrazivacka_skupina]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evis_period_idx] ON [dbo].[ev_istrazivacka_skupina] DISABLE
GO
/****** Object:  Index [fk_evo_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evo_period_idx] ON [dbo].[ev_osoblje]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evo_period_idx] ON [dbo].[ev_osoblje] DISABLE
GO
/****** Object:  Index [fk_evost_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evost_period_idx] ON [dbo].[ev_ostalo]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evost_period_idx] ON [dbo].[ev_ostalo] DISABLE
GO
/****** Object:  Index [fk_evp_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evp_period_idx] ON [dbo].[ev_patenata]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evp_period_idx] ON [dbo].[ev_patenata] DISABLE
GO
/****** Object:  Index [fk_evpda_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evpda_period_idx] ON [dbo].[ev_popis_dis_aktivnosti]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evpda_period_idx] ON [dbo].[ev_popis_dis_aktivnosti] DISABLE
GO
/****** Object:  Index [fk_evpp_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evpp_period_idx] ON [dbo].[ev_popis_publikacija]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evpp_period_idx] ON [dbo].[ev_popis_publikacija] DISABLE
GO
/****** Object:  Index [fk_evpr_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evpr_period_idx] ON [dbo].[ev_popis_radova]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evpr_period_idx] ON [dbo].[ev_popis_radova] DISABLE
GO
/****** Object:  Index [fk_evzr_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evzr_period_idx] ON [dbo].[ev_popis_zn_radova]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evzr_period_idx] ON [dbo].[ev_popis_zn_radova] DISABLE
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ev_rezultati$u_br_cilja]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[ev_rezultati] ADD  CONSTRAINT [ev_rezultati$u_br_cilja] UNIQUE NONCLUSTERED 
(
	[br_cilja] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [ev_rezultati$u_br_cilja] ON [dbo].[ev_rezultati] DISABLE
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ev_rezultati$u_br_rezultata]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[ev_rezultati] ADD  CONSTRAINT [ev_rezultati$u_br_rezultata] UNIQUE NONCLUSTERED 
(
	[br_rezultata] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [ev_rezultati$u_br_rezultata] ON [dbo].[ev_rezultati] DISABLE
GO
/****** Object:  Index [i_fk_evr_period]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [i_fk_evr_period] ON [dbo].[ev_rezultati]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [i_fk_evr_period] ON [dbo].[ev_rezultati] DISABLE
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [ev_sifarnik$u_table_col_code]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[ev_sifarnik] ADD  CONSTRAINT [ev_sifarnik$u_table_col_code] UNIQUE NONCLUSTERED 
(
	[ref_table] ASC,
	[ref_column] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [ev_sifarnik$u_table_col_code] ON [dbo].[ev_sifarnik] DISABLE
GO
/****** Object:  Index [fk_evs_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evs_period_idx] ON [dbo].[ev_suradnje]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evs_period_idx] ON [dbo].[ev_suradnje] DISABLE
GO
/****** Object:  Index [fk_evtp_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evtp_period_idx] ON [dbo].[ev_tocke_provjere]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evtp_period_idx] ON [dbo].[ev_tocke_provjere] DISABLE
GO
/****** Object:  Index [fk_evu_period_idx]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_evu_period_idx] ON [dbo].[ev_usavrsavanje]
(
	[period_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_evu_period_idx] ON [dbo].[ev_usavrsavanje] DISABLE
GO
SET ANSI_PADDING ON

GO
/****** Object:  Index [projects$u_prj_code]    Script Date: 27.05.2015. 10:44:52 ******/
ALTER TABLE [dbo].[projects] ADD  CONSTRAINT [projects$u_prj_code] UNIQUE NONCLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [projects$u_prj_code] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_prj_institution_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_prj_institution_id] ON [dbo].[projects]
(
	[institution_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_prj_institution_id] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_prj_leader_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_prj_leader_id] ON [dbo].[projects]
(
	[leader_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_prj_leader_id] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_prj_major_scientific_field_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_prj_major_scientific_field_id] ON [dbo].[projects]
(
	[major_scientific_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_prj_major_scientific_field_id] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_prj_scientific_field_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_prj_scientific_field_id] ON [dbo].[projects]
(
	[scientific_field_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_prj_scientific_field_id] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_prj_tender_type_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_prj_tender_type_id] ON [dbo].[projects]
(
	[tender_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_prj_tender_type_id] ON [dbo].[projects] DISABLE
GO
/****** Object:  Index [fk_ppr_reporting_periods_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ppr_reporting_periods_id] ON [dbo].[projects_periods_roles]
(
	[reporting_periods_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ppr_reporting_periods_id] ON [dbo].[projects_periods_roles] DISABLE
GO
/****** Object:  Index [fk_ppr_role_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ppr_role_id] ON [dbo].[projects_periods_roles]
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ppr_role_id] ON [dbo].[projects_periods_roles] DISABLE
GO
/****** Object:  Index [fk_pprus_status_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_pprus_status_id] ON [dbo].[projects_periods_roles_users_status]
(
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_pprus_status_id] ON [dbo].[projects_periods_roles_users_status] DISABLE
GO
/****** Object:  Index [fk_pprus_uppr_pk]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_pprus_uppr_pk] ON [dbo].[projects_periods_roles_users_status]
(
	[user_id] ASC,
	[project_id] ASC,
	[period_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_pprus_uppr_pk] ON [dbo].[projects_periods_roles_users_status] DISABLE
GO
/****** Object:  Index [fk_rp_project_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_rp_project_id] ON [dbo].[reporting_periods]
(
	[project_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_rp_project_id] ON [dbo].[reporting_periods] DISABLE
GO
/****** Object:  Index [fk_sf_msf_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_sf_msf_id] ON [dbo].[scientific_fields]
(
	[msf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_sf_msf_id] ON [dbo].[scientific_fields] DISABLE
GO
/****** Object:  Index [fk_ud_institution_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ud_institution_id] ON [dbo].[users_data]
(
	[institution_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ud_institution_id] ON [dbo].[users_data] DISABLE
GO
/****** Object:  Index [fk_ud_user_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ud_user_id] ON [dbo].[users_data]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ud_user_id] ON [dbo].[users_data] DISABLE
GO
/****** Object:  Index [fk_uppr_ppr_pk]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_uppr_ppr_pk] ON [dbo].[users_projects_periods_roles]
(
	[project_id] ASC,
	[period_id] ASC,
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_uppr_ppr_pk] ON [dbo].[users_projects_periods_roles] DISABLE
GO
/****** Object:  Index [fk_uppr_user_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_uppr_user_id] ON [dbo].[users_projects_periods_roles]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_uppr_user_id] ON [dbo].[users_projects_periods_roles] DISABLE
GO
/****** Object:  Index [fk_ur_role_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ur_role_id] ON [dbo].[users_roles]
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ur_role_id] ON [dbo].[users_roles] DISABLE
GO
/****** Object:  Index [fk_ur_user_id]    Script Date: 27.05.2015. 10:44:52 ******/
CREATE NONCLUSTERED INDEX [fk_ur_user_id] ON [dbo].[users_roles]
(
	[user_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER INDEX [fk_ur_user_id] ON [dbo].[users_roles] DISABLE
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_admin_form]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_form_a]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_form_b]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_collaborator]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_work_plan]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_research]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_institut]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other2]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other3]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other4]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other5]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other6]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other7]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other8]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other9]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other10]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_admin_form_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_form_a_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_form_b_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_collaborator_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_work_plan_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_research_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_institut_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other2_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other3_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other4_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other5_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other6_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other7_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other8_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other9_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [doc_other10_hr]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_1]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_2]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_3]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_4]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_5]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_6]
GO
ALTER TABLE [dbo].[epp_dokumenti] ADD  DEFAULT (NULL) FOR [admin_doc_7]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [ime]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [prezime]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [email]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [lozinka]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [oib]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [titula]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [akademski_stupanj]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [spol]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [datum_rodjenja]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [telefon]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [adresa_prebivalista]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [postanski_broj]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [grad]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [drzavljanstvo]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [osobni_web]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [ustanova]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [celnik_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [adresa_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [grad_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [postanski_broj_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [oib_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [banka_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [iban_ustanove]
GO
ALTER TABLE [dbo].[epp_korisnici] ADD  DEFAULT (NULL) FOR [telefon_ustanove]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [naziv_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [vrsta_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [nat_rok_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [akronim_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [trajanje_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [interdisc_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [prijava_zn_podrucje_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [zn_polje]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [so_podrucje_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [web_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [zaduzeni_prk_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [ustanova_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [celnik_ustanove_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [adresa_ustanove_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [grad_ustanove_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [postanski_broj_ustanove_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [oib_ustanove_pp]
GO
ALTER TABLE [dbo].[epp_projekti] ADD  DEFAULT (NULL) FOR [telefon_ustanove_pp]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [rast_zaposlenosti]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [razvoj_novih]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [tema_u_medijima]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [posjecenost_ustanova]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [poboljsanje_uvjeta]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [promjena_navika]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [utjecaj_na_politiku]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [novi_programi]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [utjecaj_na_zdrastvo]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [pojava_novog]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [ostalo]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT (N'0') FOR [opis_ostalo]
GO
ALTER TABLE [dbo].[ev_dobrobit] ADD  DEFAULT ((0)) FOR [period_id]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_ukupan]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_fin_hrzz]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_izvan_zz]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_gospodarstvo]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_dvojnih]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_radova_i_patenata]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [broj_izdavanje]
GO
ALTER TABLE [dbo].[ev_dok_disertacije] ADD  DEFAULT (NULL) FOR [period_id]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [uloga]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [godina_upisa_dok]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [godina_stjecanja_dok]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [pocetak_zaposlenja]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [trajanje_zaposlenja]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] ADD  DEFAULT (NULL) FOR [izvor_place]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [broj_nematerijalnih]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [naziv_nematerijalnih]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [opis_nematerijalnih]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [broj_proizvoda]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [naziv_proizvoda]
GO
ALTER TABLE [dbo].[ev_ostalo] ADD  DEFAULT (NULL) FOR [opis_proizvoda]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [datum_od]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [datum_do]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [mjesto]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [vrsta_publike]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [broj_sudionika]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] ADD  DEFAULT (NULL) FOR [obuhvacene_drzave]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [autori]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [naziv_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [broj_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [godiste_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [izdavac]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [mjesto_izdanja]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [relevantne_stranice]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [godina_objavljivanja]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [stalni_identifikator]
GO
ALTER TABLE [dbo].[ev_popis_publikacija] ADD  DEFAULT (NULL) FOR [otvoreni_pristup]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [naziv]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [autori]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [naziv_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [broj_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [godiste_casopisa]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [izdavac]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [mjesto_izdanja]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [relevantne_stranice]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [godina_objavljivanja]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [recenzija]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [kvartil]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [stalni_identifikator]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] ADD  DEFAULT (NULL) FOR [otvoreni_pristup]
GO
ALTER TABLE [dbo].[ev_rezultati] ADD  DEFAULT (N'00/0000') FOR [rok_dovrsetka]
GO
ALTER TABLE [dbo].[ev_rezultati] ADD  DEFAULT (N'0') FOR [postignuto]
GO
ALTER TABLE [dbo].[ev_rezultati] ADD  DEFAULT (N'00/0000') FOR [datum_realizacije]
GO
ALTER TABLE [dbo].[ev_rezultati] ADD  DEFAULT (NULL) FOR [komentar]
GO
ALTER TABLE [dbo].[ev_sifarnik] ADD  DEFAULT (NULL) FOR [seq_no]
GO
ALTER TABLE [dbo].[ev_sifarnik] ADD  DEFAULT (NULL) FOR [description]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT ((0)) FOR [broj_nacionalnih]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT (NULL) FOR [nacionalna_institucija]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT (NULL) FOR [nacionalno_ime_i_prezime]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT ((0)) FOR [broj_medjunarodnih]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT (NULL) FOR [internacionalna_institucija]
GO
ALTER TABLE [dbo].[ev_suradnje] ADD  DEFAULT (NULL) FOR [internacionalno_ime_i_prezime]
GO
ALTER TABLE [dbo].[ev_tocke_provjere] ADD  DEFAULT (N'00/0000') FOR [rok]
GO
ALTER TABLE [dbo].[ev_tocke_provjere] ADD  DEFAULT (N'0') FOR [postignuto]
GO
ALTER TABLE [dbo].[ev_tocke_provjere] ADD  DEFAULT (N'00/0000') FOR [datum_realizacije]
GO
ALTER TABLE [dbo].[ev_tocke_provjere] ADD  DEFAULT (NULL) FOR [komentari]
GO
ALTER TABLE [dbo].[ev_usavrsavanje] ADD  DEFAULT (NULL) FOR [ustanova]
GO
ALTER TABLE [dbo].[ev_usavrsavanje] ADD  DEFAULT (NULL) FOR [drzava]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [address]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [city]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [zip_code]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [oib]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [phone]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[institutions] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[major_scientific_fields] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[major_scientific_fields] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (NULL) FOR [tender_deadline]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (NULL) FOR [url]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (NULL) FOR [duration]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (N'0') FOR [is_multidisc]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (NULL) FOR [scientific_field_id]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[projects] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[projects_periods_roles] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[projects_periods_roles] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[reporting_periods] ADD  DEFAULT (getdate()) FOR [date_from]
GO
ALTER TABLE [dbo].[reporting_periods] ADD  DEFAULT (getdate()) FOR [date_to]
GO
ALTER TABLE [dbo].[reporting_periods] ADD  DEFAULT (N'0') FOR [is_final]
GO
ALTER TABLE [dbo].[reporting_periods] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[reporting_periods] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[roles] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[roles] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[scientific_fields] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[scientific_fields] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[tender_types] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[tender_types] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[users] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[users_data] ADD  DEFAULT (NULL) FOR [institution_id]
GO
ALTER TABLE [dbo].[users_data] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users_data] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[users_data] ADD  DEFAULT (NULL) FOR [address_of_residence]
GO
ALTER TABLE [dbo].[users_data] ADD  DEFAULT (NULL) FOR [personal_web]
GO
ALTER TABLE [dbo].[users_projects_periods_roles] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users_projects_periods_roles] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[users_roles] ADD  DEFAULT (getdate()) FOR [created_at]
GO
ALTER TABLE [dbo].[users_roles] ADD  DEFAULT (NULL) FOR [modified_at]
GO
ALTER TABLE [dbo].[ev_dobrobit]  WITH NOCHECK ADD  CONSTRAINT [ev_dobrobit$fk_evd_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_dobrobit] CHECK CONSTRAINT [ev_dobrobit$fk_evd_period]
GO
ALTER TABLE [dbo].[ev_dok_disertacije]  WITH NOCHECK ADD  CONSTRAINT [ev_dok_disertacije$fk_evdd_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_dok_disertacije] CHECK CONSTRAINT [ev_dok_disertacije$fk_evdd_period]
GO
ALTER TABLE [dbo].[ev_fondova]  WITH NOCHECK ADD  CONSTRAINT [ev_fondova$fk_evf_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_fondova] CHECK CONSTRAINT [ev_fondova$fk_evf_period]
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina]  WITH NOCHECK ADD  CONSTRAINT [ev_istrazivacka_skupina$fk_evis_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_istrazivacka_skupina] CHECK CONSTRAINT [ev_istrazivacka_skupina$fk_evis_period]
GO
ALTER TABLE [dbo].[ev_osoblje]  WITH NOCHECK ADD  CONSTRAINT [ev_osoblje$fk_evo_period] FOREIGN KEY([id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_osoblje] CHECK CONSTRAINT [ev_osoblje$fk_evo_period]
GO
ALTER TABLE [dbo].[ev_ostalo]  WITH NOCHECK ADD  CONSTRAINT [ev_ostalo$fk_evost_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_ostalo] CHECK CONSTRAINT [ev_ostalo$fk_evost_period]
GO
ALTER TABLE [dbo].[ev_patenata]  WITH NOCHECK ADD  CONSTRAINT [ev_patenata$fk_evp_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_patenata] CHECK CONSTRAINT [ev_patenata$fk_evp_period]
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti]  WITH NOCHECK ADD  CONSTRAINT [ev_popis_dis_aktivnosti$fk_evpda_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_popis_dis_aktivnosti] CHECK CONSTRAINT [ev_popis_dis_aktivnosti$fk_evpda_period]
GO
ALTER TABLE [dbo].[ev_popis_publikacija]  WITH NOCHECK ADD  CONSTRAINT [ev_popis_publikacija$fk_evpp_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_popis_publikacija] CHECK CONSTRAINT [ev_popis_publikacija$fk_evpp_period]
GO
ALTER TABLE [dbo].[ev_popis_radova]  WITH NOCHECK ADD  CONSTRAINT [ev_popis_radova$fk_evpr_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_popis_radova] CHECK CONSTRAINT [ev_popis_radova$fk_evpr_period]
GO
ALTER TABLE [dbo].[ev_popis_zn_radova]  WITH NOCHECK ADD  CONSTRAINT [ev_popis_zn_radova$fk_evzr_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_popis_zn_radova] CHECK CONSTRAINT [ev_popis_zn_radova$fk_evzr_period]
GO
ALTER TABLE [dbo].[ev_rezultati]  WITH NOCHECK ADD  CONSTRAINT [ev_rezultati$fk_evr_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_rezultati] CHECK CONSTRAINT [ev_rezultati$fk_evr_period]
GO
ALTER TABLE [dbo].[ev_suradnje]  WITH NOCHECK ADD  CONSTRAINT [ev_suradnje$fk_evs_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_suradnje] CHECK CONSTRAINT [ev_suradnje$fk_evs_period]
GO
ALTER TABLE [dbo].[ev_tocke_provjere]  WITH NOCHECK ADD  CONSTRAINT [ev_tocke_provjere$fk_evtp_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_tocke_provjere] CHECK CONSTRAINT [ev_tocke_provjere$fk_evtp_period]
GO
ALTER TABLE [dbo].[ev_usavrsavanje]  WITH NOCHECK ADD  CONSTRAINT [ev_usavrsavanje$fk_evu_period] FOREIGN KEY([period_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[ev_usavrsavanje] CHECK CONSTRAINT [ev_usavrsavanje$fk_evu_period]
GO
ALTER TABLE [dbo].[projects]  WITH NOCHECK ADD  CONSTRAINT [projects$fk_prj_institution_id] FOREIGN KEY([institution_id])
REFERENCES [dbo].[institutions] ([id])
GO
ALTER TABLE [dbo].[projects] CHECK CONSTRAINT [projects$fk_prj_institution_id]
GO
ALTER TABLE [dbo].[projects]  WITH NOCHECK ADD  CONSTRAINT [projects$fk_prj_leader_id] FOREIGN KEY([leader_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[projects] CHECK CONSTRAINT [projects$fk_prj_leader_id]
GO
ALTER TABLE [dbo].[projects]  WITH NOCHECK ADD  CONSTRAINT [projects$fk_prj_major_scientific_field_id] FOREIGN KEY([major_scientific_field_id])
REFERENCES [dbo].[major_scientific_fields] ([id])
GO
ALTER TABLE [dbo].[projects] CHECK CONSTRAINT [projects$fk_prj_major_scientific_field_id]
GO
ALTER TABLE [dbo].[projects]  WITH NOCHECK ADD  CONSTRAINT [projects$fk_prj_scientific_field_id] FOREIGN KEY([scientific_field_id])
REFERENCES [dbo].[scientific_fields] ([id])
GO
ALTER TABLE [dbo].[projects] CHECK CONSTRAINT [projects$fk_prj_scientific_field_id]
GO
ALTER TABLE [dbo].[projects]  WITH NOCHECK ADD  CONSTRAINT [projects$fk_prj_tender_type_id] FOREIGN KEY([tender_type_id])
REFERENCES [dbo].[tender_types] ([id])
GO
ALTER TABLE [dbo].[projects] CHECK CONSTRAINT [projects$fk_prj_tender_type_id]
GO
ALTER TABLE [dbo].[projects_periods_roles]  WITH NOCHECK ADD  CONSTRAINT [projects_periods_roles$fk_ppr_reporting_periods_id] FOREIGN KEY([reporting_periods_id])
REFERENCES [dbo].[reporting_periods] ([id])
GO
ALTER TABLE [dbo].[projects_periods_roles] CHECK CONSTRAINT [projects_periods_roles$fk_ppr_reporting_periods_id]
GO
ALTER TABLE [dbo].[projects_periods_roles]  WITH NOCHECK ADD  CONSTRAINT [projects_periods_roles$fk_ppr_role_id] FOREIGN KEY([role_id])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[projects_periods_roles] CHECK CONSTRAINT [projects_periods_roles$fk_ppr_role_id]
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status]  WITH NOCHECK ADD  CONSTRAINT [projects_periods_roles_users_status$fk_pprus_status_id] FOREIGN KEY([status_id])
REFERENCES [dbo].[projects_status] ([id])
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status] CHECK CONSTRAINT [projects_periods_roles_users_status$fk_pprus_status_id]
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status]  WITH NOCHECK ADD  CONSTRAINT [projects_periods_roles_users_status$fk_pprus_uppr_pk] FOREIGN KEY([user_id], [project_id], [period_id], [role_id])
REFERENCES [dbo].[users_projects_periods_roles] ([user_id], [project_id], [period_id], [role_id])
GO
ALTER TABLE [dbo].[projects_periods_roles_users_status] CHECK CONSTRAINT [projects_periods_roles_users_status$fk_pprus_uppr_pk]
GO
ALTER TABLE [dbo].[reporting_periods]  WITH NOCHECK ADD  CONSTRAINT [reporting_periods$fk_rp_project_id] FOREIGN KEY([project_id])
REFERENCES [dbo].[projects] ([id])
GO
ALTER TABLE [dbo].[reporting_periods] CHECK CONSTRAINT [reporting_periods$fk_rp_project_id]
GO
ALTER TABLE [dbo].[scientific_fields]  WITH NOCHECK ADD  CONSTRAINT [scientific_fields$fk_sf_msf_id] FOREIGN KEY([msf_id])
REFERENCES [dbo].[major_scientific_fields] ([id])
GO
ALTER TABLE [dbo].[scientific_fields] CHECK CONSTRAINT [scientific_fields$fk_sf_msf_id]
GO
ALTER TABLE [dbo].[users_data]  WITH NOCHECK ADD  CONSTRAINT [users_data$fk_ud_institution_id] FOREIGN KEY([institution_id])
REFERENCES [dbo].[institutions] ([id])
GO
ALTER TABLE [dbo].[users_data] CHECK CONSTRAINT [users_data$fk_ud_institution_id]
GO
ALTER TABLE [dbo].[users_data]  WITH NOCHECK ADD  CONSTRAINT [users_data$fk_ud_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[users_data] CHECK CONSTRAINT [users_data$fk_ud_user_id]
GO
ALTER TABLE [dbo].[users_projects_periods_roles]  WITH NOCHECK ADD  CONSTRAINT [users_projects_periods_roles$fk_uppr_ppr_pk] FOREIGN KEY([project_id], [period_id], [role_id])
REFERENCES [dbo].[projects_periods_roles] ([project_id], [period_id], [role_id])
GO
ALTER TABLE [dbo].[users_projects_periods_roles] CHECK CONSTRAINT [users_projects_periods_roles$fk_uppr_ppr_pk]
GO
ALTER TABLE [dbo].[users_projects_periods_roles]  WITH NOCHECK ADD  CONSTRAINT [users_projects_periods_roles$fk_uppr_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[users_projects_periods_roles] CHECK CONSTRAINT [users_projects_periods_roles$fk_uppr_user_id]
GO
ALTER TABLE [dbo].[users_roles]  WITH NOCHECK ADD  CONSTRAINT [users_roles$fk_ur_role_id] FOREIGN KEY([role_id])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[users_roles] CHECK CONSTRAINT [users_roles$fk_ur_role_id]
GO
ALTER TABLE [dbo].[users_roles]  WITH NOCHECK ADD  CONSTRAINT [users_roles$fk_ur_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[users] ([id])
GO
ALTER TABLE [dbo].[users_roles] CHECK CONSTRAINT [users_roles$fk_ur_user_id]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.epp_dokumenti' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epp_dokumenti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.epp_korisnici' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epp_korisnici'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.epp_projekti' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'epp_projekti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_dobrobit' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_dobrobit'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_dok_disertacije' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_dok_disertacije'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_fondova' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_fondova'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_istrazivacka_skupina' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_istrazivacka_skupina'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_osoblje' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_osoblje'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_ostalo' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_ostalo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_patenata' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_patenata'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_popis_dis_aktivnosti' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_popis_dis_aktivnosti'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_popis_publikacija' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_popis_publikacija'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_popis_radova' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_popis_radova'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_popis_zn_radova' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_popis_zn_radova'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_rezultati' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_rezultati'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_sifarnik' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_sifarnik'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_suradnje' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_suradnje'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_tocke_provjere' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_tocke_provjere'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.ev_usavrsavanje' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'ev_usavrsavanje'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.institutions' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'institutions'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.major_scientific_fields' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'major_scientific_fields'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.projects' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'projects'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.projects_periods_roles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'projects_periods_roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.projects_periods_roles_users_status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'projects_periods_roles_users_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.projects_status' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'projects_status'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.reporting_periods' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'reporting_periods'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.roles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.scientific_fields' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'scientific_fields'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.tender_types' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'tender_types'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.users_data' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_data'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.users_projects_periods_roles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_projects_periods_roles'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_SSMA_SOURCE', @value=N'hrzzspp.users_roles' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'users_roles'
GO
USE [master]
GO
ALTER DATABASE [hrzzspp] SET  READ_WRITE 
GO
