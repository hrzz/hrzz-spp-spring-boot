CREATE TABLE `sifarnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(100) NOT NULL,
  `seq_no` int(11) DEFAULT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_table_col_code` (`code`,`ref`)
) ENGINE=InnoDB;
