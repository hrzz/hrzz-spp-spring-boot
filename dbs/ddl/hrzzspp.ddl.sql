-- MySQL dump 10.14  Distrib 5.5.41-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: hrzzspp
-- ------------------------------------------------------
-- Server version	5.5.41-MariaDB-1ubuntu0.14.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `epp_dokumenti`
--

DROP TABLE IF EXISTS `epp_dokumenti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epp_dokumenti` (
  `id` int(11) NOT NULL,
  `call_id` int(11) NOT NULL,
  `doc_admin_form` varchar(255) DEFAULT NULL,
  `doc_form_a` varchar(255) DEFAULT NULL,
  `doc_form_b` varchar(255) DEFAULT NULL,
  `doc_collaborator` varchar(255) DEFAULT NULL,
  `doc_work_plan` varchar(255) DEFAULT NULL,
  `doc_research` varchar(255) DEFAULT NULL,
  `doc_institut` varchar(255) DEFAULT NULL,
  `doc_other` varchar(255) DEFAULT NULL,
  `doc_other2` varchar(255) DEFAULT NULL,
  `doc_other3` varchar(255) DEFAULT NULL,
  `doc_other4` varchar(255) DEFAULT NULL,
  `doc_other5` varchar(255) DEFAULT NULL,
  `doc_other6` varchar(255) DEFAULT NULL,
  `doc_other7` varchar(255) DEFAULT NULL,
  `doc_other8` varchar(255) DEFAULT NULL,
  `doc_other9` varchar(255) DEFAULT NULL,
  `doc_other10` varchar(255) DEFAULT NULL,
  `doc_admin_form_hr` varchar(255) DEFAULT NULL,
  `doc_form_a_hr` varchar(255) DEFAULT NULL,
  `doc_form_b_hr` varchar(255) DEFAULT NULL,
  `doc_collaborator_hr` varchar(255) DEFAULT NULL,
  `doc_work_plan_hr` varchar(255) DEFAULT NULL,
  `doc_research_hr` varchar(255) DEFAULT NULL,
  `doc_institut_hr` varchar(255) DEFAULT NULL,
  `doc_other_hr` varchar(255) DEFAULT NULL,
  `doc_other2_hr` varchar(255) DEFAULT NULL,
  `doc_other3_hr` varchar(255) DEFAULT NULL,
  `doc_other4_hr` varchar(255) DEFAULT NULL,
  `doc_other5_hr` varchar(255) DEFAULT NULL,
  `doc_other6_hr` varchar(255) DEFAULT NULL,
  `doc_other7_hr` varchar(255) DEFAULT NULL,
  `doc_other8_hr` varchar(255) DEFAULT NULL,
  `doc_other9_hr` varchar(255) DEFAULT NULL,
  `doc_other10_hr` varchar(255) DEFAULT NULL,
  `admin_doc_1` varchar(255) DEFAULT NULL,
  `admin_doc_2` varchar(255) DEFAULT NULL,
  `admin_doc_3` varchar(255) DEFAULT NULL,
  `admin_doc_4` varchar(255) DEFAULT NULL,
  `admin_doc_5` varchar(255) DEFAULT NULL,
  `admin_doc_6` varchar(255) DEFAULT NULL,
  `admin_doc_7` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_call_id` (`call_id`),
  CONSTRAINT `fk_projekt_sifra` FOREIGN KEY (`call_id`) REFERENCES `epp_projekti` (`sifra_pp`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `epp_korisnici`
--

DROP TABLE IF EXISTS `epp_korisnici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epp_korisnici` (
  `id` int(11) NOT NULL,
  `korisnik_id` int(11) NOT NULL,
  `ime` varchar(50) DEFAULT NULL,
  `prezime` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `lozinka` varchar(50) DEFAULT NULL,
  `oib` varchar(11) DEFAULT NULL,
  `titula` varchar(50) DEFAULT NULL,
  `akademski_stupanj` varchar(50) DEFAULT NULL,
  `spol` varchar(1) DEFAULT NULL,
  `datum_rodjenja` date DEFAULT NULL,
  `telefon` varchar(100) DEFAULT NULL,
  `adresa_prebivalista` varchar(100) DEFAULT NULL,
  `postanski_broj` varchar(5) DEFAULT NULL,
  `grad` varchar(50) DEFAULT NULL,
  `drzavljanstvo` varchar(50) DEFAULT NULL,
  `osobni_web` varchar(255) DEFAULT NULL,
  `ustanova` varchar(255) DEFAULT NULL,
  `celnik_ustanove` varchar(255) DEFAULT NULL,
  `adresa_ustanove` varchar(100) DEFAULT NULL,
  `grad_ustanove` varchar(100) DEFAULT NULL,
  `postanski_broj_ustanove` varchar(5) DEFAULT NULL,
  `oib_ustanove` varchar(11) DEFAULT NULL,
  `banka_ustanove` varchar(255) DEFAULT NULL,
  `iban_ustanove` varchar(34) DEFAULT NULL,
  `telefon_ustanove` varchar(100) DEFAULT NULL,
  `status_suradnik` int(11) NOT NULL,
  `status_doktorand` int(11) NOT NULL,
  `status_voditelj` int(11) NOT NULL,
  `status_recenzent` int(11) NOT NULL,
  `status_st_odbor` int(11) NOT NULL,
  `status_evaluator` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_korisnik_id` (`korisnik_id`),
  KEY `i_status_suradnik` (`status_suradnik`),
  KEY `i_status_doktorand` (`status_doktorand`),
  KEY `i_status_voditelj` (`status_voditelj`),
  KEY `i_status_recenzent` (`status_recenzent`),
  KEY `i_status_st_odbor` (`status_st_odbor`),
  KEY `i_status_evaluator` (`status_evaluator`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `epp_projekti`
--

DROP TABLE IF EXISTS `epp_projekti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `epp_projekti` (
  `id` int(11) NOT NULL,
  `id_pp` int(11) NOT NULL,
  `sifra_pp` int(11) NOT NULL,
  `naziv_pp` varchar(255) DEFAULT NULL,
  `vrsta_pp` varchar(5) DEFAULT NULL,
  `nat_rok_pp` varchar(12) DEFAULT NULL,
  `id_voditelj_pp` int(11) NOT NULL,
  `akronim_pp` varchar(50) DEFAULT NULL,
  `trajanje_pp` varchar(10) DEFAULT NULL,
  `interdisc_pp` varchar(5) DEFAULT NULL,
  `prijava_zn_podrucje_pp` varchar(50) DEFAULT NULL,
  `zn_polje` varchar(50) DEFAULT NULL,
  `so_podrucje_pp` varchar(50) DEFAULT NULL,
  `web_pp` varchar(255) DEFAULT NULL,
  `sazetak_hr_pp` text,
  `sazetak_en_pp` text,
  `zaduzeni_prk_pp` varchar(100) DEFAULT NULL,
  `ustanova_pp` varchar(255) DEFAULT NULL,
  `celnik_ustanove_pp` varchar(100) DEFAULT NULL,
  `adresa_ustanove_pp` varchar(100) DEFAULT NULL,
  `grad_ustanove_pp` varchar(100) DEFAULT NULL,
  `postanski_broj_ustanove_pp` varchar(5) DEFAULT NULL,
  `oib_ustanove_pp` varchar(11) DEFAULT NULL,
  `telefon_ustanove_pp` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_id_pp` (`id_pp`),
  UNIQUE KEY `u_sifra_pp` (`sifra_pp`),
  KEY `i_id_voditelj_pp` (`id_voditelj_pp`),
  CONSTRAINT `fk_epp_prj_voditelj_id` FOREIGN KEY (`id_voditelj_pp`) REFERENCES `epp_korisnici` (`korisnik_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `institutions`
--

DROP TABLE IF EXISTS `institutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institutions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `zip_code` char(5) DEFAULT NULL,
  `oib` char(11) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `major_scientific_fields`
--

DROP TABLE IF EXISTS `major_scientific_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `major_scientific_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(5) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(10) NOT NULL,
  `name` varchar(255) NOT NULL,
  `acronym` varchar(100) NOT NULL,
  `tender_deadline` varchar(10) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `duration` int(11) DEFAULT NULL,
  `summary_hr` text,
  `summary_en` text,
  `is_multidisc` char(1) DEFAULT '0',
  `scientific_field_id` int(11) DEFAULT NULL,
  `major_scientific_field_id` int(11) NOT NULL,
  `tender_type_id` int(11) NOT NULL,
  `leader_id` int(11) NOT NULL,
  `institution_id` int(11) NOT NULL,
  `institution_ceo` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_prj_code` (`code`),
  KEY `fk_prj_leader_id` (`leader_id`),
  KEY `fk_prj_tender_type_id` (`tender_type_id`),
  KEY `fk_prj_institution_id` (`institution_id`),
  KEY `fk_prj_scientific_field_id` (`scientific_field_id`),
  KEY `fk_prj_major_scientific_field_id` (`major_scientific_field_id`),
  CONSTRAINT `fk_prj_institution_id` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`),
  CONSTRAINT `fk_prj_leader_id` FOREIGN KEY (`leader_id`) REFERENCES `users` (`id`),
  CONSTRAINT `fk_prj_scientific_field_id` FOREIGN KEY (`scientific_field_id`) REFERENCES `scientific_fields` (`id`),
  CONSTRAINT `fk_prj_major_scientific_field_id` FOREIGN KEY (`major_scientific_field_id`) REFERENCES `major_scientific_fields` (`id`),
  CONSTRAINT `fk_prj_tender_type_id` FOREIGN KEY (`tender_type_id`) REFERENCES `tender_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects_periods_roles`
--

DROP TABLE IF EXISTS `projects_periods_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_periods_roles` (
  `project_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  `reporting_periods_id` int(11) NOT NULL,
  PRIMARY KEY (`project_id`,`period_id`,`role_id`),
  KEY `fk_ppr_role_id` (`role_id`),
  KEY `fk_ppr_reporting_periods_id` (`reporting_periods_id`),
  CONSTRAINT `fk_ppr_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_ppr_reporting_periods_id` FOREIGN KEY (`reporting_periods_id`) REFERENCES `reporting_periods` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects_periods_roles_users_status`
--

DROP TABLE IF EXISTS `projects_periods_roles_users_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_periods_roles_users_status` (
  `project_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`project_id`,`period_id`,`role_id`,`user_id`,`status_id`),
  KEY `fk_pprus_status_id` (`status_id`),
  KEY `fk_pprus_uppr_pk` (`user_id`,`project_id`,`period_id`,`role_id`),
  CONSTRAINT `fk_pprus_status_id` FOREIGN KEY (`status_id`) REFERENCES `projects_status` (`id`),
  CONSTRAINT `fk_pprus_uppr_pk` FOREIGN KEY (`user_id`, `project_id`, `period_id`, `role_id`) REFERENCES `users_projects_periods_roles` (`user_id`, `project_id`, `period_id`, `role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects_status`
--

DROP TABLE IF EXISTS `projects_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `reporting_periods`
--

DROP TABLE IF EXISTS `reporting_periods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reporting_periods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `date_from` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_to` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_final` char(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rp_project_id` (`project_id`),
  CONSTRAINT `fk_rp_project_id` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD COLUMN `ordinal_number` INT(11) NOT NULL AFTER `project_id`;
ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD UNIQUE INDEX `u_rp_projord` (`project_id` ASC, `ordinal_number` ASC);
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
DROP FOREIGN KEY `fk_ppr_reporting_periods_id`;
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
DROP COLUMN `reporting_periods_id`,
DROP INDEX `fk_ppr_reporting_periods_id` ,
ADD INDEX `fk_ppr_reporting_periods_id_idx` (`period_id` ASC);
ALTER TABLE `hrzzspp`.`projects_periods_roles` 
ADD CONSTRAINT `fk_ppr_reporting_periods_id`
  FOREIGN KEY (`period_id`)
  REFERENCES `hrzzspp`.`reporting_periods` (`id`);
ALTER TABLE `hrzzspp`.`reporting_periods` 
ADD COLUMN `iznos` DECIMAL(15,2) NOT NULL;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `scientific_fields`
--

DROP TABLE IF EXISTS `scientific_fields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `scientific_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `code` varchar(5) NOT NULL,
  `msf_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_sf_msf_id` (`msf_id`),
  CONSTRAINT `fk_sf_msf_id` FOREIGN KEY (`msf_id`) REFERENCES `major_scientific_fields` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tender_types`
--

DROP TABLE IF EXISTS `tender_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tender_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(5) NOT NULL,
  `description` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_account_expired` char(1) NOT NULL,
  `is_account_locked` char(1) NOT NULL,
  `is_credentials_expired` char(1) NOT NULL,
  `is_enabled` char(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_data`
--

DROP TABLE IF EXISTS `users_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `title` varchar(50) NOT NULL,
  `degree` varchar(100) NOT NULL,
  `institution_id` int(11) DEFAULT NULL,
  `phone_number` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  `oib` varchar(11) NOT NULL,
  `sex` varchar(1) NOT NULL,
  `date_of_birth` varchar(100) NOT NULL,
  `telephone_number` varchar(100) NOT NULL,
  `address_of_residence` varchar(100) DEFAULT NULL,
  `zip_code` varchar(10) NOT NULL,
  `city` varchar(100) NOT NULL,
  `nationality` varchar(100) NOT NULL,
  `personal_web` varchar(200) DEFAULT NULL,
  `institution_tmp` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ud_institution_id` (`institution_id`),
  KEY `fk_ud_user_id` (`user_id`),
  CONSTRAINT `fk_ud_institution_id` FOREIGN KEY (`institution_id`) REFERENCES `institutions` (`id`),
  CONSTRAINT `fk_ud_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_projects_periods_roles`
--

DROP TABLE IF EXISTS `users_projects_periods_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_projects_periods_roles` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `period_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`project_id`,`period_id`,`role_id`),
  KEY `fk_uppr_user_id` (`user_id`),
  KEY `fk_uppr_ppr_pk` (`project_id`,`period_id`,`role_id`),
  CONSTRAINT `fk_uppr_ppr_pk` FOREIGN KEY (`project_id`, `period_id`, `role_id`) REFERENCES `projects_periods_roles` (`project_id`, `period_id`, `role_id`),
  CONSTRAINT `fk_uppr_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_roles`
--

DROP TABLE IF EXISTS `users_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_roles` (
  `role_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `fk_ur_user_id` (`user_id`),
  KEY `fk_ur_role_id` (`role_id`),
  CONSTRAINT `fk_ur_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `fk_ur_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-05-12  9:29:24

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_sifarnik` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ref_table` VARCHAR(45) NOT NULL,
  `ref_column` VARCHAR(45) NOT NULL,
  `seq_no` INT NULL,
  `code` VARCHAR(45) NOT NULL,
  `description` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `u_table_col_code` (`ref_table`, `ref_column`, `code`))  
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_rezultati` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br_cilja` VARCHAR(15) NOT NULL,
  `br_rezultata` VARCHAR(15) NOT NULL,
  `naziv` TEXT NOT NULL,
  `zaduzena_osoba` VARCHAR(100) NOT NULL,
  `oznaka_diseminacije` CHAR(3) NOT NULL,
  `postignuto` CHAR(1) NOT NULL DEFAULT 0,
  `datum_realizacije` CHAR(7) NOT NULL DEFAULT '00/0000',
  `komentar` VARCHAR(100) NULL,
  `poveznica` varchar(256) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `u_br_cilja` (`br_cilja` ASC),
  INDEX `u_br_rezultata` (`br_rezultata` ASC),
  INDEX `i_fk_evr_period` (`period_id` ASC),
  CONSTRAINT `fk_evr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_tocke_provjere` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br_cilja` VARCHAR(15) NOT NULL,
  `br_tocke_provjere` VARCHAR(15) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `zaduzena_osoba` VARCHAR(100) NOT NULL,
  `postignuto` CHAR(1) NOT NULL DEFAULT 0,
  `datum_realizacije` CHAR(7) NULL DEFAULT '00/0000',
  `komentari` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evtp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evtp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_osoblje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `radno_mjesto` VARCHAR(45) NOT NULL,
  `broj_zena` INT NOT NULL,
  `broj_muskaraca` INT NOT NULL,
  `broj_u_priv_sektoru` INT NOT NULL,
  `broj_u_jav_sektoru` INT NOT NULL,
  `broj_u_sveucilistu` INT NOT NULL,
  `broj_u_inozemstvu` INT NOT NULL,
  `broj_nezaposlenih` INT NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evo_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evo_period`
    FOREIGN KEY (`id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;
ALTER TABLE `hrzzspp`.`ev_osoblje` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT ;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_istrazivacka_skupina` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ime_prezime` VARCHAR(25) NOT NULL,
  `titula` VARCHAR(45) NOT NULL,
  `godina_rodjenja` INT NOT NULL,
  `ustanova_zaposlenja` VARCHAR(50) NOT NULL,
  `datum_ukljucivanja` DATE NOT NULL,
  `uloga` VARCHAR(30) NULL,
  `godina_upisa_dok` INT NULL,
  `godina_stjecanja_dok` INT NULL,
  `pocetak_zaposlenja` DATE NULL,
  `trajanje_zaposlenja` INT NULL,
  `izvor_place` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evis_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evis_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_usavrsavanje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `br` INT NOT NULL,
  `vrsta_aktivnosti` VARCHAR(45) NOT NULL,
  `ime_prezime` VARCHAR(25) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `razdoblje_od` DATE NOT NULL,
  `razdoblje_do` DATE NOT NULL,
  `ustanova` VARCHAR(255) NULL,
  `drzava` VARCHAR(100) NULL,
  `usvojena_znanja` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evu_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evu_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_dok_disertacije` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_ukupan` INT NULL,
  `broj_fin_hrzz` INT NULL,
  `broj_izvan_zz` INT NULL,
  `broj_gospodarstvo` INT NULL,
  `broj_dvojnih` INT NULL,
  `broj_radova_i_patenata` INT NULL,
  `broj_izdavanje` INT NULL,
  `period_id` INT(11) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evdd_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evdd_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_radova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `autor` VARCHAR(100) NOT NULL,
  `naslov` VARCHAR(255) NOT NULL,
  `godina` INT NOT NULL,
  `ustanova` VARCHAR(100) NOT NULL,
  `mentor` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_zn_radova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `naziv` VARCHAR(100) NULL,
  `autori` VARCHAR(255) NULL,
  `naziv_casopisa` VARCHAR(100) NULL,
  `broj_casopisa` VARCHAR(45) NULL,
  `godiste_casopisa` INT NULL,
  `izdavac` VARCHAR(100) NULL,
  `mjesto_izdanja` VARCHAR(45) NULL,
  `relevantne_stranice` VARCHAR(45) NULL,
  `godina_objavljivanja` INT NULL,
  `recenzija` VARCHAR(45) NULL,
  `kvartil` VARCHAR(45) NULL,
  `stalni_identifikator` VARCHAR(255) NULL,
  `otvoreni_pristup` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evzr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evzr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_publikacija` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `vrsta` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(100) NOT NULL,
  `autori` VARCHAR(255) NULL,
  `naziv_casopisa` VARCHAR(100) NULL,
  `broj_casopisa` VARCHAR(45) NULL,
  `godiste_casopisa` INT NULL,
  `izdavac` VARCHAR(100) NULL,
  `mjesto_izdanja` VARCHAR(45) NULL,
  `relevantne_stranice` VARCHAR(45) NULL,
  `godina_objavljivanja` INT NULL,
  `stalni_identifikator` VARCHAR(255) NULL,
  `otvoreni_pristup` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_dis_aktivnosti` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `vrsta` VARCHAR(45) NOT NULL,
  `ime_i_prezime` VARCHAR(25) NOT NULL,
  `uloga` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(255) NOT NULL,
  `datum_od` DATE NULL,
  `datum_do` DATE NULL,
  `mjesto` VARCHAR(45) NULL,
  `vrsta_publike` VARCHAR(45) NULL,
  `broj_sudionika` INT NULL,
  `obuhvacene_drzave` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpda_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpda_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_patenata` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `vrsta` VARCHAR(45) NOT NULL,
  `povjerljivo` CHAR(1) NOT NULL,
  `datum_embarga` DATE NOT NULL,
  `referenca` VARCHAR(45) NOT NULL,
  `naziv` VARCHAR(100) NOT NULL,
  `prijavitelj` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evp_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evp_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

DROP TABLE  `hrzzspp`.`ev_suradnje`;
CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_suradnje` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `redni_broj` INT NOT NULL,
  `vrsta_suradnje` VARCHAR(20) NULL,
  `institucija_suradnje` VARCHAR(200) NULL,
  `ime_i_prezime_suradnje` VARCHAR(100) NULL,
  `opis` VARCHAR(256) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evs_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evs_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_fondova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_prijava` INT NOT NULL,
  `naziv_programa` VARCHAR(100) NOT NULL,
  `rok_prijave` DATE NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evf_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evf_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_ostalo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_nematerijalnih` INT NULL,
  `naziv_nematerijalnih` VARCHAR(100) NULL,
  `opis_nematerijalnih` VARCHAR(100) NULL,
  `broj_proizvoda` INT NULL,
  `naziv_proizvoda` VARCHAR(100) NULL,
  `opis_proizvoda` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evost_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evost_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_dobrobit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rast_zaposlenosti` CHAR(1) NULL DEFAULT 0,
  `razvoj_novih` CHAR(1) NULL DEFAULT 0,
  `tema_u_medijima` CHAR(1) NULL DEFAULT 0,
  `posjecenost_ustanova` CHAR(1) NULL DEFAULT 0,
  `poboljsanje_uvjeta` CHAR(1) NULL DEFAULT 0,
  `promjena_navika` CHAR(1) NULL DEFAULT 0,
  `utjecaj_na_politiku` CHAR(1) NULL DEFAULT 0,
  `novi_programi` CHAR(1) NULL DEFAULT 0,
  `utjecaj_na_zdrastvo` CHAR(1) NULL DEFAULT 0,
  `pojava_novog` CHAR(1) NULL DEFAULT 0,
  `ostalo` CHAR(1) NULL DEFAULT 0,
  `opis_ostalo` VARCHAR(100) NULL DEFAULT 0,
  `period_id` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_evd_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evd_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ev_files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(45) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_blob` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_evf_file_name` (`file_name`),
  KEY `i_evf_ref_table_id` (`ref_table`,`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `sifarnik` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ref` varchar(100) NOT NULL,
  `seq_no` int(11) DEFAULT NULL,
  `code` varchar(45) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_table_col_code` (`code`,`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;