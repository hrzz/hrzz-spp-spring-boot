CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_ostalo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_nematerijalnih` INT NULL,
  `naziv_nematerijalnih` VARCHAR(100) NULL,
  `opis_nematerijalnih` VARCHAR(100) NULL,
  `broj_proizvoda` INT NULL,
  `naziv_proizvoda` VARCHAR(100) NULL,
  `opis_proizvoda` VARCHAR(100) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evost_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evost_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;