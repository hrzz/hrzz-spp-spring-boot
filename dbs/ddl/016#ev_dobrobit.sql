CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_dobrobit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `rast_zaposlenosti` CHAR(1) NULL DEFAULT 0,
  `razvoj_novih` CHAR(1) NULL DEFAULT 0,
  `tema_u_medijima` CHAR(1) NULL DEFAULT 0,
  `posjecenost_ustanova` CHAR(1) NULL DEFAULT 0,
  `poboljsanje_uvjeta` CHAR(1) NULL DEFAULT 0,
  `promjena_navika` CHAR(1) NULL DEFAULT 0,
  `utjecaj_na_politiku` CHAR(1) NULL DEFAULT 0,
  `novi_programi` CHAR(1) NULL DEFAULT 0,
  `utjecaj_na_zdrastvo` CHAR(1) NULL DEFAULT 0,
  `pojava_novog` CHAR(1) NULL DEFAULT 0,
  `ostalo` CHAR(1) NULL DEFAULT 0,
  `opis_ostalo` VARCHAR(100) NULL DEFAULT 0,
  `period_id` INT(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `fk_evd_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evd_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;