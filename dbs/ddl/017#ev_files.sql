CREATE TABLE `ev_files` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ref_table` varchar(45) NOT NULL,
  `ref_id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `file_size` bigint(20) NOT NULL,
  `file_type` varchar(100) NOT NULL,
  `file_blob` longblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `i_evf_file_name` (`file_name`),
  KEY `i_evf_ref_table_id` (`ref_table`,`ref_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;