CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_zn_radova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj` INT NOT NULL,
  `naziv` VARCHAR(100) NULL,
  `autori` VARCHAR(255) NULL,
  `naziv_casopisa` VARCHAR(100) NULL,
  `broj_casopisa` VARCHAR(45) NULL,
  `godiste_casopisa` INT NULL,
  `izdavac` VARCHAR(100) NULL,
  `mjesto_izdanja` VARCHAR(45) NULL,
  `relevantne_stranice` VARCHAR(45) NULL,
  `godina_objavljivanja` INT NULL,
  `recenzija` VARCHAR(45) NULL,
  `kvartil` VARCHAR(45) NULL,
  `stalni_identifikator` VARCHAR(255) NULL,
  `otvoreni_pristup` VARCHAR(45) NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evzr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evzr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;