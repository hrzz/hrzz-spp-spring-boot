CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_popis_radova` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `autor` VARCHAR(100) NOT NULL,
  `naslov` VARCHAR(255) NOT NULL,
  `godina` INT NOT NULL,
  `ustanova` VARCHAR(100) NOT NULL,
  `mentor` VARCHAR(100) NOT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evpr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evpr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;