CREATE TABLE IF NOT EXISTS `hrzzspp`.`ev_osoblje_br_osoba` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `broj_osoba` INT NULL,
  `period_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_evosobr_period_idx` (`period_id` ASC),
  CONSTRAINT `fk_evosobr_period`
    FOREIGN KEY (`period_id`)
    REFERENCES `hrzzspp`.`reporting_periods` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;