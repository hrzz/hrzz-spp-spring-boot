package hr.btb.hrzzspp.selectOneRadio;

public class StatusPrk {
	
		private int statusId;
		private String statusDescription;
		
		
		public StatusPrk(int id, String description) {
			this.statusId = id;
			this.statusDescription = description;
		}

		public int getStatusId() {
			return statusId;
		}

		public void setStatusId(int statusId) {
			this.statusId = statusId;
		}

		public String getStatusDescription() {
			return statusDescription;
		}

		public void setStatusDescription(String statusDescription) {
			this.statusDescription = statusDescription;
		}
		
}
