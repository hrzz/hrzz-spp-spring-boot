package hr.btb.hrzzspp.selectOneRadio;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.ManagedBean;
import javax.faces.bean.SessionScoped;

/**
 * 
 * @author iris.zuza
 *
 */

@ManagedBean
@SessionScoped
public class SelectOneRadioStatusPrk {
		
		/*selektiranje radio buttona za status i komentar PRK-a*/
		private List<StatusPrk> statusiPrk = new ArrayList<StatusPrk>();
		
		/*selektiranje radio buttona za mnoštvo izbora u obrazscu za Evaluatore*/
		private List<StatusPrk> ciljeviAIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ciljeviBIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ciljeviCIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> implementacijaAIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> implementacijaBIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> diseminacijaIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> potporaIzbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ucinakD1Izbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ucinakD2Izbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ucinakD3Izbor = new ArrayList<StatusPrk>();
		private List<StatusPrk> ucinakD4Izbor = new ArrayList<StatusPrk>();
		
		

		private String selectedStatusPrk;
		
		public SelectOneRadioStatusPrk(){
			statusiPrk.add(new StatusPrk(1,"&#x2713;"));
			statusiPrk.add(new StatusPrk(2,"&#x21;"));
			statusiPrk.add((new StatusPrk(3, "&#63;")));
			statusiPrk.add((new StatusPrk(4, "&#x2013;")));
			
			
			ciljeviAIzbor.add(new StatusPrk(1, "Da"));
			ciljeviAIzbor.add(new StatusPrk(2, "Djelomično"));
			ciljeviAIzbor.add(new StatusPrk(3, "Ne"));
			
			ciljeviBIzbor.add(new StatusPrk(1, "Da"));
			ciljeviBIzbor.add(new StatusPrk(2, "Djelomično"));
			ciljeviBIzbor.add(new StatusPrk(3, "Ne"));
			
			ciljeviCIzbor.add(new StatusPrk(1, "Da"));
			ciljeviCIzbor.add(new StatusPrk(2, "Djelomično"));
			ciljeviCIzbor.add(new StatusPrk(3, "Ne"));
			
			implementacijaAIzbor.add(new StatusPrk(1, "Da"));
			implementacijaAIzbor.add(new StatusPrk(2, "Djelomično"));
			implementacijaAIzbor.add(new StatusPrk(3, "Ne"));
			
			implementacijaBIzbor.add(new StatusPrk(1, "Da"));
			implementacijaBIzbor.add(new StatusPrk(2, "Djelomično"));
			implementacijaBIzbor.add(new StatusPrk(3, "Ne"));
			
			diseminacijaIzbor.add(new StatusPrk(1, "Da"));
			diseminacijaIzbor.add(new StatusPrk(2, "Djelomično"));
			diseminacijaIzbor.add(new StatusPrk(3, "Ne"));
			
			potporaIzbor.add(new StatusPrk(1, "Da"));
			potporaIzbor.add(new StatusPrk(2, "Djelomično"));
			potporaIzbor.add(new StatusPrk(3, "Ne"));
			
			ucinakD1Izbor.add(new StatusPrk(1, "Da"));
			ucinakD1Izbor.add(new StatusPrk(2, "Djelomično"));
			ucinakD1Izbor.add(new StatusPrk(3, "Ne"));
			
			ucinakD2Izbor.add(new StatusPrk(1, "Da"));
			ucinakD2Izbor.add(new StatusPrk(2, "Djelomično"));
			ucinakD2Izbor.add(new StatusPrk(3, "Ne"));
			
			ucinakD3Izbor.add(new StatusPrk(1, "Da"));
			ucinakD3Izbor.add(new StatusPrk(2, "Djelomično"));
			ucinakD3Izbor.add(new StatusPrk(3, "Ne"));
			
			ucinakD4Izbor.add(new StatusPrk(1, "Da"));
			ucinakD4Izbor.add(new StatusPrk(2, "Djelomično"));
			ucinakD4Izbor.add(new StatusPrk(3, "Ne"));
			
		}
		

		public List<StatusPrk> getCiljeviBIzbor() {
			return ciljeviBIzbor;
		}


		public void setCiljeviBIzbor(List<StatusPrk> ciljeviBIzbor) {
			this.ciljeviBIzbor = ciljeviBIzbor;
		}


		public List<StatusPrk> getCiljeviCIzbor() {
			return ciljeviCIzbor;
		}


		public void setCiljeviCIzbor(List<StatusPrk> ciljeviCIzbor) {
			this.ciljeviCIzbor = ciljeviCIzbor;
		}


		public List<StatusPrk> getStatusiPrk() {
			return statusiPrk;
		}

		public void setStatusiPrk(List<StatusPrk> statusiPrk) {
			this.statusiPrk = statusiPrk;
		}

		public String getSelectedStatusPrk() {
			return selectedStatusPrk;
		}

		public void setSelectedStatusPrk(String selectedStatusPrk) {
			this.selectedStatusPrk = selectedStatusPrk;
		}


		public List<StatusPrk> getCiljeviAIzbor() {
			return ciljeviAIzbor;
		}


		public void setCiljeviAIzbor(List<StatusPrk> ciljeviAIzbor) {
			this.ciljeviAIzbor = ciljeviAIzbor;
		}
		
		public List<StatusPrk> getImplementacijaAIzbor() {
			return implementacijaAIzbor;
		}


		public void setImplementacijaAIzbor(List<StatusPrk> implementacijaAIzbor) {
			this.implementacijaAIzbor = implementacijaAIzbor;
		}


		public List<StatusPrk> getImplementacijaBIzbor() {
			return implementacijaBIzbor;
		}


		public void setImplementacijaBIzbor(List<StatusPrk> implementacijaBIzbor) {
			this.implementacijaBIzbor = implementacijaBIzbor;
		}
		
		public List<StatusPrk> getDiseminacijaIzbor() {
			return diseminacijaIzbor;
		}


		public void setDiseminacijaIzbor(List<StatusPrk> diseminacijaIzbor) {
			this.diseminacijaIzbor = diseminacijaIzbor;
		}


		public List<StatusPrk> getPotporaIzbor() {
			return potporaIzbor;
		}


		public void setPotporaIzbor(List<StatusPrk> potporaIzbor) {
			this.potporaIzbor = potporaIzbor;
		}


		public List<StatusPrk> getUcinakD1Izbor() {
			return ucinakD1Izbor;
		}


		public void setUcinakD1Izbor(List<StatusPrk> ucinakD1Izbor) {
			this.ucinakD1Izbor = ucinakD1Izbor;
		}


		public List<StatusPrk> getUcinakD2Izbor() {
			return ucinakD2Izbor;
		}


		public void setUcinakD2Izbor(List<StatusPrk> ucinakD2Izbor) {
			this.ucinakD2Izbor = ucinakD2Izbor;
		}


		public List<StatusPrk> getUcinakD3Izbor() {
			return ucinakD3Izbor;
		}


		public void setUcinakD3Izbor(List<StatusPrk> ucinakD3Izbor) {
			this.ucinakD3Izbor = ucinakD3Izbor;
		}


		public List<StatusPrk> getUcinakD4Izbor() {
			return ucinakD4Izbor;
		}


		public void setUcinakD4Izbor(List<StatusPrk> ucinakD4Izbor) {
			this.ucinakD4Izbor = ucinakD4Izbor;
		}
		
		
}
