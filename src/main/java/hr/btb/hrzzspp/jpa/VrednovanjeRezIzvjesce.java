package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vrednovanje_rez_izvjesce")
public class VrednovanjeRezIzvjesce implements Serializable{


	private static final long serialVersionUID = -8309787279348459931L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique=true)
	private int id;
	
	@Column(name = "zav_ocjena_a")
	private boolean zavOcjenaA;
	
	@Column(name = "zav_ocjena_b")
	private boolean zavOcjenaB;
	
	@Column(name = "zav_ocjena_c")
	private boolean zavOcjenaC;
	
	@Column(name = "zav_ocjena_d")
	private boolean zavOcjenaD;
	
	@Column(name = "komentar")
	private String komentar;
    
	@Column(name = "vrijeme_promjene")
	private Timestamp vrijemePromjene;
	
	@ManyToOne()
	@JoinColumn(name = "period_id", nullable=false)
	private ReportingPeriod period;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable=false)
	private User user;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isZavOcjenaA() {
		return zavOcjenaA;
	}

	public void setZavOcjenaA(boolean zavOcjenaA) {
		this.zavOcjenaA = zavOcjenaA;
	}

	public boolean isZavOcjenaB() {
		return zavOcjenaB;
	}

	public void setZavOcjenaB(boolean zavOcjenaB) {
		this.zavOcjenaB = zavOcjenaB;
	}

	public boolean isZavOcjenaC() {
		return zavOcjenaC;
	}

	public void setZavOcjenaC(boolean zavOcjenaC) {
		this.zavOcjenaC = zavOcjenaC;
	}

	public boolean isZavOcjenaD() {
		return zavOcjenaD;
	}

	public void setZavOcjenaD(boolean zavOcjenaD) {
		this.zavOcjenaD = zavOcjenaD;
	}

	public String getKomentar() {
		return komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public Timestamp getVrijemePromjene() {
		return vrijemePromjene;
	}

	public void setVrijemePromjene(Timestamp vrijemePromjene) {
		this.vrijemePromjene = vrijemePromjene;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	
}
