package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the ev_ostalo database table.
 * 
 */
@Entity
@Table(name="ev_ostalo")
@NamedQuery(name="EvOstalo.findAll", query="SELECT e FROM EvOstalo e")
public class EvOstalo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="broj")
	private Integer broj;
	
	@Column(name="naziv", length=100)
	private String naziv;

	@Column(name="opis", length=100)
	private String opis;

	@Column(name="vrsta", length=2)
	private String vrsta;
	
	// bi-directional one-to-one association to Project
	@OneToOne
	@JoinColumn(name = "period_id", nullable = false, updatable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvOstalo() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getBroj() {
		return broj;
	}

	public void setBroj(Integer broj) {
		this.broj = broj;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public String getVrsta() {
		return vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	
}