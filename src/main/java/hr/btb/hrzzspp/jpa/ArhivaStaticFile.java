package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.primefaces.model.DefaultStreamedContent;

/**
 * Root Entity class for the arhiva_static_file database table.
 * 
 */
@Entity
@Table(name = "arhiva_static_files")
public class ArhivaStaticFile implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	protected int id;
	
	@Column(name = "title", nullable = false, length = 100)
	protected String title;

	@Lob
	@Column(name = "file_blob", nullable = false)
	protected byte[] fileBlob;

	@Column(name = "file_name", nullable = false, length = 255)
	protected String fileName;

	@Column(name = "file_size", nullable = false)
	protected long fileSize;

	@Column(name = "file_type", nullable = false, length = 100)
	protected String fileType;
	
	@Column(name = "group", nullable = false, length = 45)
	protected String group;
	
	@Transient
	private DefaultStreamedContent streamedContent;

	public ArhivaStaticFile() {
	}
	
	public ArhivaStaticFile(String title, String file_name) {
		this.title = title;
		this.fileName = file_name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getFileBlob() {
		return this.fileBlob;
	}

	public void setFileBlob(byte[] fileBlob) {
		this.fileBlob = fileBlob;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

//	public StreamedContent getStreamedContent() {
//		InputStream stream = new ByteArrayInputStream(fileBlob);
//		return new DefaultStreamedContent(stream, fileType, fileName);
//	}

	@Override
	public String toString() {
		return "EvNiFile [id=" + id + ", fileName=" + fileName + ", fileSize="
				+ fileSize + ", fileType=" + fileType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + (int) (fileSize ^ (fileSize >>> 32));
		result = prime * result
				+ ((fileType == null) ? 0 : fileType.hashCode());
		result = prime * result + id;

		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArhivaStaticFile other = (ArhivaStaticFile) obj;
		if (fileName == null) {
			if (other.fileName != null) {
				return false;
			}
		} else if (!fileName.equals(other.fileName)) {
			return false;
		}
		if (fileSize != other.fileSize) {
			return false;
		}
		if (fileType == null) {
			if (other.fileType != null) {
				return false;
			}
		} else if (!fileType.equals(other.fileType)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		return true;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public DefaultStreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(DefaultStreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}
}