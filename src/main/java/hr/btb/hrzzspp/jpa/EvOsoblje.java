package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.WhereJoinTable;

/**
 * The persistent class for the ev_osoblje database table.
 * 
 */
@Entity
@Table(name = "ev_osoblje")
public class EvOsoblje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "broj_muskaraca", nullable = false)
	private int brojMuskaraca;

	@Column(name = "broj_nezaposlenih", nullable = false)
	private int brojNezaposlenih;

	@Column(name = "broj_u_inozemstvu", nullable = false)
	private int brojUInozemstvu;

	@Column(name = "broj_u_jav_sektoru", nullable = false)
	private int brojUJavSektoru;

	@Column(name = "broj_u_priv_sektoru", nullable = false)
	private int brojUPrivSektoru;

	@Column(name = "broj_u_sveucilistu", nullable = false)
	private int brojUSveucilistu;

	@Column(name = "broj_zena", nullable = false)
	private int brojZena;

	@OneToOne(optional = false)
	@JoinColumn(name = "period_id", referencedColumnName = "id", nullable =	false, updatable = false)
	private ReportingPeriod reportingPeriod;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "radno_mjesto", referencedColumnName = "code", nullable = false, updatable = false)
	@WhereJoinTable(clause = "ref_table = 'ev_osoblje' AND ref_column = 'radno_mjesto'")
	private EvSifarnik radnoMjesto;
	

	public EvOsoblje() {
	}

	public EvOsoblje(EvSifarnik radnoMjesto, ReportingPeriod reportingPeriod) {
		this.radnoMjesto = radnoMjesto;
		this.reportingPeriod = reportingPeriod;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrojMuskaraca() {
		return this.brojMuskaraca;
	}

	public void setBrojMuskaraca(int brojMuskaraca) {
		this.brojMuskaraca = brojMuskaraca;
	}

	public int getBrojNezaposlenih() {
		return this.brojNezaposlenih;
	}

	public void setBrojNezaposlenih(int brojNezaposlenih) {
		this.brojNezaposlenih = brojNezaposlenih;
	}

	public int getBrojUInozemstvu() {
		return this.brojUInozemstvu;
	}

	public void setBrojUInozemstvu(int brojUInozemstvu) {
		this.brojUInozemstvu = brojUInozemstvu;
	}

	public int getBrojUJavSektoru() {
		return this.brojUJavSektoru;
	}

	public void setBrojUJavSektoru(int brojUJavSektoru) {
		this.brojUJavSektoru = brojUJavSektoru;
	}

	public int getBrojUPrivSektoru() {
		return this.brojUPrivSektoru;
	}

	public void setBrojUPrivSektoru(int brojUPrivSektoru) {
		this.brojUPrivSektoru = brojUPrivSektoru;
	}

	public int getBrojUSveucilistu() {
		return this.brojUSveucilistu;
	}

	public void setBrojUSveucilistu(int brojUSveucilistu) {
		this.brojUSveucilistu = brojUSveucilistu;
	}

	public int getBrojZena() {
		return this.brojZena;
	}

	public void setBrojZena(int brojZena) {
		this.brojZena = brojZena;
	}

	public EvSifarnik getRadnoMjesto() {
		return radnoMjesto;
	}

	public void setRadnoMjesto(EvSifarnik radnoMjesto) {
		this.radnoMjesto = radnoMjesto;
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
}