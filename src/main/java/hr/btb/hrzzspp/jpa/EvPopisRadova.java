package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.spring.view.FileRelatedObjectInterface;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_popis_radova database table. Tablica 7.:
 * Popis doktorskih disertacija i diplomskih/magistarskih radova
 */
@Entity
@Table(name = "ev_popis_radova")
public class EvPopisRadova implements Serializable, FileRelatedObjectInterface {

	private static final long serialVersionUID = 6316878111309346623L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(nullable = false, length = 100)
	private String autor;

	@Column(nullable = false)
	private int godina;

	@Column(nullable = false, length = 100)
	private String mentor;

	@Column(nullable = false, length = 255)
	private String naslov;

	@Column(nullable = false, length = 100)
	private String ustanova;

	@Column(nullable = true, length = 255)
	private String poveznica;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;

	// bi-directional one-to-one association to FileRad
	@OneToOne(mappedBy = "rad", cascade = { CascadeType.ALL }, optional = true, orphanRemoval = true)
	private FileRad file;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvPopisRadova() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAutor() {
		return this.autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getGodina() {
		return this.godina;
	}

	public void setGodina(int godina) {
		this.godina = godina;
	}

	public String getMentor() {
		return this.mentor;
	}

	public void setMentor(String mentor) {
		this.mentor = mentor;
	}

	public String getNaslov() {
		return this.naslov;
	}

	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getUstanova() {
		return this.ustanova;
	}

	public void setUstanova(String ustanova) {
		this.ustanova = ustanova;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public FileRad getFile() {
		return file;
	}

	public void setFile(FileRad fileRad) {
		this.file = fileRad;

		// setting relation from other direction
		if (fileRad != null) {
			this.file.setRad(this);
		}
	}

	public String getPoveznica() {
		return poveznica;
	}

	public void setPoveznica(String poveznica) {
		this.poveznica = poveznica;
	}

	@Override
	public void setFile(EvFile file) {
		this.file = (FileRad) file;
		if (file != null) {
			this.file.setRad(this);
		}
	}

	@Override
	public String toString() {
		return "EvPopisRadova [id=" + id + ", autor=" + autor + ", godina="
				+ godina + ", mentor=" + mentor + ", naslov=" + naslov
				+ ", ustanova=" + ustanova + ", poveznica=" + poveznica
				+ ", reportingPeriod=" + reportingPeriod + ", file=" + file
				+ "]";
	}
}