package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * The persistent class for the ev_files database table. For Files that
 * reference ev_popis_radova table.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.TBL_EV_POPIS_RADOVA)
public class FileRad extends EvFile implements Serializable {

	private static final long serialVersionUID = -6466571739324212316L;

	@OneToOne
	@JoinColumn(name = "ref_id", referencedColumnName = "id")
	private EvPopisRadova rad;

	public EvPopisRadova getRad() {
		return rad;
	}

	public void setRad(EvPopisRadova rad) {
		this.rad = rad;
	}

	public FileRad() {
	}

	@Override
	public String toString() {
		return String
				.format("FileRad [rad=%s, id=%s, fileName=%s, fileSize=%s, fileType=%s]",
						rad, id, fileName, fileSize, fileType);
	}
}