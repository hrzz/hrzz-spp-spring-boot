/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author dalibor.harmina
 *
 */
@Entity
@Table(name = "ev_narativno_izvjesce")
public class EvNarativnoIzvjesce implements Serializable {

	private static final long serialVersionUID = -3625169554062846354L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "pp_odstupanje", nullable = false, length = 600)
	private String ppOdstupanje;

	@Column(name = "pp_objasnjenje", nullable = false, length = 600)
	private String ppObjasnjenje;

	@Column(name = "pp_nova_pitanja", nullable = false, length = 600)
	private String ppNovaPitanja;

	@Column(name = "pp_preporuke", nullable = false, length = 600)
	private String ppPreporuke;

	@Column(name = "pp_potpora", nullable = false, length = 600)
	private String ppPotpora;

	@Column(name = "ig_aktivnosti", nullable = false, length = 600)
	private String igAktivnosti;

	@Column(name = "ig_promjena", nullable = false, length = 600)
	private String igPromjena;

	@Column(name = "os_prijetnje", nullable = false, length = 600)
	private String osPrijetnje;

	@Column(name = "os_poduzimanja", nullable = false, length = 600)
	private String osPoduzimanja;

	@Column(name = "datum_mjesto", nullable = false, length = 60)
	private String datumMjesto;
	
	@Column(name = "kreiran_pdf")
	private int kreiranPdf;
	
	@Column(name = "vrijeme_kreiranja_pdf", nullable = false)
	private Timestamp vrijemeKreiranjaPdf;

	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;

	// bi-directional many-to-one association to EvNiFile
	@OneToMany(mappedBy = "narativnoIzvjesce", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
	private List<EvNiFile> evNiFiles;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EvNarativnoIzvjesce other = (EvNarativnoIzvjesce) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public List<EvNiFile> getEvNiFiles() {
		return this.evNiFiles;
	}

	public void setEvNiFiles(List<EvNiFile> evNiFiles) {
		this.evNiFiles = evNiFiles;
	}

	public EvNiFile addEvNiFile(EvNiFile evNiFile) {
		getEvNiFiles().add(evNiFile);
		evNiFile.setNarativnoIzvjesce(this);

		return evNiFile;
	}

	public EvNiFile removeEvNiFile(EvNiFile evNiFile) {
		getEvNiFiles().remove(evNiFile);
		evNiFile.setNarativnoIzvjesce(null);

		return evNiFile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getPpOdstupanje() {
		return ppOdstupanje;
	}

	public void setPpOdstupanje(String ppOdstupanje) {
		this.ppOdstupanje = ppOdstupanje;
	}

	public String getPpObjasnjenje() {
		return ppObjasnjenje;
	}

	public void setPpObjasnjenje(String ppObjasnjenje) {
		this.ppObjasnjenje = ppObjasnjenje;
	}

	public String getPpNovaPitanja() {
		return ppNovaPitanja;
	}

	public void setPpNovaPitanja(String ppNovaPitanja) {
		this.ppNovaPitanja = ppNovaPitanja;
	}

	public String getPpPreporuke() {
		return ppPreporuke;
	}

	public void setPpPreporuke(String ppPreporuke) {
		this.ppPreporuke = ppPreporuke;
	}

	public String getPpPotpora() {
		return ppPotpora;
	}

	public void setPpPotpora(String ppPotpora) {
		this.ppPotpora = ppPotpora;
	}

	public String getIgAktivnosti() {
		return igAktivnosti;
	}

	public void setIgAktivnosti(String igAktivnosti) {
		this.igAktivnosti = igAktivnosti;
	}

	public String getIgPromjena() {
		return igPromjena;
	}

	public void setIgPromjena(String igPromjena) {
		this.igPromjena = igPromjena;
	}

	public String getOsPrijetnje() {
		return osPrijetnje;
	}

	public void setOsPrijetnje(String osPrijetnje) {
		this.osPrijetnje = osPrijetnje;
	}

	public String getOsPoduzimanja() {
		return osPoduzimanja;
	}

	public void setOsPoduzimanja(String osPoduzimanja) {
		this.osPoduzimanja = osPoduzimanja;
	}

	public String getDatumMjesto() {
		return datumMjesto;
	}

	public void setDatumMjesto(String datumMjesto) {
		this.datumMjesto = datumMjesto;
	}

	public int getKreiranPdf() {
		return kreiranPdf;
	}

	public void setKreiranPdf(int kreiranPdf) {
		this.kreiranPdf = kreiranPdf;
	}

	public Timestamp getVrijemeKreiranjaPdf() {
		return vrijemeKreiranjaPdf;
	}

	public void setVrijemeKreiranjaPdf(Timestamp vrijemeKreiranjaPdf) {
		this.vrijemeKreiranjaPdf = vrijemeKreiranjaPdf;
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	@Override
	public String toString() {
		return "EvNarativnoIzvjesce [id=" + id + ", ppOdstupanje="
				+ ppOdstupanje + ", ppObjasnjenje=" + ppObjasnjenje
				+ ", ppNovaPitanja=" + ppNovaPitanja + ", ppPreporuke="
				+ ppPreporuke + ", ppPotpora=" + ppPotpora + ", igAktivnosti="
				+ igAktivnosti + ", igPromjena=" + igPromjena
				+ ", osPrijetnje=" + osPrijetnje + ", osPoduzimanja="
				+ osPoduzimanja + ", datumMjesto=" + datumMjesto
				+ ", reportingPeriod=" + reportingPeriod + ", evNiFiles="
				+ evNiFiles + "]";
	}
}
