package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_dok_disertacije database table.
 * Tablica 7.: Popis doktorskih disertacija i diplomskih/magistarskih radova
 */
@Entity
@Table(name = "ev_dok_disertacije")
public class EvDokDisertacije implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "broj_dvojnih")
	private int brojDvojnih;

	@Column(name = "broj_fin_hrzz")
	private int brojFinHrzz;

	@Column(name = "broj_gospodarstvo")
	private int brojGospodarstvo;

	@Column(name = "broj_izdavanje")
	private int brojIzdavanje;

	@Column(name = "broj_izvan_zz")
	private int brojIzvanZz;

	@Column(name = "broj_radova_i_patenata")
	private int brojRadovaIPatenata;

	@Column(name = "broj_ukupan")
	private int brojUkupan;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id")
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvDokDisertacije() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrojDvojnih() {
		return this.brojDvojnih;
	}

	public void setBrojDvojnih(int brojDvojnih) {
		this.brojDvojnih = brojDvojnih;
	}

	public int getBrojFinHrzz() {
		return this.brojFinHrzz;
	}

	public void setBrojFinHrzz(int brojFinHrzz) {
		this.brojFinHrzz = brojFinHrzz;
	}

	public int getBrojGospodarstvo() {
		return this.brojGospodarstvo;
	}

	public void setBrojGospodarstvo(int brojGospodarstvo) {
		this.brojGospodarstvo = brojGospodarstvo;
	}

	public int getBrojIzdavanje() {
		return this.brojIzdavanje;
	}

	public void setBrojIzdavanje(int brojIzdavanje) {
		this.brojIzdavanje = brojIzdavanje;
	}

	public int getBrojIzvanZz() {
		return this.brojIzvanZz;
	}

	public void setBrojIzvanZz(int brojIzvanZz) {
		this.brojIzvanZz = brojIzvanZz;
	}

	public int getBrojRadovaIPatenata() {
		return this.brojRadovaIPatenata;
	}

	public void setBrojRadovaIPatenata(int brojRadovaIPatenata) {
		this.brojRadovaIPatenata = brojRadovaIPatenata;
	}

	public int getBrojUkupan() {
		return this.brojUkupan;
	}

	public void setBrojUkupan(int brojUkupan) {
		this.brojUkupan = brojUkupan;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}