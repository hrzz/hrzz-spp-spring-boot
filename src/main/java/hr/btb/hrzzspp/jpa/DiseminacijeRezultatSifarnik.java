package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.SifarnikEnum;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = SifarnikEnum.Values.REZULTAT_DISEMINACIJA)
public class DiseminacijeRezultatSifarnik extends Sifarnik implements
		SifarnikInterface {

	private static final long serialVersionUID = -3544912451370487468L;

	private String code;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "DiseminacijeRezultatSifarnik [code=" + code + ", id=" + id
				+ ", seqNo=" + seqNo + ", ref=" + ref + ", description="
				+ description + "]";
	}
}
