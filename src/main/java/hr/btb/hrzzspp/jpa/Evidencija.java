package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="evidencija")
@NamedQuery(name="Evidencija.findAll", query="SELECT e FROM Evidencija e")
public class Evidencija  implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="title", nullable=false, length=45)
	private String title;

	@Column(name="naziv_xhtml", nullable=false, length=45)
	private String nazivXhtml;

	@Column(name="opis", nullable=false, length=45)
	private String opis;

	@Column(name="status_prk")
	private int statusPrk;
	
	@Column(name="komentar_prk", length=500)
	private String komentarPrk;
	
	@ManyToOne
	@JoinColumn(name="status_id", nullable=false)
	private StatusEvidencija statusEvidencija;
	
	@ManyToOne
	@JoinColumn(name="period_id", nullable=false)
	private ReportingPeriod reportingPeriod;

	public Evidencija() {

	}

	public Evidencija(String title, String naziv, String opis) {
		this.title = title;
		this.nazivXhtml = naziv;
		this.opis = opis;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNazivXhtml() {
		return nazivXhtml;
	}

	public void setNazivXhtml(String nazivXhtml) {
		this.nazivXhtml = nazivXhtml;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getStatusPrk() {
		return statusPrk;
	}

	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public StatusEvidencija getStatusEvidencija() {
		return statusEvidencija;
	}

	public void setStatusEvidencija(StatusEvidencija statusEvidencija) {
		this.statusEvidencija = statusEvidencija;
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
}
