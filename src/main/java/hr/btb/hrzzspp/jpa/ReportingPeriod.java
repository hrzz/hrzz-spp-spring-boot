package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

/**
 * The persistent class for the reporting_periods database table.
 * 
 */
@Entity
@Table(name = "reporting_periods")
public class ReportingPeriod implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "created_at", nullable = false)
	private Timestamp createdAt;

	@Column(name = "date_from", nullable = false)
	private Timestamp dateFrom;

	@Column(name = "date_to", nullable = false)
	private Timestamp dateTo;

	@Column(name = "is_final", nullable = false, length = 1)
	private String isFinal;

	@Column(name = "status", nullable = false, length = 1)
	private String status;

	@ManyToOne(optional = false)
    @JoinColumn(name = "status_id", nullable=false)
	private StatusRazdoblja statusRazdoblja;

	@Column(name = "modified_at")
	private Timestamp modifiedAt;

	@Column(name = "ordinal_number", nullable = false)
	private int ordinalNumber;

	@Column(name = "approved_resources", nullable = false)
	private BigDecimal approvedResources;
	
	@Column(name = "ukupna_sredstva", nullable = false)
	private BigDecimal ukupnaSredstva;
	
	@Column(name = "neutroseno", nullable = false)
	private BigDecimal neutroseno;
	
	@Column(name = "nenamjenski_utros", nullable = false)
	private BigDecimal nenamjenskiUtroseno;
	
	@Column(name = "tip")
	private String tipRazdoblja;

	// bi-directional many-to-one association to Project
	@ManyToOne
	@JoinColumn(name = "project_id", nullable = false)
	private Project project;
	
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "reportingPeriod", fetch = FetchType.EAGER)
	@OrderBy(value = "id ASC")
	private List<Evidencija> evidencijaList;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "reportingPeriod", fetch = FetchType.LAZY)
	private List<EvNarativnoIzvjesce> narativnoIzvjesceList;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "period", fetch = FetchType.LAZY)
	private List<UserRolePeriodStatus> userRolePeriodStatus;
	
	@OneToMany(targetEntity=EvObrazacEvaluacije.class, mappedBy = "period", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<EvObrazacEvaluacije> projectEvaluator;
	
	@OneToMany(mappedBy = "period", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Evaluator> evaluatorList;
	
	@OneToMany(targetEntity=SukobInteresa.class, mappedBy = "period", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<SukobInteresa> projectSukobInteresa;

	public ReportingPeriod() {
	}
	
	public ReportingPeriod(int id) {
		this.id = id;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Timestamp getDateFrom() {
		return this.dateFrom;
	}

	public void setDateFrom(Timestamp dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Timestamp getDateTo() {
		return this.dateTo;
	}

	public void setDateTo(Timestamp dateTo) {
		this.dateTo = dateTo;
	}

	public String getIsFinal() {
		return this.isFinal;
	}

	public void setIsFinal(String isFinal) {
		this.isFinal = isFinal;
	}

	public Timestamp getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public int getOrdinalNumber() {
		return this.ordinalNumber;
	}

	public void setOrdinalNumber(int ordinalNumber) {
		this.ordinalNumber = ordinalNumber;
	}

	public Project getProject() {
		return this.project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	@Override
	public String toString() {
		return String
				.format("ReportingPeriod [id=%s, isFinal=%s, ordinalNumber=%s, project=%s]",
						id, isFinal, ordinalNumber, project);
	}

	public BigDecimal getApprovedResources() {
		return approvedResources;
	}

	public void setApprovedResources(BigDecimal approvedResources) {
		this.approvedResources = approvedResources;
	}

	public List<Evidencija> getEvidencijaList() {
		return evidencijaList;
	}

	public void setEvidencijaList(List<Evidencija> evidencijaList) {
		this.evidencijaList = evidencijaList;
	}

	public List<EvNarativnoIzvjesce> getNarativnoIzvjesceList() {
		return narativnoIzvjesceList;
	}

	public void setNarativnoIzvjesceList(
			List<EvNarativnoIzvjesce> narativnoIzvjesceList) {
		this.narativnoIzvjesceList = narativnoIzvjesceList;
	}
	
	public List<UserRolePeriodStatus> getUserRolePeriodStatus() {
		return userRolePeriodStatus;
	}

	public void setUserRolePeriodStatus(List<UserRolePeriodStatus> userRolePeriodStatus) {
		this.userRolePeriodStatus = userRolePeriodStatus;
	}

	public List<EvObrazacEvaluacije> getProjectEvaluator() {
		return projectEvaluator;
	}

	public void setProjectEvaluator(List<EvObrazacEvaluacije> projectEvaluator) {
		this.projectEvaluator = projectEvaluator;
	}

	public List<Evaluator> getEvaluatorList() {
		return evaluatorList;
	}

	public void setEvaluatorList(List<Evaluator> evaluatorList) {
		this.evaluatorList = evaluatorList;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public BigDecimal getNenamjenskiUtroseno() {
		return nenamjenskiUtroseno;
	}

	public void setNenamjenskiUtroseno(BigDecimal nenamjenskiUtroseno) {
		this.nenamjenskiUtroseno = nenamjenskiUtroseno;
	}

	public BigDecimal getNeutroseno() {
		return neutroseno;
	}

	public void setNeutroseno(BigDecimal neutroseno) {
		this.neutroseno = neutroseno;
	}

	public BigDecimal getUkupnaSredstva() {
		return ukupnaSredstva;
	}

	public void setUkupnaSredstva(BigDecimal ukupnaSredstva) {
		this.ukupnaSredstva = ukupnaSredstva;
	}
	
	public String getTipRazdoblja() {
		return tipRazdoblja;
	}

	public void setTipRazdoblja(String tipRazdoblja) {
		this.tipRazdoblja = tipRazdoblja;
	}

	public List<SukobInteresa> getProjectSukobInteresa() {
		return projectSukobInteresa;
	}

	public void setProjectSukobInteresa(List<SukobInteresa> projectSukobInteresa) {
		this.projectSukobInteresa = projectSukobInteresa;
	}

	public StatusRazdoblja getStatusRazdoblja() {
		return statusRazdoblja;
	}

	public void setStatusRazdoblja(StatusRazdoblja statusRazdoblja) {
		this.statusRazdoblja = statusRazdoblja;
	}

	
	
	
}
