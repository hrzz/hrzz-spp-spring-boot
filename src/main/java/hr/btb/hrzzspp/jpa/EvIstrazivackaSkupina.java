package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ev_istrazivacka_skupina database table.
 * 
 */
@Entity
@Table(name = "ev_istrazivacka_skupina")
public class EvIstrazivackaSkupina implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name = "datum_ukljucivanja", nullable = false)
	private Date datumUkljucivanja;

	@Column(name = "godina_rodjenja", nullable = false)
	private int godinaRodjenja;

	@Column(name = "godina_stjecanja_dok")
	private int godinaStjecanjaDok;

	@Column(name = "godina_upisa_dok")
	private int godinaUpisaDok;

	@Column(name = "ime_prezime", nullable = false, length = 25)
	private String imePrezime;

	@Column(name = "izvor_place", length = 45)
	private String izvorPlace;

	@Temporal(TemporalType.DATE)
	@Column(name = "pocetak_zaposlenja")
	private Date pocetakZaposlenja;

	@Column(nullable = false, length = 45)
	private String titula;

	@Column(name = "trajanje_zaposlenja")
	private int trajanjeZaposlenja;

	@Column(length = 30)
	private String uloga;

	@Column(name = "ustanova_zaposlenja", nullable = false, length = 50)
	private String ustanovaZaposlenja;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvIstrazivackaSkupina() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatumUkljucivanja() {
		return this.datumUkljucivanja;
	}

	public void setDatumUkljucivanja(Date datumUkljucivanja) {
		this.datumUkljucivanja = datumUkljucivanja;
	}

	public int getGodinaRodjenja() {
		return this.godinaRodjenja;
	}

	public void setGodinaRodjenja(int godinaRodjenja) {
		this.godinaRodjenja = godinaRodjenja;
	}

	public int getGodinaStjecanjaDok() {
		return this.godinaStjecanjaDok;
	}

	public void setGodinaStjecanjaDok(int godinaStjecanjaDok) {
		this.godinaStjecanjaDok = godinaStjecanjaDok;
	}

	public int getGodinaUpisaDok() {
		return this.godinaUpisaDok;
	}

	public void setGodinaUpisaDok(int godinaUpisaDok) {
		this.godinaUpisaDok = godinaUpisaDok;
	}

	public String getImePrezime() {
		return this.imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public String getIzvorPlace() {
		return this.izvorPlace;
	}

	public void setIzvorPlace(String izvorPlace) {
		this.izvorPlace = izvorPlace;
	}

	public Date getPocetakZaposlenja() {
		return this.pocetakZaposlenja;
	}

	public void setPocetakZaposlenja(Date pocetakZaposlenja) {
		this.pocetakZaposlenja = pocetakZaposlenja;
	}

	public String getTitula() {
		return this.titula;
	}

	public void setTitula(String titula) {
		this.titula = titula;
	}

	public int getTrajanjeZaposlenja() {
		return this.trajanjeZaposlenja;
	}

	public void setTrajanjeZaposlenja(int trajanjeZaposlenja) {
		this.trajanjeZaposlenja = trajanjeZaposlenja;
	}

	public String getUloga() {
		return this.uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	public String getUstanovaZaposlenja() {
		return this.ustanovaZaposlenja;
	}

	public void setUstanovaZaposlenja(String ustanovaZaposlenja) {
		this.ustanovaZaposlenja = ustanovaZaposlenja;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}