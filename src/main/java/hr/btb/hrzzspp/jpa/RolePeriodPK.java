/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * @author dalibor.harmina
 *
 */
@Embeddable
public class RolePeriodPK implements Serializable  {

	private static final long serialVersionUID = -524373185467043427L;

	@Column(name = "role_id", insertable = false, updatable = false, unique = true, nullable = false)
	private int roleId;

	@Column(name = "period_id", insertable = false, updatable = false, unique = true, nullable = false)
	private int periodId;

	public RolePeriodPK() {
	}

	public int getRoleId() {
		return roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}
}
