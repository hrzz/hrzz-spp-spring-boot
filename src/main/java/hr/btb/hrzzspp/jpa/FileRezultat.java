package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * The persistent class for the ev_files database table. For Files that
 * reference ev_rezultati table.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.TBL_EV_REZULTATI)
public class FileRezultat extends EvFile implements Serializable {

	private static final long serialVersionUID = 3825126085302044509L;

	@OneToOne
	@JoinColumn(name = "ref_id", referencedColumnName = "id", nullable = false)
	private Rezultat rezultat;

	public Rezultat getRezultat() {
		return rezultat;
	}

	public void setRezultat(Rezultat rezultat) {
		this.rezultat = rezultat;
	}

	public FileRezultat() {
	}

	@Override
	public String toString() {
		return String.format(
				"FileRezultat [id=%s, fileName=%s, fileSize=%s, ref_id=%s]",
				getId(), getFileName(), getFileSize(), rezultat.getId());
	}
}