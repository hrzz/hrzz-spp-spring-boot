package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.spring.view.FileRelatedObjectInterface;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_popis_publikacija database table.
 *Tablica 9. :  Popis ostalih objavljenih publikacija prozašlih iz akitvnosti na projektu
 */
@Entity
@Table(name = ConstantsEvidencije.TBL_EV_POPIS_PUBLIKACIJA)
public class EvPopisPublikacija implements Serializable, FileRelatedObjectInterface {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(length = 255)
	private String autori;

	@Column(nullable = false)
	private int broj;

	@Column(name = "broj_casopisa", length = 45)
	private String brojCasopisa;

	@Column(name = "godina_objavljivanja")
	private int godinaObjavljivanja;

	@Column(name = "godiste_casopisa")
	private int godisteCasopisa;

	@Column(length = 100)
	private String izdavac;

	@Column(name = "mjesto_izdanja", length = 45)
	private String mjestoIzdanja;

	@Column(nullable = false, length = 100)
	private String naziv;

	@Column(name = "naziv_casopisa", length = 100)
	private String nazivCasopisa;

	@Column(name = "otvoreni_pristup", length = 45)
	private String otvoreniPristup;

	@Column(name = "relevantne_stranice", length = 45)
	private String relevantneStranice;

	@Column(nullable = false, length = 45)
	private String vrsta;

	@Column(nullable = true, length = 255)
	private String poveznica;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;

	// bi-directional one-to-one association to FilePublikacija
	@OneToOne(mappedBy = "publikacija", cascade = { CascadeType.ALL }, optional = true, orphanRemoval = true)
	private FilePublikacija file;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvPopisPublikacija() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAutori() {
		return this.autori;
	}

	public void setAutori(String autori) {
		this.autori = autori;
	}

	public int getBroj() {
		return this.broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}

	public String getBrojCasopisa() {
		return this.brojCasopisa;
	}

	public void setBrojCasopisa(String brojCasopisa) {
		this.brojCasopisa = brojCasopisa;
	}

	public int getGodinaObjavljivanja() {
		return this.godinaObjavljivanja;
	}

	public void setGodinaObjavljivanja(int godinaObjavljivanja) {
		this.godinaObjavljivanja = godinaObjavljivanja;
	}

	public int getGodisteCasopisa() {
		return this.godisteCasopisa;
	}

	public void setGodisteCasopisa(int godisteCasopisa) {
		this.godisteCasopisa = godisteCasopisa;
	}

	public String getIzdavac() {
		return this.izdavac;
	}

	public void setIzdavac(String izdavac) {
		this.izdavac = izdavac;
	}

	public String getMjestoIzdanja() {
		return this.mjestoIzdanja;
	}

	public void setMjestoIzdanja(String mjestoIzdanja) {
		this.mjestoIzdanja = mjestoIzdanja;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getNazivCasopisa() {
		return this.nazivCasopisa;
	}

	public void setNazivCasopisa(String nazivCasopisa) {
		this.nazivCasopisa = nazivCasopisa;
	}

	public String getOtvoreniPristup() {
		return this.otvoreniPristup;
	}

	public void setOtvoreniPristup(String otvoreniPristup) {
		this.otvoreniPristup = otvoreniPristup;
	}

	public String getRelevantneStranice() {
		return this.relevantneStranice;
	}

	public void setRelevantneStranice(String relevantneStranice) {
		this.relevantneStranice = relevantneStranice;
	}

	public String getVrsta() {
		return this.vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
	
	public FilePublikacija getFile() {
		return file;
	}

	public void setFile(FilePublikacija file) {
		this.file = file;
		if (this.file != null) {
			this.file.setPublikacija(this);
		}
	}

	public String getPoveznica() {
		return poveznica;
	}

	public void setPoveznica(String poveznica) {
		this.poveznica = poveznica;
	}

	@Override
	public void setFile(EvFile file) {
		this.file = (FilePublikacija) file;
		if (file != null) {
			this.file.setPublikacija(this);
		}
	}
}