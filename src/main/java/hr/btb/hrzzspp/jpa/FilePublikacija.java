package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * The persistent class for the ev_files database table. For Files that
 * reference ev_popis_publikacija table.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.TBL_EV_POPIS_PUBLIKACIJA)
public class FilePublikacija extends EvFile implements Serializable {
	private static final long serialVersionUID = 1028635061081331297L;

	@OneToOne
	@JoinColumn(name = "ref_id", referencedColumnName = "id")
	private EvPopisPublikacija publikacija;

	public EvPopisPublikacija getPublikacija() {
		return publikacija;
	}

	public void setPublikacija(EvPopisPublikacija publikacija) {
		this.publikacija = publikacija;
	}

	public FilePublikacija() {
	}

	@Override
	public String toString() {
		return String
				.format("FileRad [publikacija=%s, id=%s, fileName=%s, fileSize=%s, fileType=%s]",
						publikacija, id, fileName, fileSize, fileType);
	}
}