package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * Root Entity class for the ev_ni_files database table.
 * 
 */
@Entity
@Table(name = ConstantsEvidencije.TBL_EV_NI_FILES)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = ConstantsEvidencije.DISCRIMINATOR_EV_NI_FILES, discriminatorType = DiscriminatorType.STRING, length = 45)
public abstract class EvNiFile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	protected int id;

	@Lob
	@Column(name = "file_blob", nullable = false)
	protected byte[] fileBlob;

	@Column(name = "file_name", nullable = false, length = 45)
	protected String fileName;

	@Column(name = "file_size", nullable = false)
	protected long fileSize;

	@Column(name = "file_type", nullable = false, length = 45)
	protected String fileType;

	@Column(name = "ref_table", nullable = false, length = 45, insertable = false, updatable = false)
	protected String refTable = ConstantsEvidencije.TBL_EV_NI_FILES;
	
	public String getRefTable() {
		return this.refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	// bi-directional many-to-one association to EvNarativnoIzvjesce
	@ManyToOne
	@JoinColumn(name = "ref_id", nullable = false)
	private EvNarativnoIzvjesce narativnoIzvjesce;

	public EvNiFile() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getFileBlob() {
		return this.fileBlob;
	}

	public void setFileBlob(byte[] fileBlob) {
		this.fileBlob = fileBlob;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public EvNarativnoIzvjesce getNarativnoIzvjesce() {
		return narativnoIzvjesce;
	}

	public void setNarativnoIzvjesce(EvNarativnoIzvjesce evNarativnoIzvjesce) {
		this.narativnoIzvjesce = evNarativnoIzvjesce;
	}

	public StreamedContent getStreamedContent() {
		InputStream stream = new ByteArrayInputStream(fileBlob);
		return new DefaultStreamedContent(stream, fileType, fileName);
	}

	@Override
	public String toString() {
		return "EvNiFile [id=" + id + ", fileName=" + fileName + ", fileSize="
				+ fileSize + ", fileType=" + fileType + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + (int) (fileSize ^ (fileSize >>> 32));
		result = prime * result
				+ ((fileType == null) ? 0 : fileType.hashCode());
		result = prime * result + id;
		result = prime
				* result
				+ ((narativnoIzvjesce == null) ? 0 : narativnoIzvjesce
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		EvNiFile other = (EvNiFile) obj;
		if (fileName == null) {
			if (other.fileName != null) {
				return false;
			}
		} else if (!fileName.equals(other.fileName)) {
			return false;
		}
		if (fileSize != other.fileSize) {
			return false;
		}
		if (fileType == null) {
			if (other.fileType != null) {
				return false;
			}
		} else if (!fileType.equals(other.fileType)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (narativnoIzvjesce == null) {
			if (other.narativnoIzvjesce != null) {
				return false;
			}
		} else if (!narativnoIzvjesce.equals(other.narativnoIzvjesce)) {
			return false;
		}
		return true;
	}
}