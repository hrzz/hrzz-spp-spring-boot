package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class PeriodIzvjestaj implements Serializable, Comparable<PeriodIzvjestaj> {
 
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
     
    private String size;
     
    private String type;
    
	private String cssClass;
	
	private int userId;
	
	private String tip;
	
	private int periodId;
	
    public PeriodIzvjestaj(String name, String size, String type, String cssClass, int userId, String tip, int periodId) {
        this.name = name;
        this.size = size;
        this.type = type;
        this.cssClass = cssClass;
        this.userId = userId;
        this.tip = tip;
        this.periodId = periodId;
    }
 
    public String getName() {
//    	if(!session.hasRoles(new String [] {"PRK"})){
            return name;
//    	}else{
//            return "EVL 1";
//    	}
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getSize() {
        return size;
    }
 
    public void setSize(String size) {
        this.size = size;
    }
 
    public String getType() {
        return type;
    }
 
    public void setType(String type) {
        this.type = type;
    }
 
    public String getCssClass() {
		return cssClass;
	}

	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}

	//Eclipse Generated hashCode and equals
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((size == null) ? 0 : size.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        PeriodIzvjestaj other = (PeriodIzvjestaj) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (size == null) {
            if (other.size != null)
                return false;
        } else if (!size.equals(other.size))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return name;
    }
 
    public int compareTo(PeriodIzvjestaj document) {
        return this.getName().compareTo(document.getName());
    }

	public String getTip() {
		return tip;
	}

	public void setTip(String tip) {
		this.tip = tip;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}
	
}  