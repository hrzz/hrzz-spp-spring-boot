package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the ev_fondova database table.
 * 
 */
@Entity
@Table(name="ev_fondova")
@NamedQuery(name="EvFondova.findAll", query="SELECT e FROM EvFondova e")
public class EvFondova implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="broj_prijava", nullable=false)
	private int brojPrijava;

	@Column(name="naziv_programa", nullable=false, length=100)
	private String nazivPrograma;

	@Temporal(TemporalType.DATE)
	@Column(name="rok_prijave", nullable=false)
	private Date rokPrijave;

	@Column(nullable=false, length=45)
	private String status;

	//bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name="period_id", nullable=false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvFondova() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBrojPrijava() {
		return this.brojPrijava;
	}

	public void setBrojPrijava(int brojPrijava) {
		this.brojPrijava = brojPrijava;
	}

	public String getNazivPrograma() {
		return this.nazivPrograma;
	}

	public void setNazivPrograma(String nazivPrograma) {
		this.nazivPrograma = nazivPrograma;
	}

	public Date getRokPrijave() {
		return this.rokPrijave;
	}

	public void setRokPrijave(Date rokPrijave) {
		this.rokPrijave = rokPrijave;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}