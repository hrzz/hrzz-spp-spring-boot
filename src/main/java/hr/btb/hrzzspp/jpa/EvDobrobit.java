package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.*;

/**
 * The persistent class for the ev_dobrobit database table.
 * 
 */
@Entity
@Table(name = "ev_dobrobit")
public class EvDobrobit implements Serializable {

	private static final long serialVersionUID = -5675097376396306986L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "novi_programi", length = 1)
	private String noviProgrami;

	@Column(name = "opis_ostalo", length = 100)
	private String opisOstalo;

	@Column(length = 1)
	private String ostalo;

	@Column(name = "poboljsanje_uvjeta", length = 1)
	private String poboljsanjeUvjeta;

	@Column(name = "pojava_novog", length = 1)
	private String pojavaNovog;

	@Column(name = "posjecenost_ustanova", length = 1)
	private String posjecenostUstanova;

	@Column(name = "promjena_navika", length = 1)
	private String promjenaNavika;

	@Column(name = "rast_zaposlenosti", length = 1)
	private String rastZaposlenosti;

	@Column(name = "razvoj_novih", length = 1)
	private String razvojNovih;

	@Column(name = "tema_u_medijima", length = 1)
	private String temaUMedijima;

	@Column(name = "utjecaj_na_politiku", length = 1)
	private String utjecajNaPolitiku;

	@Column(name = "utjecaj_na_zdrastvo", length = 1)
	private String utjecajNaZdrastvo;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvDobrobit(ReportingPeriod reportingPeriod) {
		super();
		this.reportingPeriod = reportingPeriod;
	}

	public EvDobrobit() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNoviProgrami() {
		return this.noviProgrami;
	}

	public void setNoviProgrami(String noviProgrami) {
		this.noviProgrami = noviProgrami;
	}

	public String getOpisOstalo() {
		return this.opisOstalo;
	}

	public void setOpisOstalo(String opisOstalo) {
		this.opisOstalo = opisOstalo;
	}

	public String getOstalo() {
		return this.ostalo;
	}

	public void setOstalo(String ostalo) {
		this.ostalo = ostalo;
	}

	public String getPoboljsanjeUvjeta() {
		return this.poboljsanjeUvjeta;
	}

	public void setPoboljsanjeUvjeta(String poboljsanjeUvjeta) {
		this.poboljsanjeUvjeta = poboljsanjeUvjeta;
	}

	public String getPojavaNovog() {
		return this.pojavaNovog;
	}

	public void setPojavaNovog(String pojavaNovog) {
		this.pojavaNovog = pojavaNovog;
	}

	public String getPosjecenostUstanova() {
		return this.posjecenostUstanova;
	}

	public void setPosjecenostUstanova(String posjecenostUstanova) {
		this.posjecenostUstanova = posjecenostUstanova;
	}

	public String getPromjenaNavika() {
		return this.promjenaNavika;
	}

	public void setPromjenaNavika(String promjenaNavika) {
		this.promjenaNavika = promjenaNavika;
	}

	public String getRastZaposlenosti() {
		return this.rastZaposlenosti;
	}

	public void setRastZaposlenosti(String rastZaposlenosti) {
		this.rastZaposlenosti = rastZaposlenosti;
	}

	public String getRazvojNovih() {
		return this.razvojNovih;
	}

	public void setRazvojNovih(String razvojNovih) {
		this.razvojNovih = razvojNovih;
	}

	public String getTemaUMedijima() {
		return this.temaUMedijima;
	}

	public void setTemaUMedijima(String temaUMedijima) {
		this.temaUMedijima = temaUMedijima;
	}

	public String getUtjecajNaPolitiku() {
		return this.utjecajNaPolitiku;
	}

	public void setUtjecajNaPolitiku(String utjecajNaPolitiku) {
		this.utjecajNaPolitiku = utjecajNaPolitiku;
	}

	public String getUtjecajNaZdrastvo() {
		return this.utjecajNaZdrastvo;
	}

	public void setUtjecajNaZdrastvo(String utjecajNaZdrastvo) {
		this.utjecajNaZdrastvo = utjecajNaZdrastvo;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	@Override
	public String toString() {
		return String
				.format("EvDobrobit [id=%s, noviProgrami=%s, opisOstalo=%s, ostalo=%s, promjenaNavika=%s, rastZaposlenosti=%s, razvojNovih=%s, temaUMedijima=%s, utjecajNaPolitiku=%s, utjecajNaZdrastvo=%s, reportingPeriod=%s]",
						id, noviProgrami, opisOstalo, ostalo, promjenaNavika,
						rastZaposlenosti, razvojNovih, temaUMedijima,
						utjecajNaPolitiku, utjecajNaZdrastvo, reportingPeriod);
	}

}