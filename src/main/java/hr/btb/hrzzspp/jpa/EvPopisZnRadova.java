package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.spring.view.FileRelatedObjectInterface;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_popis_zn_radova database table.
 *  Tablica 8.:Popis znanstvenih radova (s domaćom ili međunarodnom recenzijom) proizašlih iz aktivnosti na projektu
 */
@Entity
@Table(name = ConstantsEvidencije.TBL_EV_POPIS_ZN_RADOVA)
public class EvPopisZnRadova implements Serializable, FileRelatedObjectInterface {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(nullable = false)
	private int broj;

	@Column(name = "godina_objavljivanja")
	private int godinaObjavljivanja;

	@Column(length = 100)
	private String izdavac;

	@Column(length = 45)
	private String kvartil;

	@Column(name = "mjesto_izdanja", length = 45)
	private String mjestoIzdanja;

	@Column(length = 100)
	private String naziv;

	@Column(name = "otvoreni_pristup", length = 45)
	private String otvoreniPristup;

	@Column(length = 45)
	private String recenzija;

	@Column(name = "relevantne_stranice", length = 45)
	private String relevantneStranice;

	@Column(name = "poveznica", length = 255)
	private String poveznica;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;

	// bi-directional one-to-one association to FileZnRad
	@OneToOne(mappedBy = "znRad", cascade = { CascadeType.ALL }, optional = true, orphanRemoval = true)
	private FileZnRad file;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}
	
	public EvPopisZnRadova() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBroj() {
		return this.broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}

	public int getGodinaObjavljivanja() {
		return this.godinaObjavljivanja;
	}

	public void setGodinaObjavljivanja(int godinaObjavljivanja) {
		this.godinaObjavljivanja = godinaObjavljivanja;
	}

	public String getIzdavac() {
		return this.izdavac;
	}

	public void setIzdavac(String izdavac) {
		this.izdavac = izdavac;
	}

	public String getKvartil() {
		return this.kvartil;
	}

	public void setKvartil(String kvartil) {
		this.kvartil = kvartil;
	}

	public String getMjestoIzdanja() {
		return this.mjestoIzdanja;
	}

	public void setMjestoIzdanja(String mjestoIzdanja) {
		this.mjestoIzdanja = mjestoIzdanja;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getOtvoreniPristup() {
		return this.otvoreniPristup;
	}

	public void setOtvoreniPristup(String otvoreniPristup) {
		this.otvoreniPristup = otvoreniPristup;
	}

	public String getRecenzija() {
		return this.recenzija;
	}

	public void setRecenzija(String recenzija) {
		this.recenzija = recenzija;
	}

	public String getRelevantneStranice() {
		return this.relevantneStranice;
	}

	public void setRelevantneStranice(String relevantneStranice) {
		this.relevantneStranice = relevantneStranice;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public FileZnRad getFile() {
		return file;
	}

	public void setFile(FileZnRad file) {
		this.file = file;
		if (this.file != null) {
			this.file.setZnRad(this);
		}
	}

	public String getPoveznica() {
		return poveznica;
	}

	public void setPoveznica(String poveznica) {
		this.poveznica = poveznica;
	}

	@Override
	public void setFile(EvFile file) {
		this.file = (FileZnRad) file;
		if (file != null) {
			this.file.setZnRad(this);
		}
	}	
}