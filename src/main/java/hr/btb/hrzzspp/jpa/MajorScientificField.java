package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author misel
 */
@Entity
@Table(name = "major_scientific_fields")
@XmlRootElement
public class MajorScientificField implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;

	@Basic(optional = false)
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Basic(optional = false)
	@Column(name = "code", nullable = false, length = 5)
	private String code;

	@Basic(optional = false)
	@Column(name = "created_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Column(name = "modified_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedAt;

	@Column(name = "mail", length = 45)
	private String mail;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "msfId")
	private List<ScientificField> scientificFieldList;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "majorScientificField")
	private List<Project> projectList;

	public MajorScientificField() {
	}

	public MajorScientificField(Integer id) {
		this.id = id;
	}

	public MajorScientificField(Integer id, String name, String code) {
		this.id = id;
		this.name = name;
		this.code = code;
	}

	public MajorScientificField(Integer id, String name, String code,
			Date createdAt) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.createdAt = createdAt;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@XmlTransient
	public List<ScientificField> getScientificFieldList() {
		return scientificFieldList;
	}

	public void setScientificFieldList(List<ScientificField> scientificFieldList) {
		this.scientificFieldList = scientificFieldList;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof MajorScientificField)) {
			return false;
		}
		MajorScientificField other = (MajorScientificField) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "hr.btb.hrzzspp.jpa.data.MajorScientificField[ id=" + id + " ]";
	}

}
