package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 * Root Entity class for the ev_files database table.
 * 
 */
@Entity
@Table(name = ConstantsEvidencije.TBL_EV_FILES)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = ConstantsEvidencije.DISCRIMINATOR_EV_FILES, discriminatorType = DiscriminatorType.STRING, length = 45)
public abstract class EvFile {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	protected int id;

	@Lob
	@Column(name = "file_blob", nullable = false)
	protected byte[] fileBlob;

	@Column(name = "file_name", nullable = false, length = 45)
	protected String fileName;

	@Column(name = "file_size", nullable = false)
	protected long fileSize;

	@Column(name = "file_type", nullable = false, length = 45)
	protected String fileType;

	public EvFile() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte[] getFileBlob() {
		return this.fileBlob;
	}

	public void setFileBlob(byte[] fileBlob) {
		this.fileBlob = fileBlob;
	}

	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public long getFileSize() {
		return this.fileSize;
	}

	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	public String getFileType() {
		return this.fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public StreamedContent getStreamedContent() {
		InputStream stream = new ByteArrayInputStream(fileBlob);
		return new DefaultStreamedContent(stream, fileType, fileName);
	}
}