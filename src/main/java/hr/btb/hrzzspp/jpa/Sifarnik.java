package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the sifarnik database table.
 * 
 */
@Entity
@Table(name = "sifarnik")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "ref", discriminatorType = DiscriminatorType.STRING, length = 100)
public abstract class Sifarnik implements Serializable {

	private static final long serialVersionUID = -8780043970185487758L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected int id;

	@Column(name = "seq_no")
	protected int seqNo;

	@Column(name = "ref", insertable=false, updatable=false)
	protected String ref;

	protected String description;

	
	public Sifarnik() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}
}