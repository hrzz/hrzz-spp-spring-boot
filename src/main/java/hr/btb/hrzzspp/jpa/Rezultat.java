package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.spring.view.FileRelatedObjectInterface;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_rezultati database table.
 * 
 */
@Entity
@Table(name = "ev_rezultati")
public class Rezultat implements Serializable, FileRelatedObjectInterface {

	private static final long serialVersionUID = -3664243837138032765L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "br_cilja", nullable = false, length = 15)
	private String brCilja;

	@Column(name = "br_rezultata", nullable = false, length = 15)
	private String brRezultata;

	@Column(name = "datum_realizacije", nullable = false, length = 7)
	private String datumRealizacije;

	@Column(length = 100)
	private String komentar;

	@Column(name = "poveznica", nullable = true, length = 255)
	private String poveznica;

	@Lob
	@Column(nullable = false)
	private String naziv;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "oznaka_diseminacije", referencedColumnName = "code", updatable = true)
	private DiseminacijeRezultatSifarnik oznakaDiseminacije;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "postignuto", referencedColumnName = "code", updatable = true)
	private PostignutoRezultatSifarnik postignuto;

	@Column(name = "zaduzena_osoba", nullable = false, length = 100)
	private String zaduzenaOsoba;

	@OneToOne(optional = false)
	@JoinColumn(name = "period_id", referencedColumnName = "id", nullable = false)
	private ReportingPeriod reportingPeriod;

	// bi-directional one-to-one association to FileRezultat
	@OneToOne(mappedBy = "rezultat", cascade = { CascadeType.ALL }, optional = true, orphanRemoval = true)
	private FileRezultat file;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int status_prk;
	

	public int getStatus_prk() {
		return status_prk;
	}

	public void setStatus_prk(int status_prk) {
		this.status_prk = status_prk;
	}

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public Rezultat() {
	}

	public Rezultat(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrCilja() {
		return this.brCilja;
	}

	public void setBrCilja(String brCilja) {
		this.brCilja = brCilja;
	}

	public String getBrRezultata() {
		return this.brRezultata;
	}

	public void setBrRezultata(String brRezultata) {
		this.brRezultata = brRezultata;
	}

	public String getDatumRealizacije() {
		return this.datumRealizacije;
	}

	public void setDatumRealizacije(String datumRealizacije) {
		this.datumRealizacije = datumRealizacije;
	}

	public String getKomentar() {
		return this.komentar;
	}

	public void setKomentar(String komentar) {
		this.komentar = komentar;
	}

	public String getPoveznica() {
		return poveznica;
	}

	public void setPoveznica(String poveznica) {
		this.poveznica = poveznica;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getZaduzenaOsoba() {
		return this.zaduzenaOsoba;
	}

	public void setZaduzenaOsoba(String zaduzenaOsoba) {
		this.zaduzenaOsoba = zaduzenaOsoba;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public PostignutoRezultatSifarnik getPostignuto() {
		return postignuto;
	}

	public void setPostignuto(PostignutoRezultatSifarnik postignuto) {
		this.postignuto = postignuto;
	}

	public DiseminacijeRezultatSifarnik getOznakaDiseminacije() {
		return oznakaDiseminacije;
	}

	public void setOznakaDiseminacije(
			DiseminacijeRezultatSifarnik oznakaDiseminacije) {
		this.oznakaDiseminacije = oznakaDiseminacije;
	}

	public FileRezultat getFile() {
		return file;
	}

	public void setFile(FileRezultat fileRezultat) {
		this.file = fileRezultat;
		if (fileRezultat != null) {
			this.file.setRezultat(this);
		}
	}

	@Override
	public String toString() {
		return String
				.format("Rezultat [id=%s, brCilja=%s, brRezultata=%s, datumRealizacije=%s, komentar=%s, naziv=%s, oznakaDiseminacije=%s, postignuto=%s, zaduzenaOsoba=%s, reportingPeriod=%s, file=%s, poveznica=%s, komentarPrk=%s,status_prk=%s]",
						id, brCilja, brRezultata, datumRealizacije, komentar,
						naziv, oznakaDiseminacije, postignuto, zaduzenaOsoba,
						reportingPeriod, file, poveznica,komentarPrk,status_prk);
	}

	@Override
	public void setFile(EvFile file) {
		this.file = (FileRezultat) file;
		if (file != null) {
			this.file.setRezultat(this);
		}
	}
}
