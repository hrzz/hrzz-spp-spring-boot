package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The persistent class for the ev_ni_files database table. For Files that are
 * of ni type = O_IZVJESCE.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.NI_O_IZVJESCE)
public class FileOIzvjesce extends EvNiFile implements Serializable {

	private static final long serialVersionUID = -4612099377050052687L;
}