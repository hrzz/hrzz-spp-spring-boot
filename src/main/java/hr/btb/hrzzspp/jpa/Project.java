package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
//import javax.persistence.NamedQueries;
//import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author misel
 */
@Entity
@Table(name = "projects")
@XmlRootElement
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Long id;

	@Basic(optional = false)
	@Column(name = "code", nullable = false, length = 10)
	private String code;

	@Basic(optional = false)
	@Column(name = "name", nullable = false, length = 255)
	private String name;

	@Basic(optional = false)
	@Column(name = "acronym", nullable = false, length = 100)
	private String acronym;

	@Column(name = "tender_deadline", length = 10)
	private String tenderDeadline;

	@Column(name = "url", length = 255)
	private String url;

	@Column(name = "duration")
	private Integer duration;

	@Lob
	@Column(name = "summary_hr", length = 65535)
	private String summaryHR;

	@Lob
	@Column(name = "summary_en", length = 65535)
	private String summaryEN;

	@Column(name = "is_multidisc", length = 1)
	private String isMultidisc;

	@JoinColumn(name = "scientific_field_id", referencedColumnName = "id")
	@ManyToOne(optional = true)
	private ScientificField scientificField;

	@JoinColumn(name = "major_scientific_field_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private MajorScientificField majorScientificField;

	@JoinColumn(name = "tender_type_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private TenderType tenderType;

	@JoinColumn(name = "leader_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private User leader;

	@JoinColumn(name = "institution_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Institution institutionId;

	@Basic(optional = false)
	@Column(name = "institution_ceo", nullable = false, length = 255)
	private String institutionCeo;

	@Basic(optional = false)
	@Column(name = "created_at", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdAt;

	@Column(name = "modified_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedAt;

	@Column(name = "celnik_ustanove", length = 60)
	private String celnikUstanove;

	@Column(name = "poveznica_mreza", length = 100)
	private String poveznicaMreza;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project", fetch = FetchType.EAGER)
	private List<ReportingPeriod> reportingPeriods;
	

	public Project() {
	}

	public Project(Long id) {
		this.id = id;
	}

	public Project(Long id, String code, String institutionCeo, String name,
			String acronym, Timestamp createdAt) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.acronym = acronym;
		this.institutionCeo = institutionCeo;
		this.createdAt = createdAt;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getInstitutionCeo() {
		return institutionCeo;
	}

	public void setInstitutionCeo(String institutionCeo) {
		this.institutionCeo = institutionCeo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public ScientificField getScientificField() {
		return scientificField;
	}

	public void setScientificField(ScientificField scientificField) {
		this.scientificField = scientificField;
	}

	public MajorScientificField getMajorScientificField() {
		return majorScientificField;
	}

	public void setMajorScientificField(
			MajorScientificField majorScientificField) {
		this.majorScientificField = majorScientificField;
	}

	public TenderType getTenderType() {
		return tenderType;
	}

	public void setTenderType(TenderType tenderTypeId) {
		this.tenderType = tenderTypeId;
	}

	public List<ReportingPeriod> getReportingPeriods() {
		return reportingPeriods;
	}

	public void setReportingPeriods(List<ReportingPeriod> reportingPeriods) {
		this.reportingPeriods = reportingPeriods;
	}

	public Institution getInstitutionId() {
		return institutionId;
	}

	public void setInstitutionId(Institution institutionId) {
		this.institutionId = institutionId;
	}



	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are
		// not set
		if (!(object instanceof Project)) {
			return false;
		}
		Project other = (Project) object;
		if ((this.id == null && other.id != null)
				|| (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("Project[id=%d, name=%s]", id, name);
	}

	public String getTenderDeadline() {
		return tenderDeadline;
	}

	public void setTenderDeadline(String tenderDeadline) {
		this.tenderDeadline = tenderDeadline;
	}

	public String getSummaryHR() {
		return summaryHR;
	}

	public void setSummaryHR(String summaryHR) {
		this.summaryHR = summaryHR;
	}

	public String getSummaryEN() {
		return summaryEN;
	}

	public void setSummaryEN(String summaryEN) {
		this.summaryEN = summaryEN;
	}

	public String getIsMultidisc() {
		return isMultidisc;
	}

	public void setIsMultidisc(String isMultidisc) {
		this.isMultidisc = isMultidisc;
	}

	public User getLeader() {
		return leader;
	}

	public void setLeader(User leaderId) {
		this.leader = leaderId;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getCelnikUstanove() {
		return celnikUstanove;
	}

	public void setCelnikUstanove(String celnikUstanove) {
		this.celnikUstanove = celnikUstanove;
	}

	public String getPoveznicaMreza() {
		return poveznicaMreza;
	}

	public void setPoveznicaMreza(String poveznicaMreza) {
		this.poveznicaMreza = poveznicaMreza;
	}

}
