package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.*;

import java.util.Date;


/**
 * The persistent class for the ev_patenata database table.
 * 
 */
@Entity
@Table(name="ev_patenata")
@NamedQuery(name="EvPatenata.findAll", query="SELECT e FROM EvPatenata e")
public class EvPatenata implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Temporal(TemporalType.DATE)
	@Column(name="datum_embarga", nullable=false)
	private Date datumEmbarga;

	@Column(nullable=false, length=100)
	private String naziv;

	@Column(nullable=false, length=1)
	private String povjerljivo;

	@Column(nullable=false, length=100)
	private String prijavitelj;

	@Column(nullable=false, length=45)
	private String referenca;

	@Column(nullable=false, length=45)
	private String vrsta;

	//bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name="period_id", nullable=false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvPatenata() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDatumEmbarga() {
		return this.datumEmbarga;
	}

	public void setDatumEmbarga(Date datumEmbarga) {
		this.datumEmbarga = datumEmbarga;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPovjerljivo() {
		return this.povjerljivo;
	}

	public void setPovjerljivo(String povjerljivo) {
		this.povjerljivo = povjerljivo;
	}

	public String getPrijavitelj() {
		return this.prijavitelj;
	}

	public void setPrijavitelj(String prijavitelj) {
		this.prijavitelj = prijavitelj;
	}

	public String getReferenca() {
		return this.referenca;
	}

	public void setReferenca(String referenca) {
		this.referenca = referenca;
	}

	public String getVrsta() {
		return this.vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}