package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the ev_suradnje database table.
 * 
 */
@Entity
@Table(name="ev_suradnje")
@NamedQuery(name="EvSuradnje.findAll", query="SELECT e FROM EvSuradnje e")
public class EvSuradnje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="redni_broj")
	private int redniBroj;

	@Column(name="vrsta_suradnje", length=20)
	private String vrstaSuradnje;

	@Column(name="institucija_suradnje", length=200)
	private String institucijaSuradnje;

	@Column(name="ime_i_prezime_suradnje", length=100)
	private String imeIPrezimeSuradnje;

	@Column(nullable=false, length=256)
	private String opis;

	//bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name="period_id", nullable=false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvSuradnje() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOpis() {
		return this.opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public int getRedniBroj() {
		return redniBroj;
	}

	public void setRedniBroj(int redniBroj) {
		this.redniBroj = redniBroj;
	}

	public String getVrstaSuradnje() {
		return vrstaSuradnje;
	}

	public void setVrstaSuradnje(String vrstaSuradnje) {
		this.vrstaSuradnje = vrstaSuradnje;
	}

	public String getInstitucijaSuradnje() {
		return institucijaSuradnje;
	}

	public void setInstitucijaSuradnje(String institucijaSuradnje) {
		this.institucijaSuradnje = institucijaSuradnje;
	}

	public String getImeIPrezimeSuradnje() {
		return imeIPrezimeSuradnje;
	}

	public void setImeIPrezimeSuradnje(String imeIPrezimeSuradnje) {
		this.imeIPrezimeSuradnje = imeIPrezimeSuradnje;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}