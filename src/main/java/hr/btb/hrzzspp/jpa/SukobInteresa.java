package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "sukob_interesa")
public class SukobInteresa implements Serializable{
	
	private static final long serialVersionUID = 3079405757139745662L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;
	
	
	@Column(name = "uvjet_povjer_da")
	private boolean uvjetPovjerljivostiDa;
	
	@Column(name = "uvjet_povjer_ne")
	private boolean uvjetPovjerljivostiNe;
	
	@Column(name = "sud_utvrdeni_si")
	private boolean sudjelovanjeUtvrdjeniSukob;
	
	@Column(name = "sud_moguci_si")
	private boolean sudjelovanjeMoguciSukob;
	
	@Column(name = "sud_ne_si")
	private boolean sudjelovanjeNemaSukob;
	
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod period;
	
	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	private User user;
	
	@Column(name = "vrijeme_promjene")
	private Timestamp vrijemePromjene;
	
	
	public SukobInteresa(){
		
	}
	
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	
	public boolean getUvjetPovjerljivostiDa() {
		return uvjetPovjerljivostiDa;
	}



	public void setUvjetPovjerljivostiDa(boolean uvjetPovjerljivostiDa) {
		this.uvjetPovjerljivostiDa = uvjetPovjerljivostiDa;
	}



	public boolean  getUvjetPovjerljivostiNe() {
		return uvjetPovjerljivostiNe;
	}



	public void setUvjetPovjerljivostiNe(boolean uvjetPovjerljivostiNe) {
		this.uvjetPovjerljivostiNe = uvjetPovjerljivostiNe;
	}



	public boolean  getSudjelovanjeUtvrdjeniSukob() {
		return sudjelovanjeUtvrdjeniSukob;
	}



	public void setSudjelovanjeUtvrdjeniSukob(boolean sudjelovanjeUtvrdjeniSukob) {
		this.sudjelovanjeUtvrdjeniSukob = sudjelovanjeUtvrdjeniSukob;
	}



	public boolean  getSudjelovanjeMoguciSukob() {
		return sudjelovanjeMoguciSukob;
	}



	public void setSudjelovanjeMoguciSukob(boolean sudjelovanjeMoguciSukob) {
		this.sudjelovanjeMoguciSukob = sudjelovanjeMoguciSukob;
	}



	public boolean  getSudjelovanjeNemaSukob() {
		return sudjelovanjeNemaSukob;
	}



	public void setSudjelovanjeNemaSukob(boolean sudjelovanjeNemaSukob) {
		this.sudjelovanjeNemaSukob = sudjelovanjeNemaSukob;
	}



	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}



	public Timestamp getVrijemePromjene() {
		return vrijemePromjene;
	}



	public void setVrijemePromjene(Timestamp vrijemePromjene) {
		this.vrijemePromjene = vrijemePromjene;
	}
	
	
}
