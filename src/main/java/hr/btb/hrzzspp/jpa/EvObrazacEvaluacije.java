package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * 
 * @author iris.zuza
 *
 */

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "evaluator_izvjesce")
public class EvObrazacEvaluacije implements Serializable{

	
	private static final long serialVersionUID = 2018259856846435955L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;	
	
	@Column(name = "ciljevi_a")
	private int ciljeviA;
	
	@Column(name = "ciljevi_b")
	private int ciljeviB;
	
	@Column(name = "ciljevi_c")
	private int ciljeviC;
	
	@Column(name = "ciljevi_komentari")
	private String ciljeviKomentari;
	
	@Column(name = "implementacija_a")
	private int implementacijaA;
	
	@Column(name = "implementacija_a_komentari")
	private String implementacijaAKomentar;
	
	@Column(name = "implementacija_b")
	private int implementacijaB;
	
	@Column(name = "implementacija_b_komentari")
	private String implementacijaBKomentar;
	
	@Column(name = "diseminacija")
	private int diseminacija;
	
	@Column(name = "potpora")
	private int potpora;
	
	@Column(name = "komentari")
	private String komentari;
	
	@Column(name = "zav_ocjena_a")
	private boolean zavOcjenaA;
	
	@Column(name = "zav_ocjena_b")
	private boolean zavOcjenaB;
	
	@Column(name = "zav_ocjena_c")
	private boolean zavOcjenaC;
	
	@Column(name = "zav_ocjena_d")
	private boolean zavOcjenaD;
	
	@Column(name = "zav_ocjena_komentari")
	private String zavOcjenaKomentari;
	
	@Column(name = "primjenjivo_uspjesan")
	private boolean primjenjivoUspjesan;
	
	@Column(name = "primjenjivo_zanimljiv")
	private boolean primjenjivoZanimljiv;
	
	@Column(name = "primjenjivo_znacajan")
	private boolean primjenjivoZnacajan;
	
	@Column(name = "primjenjivo_nacionalne")
	private boolean primjenjivoNacionalne;
	
	@Column(name = "primjenjivo_potencija")
	private boolean primjenjivoPotencija;
	
	@Column(name = "primjenjivo_istrazivaci")
	private boolean primjenjivoIstrazivaci;
	
	@Column(name = "primjenjivo_sektor")
	private boolean primjenjivoSektor;
	
	@Column(name = "primjenjivo_drugo")
	private boolean primjenjivoDrugo;
	
	@Column(name = "primjenjivo_komentari")
	private String primjenjivoKomentari;
	
	@Column(name = "ucinak_komentari")
	private String ucinakKomentari;
	
	@Column(name = "analiza_isplaceno")
	private String analizaIsplaceno;
	
	@Column(name = "analiza_neutroseno")
	private String analizaNeutroseno;
	
	@Column(name = "analiza_utroseno")
	private String analizaUtroseno;
	
	@Column(name = "analiza_procjena")
	private String analizaprocjena;
	
	@Column(name = "analiza_preporuke")
	private String analizaPreporuke;
	
	@Column(name = "ucinak_d1")
	private int ucinakD1;
	
	@Column(name = "ucinak_d2") 
	private int ucinakD2;
	
	@Column(name = "ucinak_d3") 
	private int ucinakD3;
	
	@Column(name = "ucinak_d4")
	private int ucinakD4;
	
	@ManyToOne()
	@JoinColumn(name = "period_id", nullable=false)
	private ReportingPeriod period;
	
	@ManyToOne()
    @JoinColumn(name = "user_id", nullable=false)
	private User user;
	

	public EvObrazacEvaluacije(){
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EvObrazacEvaluacije other = (EvObrazacEvaluacije) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public int getCiljeviA() {
		return ciljeviA;
	}

	public void setCiljeviA(int ciljeviA) {
		this.ciljeviA = ciljeviA;
	}

	public int getCiljeviB() {
		return ciljeviB;
	}

	public void setCiljeviB(int ciljeviB) {
		this.ciljeviB = ciljeviB;
	}

	public int getCiljeviC() {
		return ciljeviC;
	}

	public void setCiljeviC(int ciljeviC) {
		this.ciljeviC = ciljeviC;
	}

	public String getCiljeviKomentari() {
		return ciljeviKomentari;
	}

	public void setCiljeviKomentari(String ciljeviKomentari) {
		this.ciljeviKomentari = ciljeviKomentari;
	}

	public int getImplementacijaA() {
		return implementacijaA;
	}

	public void setImplementacijaA(int implementacijaA) {
		this.implementacijaA = implementacijaA;
	}

	public String getImplementacijaAKomentar() {
		return implementacijaAKomentar;
	}

	public void setImplementacijaAKomentar(String implementacijaAKomentar) {
		this.implementacijaAKomentar = implementacijaAKomentar;
	}

	public int getImplementacijaB() {
		return implementacijaB;
	}

	public void setImplementacijaB(int implementacijaB) {
		this.implementacijaB = implementacijaB;
	}

	public String getImplementacijaBKomentar() {
		return implementacijaBKomentar;
	}

	public void setImplementacijaBKomentar(String implementacijaBKomentar) {
		this.implementacijaBKomentar = implementacijaBKomentar;
	}

	public int getDiseminacija() {
		return diseminacija;
	}

	public void setDiseminacija(int diseminacija) {
		this.diseminacija = diseminacija;
	}

	public int getPotpora() {
		return potpora;
	}

	public void setPotpora(int potpora) {
		this.potpora = potpora;
	}

	public String getKomentari() {
		return komentari;
	}

	public void setKomentari(String komentari) {
		this.komentari = komentari;
	}

	public boolean getZavOcjenaA() {
		return zavOcjenaA;
	}

	public void setZavOcjenaA(boolean zavOcjenaA) {
		this.zavOcjenaA = zavOcjenaA;
	}

	public boolean getZavOcjenaB() {
		return zavOcjenaB;
	}

	public void setZavOcjenaB(boolean zavOcjenaB) {
		this.zavOcjenaB = zavOcjenaB;
	}

	public boolean getZavOcjenaC() {
		return zavOcjenaC;
	}

	public void setZavOcjenaC(boolean zavOcjenaC) {
		this.zavOcjenaC = zavOcjenaC;
	}

	public boolean getZavOcjenaD() {
		return zavOcjenaD;
	}

	public void setZavOcjenaD(boolean zavOcjenaD) {
		this.zavOcjenaD = zavOcjenaD;
	}

	public String getZavOcjenaKomentari() {
		return zavOcjenaKomentari;
	}

	public void setZavOcjenaKomentari(String zavOcjenaKomentari) {
		this.zavOcjenaKomentari = zavOcjenaKomentari;
	}

	public boolean getPrimjenjivoUspjesan() {
		return primjenjivoUspjesan;
	}

	public void setPrimjenjivoUspjesan(boolean primjenjivoUspjesan) {
		this.primjenjivoUspjesan = primjenjivoUspjesan;
	}

	public boolean getPrimjenjivoZanimljiv() {
		return primjenjivoZanimljiv;
	}

	public void setPrimjenjivoZanimljiv(boolean primjenjivoZanimljiv) {
		this.primjenjivoZanimljiv = primjenjivoZanimljiv;
	}

	public boolean getPrimjenjivoZnacajan() {
		return primjenjivoZnacajan;
	}

	public void setPrimjenjivoZnacajan(boolean primjenjivoZnacajan) {
		this.primjenjivoZnacajan = primjenjivoZnacajan;
	}

	public boolean getPrimjenjivoNacionalne() {
		return primjenjivoNacionalne;
	}

	public void setPrimjenjivoNacionalne(boolean primjenjivoNacionalne) {
		this.primjenjivoNacionalne = primjenjivoNacionalne;
	}

	public boolean getPrimjenjivoPotencija() {
		return primjenjivoPotencija;
	}

	public void setPrimjenjivoPotencija(boolean primjenjivoPotencija) {
		this.primjenjivoPotencija = primjenjivoPotencija;
	}

	public boolean getPrimjenjivoIstrazivaci() {
		return primjenjivoIstrazivaci;
	}

	public void setPrimjenjivoIstrazivaci(boolean primjenjivoIstrazivaci) {
		this.primjenjivoIstrazivaci = primjenjivoIstrazivaci;
	}

	public boolean getPrimjenjivoSektor() {
		return primjenjivoSektor;
	}

	public void setPrimjenjivoSektor(boolean primjenjivoSektor) {
		this.primjenjivoSektor = primjenjivoSektor;
	}

	public boolean getPrimjenjivoDrugo() {
		return primjenjivoDrugo;
	}

	public void setPrimjenjivoDrugo(boolean primjenjivoDrugo) {
		this.primjenjivoDrugo = primjenjivoDrugo;
	}

	public String getPrimjenjivoKomentari() {
		return primjenjivoKomentari;
	}

	public void setPrimjenjivoKomentari(String primjenjivoKomentari) {
		this.primjenjivoKomentari = primjenjivoKomentari;
	}

	public String getUcinakKomentari() {
		return ucinakKomentari;
	}

	public void setUcinakKomentari(String ucinakKomentari) {
		this.ucinakKomentari = ucinakKomentari;
	}

	public String getAnalizaIsplaceno() {
		return analizaIsplaceno;
	}

	public void setAnalizaIsplaceno(String analizaIsplaceno) {
		this.analizaIsplaceno = analizaIsplaceno;
	}

	public String getAnalizaNeutroseno() {
		return analizaNeutroseno;
	}

	public void setAnalizaNeutroseno(String analizaNeutroseno) {
		this.analizaNeutroseno = analizaNeutroseno;
	}

	public String getAnalizaUtroseno() {
		return analizaUtroseno;
	}

	public void setAnalizaUtroseno(String analizaUtroseno) {
		this.analizaUtroseno = analizaUtroseno;
	}

	public String getAnalizaprocjena() {
		return analizaprocjena;
	}

	public void setAnalizaprocjena(String analizaprocjena) {
		this.analizaprocjena = analizaprocjena;
	}

	public String getAnalizaPreporuke() {
		return analizaPreporuke;
	}

	public void setAnalizaPreporuke(String analizaPreporuke) {
		this.analizaPreporuke = analizaPreporuke;
	}
	
	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	public int getUcinakD1() {
		return ucinakD1;
	}

	public void setUcinakD1(int ucinakD1) {
		this.ucinakD1 = ucinakD1;
	}

	public int getUcinakD2() {
		return ucinakD2;
	}

	public void setUcinakD2(int ucinakD2) {
		this.ucinakD2 = ucinakD2;
	}

	@Override
	public String toString() {
		return String
				.format("EvDobrobit [id=%s, ciljeviA=%s, ciljeviB=%s, ciljeviC=%s, ciljeviKomentari=%s, implementacijaA=%s, implementacijaAKomentar=%s,"
						+ " implementacijaB=%s, implementacijaBKomentar=%s, diseminacija=%s, potpora=%s, komentari=%s, zavOcjenaA=%s, zavOcjenaB=%s,"
						+ " zavOcjenaC=%s, zavOcjenaD=%s, zavOcjenaKomentari=%s, =%s, primjenjivoUspjesan=%s, primjenjivoZanimljiv=%s, primjenjivoZnacajan=%s, primjenjivoNacionalne=%s,"
						+ " primjenjivoPotencija=%s, primjenjivoIstrazivaci=%s, primjenjivoSektor=%s, primjenjivoDrugo=%s, primjenjivoKomentari=%s, ucinakD1=%s, ucinakD2=%s,"
						+ " ucinakD3=%s, ucinakD4=%s, ucinakKomentari=%s, analizaIsplaceno=%s, analizaNeutroseno=%s, analizaUtroseno=%s, analizaprocjena=%s, analizaPreporuke=%s,"
						+ " projectId=%s, userId=%s]",
						id, ciljeviA, ciljeviB, ciljeviC, ciljeviKomentari,
						implementacijaA, implementacijaAKomentar, implementacijaB,
						implementacijaBKomentar, diseminacija, potpora, komentari, zavOcjenaA, zavOcjenaB, zavOcjenaC, zavOcjenaD, zavOcjenaKomentari,
						primjenjivoUspjesan, primjenjivoZanimljiv, primjenjivoZnacajan, primjenjivoNacionalne, primjenjivoPotencija, primjenjivoIstrazivaci,
						primjenjivoSektor, primjenjivoDrugo, primjenjivoKomentari, ucinakKomentari,
						analizaIsplaceno, analizaNeutroseno, analizaUtroseno, analizaprocjena, analizaPreporuke, period, user);
	}

	public int getUcinakD3() {
		return ucinakD3;
	}

	public void setUcinakD3(int ucinakD3) {
		this.ucinakD3 = ucinakD3;
	}

	public int getUcinakD4() {
		return ucinakD4;
	}

	public void setUcinakD4(int ucinakD4) {
		this.ucinakD4 = ucinakD4;
	}

	

	


	
}
