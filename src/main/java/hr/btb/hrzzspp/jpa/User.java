package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * The persistent class for the users database table.
 * 
 */
@Entity
@Table(name = "users")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(nullable = false)
	private int id;

	@Column(name = "created_at")
	private Timestamp createdAt;

	@Column(name = "is_account_expired")
	private String isAccountExpired;

	@Column(name = "is_account_locked")
	private String isAccountLocked;

	@Column(name = "is_credentials_expired")
	private String isCredentialsExpired;

	@Column(name = "is_enabled")
	private String isEnabled;

	@Column(name = "modified_at")
	private Timestamp modifiedAt;

	@OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
	private UserData userData;

	// bi-directional many-to-one association to UserRole
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<UserRole> userRoles;

	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	private List<UserRolePeriodStatus> userRolePeriodStatus;

	@OneToMany(targetEntity=EvObrazacEvaluacije.class, mappedBy = "user", cascade = CascadeType.ALL)
	private List<EvObrazacEvaluacije> userEvaluator;
	
	@OneToMany(targetEntity=SukobInteresa.class, mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<SukobInteresa> userSukobInteresa;
	
	private String password;

	private String username;

	public User() {
	}
	
	public User(int id) {
		this.id = id;
	}
	
	@Transient
	private String rolaPRK;

	@Transient
	private String rolaVOD;

	@Transient
	private String rolaEVL;

	@Transient
	private String rolaCOV;

	@Transient
	private String rolaCUO;

	@Transient
	private String rolaADM;

	public String getRolaPRK() {
		return rolaPRK;
	}

	public void setRolaPRK(String rolaPRK) {
		this.rolaPRK = rolaPRK;
	}

	public String getRolaVOD() {
		return rolaVOD;
	}

	public void setRolaVOD(String rolaVOD) {
		this.rolaVOD = rolaVOD;
	}

	public String getRolaEVL() {
		return rolaEVL;
	}

	public void setRolaEVL(String rolaEVL) {
		this.rolaEVL = rolaEVL;
	}

	public String getRolaCOV() {
		return rolaCOV;
	}

	public void setRolaCOV(String rolaCOV) {
		this.rolaCOV = rolaCOV;
	}

	public String getRolaCUO() {
		return rolaCUO;
	}

	public void setRolaCUO(String rolaCUO) {
		this.rolaCUO = rolaCUO;
	}

	public String getRolaADM() {
		return rolaADM;
	}

	public void setRolaADM(String rolaADM) {
		this.rolaADM = rolaADM;
	}
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreatedAt() {
		return this.createdAt;
	}

	public void setCreatedAt(Timestamp createdAt) {
		this.createdAt = createdAt;
	}

	public String getIsAccountExpired() {
		return this.isAccountExpired;
	}

	public void setIsAccountExpired(String isAccountExpired) {
		this.isAccountExpired = isAccountExpired;
	}

	public String getIsAccountLocked() {
		return this.isAccountLocked;
	}

	public void setIsAccountLocked(String isAccountLocked) {
		this.isAccountLocked = isAccountLocked;
	}

	public String getIsCredentialsExpired() {
		return this.isCredentialsExpired;
	}

	public void setIsCredentialsExpired(String isCredentialsExpired) {
		this.isCredentialsExpired = isCredentialsExpired;
	}

	public String getIsEnabled() {
		return this.isEnabled;
	}

	public void setIsEnabled(String isEnabled) {
		this.isEnabled = isEnabled;
	}

	public Timestamp getModifiedAt() {
		return this.modifiedAt;
	}

	public void setModifiedAt(Timestamp modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public List<UserRole> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public List<UserRolePeriodStatus> getUserRolePeriodStatus() {
		return userRolePeriodStatus;
	}

	public void setUserRolePeriodStatus(List<UserRolePeriodStatus> userRolePeriodStatus) {
		this.userRolePeriodStatus = userRolePeriodStatus;
	}

	public List<EvObrazacEvaluacije> getUserEvaluator() {
		return userEvaluator;
	}

	public void setUserEvaluator(List<EvObrazacEvaluacije> userEvaluator) {
		this.userEvaluator = userEvaluator;
	}

	public List<SukobInteresa> getUserSukobInteresa() {
		return userSukobInteresa;
	}

	public void setUserSukobInteresa(List<SukobInteresa> userSukobInteresa) {
		this.userSukobInteresa = userSukobInteresa;
	}
	
	

}