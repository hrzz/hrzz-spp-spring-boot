/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author dalibor.harmina
 *
 */
@Entity
@Table(name = "status_razdoblja")
public class StatusRazdoblja implements Serializable {

	private static final long serialVersionUID = -1626198008289407226L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;
	
	@Column(name = "naziv", nullable = false, length = 45)
	private String naziv;
	
	@Column(name = "role", nullable = false, length = 3)
	private String role;

    @ManyToOne(optional = false)
    @JoinColumn(name = "status_ikona_id", nullable=false, insertable=false, updatable=false)
	private StatusIkona statusIkona;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "statusRazdoblja", fetch = FetchType.LAZY)
	private List<UserRolePeriodStatus> userRolePeriodStatus;
	
	public StatusRazdoblja() {
	}
	
	public StatusRazdoblja(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public StatusIkona getStatusIkona() {
		return statusIkona;
	}

	public void setStatusIkona(StatusIkona statusIkona) {
		this.statusIkona = statusIkona;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}
}
