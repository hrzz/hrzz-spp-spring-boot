/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author dalibor.harmina
 *
 */
@Entity
@Table(name="status_evidencija")
@NamedQuery(name="StatusEvidencija.findAll", query="SELECT e FROM StatusEvidencija e")
public class StatusEvidencija implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(unique=true, nullable=false)
	private int id;

	@Column(name="naziv", nullable=false, length=45)
	private String naziv;
	
	@Column(name="ikona", nullable=false, length=45)
	private String ikona;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public StatusEvidencija(){
	}
	
	public StatusEvidencija(int id) {
		this.id = id;
	}
	
	public StatusEvidencija(int id, String naziv) {
		this.id = id;
		this.naziv = naziv;
	}

	public String getIkona() {
		return ikona;
	}

	public void setIkona(String ikona) {
		this.ikona = ikona;
	}
	
}
