package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.SifarnikEnum;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value = SifarnikEnum.Values.REZULTAT_POSTIGNUTO)
public class PostignutoRezultatSifarnik extends Sifarnik implements
		SifarnikInterface {

	private static final long serialVersionUID = 3539819930292634452L;

	private String code;

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "PostignutoRezultatSifarnik [code=" + code + ", getId()="
				+ getId() + ", getDescription()=" + getDescription()
				+ ", getSeqNo()=" + getSeqNo() + ", getRef()=" + getRef() + "]";
	}
}
