package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ev_popis_dis_aktivnosti database table.
 * 
 */
@Entity
@Table(name = "ev_popis_dis_aktivnosti")
public class EvPopisDisAktivnosti implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(nullable = false)
	private int broj;

	@Column(name = "broj_sudionika")
	private int brojSudionika;

	@Temporal(TemporalType.DATE)
	@Column(name = "datum_do")
	private Date datumDo;

	@Temporal(TemporalType.DATE)
	@Column(name = "datum_od")
	private Date datumOd;

	@Column(name = "ime_i_prezime", nullable = false, length = 25)
	private String imeIPrezime;

	@Column(length = 45)
	private String mjesto;

	@Column(nullable = false, length = 255)
	private String naziv;

	@Column(name = "obuhvacene_drzave", length = 100)
	private String obuhvaceneDrzave;

	@Column(nullable = false, length = 45)
	private String uloga;

	@Column(nullable = false, length = 45)
	private String vrsta;

	@Column(name = "vrsta_publike", length = 45)
	private String vrstaPublike;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public int getStatusPrk() {
		return statusPrk;
	}
	
	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public EvPopisDisAktivnosti() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBroj() {
		return this.broj;
	}

	public void setBroj(int broj) {
		this.broj = broj;
	}

	public int getBrojSudionika() {
		return this.brojSudionika;
	}

	public void setBrojSudionika(int brojSudionika) {
		this.brojSudionika = brojSudionika;
	}

	public Date getDatumDo() {
		return this.datumDo;
	}

	public void setDatumDo(Date datumDo) {
		this.datumDo = datumDo;
	}

	public Date getDatumOd() {
		return this.datumOd;
	}

	public void setDatumOd(Date datumOd) {
		this.datumOd = datumOd;
	}

	public String getImeIPrezime() {
		return this.imeIPrezime;
	}

	public void setImeIPrezime(String imeIPrezime) {
		this.imeIPrezime = imeIPrezime;
	}

	public String getMjesto() {
		return this.mjesto;
	}

	public void setMjesto(String mjesto) {
		this.mjesto = mjesto;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getObuhvaceneDrzave() {
		return this.obuhvaceneDrzave;
	}

	public void setObuhvaceneDrzave(String obuhvaceneDrzave) {
		this.obuhvaceneDrzave = obuhvaceneDrzave;
	}

	public String getUloga() {
		return this.uloga;
	}

	public void setUloga(String uloga) {
		this.uloga = uloga;
	}

	public String getVrsta() {
		return this.vrsta;
	}

	public void setVrsta(String vrsta) {
		this.vrsta = vrsta;
	}

	public String getVrstaPublike() {
		return this.vrstaPublike;
	}

	public void setVrstaPublike(String vrstaPublike) {
		this.vrstaPublike = vrstaPublike;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	@Override
	public String toString() {
		return String.format("EvPopisDisAktivnosti[id=%d, naziv=%s]", this.id, this.naziv);
	}
	
}