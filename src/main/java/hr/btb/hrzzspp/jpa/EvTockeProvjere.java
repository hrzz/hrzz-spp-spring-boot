package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * The persistent class for the ev_tocke_provjere database table.
 * 
 */
@Entity
@Table(name = "ev_tocke_provjere")
public class EvTockeProvjere implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "br_cilja", nullable = false, length = 15)
	private String brCilja;

	@Column(name = "br_tocke_provjere", nullable = false, length = 15)
	private String brTockeProvjere;

	@Column(name = "datum_realizacije", length = 7)
	private String datumRealizacije;

	@Column(length = 100)
	private String komentari;

	@Column(nullable = false, length = 255)
	private String naziv;

	@Column(nullable = false, length = 1)
	private String postignuto;
	
	@Column(name = "zaduzena_osoba", nullable = false, length = 100)
	private String zaduzenaOsoba;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;

	public int getStatusPrk() {
		return statusPrk;
	}

	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	

	public EvTockeProvjere() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getBrCilja() {
		return this.brCilja;
	}

	public void setBrCilja(String brCilja) {
		this.brCilja = brCilja;
	}

	public String getBrTockeProvjere() {
		return this.brTockeProvjere;
	}

	public void setBrTockeProvjere(String brTockeProvjere) {
		this.brTockeProvjere = brTockeProvjere;
	}

	public String getDatumRealizacije() {
		return this.datumRealizacije;
	}

	public void setDatumRealizacije(String datumRealizacije) {
		this.datumRealizacije = datumRealizacije;
	}

	public String getKomentari() {
		return this.komentari;
	}

	public void setKomentari(String komentari) {
		this.komentari = komentari;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getPostignuto() {
		return this.postignuto;
	}

	public void setPostignuto(String postignuto) {
		this.postignuto = postignuto;
	}

	public String getZaduzenaOsoba() {
		return this.zaduzenaOsoba;
	}

	public void setZaduzenaOsoba(String zaduzenaOsoba) {
		this.zaduzenaOsoba = zaduzenaOsoba;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	@Override
	public String toString() {
		return String.format("EvTockeProvjere[id=%d, naziv=%s]", this.id, this.naziv);
	}
}