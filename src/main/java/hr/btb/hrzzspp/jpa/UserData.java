package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
*
* @author misel
*/
@Entity
@Table(name = "users_data")
@XmlRootElement
public class UserData implements Serializable {
   private static final long serialVersionUID = 1L;
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Basic(optional = false)
   @Column(nullable = false)
   private Integer id;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 255)
   @Column(nullable = false, length = 255)
   private String name;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 255)
   @Column(nullable = false, length = 255)
   private String surname;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 50)
   @Column(nullable = false, length = 50)
   private String title;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   @Column(nullable = false, length = 100)
   private String degree;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 50)
   @Column(name = "phone_number", nullable = false, length = 50)
   private String phoneNumber;
   @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 255)
   @Column(nullable = false, length = 255)
   private String email;
   @Basic(optional = false)
   @NotNull
   @Column(name = "created_at", nullable = false)
   @Temporal(TemporalType.TIMESTAMP)
   private Date createdAt;
   @Column(name = "modified_at")
   @Temporal(TemporalType.TIMESTAMP)
   private Date modifiedAt;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 11)
   @Column(nullable = false, length = 11)
   private String oib;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 1)
   @Column(nullable = false, length = 1)
   private String sex;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   @Column(name = "date_of_birth", nullable = false, length = 100)
   private String dateOfBirth;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   @Column(name = "telephone_number", nullable = false, length = 100)
   private String telephoneNumber;
   @Size(max = 100)
   @Column(name = "address_of_residence", length = 100)
   private String addressOfResidence;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 10)
   @Column(name = "zip_code", nullable = false, length = 10)
   private String zipCode;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   @Column(nullable = false, length = 100)
   private String city;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 100)
   @Column(nullable = false, length = 100)
   private String nationality;
   @Size(max = 200)
   @Column(name = "personal_web", length = 200)
   private String personalWeb;
   @Basic(optional = false)
   @NotNull
   @Size(min = 1, max = 255)
   @Column(name = "institution_tmp", nullable = false, length = 255)
   private String institutionTmp;
   @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false)
   @OneToOne(optional = false)
   private User user;
   @JoinColumn(name = "institution_id", referencedColumnName = "id", nullable = false)
   @ManyToOne(optional = true)
   private Institution institution;

   public UserData() {
   }

   public UserData(Integer id) {
       this.id = id;
   }

   public UserData(Integer id, String name, String surname, String title, String degree, String phoneNumber, String email, Date createdAt, String oib, String sex, String dateOfBirth, String telephoneNumber, String zipCode, String city, String nationality, String institutionTmp) {
       this.id = id;
       this.name = name;
       this.surname = surname;
       this.title = title;
       this.degree = degree;
       this.phoneNumber = phoneNumber;
       this.email = email;
       this.createdAt = createdAt;
       this.oib = oib;
       this.sex = sex;
       this.dateOfBirth = dateOfBirth;
       this.telephoneNumber = telephoneNumber;
       this.zipCode = zipCode;
       this.city = city;
       this.nationality = nationality;
       this.institutionTmp = institutionTmp;
   }

   public Integer getId() {
       return id;
   }

   public void setId(Integer id) {
       this.id = id;
   }

   public String getName() {
       return name;
   }

   public void setName(String name) {
       this.name = name;
   }

   public String getSurname() {
       return surname;
   }

   public void setSurname(String surname) {
       this.surname = surname;
   }

   public String getTitle() {
       return title;
   }

   public void setTitle(String title) {
       this.title = title;
   }

   public String getDegree() {
       return degree;
   }

   public void setDegree(String degree) {
       this.degree = degree;
   }

   public String getPhoneNumber() {
       return phoneNumber;
   }

   public void setPhoneNumber(String phoneNumber) {
       this.phoneNumber = phoneNumber;
   }

   public String getEmail() {
       return email;
   }

   public void setEmail(String email) {
       this.email = email;
   }

   public Date getCreatedAt() {
       return createdAt;
   }

   public void setCreatedAt(Date createdAt) {
       this.createdAt = createdAt;
   }

   public Date getModifiedAt() {
       return modifiedAt;
   }

   public void setModifiedAt(Date modifiedAt) {
       this.modifiedAt = modifiedAt;
   }

   public String getOib() {
       return oib;
   }

   public void setOib(String oib) {
       this.oib = oib;
   }

   public String getSex() {
       return sex;
   }

   public void setSex(String sex) {
       this.sex = sex;
   }

   public String getDateOfBirth() {
       return dateOfBirth;
   }

   public void setDateOfBirth(String dateOfBirth) {
       this.dateOfBirth = dateOfBirth;
   }

   public String getTelephoneNumber() {
       return telephoneNumber;
   }

   public void setTelephoneNumber(String telephoneNumber) {
       this.telephoneNumber = telephoneNumber;
   }

   public String getAddressOfResidence() {
       return addressOfResidence;
   }

   public void setAddressOfResidence(String addressOfResidence) {
       this.addressOfResidence = addressOfResidence;
   }

   public String getZipCode() {
       return zipCode;
   }

   public void setZipCode(String zipCode) {
       this.zipCode = zipCode;
   }

   public String getCity() {
       return city;
   }

   public void setCity(String city) {
       this.city = city;
   }

   public String getNationality() {
       return nationality;
   }

   public void setNationality(String nationality) {
       this.nationality = nationality;
   }

   public String getPersonalWeb() {
       return personalWeb;
   }

   public void setPersonalWeb(String personalWeb) {
       this.personalWeb = personalWeb;
   }

   public String getInstitutionTmp() {
       return institutionTmp;
   }

   public void setInstitutionTmp(String institutionTmp) {
       this.institutionTmp = institutionTmp;
   }

   public User getUser() {
       return user;
   }

   public void setUser(User user) {
       this.user = user;
   }

   public Institution getInstitution() {
       return institution;
   }

   public void setInstitutionId(Institution institution) {
       this.institution = institution;
   }

   @Override
   public int hashCode() {
       int hash = 0;
       hash += (id != null ? id.hashCode() : 0);
       return hash;
   }

   @Override
   public boolean equals(Object object) {
       // TODO: Warning - this method won't work in the case the id fields are not set
       if (!(object instanceof UserData)) {
           return false;
       }
       UserData other = (UserData) object;
       if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
           return false;
       }
       return true;
   }

   @Override
   public String toString() {
       return "test.UsersData[ id=" + id + " ]";
   }
   
}
