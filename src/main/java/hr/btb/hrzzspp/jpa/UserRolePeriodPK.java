package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * The primary key class for the users_roles_project database table.
 * 
 */
@Embeddable
public class UserRolePeriodPK implements Serializable {
	// default serial version id, required for serializable classes.
	private static final long serialVersionUID = 1L;

	@Column(name = "role_id", insertable = false, updatable = false, unique = true, nullable = false)
	private int roleId;

	@Column(name = "user_id", insertable = false, updatable = false, unique = true, nullable = false)
	private int userId;

	@Column(name = "period_id", insertable = false, updatable = false, unique = true, nullable = false)
	private int periodId;

	public UserRolePeriodPK() {
	}

	public int getRoleId() {
		return this.roleId;
	}

	public void setRoleId(int roleId) {
		this.roleId = roleId;
	}

	public int getUserId() {
		return this.userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getPeriodId() {
		return periodId;
	}

	public void setPeriodId(int periodId) {
		this.periodId = periodId;
	}

	public boolean equals(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof UserRolePeriodPK)) {
			return false;
		}
		UserRolePeriodPK castOther = (UserRolePeriodPK) other;
		return (this.roleId == castOther.roleId)
				&& (this.userId == castOther.userId)
				&& (this.periodId == castOther.periodId);
	}

	public int hashCode() {
		final int prime = 31;
		int hash = 17;
		hash = hash * prime + this.roleId;
		hash = hash * prime + this.userId;
		hash = hash * prime + this.periodId;

		return hash;
	}
}