package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The persistent class for the ev_ni_files database table. For Files that are
 * of ni type = OSTALO5.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.NI_OSTALO5)
public class FileOstalo5 extends EvNiFile implements Serializable {
	private static final long serialVersionUID = 1L;
}