/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author dalibor.harmina
 *
 */
@Entity
@Table(name = "status_ikona")
public class StatusIkona implements Serializable {

	private static final long serialVersionUID = -2341709225957740877L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;
	
	@Column(name = "naziv", nullable = false, length = 45)
	private String naziv;
	
	@Column(name = "ikona", nullable = false, length = 45)
	private String ikona;
	
	public StatusIkona() {
	}
	
	public StatusIkona(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getIkona() {
		return ikona;
	}

	public void setIkona(String ikona) {
		this.ikona = ikona;
	}
}
