package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**vrijeme_promjene
 * The persistent class for the users_roles_periods_status database table.
 * 
 */
@Entity
@Table(name = "users_roles_periods_status")
public class UserRolePeriodStatus implements Serializable {

	private static final long serialVersionUID = 1L;

	@EmbeddedId
	private UserRolePeriodPK id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "period_id", nullable=false, insertable=false, updatable=false)
	private ReportingPeriod period;

	@ManyToOne(optional = false)
	@JoinColumn(name = "role_id", nullable=false, insertable=false, updatable=false)
	private Role role;

    @ManyToOne(optional = false)
    @JoinColumn(name = "user_id", nullable=false, insertable=false, updatable=false)
	private User user;

    @ManyToOne(optional = false)
    @JoinColumn(name = "status_id", nullable=false)
	private StatusRazdoblja statusRazdoblja;
    
	@Column(name = "vrijeme_promjene", nullable = false)
	private Timestamp vrijemePromjene;

	public UserRolePeriodStatus() {
	}

	public UserRolePeriodStatus(ReportingPeriod period, Role role, User user, StatusRazdoblja statusRazdoblja) {
		this.period = period;
		this.role = role;
		this.user = user;
		this.statusRazdoblja = statusRazdoblja;
	}


	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserRolePeriodPK getId() {
		return id;
	}

	public void setId(UserRolePeriodPK id) {
		this.id = id;
	}

	public StatusRazdoblja getStatusRazdoblja() {
		return statusRazdoblja;
	}

	public void setStatusRazdoblja(StatusRazdoblja statusRazdoblja) {
		this.statusRazdoblja = statusRazdoblja;
	}

	public Timestamp getVrijemePromjene() {
		return vrijemePromjene;
	}

	public void setVrijemePromjene(Timestamp vrijemePromjene) {
		this.vrijemePromjene = vrijemePromjene;
	}

}