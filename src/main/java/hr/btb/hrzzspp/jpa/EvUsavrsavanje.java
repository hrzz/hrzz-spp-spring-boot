package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the ev_usavrsavanje database table.
 * 
 */
@Entity
@Table(name = "ev_usavrsavanje")
public class EvUsavrsavanje implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(nullable = false)
	private int br;

	@Column(length = 100)
	private String drzava;

	@Column(name = "ime_prezime", nullable = false, length = 25)
	private String imePrezime;

	@Column(nullable = false, length = 255)
	private String naziv;

	@Temporal(TemporalType.DATE)
	@Column(name = "razdoblje_do", nullable = false)
	private Date razdobljeDo;

	@Temporal(TemporalType.DATE)
	@Column(name = "razdoblje_od", nullable = false)
	private Date razdobljeOd;

	@Column(length = 255)
	private String ustanova;

	@Column(name = "usvojena_znanja", nullable = false, length = 100)
	private String usvojenaZnanja;

	@Column(name = "vrsta_aktivnosti", nullable = false, length = 45)
	private String vrstaAktivnosti;

	// bi-directional many-to-one association to ReportingPeriod
	@ManyToOne
	@JoinColumn(name = "period_id", nullable = false)
	private ReportingPeriod reportingPeriod;
	
	@Column(name = "komentar_prk",  length = 500)
	private String komentarPrk;
	
	@Column(name = "status_prk")
	private int statusPrk;
	

	public int getStatusPrk() {
		return statusPrk;
	}

	public void setStatusPrk(int statusPrk) {
		this.statusPrk = statusPrk;
	}

	public String getKomentarPrk() {
		return komentarPrk;
	}

	public void setKomentarPrk(String komentarPrk) {
		this.komentarPrk = komentarPrk;
	}

	public EvUsavrsavanje() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBr() {
		return this.br;
	}

	public void setBr(int br) {
		this.br = br;
	}

	public String getDrzava() {
		return this.drzava;
	}

	public void setDrzava(String drzava) {
		this.drzava = drzava;
	}

	public String getImePrezime() {
		return this.imePrezime;
	}

	public void setImePrezime(String imePrezime) {
		this.imePrezime = imePrezime;
	}

	public String getNaziv() {
		return this.naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public Date getRazdobljeDo() {
		return this.razdobljeDo;
	}

	public void setRazdobljeDo(Date razdobljeDo) {
		this.razdobljeDo = razdobljeDo;
	}

	public Date getRazdobljeOd() {
		return this.razdobljeOd;
	}

	public void setRazdobljeOd(Date razdobljeOd) {
		this.razdobljeOd = razdobljeOd;
	}

	public String getUstanova() {
		return this.ustanova;
	}

	public void setUstanova(String ustanova) {
		this.ustanova = ustanova;
	}

	public String getUsvojenaZnanja() {
		return this.usvojenaZnanja;
	}

	public void setUsvojenaZnanja(String usvojenaZnanja) {
		this.usvojenaZnanja = usvojenaZnanja;
	}

	public String getVrstaAktivnosti() {
		return this.vrstaAktivnosti;
	}

	public void setVrstaAktivnosti(String vrstaAktivnosti) {
		this.vrstaAktivnosti = vrstaAktivnosti;
	}

	public ReportingPeriod getReportingPeriod() {
		return this.reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

}