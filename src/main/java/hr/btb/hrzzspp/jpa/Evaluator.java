package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Root Entity class for the evaluator database table.
 * 
 */
@Entity
@Table(name = "evaluator")
public class Evaluator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(name = "ime", nullable = false, length = 45)
	private String ime;

	@Column(name = "prezime", nullable = false, length = 45)
	private String prezime;

	@Column(name = "email", nullable = false, length = 45)
	private String email;

	@Column(name = "password", nullable = false, length = 45)
	private String password;

	@ManyToOne(cascade = CascadeType.MERGE)
	private ReportingPeriod period;

	@OneToOne(optional = false)
	@JoinColumn(name = "status_id", referencedColumnName = "id", nullable = false)
	private EvaluatorStatus status;
	
	
	
	public Evaluator(){}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public EvaluatorStatus getStatus() {
		return status;
	}

	public void setStatus(EvaluatorStatus status) {
		this.status = status;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}