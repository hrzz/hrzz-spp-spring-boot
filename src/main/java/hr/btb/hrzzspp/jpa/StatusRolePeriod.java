/**
 * 
 */
package hr.btb.hrzzspp.jpa;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @author dalibor.harmina
 *
 */
@Entity
@Table(name = "status_role_period")
public class StatusRolePeriod implements Serializable {

	private static final long serialVersionUID = 2538811611321893569L;

	@EmbeddedId
	private RolePeriodPK id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "status_id", nullable=false)
	private StatusRazdoblja statusRazdoblja;

	@ManyToOne(optional = false)
	@JoinColumn(name = "role_id", nullable=false, insertable=false, updatable=false)
	private Role role;
    
	@ManyToOne(optional = false)
	@JoinColumn(name = "period_id", nullable=false, insertable=false, updatable=false)
	private ReportingPeriod period;
    
	@Column(name = "vrijeme_promjene", nullable = false)
	private Timestamp vrijemePromjene;

	public StatusRolePeriod() {
	}

	public RolePeriodPK getId() {
		return id;
	}

	public void setId(RolePeriodPK id) {
		this.id = id;
	}

	public StatusRazdoblja getStatusRazdoblja() {
		return statusRazdoblja;
	}

	public void setStatusRazdoblja(StatusRazdoblja statusRazdoblja) {
		this.statusRazdoblja = statusRazdoblja;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public Timestamp getVrijemePromjene() {
		return vrijemePromjene;
	}

	public void setVrijemePromjene(Timestamp vrijemePromjene) {
		this.vrijemePromjene = vrijemePromjene;
	}
}
