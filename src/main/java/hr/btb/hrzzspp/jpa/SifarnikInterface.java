package hr.btb.hrzzspp.jpa;

public interface SifarnikInterface {

	int getId();
	void setId(int id);

	String getDescription();
	void setDescription(String description);

	int getSeqNo();
	void setSeqNo(int seqNo);

	String getRef();
	void setRef(String ref);

	String getCode();
	void setCode(String code);

}