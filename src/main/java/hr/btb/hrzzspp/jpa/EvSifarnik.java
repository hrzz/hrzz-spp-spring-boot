package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * The persistent class for the ev_sifarnik database table.
 * 
 */
@Entity
@Table(name = "ev_sifarnik", uniqueConstraints = @UniqueConstraint(columnNames = {
		"ref_table", "ref_column", "code" }))
@DiscriminatorColumn(name="ref_table", discriminatorType=DiscriminatorType.STRING, length=45)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class EvSifarnik implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(nullable = false, length = 45)
	private String code;

	@Column(length = 100)
	private String description;

	@Column(name = "ref_column", nullable = false, length = 45)
	private String refColumn;

	@Column(name = "ref_table", nullable = false, length = 45)
	private String refTable;

	@Column(name = "seq_no")
	private int seqNo;

	
	public EvSifarnik() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return this.code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getRefColumn() {
		return this.refColumn;
	}

	public void setRefColumn(String refColumn) {
		this.refColumn = refColumn;
	}

	public String getRefTable() {
		return this.refTable;
	}

	public void setRefTable(String refTable) {
		this.refTable = refTable;
	}

	public int getSeqNo() {
		return this.seqNo;
	}

	public void setSeqNo(int seqNo) {
		this.seqNo = seqNo;
	}
}