package hr.btb.hrzzspp.jpa;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Root Entity class for the evaluator_status database table.
 * 
 */
@Entity
@Table(name = "evaluator_status")
public class EvaluatorStatus implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;
	
	@Column(name = "naziv", nullable = false, length = 45, insertable = false, updatable = false)
	private String naziv;
	
	@Column(name = "ikona", nullable = false, length = 45)
	private String ikona;
	
	public EvaluatorStatus() {
	}
	
	public EvaluatorStatus(int id) {
		this.id = id;
	}
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public String getIkona() {
		return ikona;
	}

	public void setIkona(String ikona) {
		this.ikona = ikona;
	}

}