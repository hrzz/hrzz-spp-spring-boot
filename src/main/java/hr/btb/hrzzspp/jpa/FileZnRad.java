package hr.btb.hrzzspp.jpa;

import hr.btb.hrzzspp.business.ConstantsEvidencije;

import java.io.Serializable;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * The persistent class for the ev_files database table. For Files that
 * reference ev_popis_zn_radova table.
 */
@Entity
@DiscriminatorValue(value = ConstantsEvidencije.TBL_EV_POPIS_ZN_RADOVA)
public class FileZnRad extends EvFile implements Serializable {
	private static final long serialVersionUID = 2802148675940288926L;

	@OneToOne
	@JoinColumn(name = "ref_id", referencedColumnName = "id")
	private EvPopisZnRadova znRad;

	public EvPopisZnRadova getZnRad() {
		return znRad;
	}

	public void setZnRad(EvPopisZnRadova znRad) {
		this.znRad = znRad;
	}

	public FileZnRad() {
	}

	@Override
	public String toString() {
		return String
				.format("FileRad [znRad=%s, id=%s, fileName=%s, fileSize=%s, fileType=%s]",
						znRad, id, fileName, fileSize, fileType);
	}
}