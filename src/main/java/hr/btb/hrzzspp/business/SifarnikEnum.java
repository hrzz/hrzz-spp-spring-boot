package hr.btb.hrzzspp.business;

public enum SifarnikEnum {

	REZULTAT_DISEMINACIJA(Values.REZULTAT_DISEMINACIJA),
	REZULTAT_POSTIGNUTO(Values.REZULTAT_POSTIGNUTO);

	private SifarnikEnum(String val) {
		// force equality between name of enum instance, and value of constant
		if (!this.name().equals(val))
			throw new IllegalArgumentException("Incorrect use of ELanguage");
	}

	public static class Values {
		public static final String REZULTAT_DISEMINACIJA = "ev_rezultati.oznaka_diseminacije";
		public static final String REZULTAT_POSTIGNUTO = "ev_rezultati.postignuto";
	}
}