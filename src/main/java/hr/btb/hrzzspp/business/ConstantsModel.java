package hr.btb.hrzzspp.business;

public class ConstantsModel {
	public static final String[] titleArea = new String[15];
	public static final String[] nazivXhtmlArea = new String[15];
	public static final String[] opisArea = new String[15];
	public static final String[] listaUtjecaja = new String[11];

    
	static{
	    titleArea[0] = "Tablica 1";
	    nazivXhtmlArea[0] = "rezultati";
	    opisArea[0] = "Rezultati (OBVEZNA TABLICA)";
	    
	    titleArea[1] = "Tablica 2";
	    nazivXhtmlArea[1] = "tockeProvjere";
	    opisArea[1] = "Točke provjere (OBVEZNA TABLICA)";
	    
	    titleArea[2] = "Tablica 3";
	    nazivXhtmlArea[2] = "osoblje";
	    opisArea[2] = "Osoblje (OBVEZNA TABLICA)";
	    
	    titleArea[3] = "Tablica 4";
	    nazivXhtmlArea[3] = "istrazivackaSkupina";
	    opisArea[3] = "Istraživačka grupa (OBVEZNA TABLICA)";
	    
	    titleArea[4] = "Tablica 5";
	    nazivXhtmlArea[4] = "usavrsavanje";
	    opisArea[4] = "Usavršavanje (NIJE OBVEZNA)";
	    
	    titleArea[5] = "Tablica 6";
	    nazivXhtmlArea[5] = "brDocDisertacija";//BROJ DOKTORSKIH DISERTACIJA PROIZIŠLIH S PROJEKTA 
	    opisArea[5] = "Broj doktorskih disertacija proizašlih s projekta (NIJE OBVEZNA)";
	    
	    titleArea[6] = "Tablica 7";
	    nazivXhtmlArea[6] = "popisDokDisRadova";//POPIS DOKTORSKIH DISERTACIJA I DILPOMSKIH/MAGIRSTARSKIH RADOVA 
	    opisArea[6] = "Popis doktorskih disertacija i diplomskih/magistarskih radova (NIJE OBVEZNA)";
	    
	    titleArea[7] = "Tablica 8";
	    nazivXhtmlArea[7] = "popisZnanstvenihRadova";//POPIS ZNANSTVENIH RADOVA (S DOMAĆOM ILI MEĐUNARODNOM RECENZIJOM) PROIZAŠLIH IZ AKTIVNOSTI NA PROJEKTU
	    opisArea[7] = "Popis znanstvenih radova (s domaćom ili međunarodnom recenzijom) proizašlih iz aktivnosti na projektu";
	    
	    titleArea[8] = "Tablica 9";
	    nazivXhtmlArea[8] = "ostalePublikacije";//POPIS OSTALIH OBJAVLJENIH PUBLIKACIJA PROIZAŠLIH IZ AKTIVNOSTI NA PROJEKTU
	    opisArea[8] = "Popis ostalih objavljenih publikacija proizašlih iz aktivnosti na projektu (NIJE OBVEZNA)";
	    
	    titleArea[9] = "Tablica 10";
	    nazivXhtmlArea[9] = "diseminacijskeAktivnosti";//POPIS DISEMINACIJSKIH AKTIVNOSTI
	    opisArea[9] = "Popis diseminacijskih aktivnosti";
	    
	    titleArea[10] = "Tablica 11";
	    nazivXhtmlArea[10] = "patentZigDizajn";//POPIS SVIH PATENTNIH PRIJAVA, ŽIGOVA, INDUSTRIJSKIH DIZAJNA I DR. 
	    opisArea[10] = "Popis svih patentnih prijava, žigova, industrijskih dizajna i dr. (NIJE OBVEZNA)";
	    
	    titleArea[11] = "Tablica 12";
	    nazivXhtmlArea[11] = "novoostvareneSuradnje";
	    opisArea[11] = "Novoostvarene suradnje (NIJE OBVEZNA)";
	    
	    titleArea[12] = "Tablica 13";
	    nazivXhtmlArea[12] = "inozemniFondovi";//PRIJAVE NA INOZEMNE FONDOVE 
	    opisArea[12] = "Prijave na inozemne fondove (NIJE OBVEZNA)";
	    
	    titleArea[13] = "Tablica 14";
	    nazivXhtmlArea[13] = "ostalo";
	    opisArea[13] = "Ostalo (NIJE OBVEZNA)";
	    
	    titleArea[14] = "Tablica 15";
	    nazivXhtmlArea[14] = "utjecajProjekta";//Ima li Vaš projekt mogućnost utjecaja na društvenu i ekonomsku dobrobit? Označiti sve što je primjenjivo (mogućnost višestrukog izbora)
	    opisArea[14] = "Utjecaj projekta";
/*
	    titleArea[15] = "Narativno izvješće";
	    nazivXhtmlArea[15] = "narativno_izvjesce";
	    opisArea[15] = "Narativno izvješće";
*/	    
	    listaUtjecaja[0] = "NoviProgrami";
	    listaUtjecaja[1] = "Ostalo";
	    listaUtjecaja[2] = "PoboljsanjeUvjeta";
	    listaUtjecaja[3] = "PojavaNovog";
	    listaUtjecaja[4] = "PosjecenostUstanova";
	    listaUtjecaja[5] = "PromjenaNavika";
	    listaUtjecaja[6] = "RastZaposlenosti";
	    listaUtjecaja[7] = "RazvojNovih";
	    listaUtjecaja[8] = "TemaUMedijima";
	    listaUtjecaja[9] = "UtjecajNaPolitiku";
	    listaUtjecaja[10] = "UtjecajNaZdrastvo";
	}
	
	public static int PRK = 1;
	public static int VOD = 2;
	public static int EVL = 3;
	public static int COV = 4;
	public static int CUO = 5;
	public static int ADM = 6;
}
