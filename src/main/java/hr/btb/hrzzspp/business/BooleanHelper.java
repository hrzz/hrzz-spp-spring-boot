package hr.btb.hrzzspp.business;

public class BooleanHelper {

	public static String getDBBoolean(boolean on) {
		if (on)
			return "1";
		else
			return "0";
	}

	public static boolean getViewBoolean(String on) {
		switch (on) {
		case "1":
			return true;
		case "0":
			return false;
		default:
			return false;
		}
	}
}
