package hr.btb.hrzzspp.business;

public class ConstantsEvidencije {

	public static final String TBL_EV_FILES = "ev_files";
	public static final String TBL_EV_NI_FILES = "ev_ni_files";
	
	public static final String TBL_EV_POPIS_PUBLIKACIJA = "ev_popis_publikacija";
	public static final String FILE_PUBLIKACIJA = "FilePublikacija";
	public static final String EV_PUBLIKACIJA_PRISTUP = "otvoreni_pristup";	
	public static final String EV_PUBLIKACIJA_VRSTA = "vrsta";	

	public static final String TBL_EV_REZULTATI = "ev_rezultati";
	public static final String FILE_REZULTAT = "FileRezultat";
	public static final String EV_REZULTATI_POSTIGNUTO = "postignuto";
	public static final String EV_REZULTATI_DISEMINACIJA = "oznaka_diseminacije";

	public static final String TBL_EV_POPIS_RADOVA = "ev_popis_radova";
	public static final String FILE_RADOVI = "FileRad";

	public static final String TBL_EV_POPIS_ZN_RADOVA = "ev_popis_zn_radova";
	public static final String FILE_ZN_RADOVI = "FileZnRad";
	
	public static final String DISCRIMINATOR_EV_FILES = "ref_table";
	public static final String DISCRIMINATOR_EV_NI_FILES = "ev_ni_type";
	
	public static final String NI_O_IZVJESCE = "O_IZVJESCE";
	public static final String NI_F_IZVJESCE = "F_IZVJESCE";
	public static final String NI_R_OPREME = "R_OPREME";
	public static final String NI_RACUNI = "RACUNI";
	public static final String NI_K_KONTA = "K_KONTA";
	public static final String NI_OSTALO1 = "OSTALO1";
	public static final String NI_OSTALO2 = "OSTALO2";
	public static final String NI_OSTALO3 = "OSTALO3";
	public static final String NI_OSTALO4 = "OSTALO4";
	public static final String NI_OSTALO5 = "OSTALO5";
	public static final String NI_P_F_PLANA = "P_F_PLANA";
	public static final String NI_P_R_PLANA = "P_R_PLANA";
}
