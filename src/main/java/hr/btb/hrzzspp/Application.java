package hr.btb.hrzzspp;

import hr.btb.hrzzspp.spring.scope.ViewScope;

import javax.faces.webapp.FacesServlet;
import javax.servlet.DispatcherType;

import org.primefaces.webapp.filter.FileUploadFilter;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EntityScan(basePackages = { "hr.btb.hrzzspp.jpa" })
@EnableTransactionManagement
//@EnableScheduling /*For @component classes (in our case this would be 'ScheduledTasks.java')*/
public class Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(
			SpringApplicationBuilder application) {
		return application.sources(Application.class);
	}

	@Bean
	public ServletRegistrationBean facesServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(
				new FacesServlet(), new String[] { "*.jsf", "*.xhtml" });
		registration.setName("Faces Servlet");
		registration.setLoadOnStartup(1);
		return registration;
	}

	@Bean
	public FilterRegistrationBean facesUploadFilterRegistration() {
		FilterRegistrationBean registrationBean = new FilterRegistrationBean(
				new FileUploadFilter(), facesServletRegistration());
		registrationBean.setName("PrimeFaces FileUpload Filter");
		registrationBean.addUrlPatterns("/*");
		registrationBean.setDispatcherTypes(DispatcherType.FORWARD,
				DispatcherType.REQUEST);
		return registrationBean;
	}
	
	@Bean
	public CustomScopeConfigurer customScopeConfigurer() {
		CustomScopeConfigurer csc = new CustomScopeConfigurer();
		csc.addScope("view", new ViewScope());		
		return csc;
	}
}
