package hr.btb.hrzzspp.jsfconfig;

import java.util.Iterator;

import javax.faces.FacesException;
import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerWrapper;
import javax.faces.event.ExceptionQueuedEvent;
import javax.faces.event.ExceptionQueuedEventContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class BtbExceptionHandler extends ExceptionHandlerWrapper {

	  private static Log log = LogFactory.getLog(BtbExceptionHandler.class);
	  private ExceptionHandler wrapped;
	 
	  public BtbExceptionHandler(ExceptionHandler wrapped) {
	    this.wrapped = wrapped;
	  }
	
	@Override
	public ExceptionHandler getWrapped() {
		// TODO Auto-generated method stub
		return wrapped;
	}

	  @Override
	  public void handle() throws FacesException {
	    //Iterate over all unhandeled exceptions
	    Iterator<ExceptionQueuedEvent> i = getUnhandledExceptionQueuedEvents().iterator();
	    while (i.hasNext()) {
	      ExceptionQueuedEvent event = i.next();
	      ExceptionQueuedEventContext context =
	        (ExceptionQueuedEventContext)event.getSource();
	 
	      //obtain throwable object
	      Throwable t = context.getException();
	 
	      //here you do what ever you want with exception
	      try{
	      //log error
	    	  
	        log.error("Serious error happened!", t);
	        
	        //redirect to error view etc....  
	      }finally{
	        //after exception is handeled, remove it from queue
	        i.remove();
	      }
	    }
	    //let the parent handle the rest
	    getWrapped().handle();
	  }
	
}
