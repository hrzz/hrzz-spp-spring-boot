package hr.btb.hrzzspp.jsfconfig;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

public class BtbExceptionHandlerFactory extends ExceptionHandlerFactory {

	  private ExceptionHandlerFactory parent;
	  
	  // this injection handles jsf
	  public BtbExceptionHandlerFactory(ExceptionHandlerFactory parent) {
	    this.parent = parent;
	  }
	 
	  //create your own ExceptionHandler
	  @Override
	  public ExceptionHandler getExceptionHandler() {
	    ExceptionHandler result =
	        new BtbExceptionHandler(parent.getExceptionHandler());
	    return result;
	  }

}
