/**
 * 
 */
package hr.btb.hrzzspp.jsf.validators;

import hr.btb.hrzzspp.spring.view.SessionBean;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */
@Component
@Scope("request")
@FacesValidator("datePeriodRangeValidator")
public class DatePeriodRangeValidator implements Validator {
	
	@Autowired
	private SessionBean session;
	
	/* (non-Javadoc)
	 * @see javax.faces.validator.Validator#validate(javax.faces.context.FacesContext, javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		 if (value == null) {
	           return;
	       }
	        
	       Date checkDate = (Date)value; 
	       
	       Interval interval = new Interval(new DateTime(session.getCurrentReportingPeriod().getDateFrom().getTime()), 
	    		   new DateTime(session.getCurrentReportingPeriod().getDateTo().getTime()));
	       if (!interval.contains(new DateTime(checkDate))) {
	    	   throw new ValidatorException(new FacesMessage(
	    			    FacesMessage.SEVERITY_ERROR, "Datum realizacije nije unutar izvještajnog razdoblja!", null));
	       }

	}

	public SessionBean getSession() {
		return session;
	}
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}
}
