package hr.btb.hrzzspp.jsf.validators;

import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
@FacesValidator("dateInMonthRangeValidator")
public class DateInMonthRangeValidator implements Validator {

	private final static Logger LOG = LoggerFactory
			.getLogger(DateInMonthRangeValidator.class);

	@Autowired
	private SessionBean session;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
	 * , javax.faces.component.UIComponent, java.lang.Object)
	 */
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		
		LOG.debug("... value in validator: {} ...", value.toString());
		
		if (value == null || "".equals((String) value)) {
			return;
		}

		if (!isDateValid((String) value)) {

			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR,
					"Datum realizacije nije ispravan!", null));

		}

		if (!DateUtils.isMMGGGGInPeriod(session.getCurrentReportingPeriod(),
				(String) value)) {
			throw new ValidatorException(
					new FacesMessage(
							FacesMessage.SEVERITY_ERROR,
							"Datum realizacije nije unutar izvještajnog razdoblja!",
							null));
		}

	}

	private boolean isDateValid(String rezultat) {

		if (rezultat != null) {

			int indexofForwardSlash = rezultat.indexOf('/');
			String substringOfDatumRealizacije = rezultat.substring(0,
					indexofForwardSlash);
			int numberOfMonth = Integer.parseInt(substringOfDatumRealizacije);

			if (numberOfMonth <= 12 && numberOfMonth > 0) {
				return true;
			}

			return false;
		}
		return false;
	}

}
