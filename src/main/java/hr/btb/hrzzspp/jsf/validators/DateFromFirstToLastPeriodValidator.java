/**
 * 
 */
package hr.btb.hrzzspp.jsf.validators;

import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.SessionBean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import org.hibernate.metamodel.domain.Superclass;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */

@Component
@Scope("request")
@FacesValidator("dateFromFirstToLastPeriodValidator")
public class DateFromFirstToLastPeriodValidator extends PrimeDateRangeValidator {
	
	@Autowired
	private ReportingPeriodsService service;
	@Autowired
	private SessionBean session;
	
	 @Override
	 public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
	       if (value == null) {
	           return;
	       }
	       
	       super.validate(context, component, value);
	       
	       //Leave the null handling of startDate to required="true"       	       
	       if (super.getStartDate() == null) {
	           return;
	       }

	       DateTime startDate = new DateTime(super.getStartDate());       
	       DateTime endDate = new DateTime(super.getEndDate()); 
	       
	       DateTime firstPeriodDateFromDate = new DateTime(service.findMinReportingPeriodDateFrom(session.getCurrentProject()));
	       
	       if (startDate.isBefore(firstPeriodDateFromDate)) {
	    	   throw new ValidatorException(new FacesMessage(
	    			    FacesMessage.SEVERITY_ERROR, "Datum realizacije nije unutar izvještajnog razdoblja!", null));
	       }
	       
	       if (endDate.isAfter(startDate.plusYears(4))) {
	    	   throw new ValidatorException(new FacesMessage(
	    			    FacesMessage.SEVERITY_ERROR, "Datum realizacije nije unutar izvještajnog razdoblja!", null));
	       }
	       
	}

}
