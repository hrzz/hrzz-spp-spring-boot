package hr.btb.hrzzspp.jsf.validators;

import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.SessionBean;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.ValidatorException;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


/**
 * @author iris.zuza
 *
 */
@Component
@Scope("request")
@FacesValidator("yearFromToCurrentPeriodValidator")
public class YearFromToCurrentPeriodValidator extends PrimeDateRangeValidator{

	@Autowired
	private ReportingPeriodsService service;
	
	@Autowired
	private SessionBean session;
	
	
	
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		// TODO Auto-generated method stub
		if(value == null){
			return;
		}
		super.validate(context, component, value);
		
		
		int startYearCurrentPeriod = (new DateTime(session.getCurrentReportingPeriod().getDateFrom())).getYear();
		int endYearCurrentPeriod = (new DateTime(session.getCurrentReportingPeriod().getDateTo()).getYear());
		
		if((int) value < startYearCurrentPeriod || (int) value>endYearCurrentPeriod){
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Unos godine nije unutar trajanja izvještajnog razdoblja!", null));
		}
		
		
		
	}
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
