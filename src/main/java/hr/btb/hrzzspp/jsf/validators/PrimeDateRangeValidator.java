package hr.btb.hrzzspp.jsf.validators;

import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.primefaces.component.calendar.Calendar;

@FacesValidator("primeDateRangeValidator")
public class PrimeDateRangeValidator implements Validator {

	private Date startDate = null;
	private Date endDate = null;

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
	
		
		if (value == null) {
			return;
		}

		// Leave the null handling of startDate to required="true"
		Object startDateValue = component.getAttributes().get("startDate");

		if (startDateValue == null) {
			return;
		}

		Date startDate = null;
		Date endDate = null;

		if (startDateValue instanceof org.primefaces.component.calendar.Calendar)
			startDate = (Date) ((Calendar) startDateValue).getValue();
		else if (startDateValue instanceof java.sql.Date)
			startDate = new java.util.Date(
					((java.sql.Date) startDateValue).getTime());
		else
			startDate = (Date) startDateValue;

		if (value instanceof java.sql.Date)
			value = new Date(((java.sql.Date) value).getTime());
		else
			endDate = (Date) value;

		this.startDate = startDate;
		this.endDate = endDate;

		if (endDate.before(startDate) ) {
			throw new ValidatorException(new FacesMessage(
					FacesMessage.SEVERITY_ERROR, "Datum realizacije nije unutar izvještajnog razdoblja!", null));
		}

	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
