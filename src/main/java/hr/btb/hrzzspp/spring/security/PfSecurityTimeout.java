package hr.btb.hrzzspp.spring.security;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

@ManagedBean
public class PfSecurityTimeout {

	@ManagedProperty(value="#{securityTimeout}")
	SecurityTimeout securityTimeout;

	public SecurityTimeout getSecurityTimeout() {
		return securityTimeout;
	}

	public void setSecurityTimeout(SecurityTimeout securityTimeout) {
		this.securityTimeout = securityTimeout;
	}
	
	public boolean isSessionExpired(){
		
		return securityTimeout.isSessionExpired();
	}
	
}
