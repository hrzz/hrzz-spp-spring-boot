package hr.btb.hrzzspp.spring.security;

import javax.faces.context.FacesContext;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class ForceLogout {

	public void logout(){
		
		if (SecurityChecker.isAuthenticated()){
			FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			SecurityContextHolder.getContext().setAuthentication(null);
		}
	}
	
	public void onload() {
	   
		logout();
		
	}
	
}
