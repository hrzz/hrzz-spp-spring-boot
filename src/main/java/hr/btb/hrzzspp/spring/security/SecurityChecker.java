package hr.btb.hrzzspp.spring.security;

import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityChecker {

	final static Logger LOG = LoggerFactory.getLogger(SecurityChecker.class);

	public static Collection<? extends GrantedAuthority> getAllRoles() {
		return SecurityContextHolder.getContext().getAuthentication()
				.getAuthorities();
	}

	public static boolean hasRoles(String[] roles) {

		boolean result = false;

		for (GrantedAuthority authority : getAllRoles()) {
			String userRole = authority.getAuthority();

			for (String role : roles) {
				
				role = "ROLE_"+ role;
				
				if (role.equals(userRole)) {
					result = true;
					break;
				}

			}

			if (result) {
				break;
			}
		}

		return result;
	}

	public static boolean isAuthenticated() {

		Authentication authentication = SecurityContextHolder.getContext()
				.getAuthentication();
		return authentication != null
				&& !(authentication instanceof AnonymousAuthenticationToken)
				&& authentication.isAuthenticated();
	}
}