package hr.btb.hrzzspp.spring.security;

import javax.annotation.PostConstruct;

import hr.btb.hrzzspp.spring.view.SessionBean;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class PreventAction {

	private boolean disabled;
	private boolean periodActive;
	private boolean rendered;
	private boolean evlRola;
	private boolean prkRola;
	private boolean vodRola;
	private boolean disablePosaljiIzvjesca;
	private boolean disableZatvoriRazdoblje;
	private boolean renderedSpremiPDFOpIzvj;
	
	@Autowired
	private SessionBean session;
	
	@PostConstruct
	public void init(){
		
	}

	public boolean isDisabled() {
		return disabled;
	}

	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	public boolean isPeriodActive() {
		//return periodActive;
		if (session.getCurrentReportingPeriod().getStatus().equals("1"))
			return true;
		else
			return false;
	}

	public void setPeriodActive(boolean periodActive) {
		this.periodActive = periodActive;
	}

	public boolean isRendered() {
		//return rendered;
		
		if (isVodRola())
			return true;
		else 
			return false;

	}
	
	public boolean isRendered(String element) {
		
		boolean rendered = false;
		
		if (isPrkRola()) {
			
			if (element.equals("OPCIJE"))
				rendered = true;
			
			else if (element.equals("EDIT"))
				rendered = true;
			
			else if (element.equals("DELETE"))
				rendered = true;
			
			else if (element.equals("PRK"))
				rendered = true;
			
			else if (element.equals("OPISNO_IZVJESCE_FOOTER"))
				rendered = true;

			else if (element.equals("UPLOAD_FILE_FOOTER"))
				rendered = true;
		
		} else if (isVodRola()) {
			
			if (element.equals("OPCIJE"))
				rendered = true;
			
			else if (element.equals("EDIT"))
				rendered = true;
			
			else if (element.equals("DELETE"))
				rendered = true;
			
			else if (element.equals("PRK"))
				rendered = false;
			
			else if (element.equals("OPISNO_IZVJESCE_FOOTER"))
				rendered = true;

			else if (element.equals("UPLOAD_FILE_FOOTER"))
				rendered = true;
			
		} else if (isEvlRola()) {
			
			if (element.equals("OPCIJE"))
				rendered = false;
			
			else if (element.equals("EDIT"))
				rendered = false;
			
			else if (element.equals("DELETE"))
				rendered = false;
			
			else if (element.equals("PRK"))
				rendered = true;
			
			else if (element.equals("OPISNO_IZVJESCE_FOOTER"))
				rendered = false;

			else if (element.equals("UPLOAD_FILE_FOOTER"))
				rendered = false;
		} 
		
		return rendered;
	}
	
	public boolean isDisabled(String element) {
		
		boolean disabled = true;
		
		if (isPrkRola()) {
			
			 if (element.equals("EDIT"))
				disabled = true;
			
			else if (element.equals("DELETE"))
				disabled = true;
			
			else if (element.equals("PRK"))
				disabled = false;
		
		} else if (isVodRola()) {
			
			 if (element.equals("EDIT"))
				disabled = false || !isPeriodActive();
			
			else if (element.equals("DELETE"))
				disabled = false || !isPeriodActive();
			
			else if (element.equals("PRK"))
				disabled = true;
			
		} else if (isEvlRola()) {
			
			if (element.equals("EDIT"))
				disabled = true;
			
			else if (element.equals("DELETE"))
				disabled = true;
			
			else if (element.equals("PRK"))
				disabled = true;
		} 
		
		return disabled;
	}
	
	public void setRendered(boolean rendered) {
		this.rendered = rendered;
	}

	public boolean isEvlRola() {
		if (session.hasRoles(new String [] {"EVL"}))
			return true;
		else 
			return false;
	}

	public void setEvlRola(boolean evlRola) {
		this.evlRola = evlRola;
	}

	public boolean isPrkRola() {
		if (session.hasRoles(new String [] {"PRK"}))
			return true;
		else 
			return false;
	}

	public void setPrkRola(boolean prkRola) {
		this.prkRola = prkRola;
	}

	public boolean isVodRola() {
		if (session.hasRoles(new String [] {"VOD"}))
			return true;
		else 
			return false;
	}

	public void setVodRola(boolean vodRola) {
		this.vodRola = vodRola;
	}

	public boolean isDisablePosaljiIzvjesca() {
		if(isEvlRola()){
			return true;
		}
		if("0".equals(session.getCurrentReportingPeriod().getStatus())){
			return false;
		}else{
			return true;
		}
	}

	public void setDisablePosaljiIzvjesca(boolean disablePosaljiIzvjesca) {
		this.disablePosaljiIzvjesca = disablePosaljiIzvjesca;
	}

	public boolean isDisableZatvoriRazdoblje() {
		if(isEvlRola()){
			return true;
		}
		if("1".equals(session.getCurrentReportingPeriod().getStatus())){
			return false;
		}else{
			return true;
		}
	}

	public void setDisableZatvoriRazdoblje(boolean disableZatvoriRazdoblje) {
		this.disableZatvoriRazdoblje = disableZatvoriRazdoblje;
	}

	public boolean isRenderedSpremiPDFOpIzvj() {
		if(isEvlRola()){
			return false;
		}
		
		if("0".equals(session.getCurrentReportingPeriod().getStatus())){
			return true;
		}else{
			return false;
		}
	}

	public void setRenderedSpremiPDFOpIzvj(boolean renderedSpremiPDFOpIzvj) {
		this.renderedSpremiPDFOpIzvj = renderedSpremiPDFOpIzvj;
	}

}
