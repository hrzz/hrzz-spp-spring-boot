package hr.btb.hrzzspp.spring.security;

import org.springframework.stereotype.Component;

@Component
public class SecurityTimeout {

	public boolean isSessionExpired(){
				
		return SecurityChecker.isAuthenticated();
		
	}
	
}
