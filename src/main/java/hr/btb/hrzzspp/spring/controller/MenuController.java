package hr.btb.hrzzspp.spring.controller;

import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.SukobInteresa;
import hr.btb.hrzzspp.spring.config.AppCustomSettings;
import hr.btb.hrzzspp.spring.security.SecurityChecker;
import hr.btb.hrzzspp.spring.services.MajorScientificFieldsService;
import hr.btb.hrzzspp.spring.services.MenuService;
import hr.btb.hrzzspp.spring.services.ProjectsService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.SukobInteresaService;
import hr.btb.hrzzspp.spring.services.UsersService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.TreeNodeUtil;
import hr.btb.hrzzspp.spring.view.evidencije.EvOstaloView;
import hr.btb.hrzzspp.spring.view.evidencije.ReportingPeriodTree;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.el.MethodExpression;
import javax.faces.component.UIComponent;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.MethodExpressionActionListener;

import org.primefaces.component.breadcrumb.BreadCrumb;
import org.primefaces.component.menubar.Menubar;
import org.primefaces.component.menuitem.UIMenuItem;
import org.primefaces.component.submenu.UISubmenu;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.extensions.util.ComponentUtils;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class MenuController implements Serializable {

	private static final long serialVersionUID = -1258136275137302754L;
	private static final Logger LOG = LoggerFactory
			.getLogger(MenuController.class);

	private MenuService menuService;
	private ProjectsService projectsService;
	private MajorScientificFieldsService msfService;
	private Map<String, String> pageDescription;
	List<ReportingPeriodTree> rpTreeList = new ArrayList<ReportingPeriodTree>();

	private static final String USERS = "KORISNICI";
	private static final String PROJECTS = "PROJEKTI";
	private static final String MANAGEMENT = "UPRAVLJANJE";

	private BreadCrumb breadcrumb;
	private BreadCrumb bTmp;
	private Menubar indexMenu;

	private boolean firstTimeRendered = true;

	@Autowired
	private UsersService users;
	
	@Autowired
	private TreeNodeUtil treeNodeUtil;

	@Autowired
	private SessionBean sessionBean;
	
	@Autowired
	private ReportingPeriodsService reportingPeriodsService;

	@Autowired
	private SukobInteresaService sukobInteresaService;
	
	@Autowired
	private AppCustomSettings appCustomSettings;

	@Autowired
	private ReportingPeriodsService reportingPeriods;
	
	@Autowired
	EvOstaloView evOstaloView;
	
	private String getPageBreadCrumbClientId() {
		return ComponentUtils.findComponentClientId("bread_crumb");
	}

	private String getPageMenuClientId() {
		return ComponentUtils.findComponentClientId("class_menu");
	}

	public BreadCrumb getPageBreadCrumb() {

		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		UIComponent menu = viewRoot.findComponent(getPageBreadCrumbClientId());

		bTmp = (BreadCrumb) menu;

		return (BreadCrumb) menu;
	}

	public Menubar getPageMenu() {

		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		UIComponent menu = viewRoot.findComponent(getPageMenuClientId());

		return (Menubar) menu;
	}

	@PostConstruct
	public void init() {

		Authentication auth = SecurityContextHolder.getContext()
				.getAuthentication();
		String name = auth.getName(); // get logged in username
		sessionBean.setCurrentUserData(users.getUserDataByUser(users.getUsers(
				name).get(0)));

	}
	
	public void goToPopisEvidencija() {
		
		Project projekt = sessionBean.getCurrentProject();
		
		if (projekt != null) {
			
			List<ReportingPeriodTree> rpTreeList = new ArrayList<ReportingPeriodTree>();
			
			List<ReportingPeriod> reportingPeriodList = reportingPeriodsService.getReportingPeriods(projekt);
			
			for(ReportingPeriod rp : reportingPeriodList) {
				
				ReportingPeriodTree rpt = new ReportingPeriodTree();
				
				TreeNode tree = treeNodeUtil.postaviPeriodIzvjescivanja(rp);
				rpt.setPeriod(rp);
				rpt.setTree(tree);
				
				rpTreeList.add(rpt);
			}
			
			setRpTreeList(rpTreeList);
			
			menuAction("popisEvidencija", "1");
		}
		else {
			
			PfUtils.error("Došlo je do greške!");
		}
	}

	private String pageId = "home";
	private String sMenuId = null;

	public MenuController() {
		LOG.debug("... MenuController no-args constructor ...");
	}

	public String getsMenuId() {
		return sMenuId;
	}

	public void setsMenuId(String sMenuId) {
		this.sMenuId = sMenuId;
	}

	private String getStringCommand(String pageId, String sMenuId) {
		return String.format(
				"#{menuController.callMenuAndSetScientificField('%s','%s')}",
				pageId, sMenuId);
	}

	private String getStringCommandKorisnici(String pageId, String sMenuId) {
		return String.format("#{menuController.menuAction('%s','%s')}", pageId,
				sMenuId);
	}

	@SuppressWarnings("rawtypes")
	private MethodExpression getMethodExpression(String methodEL, Class[] cls) {

		FacesContext context = FacesContext.getCurrentInstance();
		MethodExpression methodExpression = context
				.getApplication()
				.getExpressionFactory()
				.createMethodExpression(context.getELContext(), methodEL, null,
						cls);

		return methodExpression;
	}

	private void menuBuilder() {

		indexMenu.getChildren().clear();

		pageDescription = new HashMap<>();

		pageDescription.put("users", "Korisnici");
		pageDescription.put("home", "Home");
		pageDescription.put("ostalo", "Ostalo");
		pageDescription.put("utjecajProjekta", "Utjecaj projekta");
		pageDescription.put("popisEvidencija", "Pregled razdoblja");
		pageDescription.put("popisEvidencija2", "Pregled razdoblja");
		pageDescription.put("osoblje", "Osoblje");
		pageDescription.put("projects", "Pregled projekata");
		pageDescription.put("usavrsavanje", "Usavršavanje");
		pageDescription.put("rezultati", "Rezultati");
		pageDescription.put("popisZnanstvenihRadova",
				"Popis znanstvenih radova");
		pageDescription.put("patentZigDizajn", "Patent žig dizajn");
		pageDescription.put("ostalePublikacije", "Ostale publikacije");
		pageDescription.put("diseminacijskeAktivnosti",
				"Diseminacijske aktivnosti");
		pageDescription.put("popisDokDisRadova", "Popis doktorskih radova");
		pageDescription.put("novoostvareneSuradnje", "Novoostvarene suradnje");
		pageDescription.put("inozemniFondovi", "Inozemni fondovi");
		pageDescription.put("brDocDisertacija", "Broj doktorskih disertacija");
		pageDescription.put("istrazivackaSkupina", "Istraživačka grupa");
		pageDescription.put("tockeProvjere", "Točke provjere");
		pageDescription.put("narativno_izvjesce", "Opisno izvješće");
		pageDescription.put("uploadIzvjesca", "Upload izvješća");
		pageDescription.put("obrazacEvaluacije", "Obrazac evaluacije");
		pageDescription.put("pregledKorisnika", "Profil korisnika");
		pageDescription.put("evidencijeVOD", "Evidencije");
		pageDescription.put("sukobInteresa", "Izjava o povjerljivosti");

		UISubmenu subMenuKorisnici = new UISubmenu();
		subMenuKorisnici.setLabel(USERS);
		// subMenuKorisnici.setId(USERS.toLowerCase());

		if (!SecurityChecker.hasRoles(new String[] { "PRK" })) {
			subMenuKorisnici.setStyleClass("submenu-visible");
			subMenuKorisnici.setRendered(false);
		}

		UISubmenu subMenuProjekti = new UISubmenu();
		subMenuProjekti.setLabel(PROJECTS);
		// subMenuProjekti.setId(PROJECTS.toLowerCase());

		if (!SecurityChecker.hasRoles(new String[] { "PRK" }))
			subMenuProjekti.setStyleClass("submenu-visible");

		// izbačeno, ne vidi nitko
		// UISubmenu subMenuUpravljanje = new UISubmenu();
		// subMenuUpravljanje.setLabel("MANAGEMENT");
		// // subMenuUpravljanje.setId(MANAGEMENT.toLowerCase());
		//
		// if (SecurityChecker.hasRoles(new String[] { "EVL" })
		// || SecurityChecker.hasRoles(new String[] { "VOD" })) {
		// subMenuUpravljanje.setStyleClass("submenu-visible");
		// subMenuKorisnici.setRendered(false);
		// }

		List<MajorScientificField> msfList = this.menuService
				.getAllMajorScientificFields();

		for (MajorScientificField msf : msfList) {

			UIMenuItem item = new UIMenuItem();
			item.setValue(msf.getName());

			item.setActionExpression(getMethodExpression(
					getStringCommand("projects", String.valueOf(msf.getId())),
					new Class[] { String.class, String.class }));
			item.addActionListener(new MethodExpressionActionListener(
					getMethodExpression("#{menuController.menuAction}",
							new Class[] { ActionEvent.class })));

			if (msf.getCode().equals("ALL"))
				item.setContainerStyleClass("menu-all-projects");

			subMenuProjekti.getChildren().add(item);
		}

		// Korisnici
		UIMenuItem itemKor = new UIMenuItem();
		itemKor.setValue("Pregled korisnika");
		itemKor.setActionExpression(getMethodExpression(
				getStringCommandKorisnici("users", String.valueOf(1)),
				new Class[] { String.class, String.class }));
		itemKor.addActionListener(new MethodExpressionActionListener(
				getMethodExpression("#{menuController.menuAction}",
						new Class[] { ActionEvent.class })));
		subMenuKorisnici.getChildren().add(itemKor);

		indexMenu.getChildren().add(subMenuKorisnici);
		indexMenu.getChildren().add(subMenuProjekti);
		// indexMenu.getChildren().add(subMenuUpravljanje);

		UIMenuItem logout = new UIMenuItem();
		logout.setValue("Odjava ");
		logout.setIcon("ui-icon-extlink");
		logout.setContainerStyleClass("logout-button");
		logout.setUrl("j_spring_security_logout");

		indexMenu.getChildren().add(logout);

		UIMenuItem description = new UIMenuItem();
		description.setValue(sessionBean.getNameAndSurname());
		description.setDisabled(true);
		description.setContainerStyleClass("current-user");

		indexMenu.getChildren().add(description);

		UIMenuItem version = new UIMenuItem();
		version.setValue(appCustomSettings.getVersion());
		version.setDisabled(true);
		version.setContainerStyleClass("app-version");

		indexMenu.getChildren().add(version);

		RequestContext.getCurrentInstance().update(indexMenu.getClientId());
	}

	public void pageDispatcher(String pageId, String sMenuId) {
		menuAction(pageId, sMenuId);
	}

	private void clearBreadCrumb() {

		LOG.debug("?????????????" + getPageBreadCrumb());

		getPageBreadCrumb().getElements().clear();
		UIMenuItem item = new UIMenuItem();
		item.setValue("Home");
		item.setActionExpression(getMethodExpression(
				getStringCommand("home", "HOME"), new Class[] { String.class,
						String.class }));
		item.addActionListener(new MethodExpressionActionListener(
				getMethodExpression("#{menuController.menuAction}",
						new Class[] { ActionEvent.class })));
		getPageBreadCrumb().getChildren().add(item);

	}

	private void refreshBreadCrumb() {
		RequestContext.getCurrentInstance().update(
				getPageBreadCrumb().getClientId());
	}

	public void menuAction(ActionEvent ev) {
		// reload breadcrumb
		UIMenuItem item = (UIMenuItem) ev.getComponent();
		clearBreadCrumb();
		fillBreadcrumb(item);
		refreshBreadCrumb();
	}

	private void fillBreadcrumb(UIComponent ui) {

		if (ui.getParent() != null) {
			if (!(ui instanceof Menubar)) {

				fillBreadcrumb(ui.getParent());

				if (ui instanceof UIMenuItem || ui instanceof UISubmenu) {

					UIMenuItem item = new UIMenuItem();

					if (ui instanceof UIMenuItem) {
						UIMenuItem tmpItem = (UIMenuItem) ui;
						item.setValue(tmpItem.getValue());
						item.setStyleClass("simple-breadcrumb-item");
						getPageBreadCrumb().getChildren().add(item);
					} else {
						UISubmenu tmpItem = (UISubmenu) ui;
						item.setValue(tmpItem.getLabel());
						item.setStyleClass("simple-breadcrumb-item");
						getPageBreadCrumb().getChildren().add(item);
					}
				}
			}
		}
	}

	public void onload() {

		loadFirstTime();

		indexMenu = (Menubar) getPageMenu();
		menuBuilder();

		UIViewRoot viewRoot = FacesContext.getCurrentInstance().getViewRoot();
		UIComponent menu = viewRoot.findComponent(getPageBreadCrumbClientId());

		BreadCrumb bc = (BreadCrumb) menu;

		menu.getChildren().clear();

		if (bTmp != null)
			for (UIComponent comp : bTmp.getChildren()) {

				UIMenuItem item = new UIMenuItem();

				UIMenuItem tmp = (UIMenuItem) comp;
				item.setValue(tmp.getValue());

				item.setActionExpression(tmp.getActionExpression());

				if (tmp.getActionListeners().length != 0)
					item.addActionListener(new MethodExpressionActionListener(
							getMethodExpression(
									"#{menuController.breadcrumbAction}",
									new Class[] { ActionEvent.class })));

				bc.getChildren().add(item);
			}

		RequestContext.getCurrentInstance().update(menu.getClientId());
		/*
		 * loadFirstTime();
		 * 
		 * if (indexMenu.getChildCount() == 0) { // ako je okinut F5 :(
		 * 
		 * menuBuilder(); clearBreadCrumb(); refreshBreadCrumb(); }
		 */
	}

	public void loadFirstTime() {

		if (firstTimeRendered) {

			if (!SecurityChecker.hasRoles(new String[] { "PRK" }))
				RequestContext.getCurrentInstance().execute(
						"$( '.menu-all-projects' ).children()[0].onclick()");

			firstTimeRendered = false;
		}
	}

	public void callMenuAndSetScientificField(String pageId, String sMenuId) {

		this.sessionBean.setFilteredProjectByScientificField(Integer
				.valueOf(sMenuId));
		menuAction(pageId, sMenuId);

	}

	public void menuAction(String pageId, String sMenuId) {

		// add2 breadcrumb

		goTo(pageId, sMenuId);

		UIMenuItem item = new UIMenuItem();
		String pID = pageId.toLowerCase();
		String desc = null;

		desc = pageDescription.get(pageId);

		if (desc == null)
			item.setValue(pID.substring(0, 1).toUpperCase() + pID.substring(1));
		else
			item.setValue(desc);

		item.setActionExpression(getMethodExpression(String.format(
				"#{menuController.breadcrumbAction('%s','%s')}", pageId, "1"),
				new Class[] { String.class, String.class }));
		item.addActionListener(new MethodExpressionActionListener(
				getMethodExpression("#{menuController.breadcrumbAction}",
						new Class[] { ActionEvent.class })));

		getPageBreadCrumb().getChildren().add(item);
		refreshBreadCrumb();

	}

	public void breadcrumbAction(String pageId, String sMenuId) {
		goTo(pageId, sMenuId);
	}

	public void breadcrumbAction(ActionEvent ev) {
		removeBreadcrumbItem((UIMenuItem) ev.getComponent());
	}

	private void removeRestoredBreadcrumbItem(UIMenuItem bItem) {

		System.out.println("Item is:" + bItem.getIcon());

		for (UIComponent c : getPageBreadCrumb().getChildren())
			System.out.println("???????" + c.getId());

	}

	private void removeBreadcrumbItem(UIMenuItem bItem) {

		List<UIComponent> elements = getPageBreadCrumb().getChildren();
		List<Integer> tmp = new ArrayList<>();

		int itemIndex = -1;
		int i = 0;

		for (UIComponent item : elements) {
			// if (bItem.getId().equals(elements.get(i).getId()))
			if (bItem.getId().equals(item.getId()))
				itemIndex = i;

			if (itemIndex >= 0 && i > itemIndex) {

				tmp.add(i);
			}

			i += 1;
		}

		Comparator<Integer> comparator = Collections.reverseOrder();
		Collections.sort(tmp, comparator);

		for (Integer index : tmp) {
			elements.remove(index.intValue());
		}

		refreshBreadCrumb();
	}

	private void goTo(String pageId, String sMenuId) {

		LOG.debug("Set page:" + pageId + "->" + sMenuId);
		this.pageId = pageId;
		this.sMenuId = sMenuId;

		if (pageId.equals("projects")) {

			sessionBean.setCurrentProject(null);

			this.sessionBean.setProjects(projectsService
					.getMsfFilteredProjects(sessionBean));

			this.sessionBean.setMajorScientificField(msfService
					.getMSFById(Integer.valueOf(sMenuId)));
		}

		if (pageId.equals("ostalo")) {

			// evOstaloView.refresh();

		}

		refreshContent();
	}

	private void refreshContent() {

		RequestContext context = RequestContext.getCurrentInstance();
		FacesContext ctx = FacesContext.getCurrentInstance();

		try {
			context.update(ctx.getViewRoot().findComponent(":pnlContent")
					.getClientId());
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	public void redirectFromProject(SelectEvent event) {

		List<ReportingPeriod> rpevl = reportingPeriods
				.getReportingPeriods(sessionBean.getCurrentProject());

		if (rpevl.size() > 0) {

			for (int i = 0; i < rpevl.size(); i++) {

				ReportingPeriod tempRP = rpevl.get(i);

				boolean isAktivanStatus = false;

				String status = tempRP.getStatus();

				if (status != null) {

					isAktivanStatus = status.equals("1") || status.equals("2")
							|| status.equals("0");
				}

				if (isAktivanStatus) {
					sessionBean.setCurrentReportingPeriod(tempRP);
					break;
				}
			}
		}
		if (sessionBean.getCurrentReportingPeriod() == null) {
			sessionBean.setCurrentReportingPeriod(rpevl.get(0));
		}

		if (SecurityChecker.hasRoles((new String[] { "EVL" }))
				|| SecurityChecker.hasRoles((new String[] { "COV" }))
				|| SecurityChecker.hasRoles((new String[] { "CUO" }))) {
			/*
			 * otvaranje izjave o povjerljivosti ako se prvi put ulazi kao EVL, CUO
			 * ILI COV
			 */
			List<SukobInteresa> izjaveSukobList = sukobInteresaService
					.getObrazacSukobInteresaByUserAndPeriod(sessionBean
							.getCurrentUserData().getUser(), sessionBean
							.getCurrentReportingPeriod());

			if (izjaveSukobList.size() > 0) {

				goToPopisEvidencija();

			} else {
				menuAction("sukobInteresa", "1");
			}

		} else if (SecurityChecker.hasRoles(new String[] { "VOD" })) {

			menuAction("evidencijeVOD", "1");

		} else if (SecurityChecker.hasRoles(new String[] { "PRK" })){

			goToPopisEvidencija();
			
		} else {

			PfUtils.info("Nemate određeno razdoblje za odabrani projekt! Molimo kontaktirajte projektnog koordinatora za dodatne informacije!");
			
		}

	}

	public void redirect(SelectEvent event) {
		/*
		 * LOG.debug("... SelectEvent object: {}",
		 * event.getObject().getClass().getName()); Project selectedProject =
		 * (Project) event.getObject(); if (selectedProject == null)
		 * LOG.debug("... selectedProject is NULL ...");
		 * LOG.debug("... redirectam za projekt: {} ...", selectedProject);
		 */
		/*
		 * LOG.debug("... SelectEvent object: {}",
		 * event.getObject().getClass().getName()); ReportingPeriod period =
		 * (ReportingPeriod)event.getObject();
		 * 
		 * if (period == null) LOG.debug("... selectedPeriod is NULL ...");
		 * LOG.debug("... redirectam za period: {} ...", period);
		 * //LOG.debug("... session period je: {} ...",
		 * sessionBean.getCurrentReportingPeriod().getId());
		 */

		// goToPopisEvidencija();

		// refreshContent();
	}

	public SukobInteresaService getSukobInteresaService() {
		return sukobInteresaService;
	}

	public void setSukobInteresaService(
			SukobInteresaService sukobInteresaService) {
		this.sukobInteresaService = sukobInteresaService;
	}

	public ReportingPeriodsService getReportingPeriods() {
		return reportingPeriods;
	}

	public EvOstaloView getEvOstaloView() {
		return evOstaloView;
	}

	public void setEvOstaloView(EvOstaloView evOstaloView) {
		this.evOstaloView = evOstaloView;
	}

	public void setReportingPeriods(ReportingPeriodsService reportingPeriods) {
		this.reportingPeriods = reportingPeriods;
	}

	public void setBreadcrumb(BreadCrumb breadcrumb) {
		this.breadcrumb = breadcrumb;
	}

	public BreadCrumb getBreadcrumb() {
		return this.breadcrumb;
	}

	public Menubar getIndexMenu() {
		return indexMenu;
	}

	public void setIndexMenu(Menubar indexMenu) {
		this.indexMenu = indexMenu;
	}

	public String getPageId() {
		return pageId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	@Autowired
	public void setMenuService(MenuService menuService) {
		LOG.debug("... setting menuService ...");
		this.menuService = menuService;
	}

	@Autowired
	public void setProjectsService(ProjectsService projectsService) {
		LOG.debug("... setting projectsService ...");
		this.projectsService = projectsService;
	}

	@Autowired
	public void setMsfService(MajorScientificFieldsService msfService) {
		LOG.debug("... setting msfService ...");

		this.msfService = msfService;
	}

	public void setCurrentProject(Project currentProject) {
		LOG.debug("... setting currentProject in MenuController ... {}",
				currentProject);
	}

	public UsersService getUsers() {
		return users;
	}

	public void setUsers(UsersService users) {
		this.users = users;
	}

	public Map<String, String> getPageDescription() {
		return pageDescription;
	}

	public void setPageDescription(Map<String, String> pageDescription) {
		this.pageDescription = pageDescription;
	}

	public BreadCrumb getbTmp() {
		return bTmp;
	}

	public void setbTmp(BreadCrumb bTmp) {
		this.bTmp = bTmp;
	}

	public boolean isFirstTimeRendered() {
		return firstTimeRendered;
	}

	public void setFirstTimeRendered(boolean firstTimeRendered) {
		this.firstTimeRendered = firstTimeRendered;
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public ReportingPeriodsService getReportingPeriodsService() {
		return reportingPeriodsService;
	}

	public void setReportingPeriodsService(
			ReportingPeriodsService reportingPeriodsService) {
		this.reportingPeriodsService = reportingPeriodsService;
	}

	public AppCustomSettings getAppCustomSettings() {
		return appCustomSettings;
	}

	public void setAppCustomSettings(AppCustomSettings appCustomSettings) {
		this.appCustomSettings = appCustomSettings;
	}

	public MenuService getMenuService() {
		return menuService;
	}

	public ProjectsService getProjectsService() {
		return projectsService;
	}

	public MajorScientificFieldsService getMsfService() {
		return msfService;
	}

	public List<ReportingPeriodTree> getRpTreeList() {
		return rpTreeList;
	}

	public void setRpTreeList(List<ReportingPeriodTree> rpTreeList) {
		this.rpTreeList = rpTreeList;
	}

	public TreeNodeUtil getTreeNodeUtil() {
		return treeNodeUtil;
	}

	public void setTreeNodeUtil(TreeNodeUtil treeNodeUtil) {
		this.treeNodeUtil = treeNodeUtil;
	}
	
}
