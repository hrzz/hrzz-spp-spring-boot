package hr.btb.hrzzspp.spring.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class LoginUtil {

	private boolean isFirstTimeEntry = true;

	public boolean isFirstTimeEntry() {
		return isFirstTimeEntry;
	}

	public void setFirstTimeEntry(boolean isFirstTimeEntry) {
		this.isFirstTimeEntry = isFirstTimeEntry;
	}
	
}
