package hr.btb.hrzzspp.spring.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RedirectController {

	private static final Logger LOG = LoggerFactory
			.getLogger(RedirectController.class);

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String baseUrlRedirect(HttpServletRequest request,
			HttpServletResponse httpServletResponse) {
		LOG.debug("... redirecting to index.xhtml ...");
		return "redirect:" + request.getRequestURL().append("index.xhtml").toString();
	}
}
