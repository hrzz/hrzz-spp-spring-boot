package hr.btb.hrzzspp.spring.controller;

import hr.btb.hrzzspp.spring.security.ForceLogout;
import hr.btb.hrzzspp.spring.security.SecurityChecker;

import java.io.IOException;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class LoginController {

	@Autowired
	private LoginUtil loginUtil;
	
	@Autowired
	private ForceLogout forceLogout;	
	
	// This is the action method called when the user clicks the "login" button
	public String doLogin() throws IOException, ServletException {
		
		forceLogout.logout();
		loginUtil.setFirstTimeEntry(false);
		
		ExternalContext context = FacesContext.getCurrentInstance()
				.getExternalContext();

		RequestDispatcher dispatcher = ((ServletRequest) context.getRequest())
				.getRequestDispatcher("/j_spring_security_check");

		dispatcher.forward((ServletRequest) context.getRequest(),
				(ServletResponse) context.getResponse());

		FacesContext.getCurrentInstance().responseComplete();
		// It's OK to return null here because Faces is just going to exit.
		return null;
	}
	
	public void checkAuth() {
		
		System.out.println("Provjeravam login...." + loginUtil.isFirstTimeEntry() + "->" + SecurityChecker.isAuthenticated());
		
		if (!loginUtil.isFirstTimeEntry()){
			
			if (!SecurityChecker.isAuthenticated()){
				System.out.println("Pogrešno korisničko ime ili lozinka");
				
			    FacesContext context = FacesContext.getCurrentInstance();  
			    context.addMessage(null, new FacesMessage("Second Message", "Additional Message Detail"));
				
				//RequestContext.getCurrentInstance().execute("g.renderMessage({summary:'infooo text', detail: 'detaaaaaaail text', severity: 0});");
				//PfUtils.fatal("Prijava:", "Pogrešno korisničko ime ili lozinka");
			}
			
			loginUtil.setFirstTimeEntry(true);
		}
		
	}
	
}