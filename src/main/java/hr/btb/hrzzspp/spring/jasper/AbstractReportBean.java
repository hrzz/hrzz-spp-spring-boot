package hr.btb.hrzzspp.spring.jasper;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
//import dao.Database;
 
public abstract class AbstractReportBean {
 
    public enum ExportOption {
 
        PDF, HTML, EXCEL, RTF
    }
    private ExportOption exportOption;
    private final String COMPILE_DIR = "/reports/";
    private String message;
 
    ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
    ServletContext context = (ServletContext) externalContext.getContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
    
    public AbstractReportBean() {
        super();
        setExportOption(ExportOption.PDF);
    }
 
    protected JasperPrint prepareReport() throws JRException, IOException {
 
        //ReportConfigUtil.compileReport(context, getCompileDir(), getCompileFileName());
        //File reportFile = new File(ReportConfigUtil.getJasperFilePath(context, getCompileDir(), getCompileFileName() + ".jasper"));        
        //ExternalContext ext = FacesContext.getCurrentInstance().getExternalContext();      
        //BufferedReader bf = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/reports/narativnoIzvjesce.jasper")));
        InputStream is = getClass().getResourceAsStream(COMPILE_DIR + getReportId() + ".jasper");
        //JasperPrint jasperPrint = ReportConfigUtil.fillReport(reportFile, getReportParameters(),new JREmptyDataSource());
        JasperPrint jasperPrint = ReportConfigUtil.fillReport(is, getReportParameters(),new JREmptyDataSource());
        
        return jasperPrint;
     
    }
 
    protected OutputStream prepareAndSave() throws JRException, IOException{
    	
    	JasperPrint jasperPrint = prepareReport();    	
    	OutputStream out = new ByteArrayOutputStream();
        	
    	JasperExportManager.exportReportToPdfStream(jasperPrint, out);
    	
    	return out;
    	
    	/*
        JasperExportManager.exportReportToPdfFile(jasperPrint,
                context.getRealPath(getExportFileName()));
                
        try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
                
    }
    
    public ExportOption getExportOption() {
        return exportOption;
    }
 
    public void setExportOption(ExportOption exportOption) {
        this.exportOption = exportOption;
    }
 
    protected String getCompileDir() {
        return COMPILE_DIR;
    }
 
    protected abstract String getCompileFileName();
    protected abstract String getExportFileName();
    protected abstract Map<String, Object> getReportParameters();
    protected abstract String getReportId();
    
    public String getMessage() {
        return message;
    }
 
    public void setMessage(String message) {
        this.message = message;
    }
}