package hr.btb.hrzzspp.spring.jasper;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ReportBean extends AbstractReportBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6248165437710949761L;
	private String COMPILE_FILE_NAME = "narativnoIzvjesce";
	private String exportFile = "/resources/" + COMPILE_FILE_NAME + "_"
			+ new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date())
			+ ".pdf";
	private StreamedContent streamedContent;
	private String reportTitle = "PDF - pregled";
	private Map<String, Object> reportParameters = null;

	public void setReportId(String id){
		this.COMPILE_FILE_NAME = id;
	}

	public String getExportFile() {
		return exportFile;
	}

	public void setExportFile(String exportFile) {
		this.exportFile = exportFile;
	}

	@Override
	protected String getCompileFileName() {
		return COMPILE_FILE_NAME;
	}

	public String execute() {

		try {

			super.prepareAndSave();
			FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	public String executeAndView() {

		try {

			OutputStream out = super.prepareAndSave();
			InputStream in = new ByteArrayInputStream(
					((ByteArrayOutputStream) out).toByteArray());
			streamedContent = new DefaultStreamedContent(in, "application/pdf");

			Map<String, Object> session = FacesContext.getCurrentInstance()
					.getExternalContext().getSessionMap();
			
			byte[] b = (byte[]) session.get("reportBytes");
			
			if (b != null) {
				streamedContent = new DefaultStreamedContent(
						new ByteArrayInputStream(b), "application/pdf");
			}

			RequestContext requestContext = RequestContext.getCurrentInstance();
			requestContext.execute("PF('pdfPreview').show();");

			// FacesContext.getCurrentInstance().responseComplete();

		} catch (Exception e) {
			// make your own exception handling
			e.printStackTrace();
		}

		return null;
	}

	public StreamedContent getStreamedContent() {

		if (FacesContext.getCurrentInstance().getRenderResponse()) {
			return new DefaultStreamedContent();
		} else {
			return streamedContent;
		}

	}

	@Override
	protected String getExportFileName() {
		// TODO Auto-generated method stub
		return exportFile;
	}

	@Override
	protected Map<String, Object> getReportParameters() {
		// TODO Auto-generated method stub
		if (reportParameters != null)
			return reportParameters;
		else
			return new HashMap<String, Object>();
	}
	
	public void setReportParameters(Map<String, Object> params){
		this.reportParameters = params;
	}

	public String getReportTitle() {
		return reportTitle;
	}

	public void setReportTitle(String reportTitle) {
		this.reportTitle = reportTitle;
	}

	@Override
	protected String getReportId() {
		// TODO Auto-generated method stub
		return COMPILE_FILE_NAME;
	}

}
