package hr.btb.hrzzspp.spring.config;

import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserRole;
import hr.btb.hrzzspp.spring.services.UsersService;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebMvcSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Autowired
	private UsersService userService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {

		http.csrf().disable();

		http.authorizeRequests().antMatchers("/resources/**", "/static/**", "/j_spring_security_check", "/login.xhtml")
				.permitAll().anyRequest().authenticated().and().formLogin()
				.usernameParameter("j_username") /* BY DEFAULT IS username!!! */
				.passwordParameter("j_password") /* BY DEFAULT IS password!!! */
				.loginProcessingUrl("/j_spring_security_check").loginPage("/login.xhtml")
				.defaultSuccessUrl("/index.xhtml").permitAll().and().logout()
				.logoutRequestMatcher(new AntPathRequestMatcher("/j_spring_security_logout"))
				.logoutSuccessUrl("/login.xhtml").permitAll();
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {

		// String role = "ADMIN"; //izvuci iz baze
/*
		for (User user : userService.getAllUsers()) {

			for (UserRole role : user.getUserRoles())
				auth.inMemoryAuthentication().withUser(user.getUsername()).password(user.getPassword())
						.roles(role.getRole().getName());

		}
*/
		// using JDBC
		// JndiDataSourceLookup dataSourceLookup = new JndiDataSourceLookup();
		// DataSource dataSource =
		// dataSourceLookup.getDataSource("java:comp/env/jdbc/hrzzspp");

/*
		com.mysql.jdbc.jdbc2.optional.MysqlDataSource ds = new com.mysql.jdbc.jdbc2.optional.MysqlDataSource();
		ds.setServerName("localhost");
		ds.setPortNumber(3306);
		ds.setDatabaseName("hrzzspp");
		ds.setUser("root");
		ds.setPassword("root");
*/
		
		
		final String findUserQuery = "select username username,password password,is_enabled enabled from users where username = ?";
		final String findRoles = "select username username, concat('ROLE_',roles.name) role from users,users_roles,roles where users.id = users_roles.user_id and users_roles.role_id = roles.id and  username = ?";

		auth.jdbcAuthentication().dataSource(jdbcTemplate.getDataSource()).usersByUsernameQuery(findUserQuery)
				.authoritiesByUsernameQuery(findRoles);
		
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/javax.faces.resource/**");
	}
	
}

