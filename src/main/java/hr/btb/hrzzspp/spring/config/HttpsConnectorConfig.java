package hr.btb.hrzzspp.spring.config;

import java.io.File;
import java.io.IOException;

import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

// Isključena konfiguracija
//@Configuration
//@Profile(value = "production")
public class HttpsConnectorConfig {

	@Bean
	public EmbeddedServletContainerFactory servletContainer() {
		TomcatEmbeddedServletContainerFactory tomcat = new TomcatEmbeddedServletContainerFactory();
		tomcat.addAdditionalTomcatConnectors(createSslConnector());
		return tomcat;
	}

	private Connector createSslConnector() {
		Connector connector = new Connector(
				"org.apache.coyote.http11.Http11NioProtocol");
		Http11NioProtocol protocol = (Http11NioProtocol) connector
				.getProtocolHandler();
		try {
			File keystore = new ClassPathResource("keystore/hrzzspp.jks")
					.getFile();
			// File truststore = new ClassPathResource("keystore").getFile();
			connector.setScheme("https");
			connector.setSecure(true);
			connector.setPort(8443);
			protocol.setSSLEnabled(true);
			protocol.setKeystoreFile(keystore.getAbsolutePath());
			protocol.setKeystorePass("Hr2Z.C3rT1fik@t.2015!");
			// protocol.setTruststoreFile(truststore.getAbsolutePath());
			// protocol.setTruststorePass("changeit");
			protocol.setKeyAlias("hrzzspp-cert-alias-name");
			return connector;
		} catch (IOException ex) {
			throw new IllegalStateException("can't access keystore: ["
					+ "keystore/hrzzspp.jks" + "] or truststore: ["
					+ "keystore/hrzzspp.jks" + "]", ex);
		}
	}
}
