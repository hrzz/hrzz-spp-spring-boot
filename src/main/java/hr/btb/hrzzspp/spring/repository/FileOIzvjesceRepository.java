package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.FileOIzvjesce;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FileOIzvjesceRepository extends JpaRepository<FileOIzvjesce, Integer> {
	FileOIzvjesce findByNarativnoIzvjesce(EvNarativnoIzvjesce narativnoIzvjesce);
}
