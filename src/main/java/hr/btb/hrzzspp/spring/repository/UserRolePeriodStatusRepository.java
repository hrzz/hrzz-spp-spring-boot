package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRolePeriodStatusRepository extends JpaRepository<UserRolePeriodStatus, Long> {
	public List<UserRolePeriodStatus> findByPeriod(ReportingPeriod period);
	public List<UserRolePeriodStatus> findByPeriodAndRole(ReportingPeriod period, Role role);
	public List<UserRolePeriodStatus> findByUserAndRole(User user, Role role);
}

