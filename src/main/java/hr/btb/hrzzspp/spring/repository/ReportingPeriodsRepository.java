package hr.btb.hrzzspp.spring.repository;

import java.sql.Timestamp;
import java.util.List;

import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ReportingPeriodsRepository extends JpaRepository<ReportingPeriod, Long> {

	List<ReportingPeriod> findByProject(@Param("pr") Project pr);
	
	ReportingPeriod findById(int id);
	
    @Query("SELECT max(e.ordinalNumber) FROM ReportingPeriod e WHERE e.project = :pr")
    public Integer findMaxOrdNum(@Param("pr") Project pr);
    
    @Query("SELECT max(e.dateTo) FROM ReportingPeriod e WHERE e.project = :pr")
    public Timestamp findMaxDateTo(@Param("pr") Project pr);
    
    @Query("SELECT max(e.dateTo) FROM ReportingPeriod e WHERE e.project = :pr and e.id != :period")
    public Timestamp findMaxDateToExcludeCurr(@Param("pr") Project pr, @Param("period") int period);
    
    @Query("SELECT min(e.dateFrom) FROM ReportingPeriod e WHERE e.project = :pr and e.id != :period")
    public Timestamp findMinDateFromExcludeCurr(@Param("pr") Project pr, @Param("period") int period);
    
    @Query("SELECT min(e.dateFrom) FROM ReportingPeriod e WHERE e.project = :pr")
    public Timestamp findMinReportingPeriodDateFrom(@Param("pr") Project pr);
    
    @Query("SELECT e FROM ReportingPeriod e WHERE e.project = :pr and :refDate >= e.dateFrom and :refDate <= e.dateTo")
    public List<ReportingPeriod> findByPeriodInterval(@Param("pr") Project pr,@Param("refDate") Timestamp refDate);
    
}
