package hr.btb.hrzzspp.spring.repository;

import java.util.List;

import hr.btb.hrzzspp.jpa.VrednovanjeRezIzvjesce;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VrednovanjeRezIzvjesceRepository extends
		JpaRepository<VrednovanjeRezIzvjesce, Integer> {

	@Query("SELECT r FROM VrednovanjeRezIzvjesce r WHERE r.period.id=:periodId order by r.vrijemePromjene desc")
	public List<VrednovanjeRezIzvjesce> findRezVredByPeriodUserLastModified(@Param("periodId") int periodId, Pageable page);
}