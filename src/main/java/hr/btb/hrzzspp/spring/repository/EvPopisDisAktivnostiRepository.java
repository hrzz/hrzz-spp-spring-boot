package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvPopisDisAktivnosti;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvPopisDisAktivnostiRepository extends JpaRepository<EvPopisDisAktivnosti, Integer> {
	public List<EvPopisDisAktivnosti> findByReportingPeriod(ReportingPeriod reportingPeriod);
}