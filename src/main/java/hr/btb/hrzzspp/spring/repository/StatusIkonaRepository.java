package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.StatusIkona;

import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusIkonaRepository extends JpaRepository<StatusIkona, Integer>{

}
