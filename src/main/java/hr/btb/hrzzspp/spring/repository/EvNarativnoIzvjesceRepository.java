package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvNarativnoIzvjesceRepository extends JpaRepository<EvNarativnoIzvjesce, Integer> {
	
	public EvNarativnoIzvjesce findByReportingPeriod(ReportingPeriod reportingPeriod);

}

