package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvPopisZnRadova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvPopisZnRadovaRepository extends JpaRepository<EvPopisZnRadova, Integer>{
	public List<EvPopisZnRadova> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
