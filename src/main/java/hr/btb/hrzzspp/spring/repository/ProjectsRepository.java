package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ScientificField;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface ProjectsRepository extends JpaRepository<Project, Long> {
	List<Project> findByDuration(@Param("duration") int duration);
	List<Project> findByScientificField(@Param("sf") ScientificField sf);
	List<Project> findByMajorScientificField(@Param("msf") MajorScientificField msf);
	List<Project> findByMajorScientificFieldAndLeaderId(@Param("msf") MajorScientificField msf,@Param("leaderId") int leaderId);
	List<Project> findByName(@Param("name") String name);
	List<Project> findByLeaderId(@Param("leaderId") int leaderId);
	
	@Query("SELECT p FROM Project p, ReportingPeriod rp, UserRolePeriodStatus urps WHERE urps.user.id=:userId and rp.id=urps.period.id and p.id=rp.project.id")
    public List<Project> findProjectsByUserId(@Param("userId") int userId);
}
