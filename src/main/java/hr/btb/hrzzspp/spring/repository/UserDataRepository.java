package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserData;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface UserDataRepository extends JpaRepository<UserData, Integer> {
	UserData findByUser(@Param("user") User user);
}
