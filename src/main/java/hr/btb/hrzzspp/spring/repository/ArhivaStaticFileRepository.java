package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.ArhivaStaticFile;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ArhivaStaticFileRepository extends JpaRepository<ArhivaStaticFile, Integer> {
}
