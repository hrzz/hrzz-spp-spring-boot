package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.PostignutoRezultatSifarnik;
import hr.btb.hrzzspp.jpa.SifarnikInterface;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface PostignutoRezultatSifarnikRepository extends
		JpaRepository<PostignutoRezultatSifarnik, Integer> {

	public SifarnikInterface findByCode(@Param("code") String code);

}
