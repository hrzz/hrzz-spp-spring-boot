package hr.btb.hrzzspp.spring.repository;

import java.util.List;

import hr.btb.hrzzspp.jpa.User;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
	List<User> findByUsername(@Param("username") String username);
	User findById(@Param("id") int id);
}
