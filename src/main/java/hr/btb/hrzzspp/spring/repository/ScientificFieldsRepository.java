package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.jpa.ScientificField;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ScientificFieldsRepository extends
		JpaRepository<ScientificField, Integer> {

	@Query("SELECT sf FROM ScientificField sf WHERE msfId = :msf")
	ScientificField findByMajorScientificField(@Param("msf") MajorScientificField msf);
}
