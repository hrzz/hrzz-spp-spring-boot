package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.FileRad;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRadRepository extends JpaRepository<FileRad, Integer> {

}
