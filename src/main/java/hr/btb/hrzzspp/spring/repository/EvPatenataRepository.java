package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvPatenata;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvPatenataRepository extends JpaRepository<EvPatenata, Integer> {
	public List<EvPatenata> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
