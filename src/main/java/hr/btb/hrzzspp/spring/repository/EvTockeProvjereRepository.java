package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvTockeProvjere;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvTockeProvjereRepository extends JpaRepository<EvTockeProvjere, Integer> {
	public List<EvTockeProvjere> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
