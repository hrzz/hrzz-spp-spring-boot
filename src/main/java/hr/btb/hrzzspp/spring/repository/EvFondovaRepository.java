package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvFondova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvFondovaRepository extends JpaRepository<EvFondova, Integer> {
	public List<EvFondova> findByReportingPeriod(ReportingPeriod reportingPeriod);

}