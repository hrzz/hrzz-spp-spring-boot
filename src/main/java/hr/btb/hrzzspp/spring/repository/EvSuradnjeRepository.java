package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvSuradnje;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvSuradnjeRepository extends JpaRepository<EvSuradnje, Integer> {
	public List<EvSuradnje> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
