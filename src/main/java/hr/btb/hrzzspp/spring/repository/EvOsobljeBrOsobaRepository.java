/**
 * 
 */
package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvOsobljeBrOsoba;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dalibor.harmina
 *
 */
public interface EvOsobljeBrOsobaRepository extends JpaRepository<EvOsobljeBrOsoba, Integer> {

	EvOsobljeBrOsoba findByReportingPeriod(ReportingPeriod reportingPeriod);

}