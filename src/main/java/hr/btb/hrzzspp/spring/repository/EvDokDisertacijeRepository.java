package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvDokDisertacije;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvDokDisertacijeRepository extends JpaRepository<EvDokDisertacije, Integer> {
	public List<EvDokDisertacije> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
