package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvPopisRadova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvPopisRadovaRepository extends JpaRepository<EvPopisRadova, Integer> {
	public List<EvPopisRadova> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
