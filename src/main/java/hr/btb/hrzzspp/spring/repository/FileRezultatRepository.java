package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.FileRezultat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRezultatRepository extends JpaRepository<FileRezultat, Integer> {

}
