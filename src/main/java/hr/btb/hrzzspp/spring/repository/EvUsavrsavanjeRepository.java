package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvUsavrsavanje;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvUsavrsavanjeRepository extends JpaRepository<EvUsavrsavanje, Integer> {
	public List<EvUsavrsavanje> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
