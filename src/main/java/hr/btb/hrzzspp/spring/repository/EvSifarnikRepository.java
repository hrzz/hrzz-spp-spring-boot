package hr.btb.hrzzspp.spring.repository;

import java.util.List;

import hr.btb.hrzzspp.jpa.EvSifarnik;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EvSifarnikRepository extends
		JpaRepository<EvSifarnik, Integer> {

	@Query("SELECT e FROM EvSifarnik e WHERE e.refTable = :table and e.refColumn = :column")
	public List<EvSifarnik> findByRefTableAndRefColumn(
			@Param("table") String table, @Param("column") String column);

	public EvSifarnik findByRefTableAndRefColumnAndCode(
			@Param("table") String table, @Param("column") String column,
			@Param("code") String code);
}
