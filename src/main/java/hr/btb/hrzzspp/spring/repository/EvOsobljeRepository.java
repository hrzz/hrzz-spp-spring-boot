package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvOsoblje;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvOsobljeRepository extends JpaRepository<EvOsoblje, Integer> {

	List<EvOsoblje> findByReportingPeriod(ReportingPeriod reportingPeriod);

}
