package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Rezultat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface RezultatRepository extends JpaRepository<Rezultat, Integer> {
	public List<Rezultat> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
