package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.Evaluator;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface EvaluatorRepository extends JpaRepository<Evaluator, Integer> {
	List<Evaluator> findByEmail(@Param("email") String email);
	List<Evaluator> findByPeriod(ReportingPeriod period);
	List<Evaluator> findByPeriodAndEmail(@Param("period_id") ReportingPeriod period, @Param("email") String email);
	
//	@Query("SELECT e FROM Evaluator e WHERE e.period = :period and e.email = :email")
//    public List<Evaluator> findByPeriodAndEmail(@Param("period") ReportingPeriod period, @Param("email") String email);
}
