/**
 * 
 */
package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.StatusRolePeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author dalibor.harmina
 *
 */
@Repository
public interface StatusRolePeriodRepository extends JpaRepository<StatusRolePeriod, Long>{
	public List<StatusRolePeriod> findByPeriod(ReportingPeriod period);
	public List<StatusRolePeriod> findByRole(Role role);
	public StatusRolePeriod findByPeriodAndRole(ReportingPeriod period, Role role);
}
