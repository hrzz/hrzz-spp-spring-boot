package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvIstrazivackaSkupina;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvIstrazivackaSkupinaRepository extends JpaRepository<EvIstrazivackaSkupina, Integer> {
	public List<EvIstrazivackaSkupina> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
