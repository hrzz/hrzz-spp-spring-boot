package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvPopisPublikacija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvPopisPublikacijaRepository extends JpaRepository<EvPopisPublikacija, Integer> {
	public List<EvPopisPublikacija> findByReportingPeriod(ReportingPeriod reportingPeriod);
}
