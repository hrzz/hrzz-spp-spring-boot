package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.UserRole;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {
	
	List<UserRole> findRoleByUserId(@Param("user_id") int userId);
	
	List<UserRole> findByRole(@Param("role") Role role);
	
	
}

