package hr.btb.hrzzspp.spring.repository;


import hr.btb.hrzzspp.jpa.EvObrazacEvaluacije;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


public interface EvObrazacEvaluacijeRepository extends JpaRepository<EvObrazacEvaluacije, Integer> {
	
	
	List<EvObrazacEvaluacije> findByPeriod(ReportingPeriod reportingPeriod);
	EvObrazacEvaluacije findByUserAndPeriod(User user, ReportingPeriod reportingPeriod);
}
