package hr.btb.hrzzspp.spring.repository;

import java.util.List;

import hr.btb.hrzzspp.jpa.EvOstalo;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.springframework.data.repository.query.Param;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvOstaloRepository extends JpaRepository<EvOstalo, Integer> {
	
	List<EvOstalo> findByReportingPeriod(@Param("rp") ReportingPeriod rp);
}
