package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.EvDobrobit;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EvDobrobitRepository extends JpaRepository<EvDobrobit, Integer> {
	
	EvDobrobit findByReportingPeriod(ReportingPeriod reportingPeriod);

}
