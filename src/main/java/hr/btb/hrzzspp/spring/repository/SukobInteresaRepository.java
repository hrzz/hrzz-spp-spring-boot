package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.SukobInteresa;
import hr.btb.hrzzspp.jpa.User;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SukobInteresaRepository extends JpaRepository<SukobInteresa, Integer>{
	
	@Query("SELECT r FROM SukobInteresa r WHERE r.period.id=:periodId")
	public SukobInteresa findSukobInterByPeriodLastModified(@Param("periodId") int periodId);
	
	
	List<SukobInteresa> findByUserAndPeriod(@Param("user_id") User user, @Param("period_id") ReportingPeriod reportingPeriod );
}
