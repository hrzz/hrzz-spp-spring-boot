package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.DiseminacijeRezultatSifarnik;
import hr.btb.hrzzspp.jpa.SifarnikInterface;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

public interface DiseminacijeRezultatSifarnikRepository extends
		JpaRepository<DiseminacijeRezultatSifarnik, Integer> {

	public SifarnikInterface findByCode(@Param("code") String code);

}
