package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.MajorScientificField;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MajorScientificFieldsRepository extends JpaRepository<MajorScientificField, Integer> {

	MajorScientificField findByCode(String code);
	
}
