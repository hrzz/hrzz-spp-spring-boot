/**
 * 
 */
package hr.btb.hrzzspp.spring.repository;

import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author dalibor.harmina
 *
 */
public interface EvidencijaRepository extends JpaRepository<Evidencija, Integer> {
	public List<Evidencija> findByReportingPeriod(ReportingPeriod reportingPeriod);

}
