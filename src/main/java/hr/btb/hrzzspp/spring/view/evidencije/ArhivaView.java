package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.ArhivaStaticFile;
import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.scope.SpringViewScoped;
import hr.btb.hrzzspp.spring.services.ArhivaStaticFileService;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.model.DefaultStreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author matija.spendic
 *
 */
@Component
//@Scope("request")
@SpringViewScoped
public class ArhivaView implements Serializable{
	
	

	private static final long serialVersionUID = -7456954292137477766L;

	@Autowired
	private ArhivaStaticFileService arhivaStaticFileService;
	
	private List<ArhivaStaticFile> projectEngList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> projectHrvList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> ugovorList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> radniPlanList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> financijskiPlanList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> izvjescaList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> evaluacijaList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> ostaloPrvoRazdobljeList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> ostaloDrugoRazdobljeList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> ostaloTreceRazdobljeList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> ostaloCetvrtoRazdobljeList = new ArrayList<ArhivaStaticFile>();
	private List<ArhivaStaticFile> dokumentiList = new ArrayList<ArhivaStaticFile>();
	
	private DefaultStreamedContent streamedContent;
	private ArhivaStaticFile selectedArhivaStaticFile;
	
	@Autowired
	private SessionBean session;
	
	@Autowired
	private ReportingPeriodsService reportingPeriodService;
	
	@Autowired
	private EvNarativnoIzvjesceService evNarativnoIzvjesceService;
	
	@PostConstruct
	public void init() {
		
		List<ArhivaStaticFile> fileList = arhivaStaticFileService.getArhivaStaticFiles();
		
		Project project = session.getCurrentProject();
		List<ReportingPeriod> rpList = reportingPeriodService.getReportingPeriods(project);
		
		fileList = populateOstalo(fileList, rpList);
		
		populateLists(fileList);
	}
	
	private List<ArhivaStaticFile> populateOstalo(
			List<ArhivaStaticFile> fileList, List<ReportingPeriod> rpList) {
		
		for (int i=0; i<rpList.size(); i++) {
			
			ReportingPeriod tempRP = rpList.get(0);
			EvNarativnoIzvjesce tempNI = evNarativnoIzvjesceService.getEvNarativnoIzvjesceByPeriod(tempRP);
			
			if (tempNI != null) {
				
				List<EvNiFile> evNiFileList = tempNI.getEvNiFiles();
				
				if (tempNI.getEvNiFiles().size() > 0) {
					
					for (int j=0; j<evNiFileList.size(); j++) {
						
						EvNiFile tempEvNiFile = evNiFileList.get(j);
					
						ArhivaStaticFile temp = new ArhivaStaticFile();
						temp.setFileBlob(tempEvNiFile.getFileBlob());
						temp.setFileName(tempEvNiFile.getFileName());
						temp.setFileSize(tempEvNiFile.getFileSize());
						temp.setFileType(tempEvNiFile.getFileType());
						temp.setTitle("");
						
						if (i == 0) {
							
							temp.setGroup("OSTALO_1");
							
						} else if (i == 1) {
							
							temp.setGroup("OSTALO_2");
							
						} else if (i == 2) {
							
							temp.setGroup("OSTALO_3");
							
						} else if (i == 4) {
							
							temp.setGroup("OSTALO_4");
						}
						
						fileList.add(temp);
					}
				}
			
			} else {
				
				break;
			}
		}
		
		return fileList;
	}

	/*
	 * populateLists
	 * -funkcija puni odgovarajuću 
	 * listu podataka ovisno o grupi
	 */
	private void populateLists(List<ArhivaStaticFile> fileList) {
		
		for (int i=0; i<fileList.size(); i++) {
			
			ArhivaStaticFile temp = fileList.get(i);
			String tempGroup = temp.getGroup();
			
			if (tempGroup != null) {
				
				if (tempGroup.equals("PROJECT_ENG")) {
					
					projectEngList.add(temp);
					
				} else if (tempGroup.equals("PROJECT_HRV")) {
					
					projectHrvList.add(temp);
					
				} else if (tempGroup.equals("UGOVOR")) {
					
					ugovorList.add(temp);
				
				} else if (tempGroup.equals("OSTALO_1")) {
					
					ostaloPrvoRazdobljeList.add(temp);
					
				} else if (tempGroup.equals("OSTALO_2")) {
					
					ostaloDrugoRazdobljeList.add(temp);
					
				} else if (tempGroup.equals("OSTALO_3")) {
					
					ostaloTreceRazdobljeList.add(temp);
					
				} else if (tempGroup.equals("OSTALO_4")) {
					
					ostaloCetvrtoRazdobljeList.add(temp);
				}
			}
		}
	}
	
	public void setStreamedContent(){
		
		if (selectedArhivaStaticFile != null) {
			
			InputStream stream = new ByteArrayInputStream(selectedArhivaStaticFile.getFileBlob());
			streamedContent = new DefaultStreamedContent(stream, selectedArhivaStaticFile.getFileType(), selectedArhivaStaticFile.getFileName());
		}
	}
	

	/*
	 * getters and setters:
	 */

	public List<ArhivaStaticFile> getProjectEngList() {
		return projectEngList;
	}

	public void setProjectEngList(List<ArhivaStaticFile> projectEngList) {
		this.projectEngList = projectEngList;
	}

	public List<ArhivaStaticFile> getProjectHrvList() {
		return projectHrvList;
	}

	public void setProjectHrvList(List<ArhivaStaticFile> projectHrvList) {
		this.projectHrvList = projectHrvList;
	}

	public List<ArhivaStaticFile> getUgovorList() {
		return ugovorList;
	}

	public void setUgovorList(List<ArhivaStaticFile> ugovorList) {
		this.ugovorList = ugovorList;
	}

	public List<ArhivaStaticFile> getRadniPlanList() {
		return radniPlanList;
	}
	
	public ArhivaStaticFileService getArhivaStaticFileService() {
		return arhivaStaticFileService;
	}

	public void setArhivaStaticFileService(
			ArhivaStaticFileService arhivaStaticFileService) {
		this.arhivaStaticFileService = arhivaStaticFileService;
	}

	public void setRadniPlanList(List<ArhivaStaticFile> radniPlanList) {
		this.radniPlanList = radniPlanList;
	}

	public List<ArhivaStaticFile> getFinancijskiPlanList() {
		return financijskiPlanList;
	}

	public void setFinancijskiPlanList(List<ArhivaStaticFile> financijskiPlanList) {
		this.financijskiPlanList = financijskiPlanList;
	}

	public List<ArhivaStaticFile> getIzvjescaList() {
		return izvjescaList;
	}

	public void setIzvjescaList(List<ArhivaStaticFile> izvjescaList) {
		this.izvjescaList = izvjescaList;
	}

	public List<ArhivaStaticFile> getEvaluacijaList() {
		return evaluacijaList;
	}

	public void setEvaluacijaList(List<ArhivaStaticFile> evaluacijaList) {
		this.evaluacijaList = evaluacijaList;
	}

	public List<ArhivaStaticFile> getOstaloPrvoRazdobljeList() {
		return ostaloPrvoRazdobljeList;
	}

	public void setOstaloPrvoRazdobljeList(
			List<ArhivaStaticFile> ostaloPrvoRazdobljeList) {
		this.ostaloPrvoRazdobljeList = ostaloPrvoRazdobljeList;
	}

	public List<ArhivaStaticFile> getOstaloDrugoRazdobljeList() {
		return ostaloDrugoRazdobljeList;
	}

	public void setOstaloDrugoRazdobljeList(
			List<ArhivaStaticFile> ostaloDrugoRazdobljeList) {
		this.ostaloDrugoRazdobljeList = ostaloDrugoRazdobljeList;
	}

	public List<ArhivaStaticFile> getOstaloTreceRazdobljeList() {
		return ostaloTreceRazdobljeList;
	}

	public void setOstaloTreceRazdobljeList(
			List<ArhivaStaticFile> ostaloTreceRazdobljeList) {
		this.ostaloTreceRazdobljeList = ostaloTreceRazdobljeList;
	}

	public List<ArhivaStaticFile> getOstaloCetvrtoRazdobljeList() {
		return ostaloCetvrtoRazdobljeList;
	}

	public void setOstaloCetvrtoRazdobljeList(
			List<ArhivaStaticFile> ostaloCetvrtoRazdobljeList) {
		this.ostaloCetvrtoRazdobljeList = ostaloCetvrtoRazdobljeList;
	}

	public List<ArhivaStaticFile> getDokumentiList() {
		return dokumentiList;
	}

	public void setDokumentiList(List<ArhivaStaticFile> dokumentiList) {
		this.dokumentiList = dokumentiList;
	}

	public DefaultStreamedContent getStreamedContent() {
		return streamedContent;
	}

	public void setStreamedContent(DefaultStreamedContent streamedContent) {
		this.streamedContent = streamedContent;
	}

	public ArhivaStaticFile getSelectedArhivaStaticFile() {
		return selectedArhivaStaticFile;
	}

	public void setSelectedArhivaStaticFile(ArhivaStaticFile selectedArhivaStaticFile) {
		this.selectedArhivaStaticFile = selectedArhivaStaticFile;
	}

	public ReportingPeriodsService getReportingPeriodService() {
		return reportingPeriodService;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}

	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvNarativnoIzvjesceService getEvNarativnoIzvjesceService() {
		return evNarativnoIzvjesceService;
	}

	public void setEvNarativnoIzvjesceService(
			EvNarativnoIzvjesceService evNarativnoIzvjesceService) {
		this.evNarativnoIzvjesceService = evNarativnoIzvjesceService;
	}
	
}
