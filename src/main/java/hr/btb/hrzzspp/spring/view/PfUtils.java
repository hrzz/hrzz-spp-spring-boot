package hr.btb.hrzzspp.spring.view;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;

public class PfUtils {

	private static final String INS_SUCCESSFUL = "Podaci uspješno spremljeni";
	private static final String UPD_SUCCESSFUL = "Podaci uspješno spremljeni";
	private static final String INS_FAIL = "Došlo je do greške prilikom spremanja!";
	private static final String UPD_FAIL = "Došlo je do greške prilikom promjene!";
	
	private static final String OUTPUT_DATE_FOMAT = "dd.MM.yyyy";
	
	public static String getOutputDateFormat(){
		return OUTPUT_DATE_FOMAT;
	}
	
	public static void info(String msg) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, null));
	}

	public static void info(String msg, String details) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, details));
	}

	public static void warn(String msg) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_WARN, msg, null));
	}

	public static void warn(String msg, String details) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, details));
	}

	public static void error(String msg) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, null));
	}

	public static void error(String msg, String details) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, details));
	}

	public static void fatal(String msg) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, null));
	}

	public static void fatal(String msg, String details) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_INFO, msg, details));
	}

	public static void insertSuccessful() {
		info("Unos", INS_SUCCESSFUL);
	}

	public static void insertFail() {
		info("Unos", INS_FAIL);
	}

	public static void updateSuccessful() {
		info("Promjena", UPD_SUCCESSFUL);
	}

	public static void updateFail() {
		info("Promjena", UPD_FAIL);
	}

	public static void dialogInsertSuccessful() {

		info("Unos", INS_SUCCESSFUL);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("waitAndClose();");
	}

	public static void dialogUpdateSuccessful() {

		info("Promjena", UPD_SUCCESSFUL);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("waitAndClose();");
	}

	public static void dialogInsertFail() {

		info("Unos", INS_FAIL);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("waitAndClose();");
	}

	public static void dialogUpdateFail() {

		info("Promjena", UPD_FAIL);

		RequestContext requestContext = RequestContext.getCurrentInstance();
		requestContext.execute("waitAndClose();");
	}

	public static void dialogFail( final String action, final String message ) {

		warn(action, message);

	}
}