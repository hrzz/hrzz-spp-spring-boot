package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.EvFile;

public interface FileRelatedObjectInterface {
	
	EvFile getFile();
	String getPoveznica();
	void setFile(EvFile file);
}
