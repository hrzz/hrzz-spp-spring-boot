package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsModel;
import hr.btb.hrzzspp.jpa.EvDobrobit;
import hr.btb.hrzzspp.spring.repository.EvDobrobitRepository;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class UtjecajProjektaView {
	
	private static final Logger LOG = LoggerFactory.getLogger(UtjecajProjektaView.class);
	
	private static final String TABLE_NAME = "Tablica 15";

	@Autowired
	private SessionBean session;
	@Autowired
	private EvDobrobitRepository evDobrobitRepository;
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	private String[] selectedDobrobiti;

	private EvDobrobit evDobrobit = new EvDobrobit();

	@PostConstruct
	private void init() {
		LOG.debug("... UtjecajProjektaView init ...");
		evDobrobit = evDobrobitRepository.findByReportingPeriod(session
				.getCurrentReportingPeriod());

		if (evDobrobit == null) {
			evDobrobit = new EvDobrobit(session.getCurrentReportingPeriod());
		} else {
			List<String> tmpList = new ArrayList<>();
			for (String propertyName : ConstantsModel.listaUtjecaja) {
				Method method;
				try {
					method = evDobrobit.getClass().getMethod("get" + propertyName,
							new Class[] {});
					String val = (String) method.invoke(evDobrobit, new Object[]{});
					LOG.debug("... propertyName: {} | val={}", propertyName, val);
					if ("1".equals(val)) {
						tmpList.add("set" + propertyName);
					}
					
				} catch (NoSuchMethodException | SecurityException
						| IllegalAccessException | IllegalArgumentException
						| InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (tmpList.size() > 0) {
				selectedDobrobiti = new String[tmpList.size()];
				tmpList.toArray(selectedDobrobiti);
				LOG.debug("... selectedDobrobiti : {}", Arrays.toString(selectedDobrobiti));
			}
		}
	}
	
	
	
	
	public void save() {
		LOG.debug("... selectedDobrobiti: {}", Arrays.toString(selectedDobrobiti));
		Method method;
		for (String methodName : selectedDobrobiti) {
			try {
				method = evDobrobit.getClass().getMethod(methodName,
						String.class);
				method.invoke(evDobrobit, "1");
			} catch (NoSuchMethodException | SecurityException
					| IllegalAccessException | IllegalArgumentException
					| InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		LOG.debug("... saving evDobrobit ... {}", evDobrobit);
		evDobrobitRepository.save(evDobrobit);
		
		//promijena statusa evidencije
		evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
		
		PfUtils.info("Utjecaj projekta","Podaci uspješno spremljeni.");
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", evDobrobit);
			evDobrobitRepository.save(evDobrobit);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	
	public EvDobrobit getEvDobrobit() {
		return evDobrobit;
	}

	public void setEvDobrobit(EvDobrobit evDobrobit) {
		this.evDobrobit = evDobrobit;
	}

	public String[] getSelectedDobrobiti() {
		return selectedDobrobiti;
	}

	public void setSelectedDobrobiti(String[] selectedDobrobiti) {
		this.selectedDobrobiti = selectedDobrobiti;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
