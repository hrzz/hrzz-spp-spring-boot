package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvDokDisertacije;
import hr.btb.hrzzspp.spring.services.EvDokDisertacijeService;
import hr.btb.hrzzspp.spring.view.DtTotals;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class BrDocDisertacijaView implements Serializable {

	private static final long serialVersionUID = -2067861626807491250L;
	private static final Logger LOG = LoggerFactory
			.getLogger(BrDocDisertacijaView.class);
	
	private static final String TABLE_NAME = "Tablica 6";

	private EvDokDisertacije odabranaEvDokDisertacije = new EvDokDisertacije();
	private EvDokDisertacije novaEvDokDisertacije;

	SessionBean session;

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	private EvDokDisertacijeService evDokDisertacijeService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public BrDocDisertacijaView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvDokDisertacije = new EvDokDisertacije();
	}

	public List<EvDokDisertacije> fillTable() {
		List<EvDokDisertacije> lista = evDokDisertacijeService
				.getEvDokDisertacijeByPeriod(session
						.getCurrentReportingPeriod());
		if (lista.size() == 0) {
			EvDokDisertacije evDokDisertacije = new EvDokDisertacije();
			evDokDisertacije.setBrojDvojnih(0);
			evDokDisertacije.setBrojFinHrzz(0);
			evDokDisertacije.setBrojGospodarstvo(0);
			evDokDisertacije.setBrojIzdavanje(0);
			evDokDisertacije.setBrojIzvanZz(0);
			evDokDisertacije.setBrojRadovaIPatenata(0);
			evDokDisertacije.setBrojUkupan(0);
			evDokDisertacije.setReportingPeriod(session
					.getCurrentReportingPeriod());
			lista.add(evDokDisertacije);
		}

		return lista;
	}

	public void insert() {
		try {

			novaEvDokDisertacije.setBrojUkupan(novaEvDokDisertacije
					.getBrojFinHrzz()
					+ novaEvDokDisertacije.getBrojIzvanZz()
					+ novaEvDokDisertacije.getBrojGospodarstvo()
					+ novaEvDokDisertacije.getBrojDvojnih()
					+ novaEvDokDisertacije.getBrojRadovaIPatenata()
					+ novaEvDokDisertacije.getBrojIzdavanje());

			if (novaEvDokDisertacije.getBrojUkupan() == 0) {
				PfUtils.dialogFail("Unos", "Morate unijeti bar jedan broj disertacija!");
				return;
			}

			novaEvDokDisertacije.setReportingPeriod(session
					.getCurrentReportingPeriod());
			evDokDisertacijeService.saveDokDisertacije(novaEvDokDisertacije);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogInsertSuccessful();
			fillTable();
		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvDokDisertacije);
			evDokDisertacijeService.saveDokDisertacije(odabranaEvDokDisertacije);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public void update() {

		try {

			odabranaEvDokDisertacije.setBrojUkupan(odabranaEvDokDisertacije
					.getBrojFinHrzz()
					+ odabranaEvDokDisertacije.getBrojIzvanZz()
					+ odabranaEvDokDisertacije.getBrojGospodarstvo()
					+ odabranaEvDokDisertacije.getBrojDvojnih()
					+ odabranaEvDokDisertacije.getBrojRadovaIPatenata()
					+ odabranaEvDokDisertacije.getBrojIzdavanje());

			if (odabranaEvDokDisertacije.getBrojUkupan() == 0) {
				PfUtils.dialogFail("Unos", "Morate unijeti bar jedan broj disertacija!");;
				return;
			}

			evDokDisertacijeService
					.saveDokDisertacije(odabranaEvDokDisertacije);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogUpdateSuccessful();

			fillTable();

		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
		}

	}

	public void setDelete(EvDokDisertacije selected) {
		evDokDisertacijeService.deleteDokDisertacije(selected);
//		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvDokDisertacije = new EvDokDisertacije();
	}

	public void onRowSelect(SelectEvent event) {
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvDokDisertacije = (EvDokDisertacije) event.getObject();
		LOG.debug("... odabranaEvDokDisertacije: {}", odabranaEvDokDisertacije);
	}

	public void setCurrentRow(EvDokDisertacije selected) {
		odabranaEvDokDisertacije = selected;
	}

	public EvDokDisertacije getOdabranaEvDokDisertacije() {
		return odabranaEvDokDisertacije;
	}

	public void setOdabranaEvDokDisertacije(
			EvDokDisertacije odabranaEvDokDisertacije) {
		this.odabranaEvDokDisertacije = odabranaEvDokDisertacije;
	}

	@Autowired
	public void setEvDokDisertacijeService(
			EvDokDisertacijeService evDokDisertacijeService) {
		this.evDokDisertacijeService = evDokDisertacijeService;
	}

	public EvDokDisertacije getNovaEvDokDisertacije() {
		return novaEvDokDisertacije;
	}

	public void setNovaEvDokDisertacije(EvDokDisertacije novaEvDokDisertacije) {
		this.novaEvDokDisertacije = novaEvDokDisertacije;
	}

	public int getSumBrDok() {
		return DtTotals.getTotal(fillTable(), "brojUkupan").intValue();
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
