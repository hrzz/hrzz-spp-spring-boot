package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.spring.services.EvOstaloService;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class EvOstaloView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SessionBean session;
	private EvOstaloService evOstaloService;

	public EvOstaloService getEvOstaloService() {
		return evOstaloService;
	}

	@Autowired
	public void setEvOstaloService(EvOstaloService evOstaloService) {
		this.evOstaloService = evOstaloService;
	}

	public SessionBean getSession() {
		return session;
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

}
