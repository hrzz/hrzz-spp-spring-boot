package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.PeriodIzvjestaj;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.StatusRolePeriod;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.spring.services.StatusRolePeriodService;
import hr.btb.hrzzspp.spring.services.UserRolePeriodStatusService;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class TreeNodeUtil implements Serializable {
	
	private static final long serialVersionUID = -1258136275137302754L;
	
	public static final String STAT_ROOT = "ROOT";
	public static final String STAT_EVID = "EVID";
	public static final String STAT_EVL = "EVL";
	public static final String STAT_OEVL = "OEVL";
	public static final String STAT_UO = "UO";
	public static final String STAT_FIN = "FIN";
	
	@Autowired
	private SessionBean sessionBean;
	
	@Autowired
	private StatusRolePeriodService statusRolePeriodService;
	
	@Autowired
	private UserRolePeriodStatusService userRolePeriodStatusService;
	
	private String nazivMarkeri;
	private String datumMarkeri;
	
	public TreeNode postaviPeriodIzvjescivanja(ReportingPeriod period) {
		
		System.out.println("PERIOD -----------------------> " + period.getId());

        TreeNode root = new DefaultTreeNode(new PeriodIzvjestaj("Files", "-", "Folder", "", 0, STAT_ROOT, period.getId()), null);
        
        StatusRolePeriod statusRolePeriodVOD = statusRolePeriodService.findByPeriodAndRole(period, new Role(2));
        String ikonaVOD = "";
        if(statusRolePeriodVOD != null && statusRolePeriodVOD.getStatusRazdoblja()!=null){
        	ikonaVOD =  statusRolePeriodVOD.getStatusRazdoblja().getStatusIkona().getIkona();
        }
        TreeNode evidencije = new DefaultTreeNode(new PeriodIzvjestaj("Evidencije", "", "", ikonaVOD, 0, STAT_EVID, period.getId()), root);
        
        StatusRolePeriod statusRolePeriodEVL = statusRolePeriodService.findByPeriodAndRole(period, new Role(3));
        String ikonaEVL = "";
        if(statusRolePeriodEVL != null && statusRolePeriodEVL.getStatusRazdoblja()!=null){
        	ikonaEVL =  statusRolePeriodEVL.getStatusRazdoblja().getStatusIkona().getIkona();
        }
        TreeNode vrednovanje = new DefaultTreeNode(new PeriodIzvjestaj("Vrednovanje", "", "", ikonaEVL, 0, STAT_EVL, period.getId()), root);
        
        StatusRolePeriod statusRolePeriodCOV = statusRolePeriodService.findByPeriodAndRole(period, new Role(4));
        String ikonaCOV = "";
        if(statusRolePeriodCOV != null && statusRolePeriodCOV.getStatusRazdoblja()!=null){
        	ikonaCOV =  statusRolePeriodCOV.getStatusRazdoblja().getStatusIkona().getIkona();
        }
        TreeNode odborZaVrednovanje = new DefaultTreeNode(new PeriodIzvjestaj("Odbor za vrednovanje", "", "", ikonaCOV, 0, STAT_OEVL, period.getId()), root);
        
        StatusRolePeriod statusRolePeriodCUO = statusRolePeriodService.findByPeriodAndRole(period, new Role(5));
        String ikonaCUO = "";
        if(statusRolePeriodCUO != null && statusRolePeriodCUO.getStatusRazdoblja()!=null){
        	ikonaCUO =  statusRolePeriodCUO.getStatusRazdoblja().getStatusIkona().getIkona();
        }
        TreeNode upravniOdbor = new DefaultTreeNode(new PeriodIzvjestaj("Upravni odbor", "", "", ikonaCUO, 0, STAT_UO, period.getId()), root);
        
        StatusRolePeriod statusRolePeriodFIN = statusRolePeriodService.findByPeriodAndRole(period, new Role(1));
        String ikonaFIN = "";
        if(statusRolePeriodFIN != null && statusRolePeriodFIN.getStatusRazdoblja()!=null){
        	ikonaFIN =  statusRolePeriodFIN.getStatusRazdoblja().getStatusIkona().getIkona();
        }
        TreeNode financiranje = new DefaultTreeNode(new PeriodIzvjestaj("Financiranje", "", "", ikonaFIN, 0, STAT_FIN, period.getId()), root);
         
        // vrednovatelji(evaluatori)
        List<UserRolePeriodStatus> vrednovatelji = userRolePeriodStatusService.findByPeriodAndRole(period, new Role(3));
        int i = 0;
        for(UserRolePeriodStatus vrednovatelj : vrednovatelji){
        	i++;
	        new DefaultTreeNode("document", 
	        		new PeriodIzvjestaj (getNazivMarkeri(vrednovatelj.getUser().getUserData().getName()+" "+vrednovatelj.getUser().getUserData().getSurname(), i), 
	        				getDatumMarkeri(vrednovatelj.getVrijemePromjene()), getSatiMarkeri(vrednovatelj.getVrijemePromjene()), vrednovatelj.getStatusRazdoblja().getStatusIkona().getIkona(), vrednovatelj.getUser().getId(), STAT_EVL, period.getId()), vrednovanje);
        }
        //odbor za vrednovanje
        List<UserRolePeriodStatus> odborZaVrednovanjeList = userRolePeriodStatusService.findByPeriodAndRole(period, new Role(4));
        for(UserRolePeriodStatus clanOdboraZaVredn : odborZaVrednovanjeList){
	        new DefaultTreeNode("document", 
	        		new PeriodIzvjestaj (clanOdboraZaVredn.getUser().getUserData().getName()+" "+clanOdboraZaVredn.getUser().getUserData().getSurname(), 
	        				getDatumMarkeri(clanOdboraZaVredn.getVrijemePromjene()), getSatiMarkeri(clanOdboraZaVredn.getVrijemePromjene()), clanOdboraZaVredn.getStatusRazdoblja().getStatusIkona().getIkona(), clanOdboraZaVredn.getUser().getId(), STAT_OEVL, period.getId()), odborZaVrednovanje);
        }
        //upravni odbor
        List<UserRolePeriodStatus> upravniOdborList = userRolePeriodStatusService.findByPeriodAndRole(period, new Role(5));
        for(UserRolePeriodStatus clanUpravnogOdbora : upravniOdborList){
	        new DefaultTreeNode("document", 
	        		new PeriodIzvjestaj (clanUpravnogOdbora.getUser().getUserData().getName()+" "+clanUpravnogOdbora.getUser().getUserData().getSurname(), 
	        				getDatumMarkeri(clanUpravnogOdbora.getVrijemePromjene()), getSatiMarkeri(clanUpravnogOdbora.getVrijemePromjene()), clanUpravnogOdbora.getStatusRazdoblja().getStatusIkona().getIkona(), clanUpravnogOdbora.getUser().getId(), STAT_UO, period.getId()), upravniOdbor);
        }
         
        return root;
    }
	
	public String getNazivMarkeri(String nazivMarkeri, int i) {
		if (sessionBean.hasRoles(new String [] {"PRK"})){
			return nazivMarkeri;
		} else {
			return "EVL "+i;
		}
	}

	public String getDatumMarkeri(Timestamp vrijemePromjene) {
		return new SimpleDateFormat("dd.MM.yyyy.").format(vrijemePromjene);
	}
	
	public void setDatumMarkeri(String datumMarkeri) {
		this.datumMarkeri = datumMarkeri;
	}
	
	public String getSatiMarkeri(Timestamp vrijemePromjene) {
		return new SimpleDateFormat("HH:mm").format(vrijemePromjene);
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}
	
	public String getNazivMarkeri() {
		return nazivMarkeri;
	}

	public void setNazivMarkeri(String nazivMarkeri) {
		this.nazivMarkeri = nazivMarkeri;
	}
	
	public String getDatumMarkeri() {
		return datumMarkeri;
	}

	public StatusRolePeriodService getStatusRolePeriodService() {
		return statusRolePeriodService;
	}

	public void setStatusRolePeriodService(
			StatusRolePeriodService statusRolePeriodService) {
		this.statusRolePeriodService = statusRolePeriodService;
	}

	public UserRolePeriodStatusService getUserRolePeriodStatusService() {
		return userRolePeriodStatusService;
	}

	public void setUserRolePeriodStatusService(
			UserRolePeriodStatusService userRolePeriodStatusService) {
		this.userRolePeriodStatusService = userRolePeriodStatusService;
	}

}
