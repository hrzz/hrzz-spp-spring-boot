package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvPatenata;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.spring.services.EvPatenataService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("session")
public class PatentZigDizajnView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = 4123578861575991640L;
	private static final Logger LOG = LoggerFactory
			.getLogger(TockeProvjereView.class);
	
	private static final String TABLE_NAME = "Tablica 11";
	
	private EvPatenata odabranaEvPatenata = new EvPatenata();	
	private EvPatenata novaEvPatenata;
	
	private EvSifarnik lovObjectVrsta;
	private EvSifarnik lovObjectPovjerljivo;
	
	private SessionBean session;
	
	@Autowired
	private EvPatenataService evPatenataService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;
	
	SimpleDateFormat dateformat = new SimpleDateFormat("yyyy.MM.dd");
	private String datumOdS;
	private String datumDoS;
	
	public void PatentZigDizjanView(){
		
	}
	
	@PostConstruct
	public void init(){
		this.novaEvPatenata = new EvPatenata();
	}
	
	public List<EvPatenata> fillTable(){
		
 		return evPatenataService.getEvPatenataByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void insert(){
		
		try {
			novaEvPatenata.setReportingPeriod(session.getCurrentReportingPeriod());
			novaEvPatenata.setVrsta(lovObjectVrsta.getCode());
			novaEvPatenata.setPovjerljivo(lovObjectPovjerljivo.getCode());
			evPatenataService.saveEvPatenata(novaEvPatenata);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
		} catch(Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}

	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvPatenata);
			evPatenataService.saveEvPatenata(odabranaEvPatenata);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	
	public void update() {

		try {
			odabranaEvPatenata.setVrsta(lovObjectVrsta.getCode());
			odabranaEvPatenata.setPovjerljivo(lovObjectPovjerljivo.getCode());
			evPatenataService.saveEvPatenata(odabranaEvPatenata);

			PfUtils.dialogUpdateSuccessful();
		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
		}

	}
	
	public void setDelete(EvPatenata selected) {
		evPatenataService.deleteEvPatenata(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	public void clean(){
		novaEvPatenata = new EvPatenata();
	}
	
	public void onRowSelect(SelectEvent event){
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvPatenata = (EvPatenata)event.getObject();
		LOG.debug("... odabranaEvTockaProvjere: {}", odabranaEvPatenata);
	}
	
	public void setCurrentRow(EvPatenata selected) {
		
		this.lovObjectVrsta = findByKey("ev_patenata","vrsta",selected.getVrsta());
		this.lovObjectPovjerljivo = findByKey("ev_patenata","povjerljivo",selected.getPovjerljivo());
		odabranaEvPatenata = selected;
	}
	
	public EvPatenata getOdabranaEvPatenata() {
		return odabranaEvPatenata;
	}

	public void setOdabranaEvPatenata(EvPatenata odabranaEvPatenata) {
		this.odabranaEvPatenata = odabranaEvPatenata;
	}
	
	@Autowired
	public void setEvPatenata(EvPatenataService evPatenataService){
		this.evPatenataService = evPatenataService;
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvPatenata getNovaEvPatenata() {
		return novaEvPatenata;
	}
	
	public void setNovaEvPatenata(EvPatenata novaEvPatenata) {
		this.novaEvPatenata = novaEvPatenata;
	}

	public EvSifarnik getLovObjectVrsta() {
		return lovObjectVrsta;
	}

	public void setLovObjectVrsta(EvSifarnik lovObjectVrsta) {
		this.lovObjectVrsta = lovObjectVrsta;
	}

	public EvSifarnik getLovObjectPovjerljivo() {
		return lovObjectPovjerljivo;
	}

	public void setLovObjectPovjerljivo(EvSifarnik lovObjectPovjerljivo) {
		this.lovObjectPovjerljivo = lovObjectPovjerljivo;
	}

	public String getDatumOdS() {
		return datumOdS;
	}

	public void setDatumOdS(String datumOdS) {
		this.datumOdS = datumOdS;
	}

	public String getDatumDoS() {
		return datumDoS;
	}

	public void setDatumDoS(String datumDoS) {
		this.datumDoS = datumDoS;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
