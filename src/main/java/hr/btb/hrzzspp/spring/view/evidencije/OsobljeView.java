package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvOsoblje;
import hr.btb.hrzzspp.jpa.EvOsobljeBrOsoba;
import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.StatusEvidencija;
import hr.btb.hrzzspp.spring.services.OsobljeService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.util.List;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.primefaces.event.CellEditEvent;
import org.primefaces.event.RowEditEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class OsobljeView {

	private static final Logger LOG = LoggerFactory
			.getLogger(OsobljeView.class);
	
	private static final String TABLE_NAME = "Tablica 3";

	private SessionBean session;
	private OsobljeService osobljeService;

	private EvOsoblje selectedOsoblje = new EvOsoblje();
	private EvOsobljeBrOsoba evOsobljeBrOsoba = new EvOsobljeBrOsoba();
	private List<EvOsoblje> osobljeOnProject;
	private Integer brOsoba;

	@Autowired
	private ReportingPeriodsService reportingPeriodService;
	
	public void init() {
		LOG.debug("... OsosbljeView init ... currentProject: {}",
				session.getCurrentProject());
		List<EvOsoblje> osoblje = osobljeService
				.getOsobljeForReportingPeriod(session
						.getCurrentReportingPeriod());
		if (osoblje == null || osoblje.size() == 0) {
			this.osobljeOnProject = osobljeService.getFreshOsobljeList(session
					.getCurrentReportingPeriod());
		} else {
			this.osobljeOnProject = osoblje;
		}
		//evOsobljeBrOsoba = osobljeService.findOsobljeBrOsoba(session
		//		.getCurrentReportingPeriod());
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	public void setOsobljeService(OsobljeService osobljeService) {
		this.osobljeService = osobljeService;
	}

	public List<EvOsoblje> getOsobljeOnProject() {
		
		return osobljeOnProject;
	}

	public EvOsoblje getSelectedOsoblje() {
		return selectedOsoblje;
	}

	public void setSelectedOsoblje(EvOsoblje selectedOsoblje) {
		this.selectedOsoblje = selectedOsoblje;
	}

	public void onCellEdit(CellEditEvent event) {
		Object oldValue = event.getOldValue();
		Object newValue = event.getNewValue();

		if (newValue != null && !newValue.equals(oldValue)) {
			String msgTxt = oldValue + " -> " + newValue;
			LOG.debug(msgTxt);

			// FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO,
			// "Promijenjena vrijednost", msgTxt);
			// FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void save(ActionEvent event) {

	}
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", evOsobljeBrOsoba);
			evOsobljeBrOsoba.setReportingPeriod(session.getCurrentReportingPeriod());
			osobljeService.saveOsobljeBrOsoba(evOsobljeBrOsoba);
			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Greška pri spremanju podataka!");
			e.printStackTrace();
		}
	}
	
	/* Razlog zašto je jedino u ovom view-u (od svih evidencija)
	 * ova metoda (umjesto da se koristi iz PopisEvidencijaView)
	 * - Requested bean is currently in creation:  circular dependencies 
	 * (uzajamni Autowired između beanova)
	 * 
	 * changeStatusEvidencije
	 * -funkcija za promijenu statusa
	 * svake pojedine evidencije na
	 * osnovu imena tablice i trenutnog
	 * perioda
	 */
	public void changeStatusEvidencije(String tableName) {
		
		for(Evidencija ev: session.getCurrentReportingPeriod().getEvidencijaList()) {
			
			if (ev.getTitle().equals(tableName)) {
				
				if (ev.getStatusEvidencija().getId() == 1) {
					
					ev.setStatusEvidencija(new StatusEvidencija(2));
					reportingPeriodService.saveReportingPeriod(session.getCurrentReportingPeriod());
				}
				
				break;
			}
		}
	}
	
	public void save() {
		LOG.debug("... saving osobljeOnProject ...");
		try {
			osobljeService.saveOsobljeOnProject(osobljeOnProject);
			if ( brOsoba == null) {
				brOsoba = Integer.valueOf(0);
			}
			if (evOsobljeBrOsoba == null){
				
				evOsobljeBrOsoba = new EvOsobljeBrOsoba();
				evOsobljeBrOsoba.setBrojOsoba(brOsoba);
				evOsobljeBrOsoba.setReportingPeriod(session.getCurrentReportingPeriod());
				
			} else {
				evOsobljeBrOsoba.setBrojOsoba(brOsoba);
				evOsobljeBrOsoba.setReportingPeriod(session.getCurrentReportingPeriod());
			}
				
			osobljeService.saveOsobljeBrOsoba(evOsobljeBrOsoba);
			
			//promijena statusa evidencije
			changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.info("Unos osoblja","Podaci uspješno spremljeni");
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}

	public void onEdit(RowEditEvent event) {

		try {

			osobljeService.saveOsobljeOnProject((EvOsoblje) event.getObject());
			PfUtils.updateSuccessful();

			FacesContext.getCurrentInstance().getPartialViewContext()
					.getRenderIds().add("osobljeDTForm:osobljeDT");

		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}

	}

	public void refresh() {

	}

	public int getBrojTotal(String what) {
		int total = 0;

		for (EvOsoblje row : osobljeOnProject) {
			switch (what) {
			case "zene":
				total += row.getBrojZena();
				break;
			case "muskaraci":
				total += row.getBrojMuskaraca();
				break;
			case "nezaposleni":
				total += row.getBrojNezaposlenih();
				break;
			case "javni":
				total += row.getBrojUJavSektoru();
				break;
			case "privatni":
				total += row.getBrojUPrivSektoru();
				break;
			case "sveuciliste":
				total += row.getBrojUSveucilistu();
				break;
			case "inozemstvo":
				total += row.getBrojUInozemstvu();
				break;
			}
		}
		// LOG.debug("... calculating brojZena ... broj: {}", total);

		return total;
	}

	public EvOsobljeBrOsoba getEvOsobljeBrOsoba() {
		return evOsobljeBrOsoba;
	}

	public void setEvOsobljeBrOsoba(EvOsobljeBrOsoba evOsobljeBrOsoba) {
		this.evOsobljeBrOsoba = evOsobljeBrOsoba;
	}

	public Integer getBrOsoba() {
		
		//fix posetiraj evOsobljeBrOsoba
		evOsobljeBrOsoba = osobljeService.findOsobljeBrOsoba(session
				.getCurrentReportingPeriod());
				
		if (evOsobljeBrOsoba != null)
			brOsoba = evOsobljeBrOsoba.getBrojOsoba();
		else {
			evOsobljeBrOsoba = new EvOsobljeBrOsoba();
			brOsoba = 0;
		}
		
		return brOsoba;
	}

	public void setBrOsoba(Integer brOsoba) {
		this.brOsoba = brOsoba;
	}

	public ReportingPeriodsService getReportingPeriodService() {
		return reportingPeriodService;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}

}
