package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.Institution;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.ManagedBean;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

/**
 * @author dalibor.harmina
 *
 */
@ManagedBean
@ViewScoped
public class InstitutionsView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1016255799507863293L;
	
	private List<Institution> listInstitutions;
	private Institution odabranaInstitucija;
	/**
	 * 
	 */
	public InstitutionsView() {
		// TODO Auto-generated constructor stub
	}
	
	@PostConstruct
	public void init() {
		this.listInstitutions = fillInstitutions();
	}
	
	public List<Institution> fillInstitutions() {
		Institution inst = new Institution();
		inst.setId(new Integer( 9987 ));
		inst.setAddress("Tamo 8");
		inst.setCity("Zabreg");
		inst.setName("Institucija super");
		inst.setOib("90947389487");
		inst.setZipCode("10000");
		
		List<Institution> listInst = new ArrayList<Institution>();
		listInst.add(inst);
		return listInst;
	}

	public List<Institution> getListInstitutions() {
		return listInstitutions;
	}

	public void setListInstitutions(List<Institution> listInstitutions) {
		this.listInstitutions = listInstitutions;
	}

	public Institution getOdabranaInstitucija() {
		return odabranaInstitucija;
	}

	public void setOdabranaInstitucija(Institution odabranaInstitucija) {
		this.odabranaInstitucija = odabranaInstitucija;
	}

}
