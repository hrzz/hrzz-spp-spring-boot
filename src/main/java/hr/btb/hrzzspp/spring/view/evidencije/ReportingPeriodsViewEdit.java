package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ReportingPeriodsViewEdit extends EvSifarnik implements Serializable{

	private static final long serialVersionUID = -4342317303449362630L;

	@Autowired
	private ReportingPeriodsService reportingPeriodsService;

	private Date dateFrom;
	private Date dateTo;
	private BigDecimal approvedResources;
	private BigDecimal ukupnaSredstva;
	private BigDecimal neutroseno;
	private BigDecimal nenamjenskiUtroseno;
	private EvSifarnik lovObjectTipRazdoblja;

	private ReportingPeriod currentRow;

	private SessionBean session;
	
	public SessionBean getSession() {
		return session;
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}
	
	public ReportingPeriod getCurrentRow() {
		return currentRow;
	}

	public void setCurrentRow(ReportingPeriod currentRow) {
		this.currentRow = currentRow;
	}

	public void update() {

		try {

			java.sql.Timestamp minTimestampDateFrom = null;
			java.util.Date minUtilDateFrom = null;

			minTimestampDateFrom = reportingPeriodsService
					.findMinDateFromExcludeCurr(session.getCurrentProject(),currentRow.getId());

			if (minTimestampDateFrom != null)
				minUtilDateFrom = new Date(minTimestampDateFrom.getTime());

			System.out.println("max datum je:" + minTimestampDateFrom);
			
			if (dateFrom.compareTo(dateTo) > 0) {
				PfUtils.error("Unos razdoblja",
						"Datum do ne može biti manji od datuma od");
			//} else if (minTimestampDateFrom != null
			//		&& dateTo.compareTo(minUtilDateFrom) < 0) {
			//	PfUtils.error("Unos razdoblja",
			//			"Datum od postoji u intervalima definiranih razdoblja");
			} else {

				long dateFromL = dateFrom.getTime();
				long dateToL = dateTo.getTime();
				dateFrom = new Date(dateFromL);
				dateTo = new Date(dateToL);
				currentRow.setDateFrom(DateUtils.toTimestamp(dateFrom));
				currentRow.setDateTo(DateUtils.toTimestamp(dateTo));
				currentRow.setApprovedResources(approvedResources);
				currentRow.setUkupnaSredstva(ukupnaSredstva);
				currentRow.setNeutroseno(neutroseno);
				currentRow.setNenamjenskiUtroseno(nenamjenskiUtroseno);
				currentRow.setTipRazdoblja(lovObjectTipRazdoblja.getCode());

				reportingPeriodsService.saveReportingPeriod(currentRow);

				PfUtils.info("Promjena razdoblja", "Razdoblje uspješno promijenjeno.");

			}

		} catch (Exception e) {
			PfUtils.updateFail();
			e.printStackTrace();
		}
	}

	public Date getDateFrom() {

		if (currentRow != null)
			return DateUtils.toDate(currentRow.getDateFrom());
		else
			return null;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {

		if (currentRow != null)
			return DateUtils.toDate(currentRow.getDateTo());
		else
			return null;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public BigDecimal getApprovedResources() {

		if (currentRow != null)
			return currentRow.getApprovedResources();
		else
			return null;

	}

	public void setApprovedResources(BigDecimal approvedResources) {
		this.approvedResources = approvedResources;
	}
	
	public BigDecimal getUkupnaSredstva() {
		
		if (currentRow != null)
			return currentRow.getUkupnaSredstva();
		else
			return null;
	}

	public void setUkupnaSredstva(BigDecimal ukupnaSredstva) {
		this.ukupnaSredstva = ukupnaSredstva;
	}

	public BigDecimal getNeutroseno() {
		
		if (currentRow != null)
			return currentRow.getNeutroseno();
		else
			return null;
	}

	public void setNeutroseno(BigDecimal neutroseno) {
		this.neutroseno = neutroseno;
	}

	public BigDecimal getNenamjenskiUtroseno() {
		
		if (currentRow != null)
			return currentRow.getNenamjenskiUtroseno();
		else
			return null;
	}

	public void setNenamjenskiUtroseno(BigDecimal nenamjenskiUtroseno) {
		this.nenamjenskiUtroseno = nenamjenskiUtroseno;
	}

	public EvSifarnik getLovObjectTipRazdoblja() {
		return lovObjectTipRazdoblja;
	}

	public void setLovObjectTipRazdoblja(EvSifarnik lovObjectTipRazdoblja) {
		this.lovObjectTipRazdoblja = lovObjectTipRazdoblja;
	}

}
