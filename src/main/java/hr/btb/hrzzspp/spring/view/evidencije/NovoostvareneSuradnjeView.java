package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.EvSuradnje;
import hr.btb.hrzzspp.spring.services.EvSuradnjeService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * 
 * Tablica 12. NOVOOSTVARENE SURADNJE (NIJE OBVEZNA)
 */

@Component
@Scope("session")
public class NovoostvareneSuradnjeView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -218630701113751026L;
	private static final Logger LOG = LoggerFactory
			.getLogger(NovoostvareneSuradnjeView.class);
	
	private static final String TABLE_NAME = "Tablica 12";

	private EvSuradnje odabranaEvSuradnje = new EvSuradnje();
	private EvSuradnje novaEvSuradnje;
	private EvSifarnik lovObject;

	private SessionBean session;

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	private EvSuradnjeService evSuradnjeService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public NovoostvareneSuradnjeView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvSuradnje = new EvSuradnje();
	}

	public List<EvSuradnje> fillTable() {
		return evSuradnjeService.getEvSuradnjeByPeriod(session.getCurrentReportingPeriod());
	}

	public void insert() {

		try {
			novaEvSuradnje.setReportingPeriod(session.getCurrentReportingPeriod());
			novaEvSuradnje.setVrstaSuradnje(lovObject.getCode());
			evSuradnjeService.saveEvSuradnje(novaEvSuradnje);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvSuradnje);
			evSuradnjeService.saveEvSuradnje(odabranaEvSuradnje);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	

	public void update() {
		try {
			
			odabranaEvSuradnje.setVrstaSuradnje(lovObject.getCode());
			evSuradnjeService.saveEvSuradnje(odabranaEvSuradnje);
			
			PfUtils.dialogUpdateSuccessful();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
	public void setDelete(EvSuradnje selected){
		evSuradnjeService.deleteEvSuradnje(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	public void clean() {
		novaEvSuradnje = new EvSuradnje();
	}

	public void onRowSelect(SelectEvent event) {
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvSuradnje = (EvSuradnje) event.getObject();
		LOG.debug("... odabranaEvSuradnje: {}", odabranaEvSuradnje);
	}
	
	public void setCurrentRow(EvSuradnje selected) {
		this.lovObject = findByKey("ev_suradnje", "vrsta_suradnje", selected.getVrstaSuradnje());
		odabranaEvSuradnje = selected;
	}

	public EvSuradnje getOdabranaEvSuradnje() {
		return odabranaEvSuradnje;
	}

	public void setOdabranaEvSuradnje(EvSuradnje odabranaEvSuradnje) {
		this.odabranaEvSuradnje = odabranaEvSuradnje;
	}

	@Autowired
	public void setEvSuradnjeService(EvSuradnjeService evSuradnjeService) {
		this.evSuradnjeService = evSuradnjeService;
	}

	public EvSuradnje getNovaEvSuradnje() {
		return novaEvSuradnje;
	}

	public void setNovaEvSuradnje(EvSuradnje novaEvSuradnje) {
		this.novaEvSuradnje = novaEvSuradnje;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
