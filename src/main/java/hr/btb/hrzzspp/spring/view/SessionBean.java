package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.StatusIkona;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.spring.security.SecurityChecker;
import hr.btb.hrzzspp.spring.services.StatusIkonaService;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */
@Component
@Scope("session")
public class SessionBean {

	private static final Logger LOG = LoggerFactory
			.getLogger(SessionBean.class);

	private ReportingPeriod currentReportingPeriod;
	private Project currentProject;
	private UserData currentUserData;
	private List<Project> projects;
	private MajorScientificField majorScientificField;
	private Evidencija evidencijaSelected;
	private String rpDateFromFormat;
	private String rpDateToFormat;
	private Integer filteredProjectByScientificField;
	private List<StatusIkona> listStatusIkona;
	
	@Autowired
	private StatusIkonaService statusIkonaService;


	@PostConstruct
	public void init() {
		setListStatusIkona(statusIkonaService.getAllStatusIkona());
	}
	
	public Evidencija getEvidencijaSelected() {
		return evidencijaSelected;
	}

	public void setEvidencijaSelected(Evidencija evidencijaSelected) {
		this.evidencijaSelected = evidencijaSelected;
	}

	public boolean hasRoles(String[] roles) {
		return SecurityChecker.hasRoles(roles);
	}
	
	public ReportingPeriod getCurrentReportingPeriod() {
		return currentReportingPeriod;
	}

	public void setCurrentReportingPeriod(ReportingPeriod currentReportingPeriod) {
		this.currentReportingPeriod = currentReportingPeriod;
	}

	public Project getCurrentProject() {
		return currentProject;
	}

	public void setCurrentProject(Project currentProject) {
		LOG.debug("... setting currentProject in SessionBean ... {}",
				currentProject);
		this.currentProject = currentProject;
	}

	public UserData getCurrentUserData() {
		return currentUserData;
	}

	public void setCurrentUserData(UserData currentUserData) {
		this.currentUserData = currentUserData;
	}

	public String getNameAndSurname() {
		// return nameAndSurname;
		return String.format("%s %s", currentUserData.getName(),
				currentUserData.getSurname());
	}

	public List<Project> getProjects() {
		return projects;
	}

	public void setProjects(List<Project> projects) {
		this.projects = projects;
	}

	public MajorScientificField getMajorScientificField() {
		return majorScientificField;
	}

	public void setMajorScientificField(
			MajorScientificField majorScientificField) {
		this.majorScientificField = majorScientificField;
	}

	public String getRpDateFromFormat() {

		if (currentReportingPeriod != null) {

			rpDateFromFormat = new SimpleDateFormat("dd.MM.yyyy.")
					.format(currentReportingPeriod.getDateFrom());
			return rpDateFromFormat;
		} else {
			return null;
		}
	}

	public void setRpDateFromFormat(String rpDateFromFormat) {
		this.rpDateFromFormat = rpDateFromFormat;
	}

	public String getRpDateToFormat() {

		if (currentReportingPeriod != null) {

			rpDateToFormat = new SimpleDateFormat("dd.MM.yyyy.")
					.format(currentReportingPeriod.getDateTo());
			return rpDateToFormat;
		} else {
			return null;
		}
	}

	public void setRpDateToFormat(String rpDateToFormat) {
		this.rpDateToFormat = rpDateToFormat;
	}

	public Integer getFilteredProjectByScientificField() {
		return filteredProjectByScientificField;
	}

	public void setFilteredProjectByScientificField(
			Integer filteredProjectByScientificField) {		
		this.filteredProjectByScientificField = filteredProjectByScientificField;
	}

	public List<StatusIkona> getListStatusIkona() {
		return listStatusIkona;
	}

	public void setListStatusIkona(List<StatusIkona> listStatusIkona) {
		this.listStatusIkona = listStatusIkona;
	}

	public String cssIkona(int id) {
		for(StatusIkona statusIkona : getListStatusIkona()){
			if(statusIkona.getId() == id){
				return statusIkona.getIkona();
			}
		}
		return "";
	}
}
