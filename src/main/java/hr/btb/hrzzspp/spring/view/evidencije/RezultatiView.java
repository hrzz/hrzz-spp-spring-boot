package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.business.SifarnikEnum;
import hr.btb.hrzzspp.jpa.DiseminacijeRezultatSifarnik;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.FileRezultat;
import hr.btb.hrzzspp.jpa.PostignutoRezultatSifarnik;
import hr.btb.hrzzspp.jpa.Rezultat;
import hr.btb.hrzzspp.spring.services.RezultatService;
import hr.btb.hrzzspp.spring.view.FileUploadBean;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 */
@Component
@Scope("session")
public class RezultatiView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -8066265450947271724L;
	private static final Logger LOG = LoggerFactory
			.getLogger(RezultatiView.class);

	private static final String TABLE_NAME = "Tablica 1";
	
	@Autowired
	private RezultatService rezultatService;
	@Autowired
	private SessionBean session;
	@Autowired
	private FileUploadBean fileUploadBean;
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	private Rezultat selectedRezultat=new Rezultat();
	private Rezultat newRezultat;
	private String oznakaDiseminacijeCode;
	private EvSifarnik lovObjectPos;
	private EvSifarnik lovObjectOD;
	
	public RezultatiView() {
	}

	@PostConstruct
	public void init() {
		newRezultat = new Rezultat();
	}

	public List<Rezultat> fillTable() {
		this.fileUploadBean.clean().setFileEntityName(
				ConstantsEvidencije.FILE_REZULTAT);
		return rezultatService.getRezultatiByReportingPeriod(session
				.getCurrentReportingPeriod());
	}

	public void save() {
		try {
//			if (!isDateValid(newRezultat)) {
//				PfUtils.dialogFail("Unos", "Datum realizacije nije ispravan!");
//				return;
//			}
//			if (!DateUtils.isMMGGGGInPeriod(session.getCurrentReportingPeriod(), newRezultat.getDatumRealizacije())){
//				PfUtils.dialogFail("Unos", "Datum realizacije nije unutar izvještajnog razdoblja!");
//				return;
//			}
			newRezultat.setReportingPeriod(session.getCurrentReportingPeriod());

			newRezultat
					.setOznakaDiseminacije((DiseminacijeRezultatSifarnik) rezultatService
							.getSifarnikForRefAndCode(
									SifarnikEnum.Values.REZULTAT_DISEMINACIJA,
									lovObjectOD.getCode()));
			newRezultat
					.setPostignuto((PostignutoRezultatSifarnik) rezultatService 
							.getSifarnikForRefAndCode(
									SifarnikEnum.Values.REZULTAT_POSTIGNUTO,
									lovObjectPos.getCode()));

			if (fileUploadBean.getEvFile() != null) {
				newRezultat.setFile((FileRezultat) fileUploadBean.getEvFile());
			} else {
				String poveznica_upis = fileUploadBean.getPoveznica();
				if (poveznica_upis != null && !"".equals(poveznica_upis)) {
					if (!poveznica_upis.startsWith("http://") && !poveznica_upis.startsWith("https://")) {
						newRezultat.setPoveznica("http://"
								+ poveznica_upis);
					} else {
						newRezultat.setPoveznica(poveznica_upis);
					}
				}
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");

		}
		LOG.debug("... saving newRezultat ... {}", newRezultat);
		rezultatService.saveRezultat(newRezultat);

		// reset properties for next instance
		
			newRezultat = new Rezultat(session.getCurrentReportingPeriod());
			clean();
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
	}
	
	public void spremiPRK(){
		try{
			LOG.debug("... updating selectedRezultat: {} ...", selectedRezultat);
			rezultatService.saveRezultat(selectedRezultat);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public void update() {
		try {

//			if (!isDateValid(selectedRezultat)) {
//				PfUtils.dialogFail("Unos", "Datum realizacije nije ispravan!");
//				return;
//			}
//			if (!DateUtils.isMMGGGGInPeriod(session.getCurrentReportingPeriod(), selectedRezultat.getDatumRealizacije())){
//				PfUtils.dialogFail("Unos", "Datum realizacije nije unutar izvještajnog razdoblja!");
//				return;
//			}
			String poveznica_upis = fileUploadBean.getPoveznica();

			// kada je već postavljen file, i želi se editirati
			if (selectedRezultat.getFile() != null) {

				if (fileUploadBean.getEvFile() != null) {
					selectedRezultat.getFile().setRezultat(null);
					selectedRezultat.setFile(null);
					rezultatService.saveRezultat(selectedRezultat);
					selectedRezultat.setFile((FileRezultat) fileUploadBean
							.getEvFile());
				}
				selectedRezultat.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada nema linka, a dodaje se file
			} else if ("".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() != null) {

				if (selectedRezultat.getFile() != null) {
					selectedRezultat.getFile().setRezultat(null);
					selectedRezultat.setFile(null);
					rezultatService.saveRezultat(selectedRezultat);
				}
				selectedRezultat.setFile((FileRezultat) fileUploadBean
						.getEvFile());
				selectedRezultat.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada je upisan link, a link se želi editirati
			} else if (poveznica_upis != null && !"".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() == null) {
				if (!poveznica_upis.startsWith("http://")
						&& !poveznica_upis.startsWith("https://")) {
					selectedRezultat.setPoveznica("http://"
							+ poveznica_upis);
				} else {
					selectedRezultat.setPoveznica(poveznica_upis);
				}

				// kada je upisan link, a dodaje se file
			} else if (poveznica_upis != null
					&& fileUploadBean.getEvFile() != null) {

				if (selectedRezultat.getFile() != null) {
					selectedRezultat.getFile().setRezultat(null);
					selectedRezultat.setFile(null);
					rezultatService.saveRezultat(selectedRezultat);
				}
				selectedRezultat.setFile((FileRezultat) fileUploadBean
						.getEvFile());
				selectedRezultat.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));
			} else if (poveznica_upis.equals("")) {
				selectedRezultat.setPoveznica(poveznica_upis);
			}
			
			selectedRezultat
					.setOznakaDiseminacije((DiseminacijeRezultatSifarnik) rezultatService
							.getSifarnikForRefAndCode(
									SifarnikEnum.Values.REZULTAT_DISEMINACIJA,
									lovObjectOD.getCode()));
			selectedRezultat
					.setPostignuto((PostignutoRezultatSifarnik) rezultatService
							.getSifarnikForRefAndCode(
									SifarnikEnum.Values.REZULTAT_POSTIGNUTO,
									lovObjectPos.getCode()));

			LOG.debug("... updating selectedRezultat: {} ...", selectedRezultat);
				rezultatService.saveRezultat(selectedRezultat);

				PfUtils.dialogUpdateSuccessful();
				
			
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
		clean();
	}
	
	

	//Check if DatumRealizacije iz greater than 12. e.g. MM/YYYY => 07/2015 (valid), 14/2015 (not valid)
//	private boolean isDateValid(Rezultat newRezultat) { 
//		
//		if (newRezultat.getDatumRealizacije() != null) {
//
//			int indexofForwardSlash = newRezultat.getDatumRealizacije()
//					.indexOf('/');
//			String substringOfDatumRealizacije = newRezultat
//					.getDatumRealizacije().substring(0, indexofForwardSlash);
//			int numberOfMonth = Integer.parseInt(substringOfDatumRealizacije);
//
//			if (numberOfMonth <= 12 && numberOfMonth > 0) {
//				return true;
//			}
//
//			return false;
//		}
//		return false;
//	}
	
	public void setDelete(Rezultat selected) {
		rezultatService.deleteRezultat(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan.");
	}
	
	public void deleteFileRezultat() {
		LOG.debug("... deleting file reference ... {}", selectedRezultat);
		selectedRezultat.setFile(null);
		try {
			rezultatService.saveRezultat(selectedRezultat);
		} catch (Exception e) {
			PfUtils.error("Greška","Greška prilikom brisanja datoteke!");
			LOG.error("Error deleting fileRezultat: " + e.toString());
		}
		PfUtils.info("Brisanje datoteke","Datoteka uspješno izbrisana");
	}
	
	public void cancelFileDelete() {
		LOG.debug("... canceling file delete ...");
		selectedRezultat = null;
	}

	public void clean() {
//		newRezultat = new Rezultat();
		fileUploadBean.clean();
	}

	public Rezultat getSelectedRezultat() {
		return selectedRezultat;
	}

	public void setSelectedRezultat(Rezultat selectedRezultat) {
		LOG.debug("... setting selectedRezultat: {} ...", selectedRezultat);
		this.selectedRezultat = selectedRezultat;
		// setting selectedRow for printing fileName from fileUploadBean
		this.fileUploadBean.setSelectedRow(selectedRezultat);

		this.lovObjectOD = findByKey(ConstantsEvidencije.TBL_EV_REZULTATI,
				ConstantsEvidencije.EV_REZULTATI_DISEMINACIJA, selectedRezultat
						.getOznakaDiseminacije().getCode());
		this.lovObjectPos = findByKey(ConstantsEvidencije.TBL_EV_REZULTATI,
				ConstantsEvidencije.EV_REZULTATI_POSTIGNUTO, selectedRezultat
						.getPostignuto().getCode());
	}

	public Rezultat getNewRezultat() {
		return newRezultat;
	}

	public void setNewRezultat(Rezultat newRezultat) {
		this.newRezultat = newRezultat;
	}

	public void setRezultatService(RezultatService rezultatService) {
		this.rezultatService = rezultatService;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public String getOznakaDiseminacijeCode() {
		return oznakaDiseminacijeCode;
	}

	public void setOznakaDiseminacijeCode(String oznakaDiseminacijeCode) {
		this.oznakaDiseminacijeCode = oznakaDiseminacijeCode;
	}

	public EvSifarnik getLovObjectPos() {
		return lovObjectPos;
	}

	public void setLovObjectPos(EvSifarnik lovObjectPos) {
		this.lovObjectPos = lovObjectPos;
	}

	public EvSifarnik getLovObjectOD() {
		return lovObjectOD;
	}

	public void setLovObjectOD(EvSifarnik lovObjectOD) {
		this.lovObjectOD = lovObjectOD;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

}
