package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvPopisRadova;
import hr.btb.hrzzspp.jpa.FileRad;
import hr.btb.hrzzspp.spring.services.EvPopisRadovaService;
import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.FileUploadBean;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Tablica 7.: Popis doktorskih disertacija i diplomskih/magistarskih radova
 */
@Component
@Scope("session")
public class DoktorskeDisertacijeView implements Serializable {

	private static final long serialVersionUID = -8466567856786409134L;
	private static final Logger LOG = LoggerFactory
			.getLogger(DoktorskeDisertacijeView.class);
	
	private static final String TABLE_NAME = "Tablica 7";

	private EvPopisRadova odabranaEvPopisRadova = new EvPopisRadova();
	private EvPopisRadova novaEvPopisRadova;

	@Autowired
	private SessionBean session;
	@Autowired
	private EvPopisRadovaService evPopisRadovaService;
	@Autowired
	private FileUploadBean fileUploadBean;
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public DoktorskeDisertacijeView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvPopisRadova = new EvPopisRadova();
	}

	public List<EvPopisRadova> fillTable() {
		// setting entity for file upload - specific type for table
		// ev_popis_radova
		this.fileUploadBean.clean().setFileEntityName(
				ConstantsEvidencije.FILE_RADOVI);
		return evPopisRadovaService.getEvPopisRadovaByPeriod(session
				.getCurrentReportingPeriod());
	}

	public void insert() {
		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(),
					novaEvPopisRadova.getGodina())) {
				PfUtils.dialogFail("Unos",
						"Godina nije unutar izvještajnog razdoblja!");
				return;
			}
			novaEvPopisRadova.setReportingPeriod(session
					.getCurrentReportingPeriod());

			String poveznica_upis = novaEvPopisRadova.getPoveznica();
			if (fileUploadBean.getEvFile() != null) {
				novaEvPopisRadova.setFile((FileRad) fileUploadBean.getEvFile());
			}
			novaEvPopisRadova.setPoveznica(fileUploadBean.getPoveznica());
			poveznica_upis = novaEvPopisRadova.getPoveznica();
			novaEvPopisRadova.setPoveznica(poveznica_upis.replace(
					poveznica_upis, ""));

			if (fileUploadBean.getEvFile() == null) {
				if (poveznica_upis != null && !"".equals(poveznica_upis)) {
					if (!poveznica_upis.startsWith("http://")
							&& !poveznica_upis.startsWith("https://")) {
						novaEvPopisRadova.setPoveznica("http://"
								+ poveznica_upis);
					} else {
						novaEvPopisRadova.setPoveznica(poveznica_upis);
					}
				}
			}

			evPopisRadovaService.savePopisRadova(novaEvPopisRadova);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			e.printStackTrace();

			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
		clean();
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvPopisRadova);
			evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public void update() {

		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(),
					odabranaEvPopisRadova.getGodina())) {
				PfUtils.dialogFail("Promjena",
						"Godina nije unutar izvještajnog perioda!");
				return;
			}

			String poveznica_upis = fileUploadBean.getPoveznica();

			// kada je već postavljen file, i želi se editirati
			if (odabranaEvPopisRadova.getFile() != null) {

				if (fileUploadBean.getEvFile() != null) {
					odabranaEvPopisRadova.getFile().setRad(null);
					odabranaEvPopisRadova.setFile(null);
					evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);
					odabranaEvPopisRadova.setFile((FileRad) fileUploadBean
							.getEvFile());
				}
				odabranaEvPopisRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada nema linka, a dodaje se file
			} else if ("".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisRadova.getFile() != null) {
					odabranaEvPopisRadova.getFile().setRad(null);
					odabranaEvPopisRadova.setFile(null);
					evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);
				}
				odabranaEvPopisRadova.setFile((FileRad) fileUploadBean
						.getEvFile());
				odabranaEvPopisRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada je upisan link, a link se želi editirati
			} else if (poveznica_upis != null && !"".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() == null) {
				if (!poveznica_upis.startsWith("http://")
						&& !poveznica_upis.startsWith("https://")) {
					odabranaEvPopisRadova.setPoveznica("http://"
							+ poveznica_upis);
				} else {
					odabranaEvPopisRadova.setPoveznica(poveznica_upis);
				}

				// kada je upisan link, a dodaje se file
			} else if (poveznica_upis != null
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisRadova.getFile() != null) {
					odabranaEvPopisRadova.getFile().setRad(null);
					odabranaEvPopisRadova.setFile(null);
					evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);
				}
				odabranaEvPopisRadova.setFile((FileRad) fileUploadBean
						.getEvFile());
				odabranaEvPopisRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));
			} else if (poveznica_upis.equals("")) {
				odabranaEvPopisRadova.setPoveznica(poveznica_upis);
			}

			LOG.debug("... updating odabranaEvPopisRadova: {} ...",
					odabranaEvPopisRadova);
			evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);

			PfUtils.dialogUpdateSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
		clean();
	}

	public void deleteFileRezultat() {
		LOG.debug("... deleting file reference ... {}", odabranaEvPopisRadova);
		odabranaEvPopisRadova.setFile(null);
		try {
			evPopisRadovaService.savePopisRadova(odabranaEvPopisRadova);
		} catch (Exception e) {
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
			LOG.error("Error deleting fileRad: " + e.toString());
		}
		PfUtils.info("Brisanje datoteke","Datoteka uspješno izbrisana");
	}

	public void cancelFileDelete() {
		LOG.debug("... canceling file delete ...");
		odabranaEvPopisRadova = null;
	}

	public void setDelete(EvPopisRadova selected) {
		evPopisRadovaService.deleteEvPopisRadova(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvPopisRadova = new EvPopisRadova();
		novaEvPopisRadova.setGodina((new DateTime(System.currentTimeMillis())).getYear());
		fileUploadBean.clean();
	}

	public EvPopisRadova getOdabranaEvPopisRadova() {
		return odabranaEvPopisRadova;
	}

	public void setOdabranaEvPopisRadova(EvPopisRadova odabranaEvPopisRadova) {
		LOG.debug("... setting odabranaEvPopisRadova ... {}",
				odabranaEvPopisRadova);
		this.odabranaEvPopisRadova = odabranaEvPopisRadova;
		this.fileUploadBean.setSelectedRow(odabranaEvPopisRadova);
	}

	public void setEvPopisRadovaService(
			EvPopisRadovaService evPopisRadovaService) {
		this.evPopisRadovaService = evPopisRadovaService;
	}

	public EvPopisRadova getNovaEvPopisRadova() {
		return novaEvPopisRadova;
	}

	public void setNovaEvPopisRadova(EvPopisRadova novaEvPopisRadova) {
		this.novaEvPopisRadova = novaEvPopisRadova;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

}
