package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvIstrazivackaSkupina;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.spring.services.EvIstrazivackaSkupinaService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class IstrazivackaSkupinaView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -4904284072206207325L;
	private static final Logger LOG = LoggerFactory.getLogger(IstrazivackaSkupinaView.class);
	
	private static final String TABLE_NAME = "Tablica 4";
	
	private EvIstrazivackaSkupina odabranaEvIstrazivackaSkupina = new EvIstrazivackaSkupina();
	private EvIstrazivackaSkupina novaEvIstrazivackaSkupina;
	private EvSifarnik lovObject;
	
	private SessionBean session;
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}
	
	@Autowired
	private EvIstrazivackaSkupinaService evIstrazivackaSkupinaService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public IstrazivackaSkupinaView() {
		
	}
	
	@PostConstruct
	public void init(){
		this.novaEvIstrazivackaSkupina = new EvIstrazivackaSkupina();
	}
	
	public List<EvIstrazivackaSkupina> fillTable(){
		return evIstrazivackaSkupinaService.getEvIstrazivackaSkupinaByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void insert(){
		try{
			novaEvIstrazivackaSkupina.setReportingPeriod(session.getCurrentReportingPeriod());
			novaEvIstrazivackaSkupina.setTitula(lovObject.getCode());
			
			evIstrazivackaSkupinaService.saveIstrazivackaSkupina(novaEvIstrazivackaSkupina);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvIstrazivackaSkupina);
			evIstrazivackaSkupinaService.saveIstrazivackaSkupina(odabranaEvIstrazivackaSkupina);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public void update() {

		try {
			odabranaEvIstrazivackaSkupina.setTitula(lovObject.getCode());

			evIstrazivackaSkupinaService.saveIstrazivackaSkupina(odabranaEvIstrazivackaSkupina);
			PfUtils.dialogUpdateSuccessful();

		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}

	}

	public void setDelete(EvIstrazivackaSkupina selected) {
		evIstrazivackaSkupinaService.deleteIstrazivackaSkupina(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	public void clean(){
		novaEvIstrazivackaSkupina = new EvIstrazivackaSkupina();
	}

	public void onRowSelect(SelectEvent event) {
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvIstrazivackaSkupina = (EvIstrazivackaSkupina) event.getObject();
		LOG.debug("... odabranaEvTockaProvjere: {}", odabranaEvIstrazivackaSkupina);
	}

	public void setCurrentRow(EvIstrazivackaSkupina selected) {

		this.lovObject = findByKey("ev_istrazivacka_skupina","titula",selected.getTitula());
		
		odabranaEvIstrazivackaSkupina = selected;
	}

	public EvIstrazivackaSkupina getOdabranaEvIstrazivackaSkupina() {
		return odabranaEvIstrazivackaSkupina;
	}

	public void setOdabranaEvIstrazivackaSkupina(EvIstrazivackaSkupina odabranaEvIstrazivackaSkupina) {
		this.odabranaEvIstrazivackaSkupina = odabranaEvIstrazivackaSkupina;
	}

	public void setEvIstrazivackaSkupinaService(EvIstrazivackaSkupinaService evIstrazivackaSkupinaService) {
		this.evIstrazivackaSkupinaService = evIstrazivackaSkupinaService;
	}

	public EvIstrazivackaSkupina getNovaEvIstrazivackaSkupina() {
		return novaEvIstrazivackaSkupina;
	}

	public void setNovaEvIstrazivackaSkupina(EvIstrazivackaSkupina novaEvIstrazivackaSkupina) {
		this.novaEvIstrazivackaSkupina = novaEvIstrazivackaSkupina;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
	
}
