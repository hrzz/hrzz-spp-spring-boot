package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvFondova;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.spring.services.EvFondovaService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class InozemniFondoviView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = 1681853854797007178L;
	private static final Logger LOG = LoggerFactory
			.getLogger(InozemniFondoviView.class);
	
	private static final String TABLE_NAME = "Tablica 13";

	private EvFondova odabranaEvFondova = new EvFondova();
	private EvFondova novaEvFondova;
	private EvSifarnik lovObject;

	SessionBean session;

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	private EvFondovaService evFondovaService;

	@Autowired
	private EvidencijeVODView evidencijeVODView;
	
	public InozemniFondoviView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvFondova = new EvFondova();
	}

	public List<EvFondova> fillTable() {
		return evFondovaService.getEvFondovaByPeriod(session
				.getCurrentReportingPeriod());
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvFondova);
			evFondovaService.saveFondova(odabranaEvFondova);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	

	public void insert() {

		try {

//			if (!DateUtils.isDateInPeriod(session.getCurrentReportingPeriod().getDateFrom()
//					.getTime(), session.getCurrentReportingPeriod().getDateTo()
//					.getTime(), novaEvFondova.getRokPrijave().getTime())) {
//				PfUtils.dialogFail("Unos", "Rok prijave nije unutar izvještajnog razdoblja!");
//				return;
//			}

			novaEvFondova.setStatus(lovObject.getCode());
			novaEvFondova.setReportingPeriod(session
					.getCurrentReportingPeriod());
			evFondovaService.saveFondova(novaEvFondova);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void update() {

		try {

//			if (!DateUtils.isDateInPeriod(session.getCurrentReportingPeriod().getDateFrom().getTime(), 
//					session.getCurrentReportingPeriod().getDateTo().getTime(), 
//					odabranaEvFondova.getRokPrijave().getTime())) {
//				PfUtils.dialogFail("Promjena", "Rok prijave nije unutar izvještajnog razdoblja!");
//				return;
//			}
			odabranaEvFondova.setStatus(lovObject.getCode());
			evFondovaService.saveFondova(odabranaEvFondova);
			PfUtils.dialogUpdateSuccessful();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void setDelete(EvFondova selected) {
		evFondovaService.deleteFondova(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvFondova = new EvFondova();
	}

	public void onRowSelect(SelectEvent event) {
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvFondova = (EvFondova) event.getObject();
		LOG.debug("... odabranaEvFondova: {}", odabranaEvFondova);
	}

	public void setCurrentRow(EvFondova selected) {

		this.lovObject = findByKey("ev_fondova", "status", selected.getStatus());

		odabranaEvFondova = selected;
	}

	public EvFondova getOdabranaEvFondova() {
		return odabranaEvFondova;
	}

	public void setOdabranaEvFondova(EvFondova odabranaEvFondova) {
		this.odabranaEvFondova = odabranaEvFondova;
	}

	@Autowired
	public void setEvFondovaService(EvFondovaService evFondovaService) {
		this.evFondovaService = evFondovaService;
	}

	public EvFondova getNovaEvFondova() {
		return novaEvFondova;
	}

	public void setNovaEvFondova(EvFondova novaEvFondova) {
		this.novaEvFondova = novaEvFondova;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
	
}