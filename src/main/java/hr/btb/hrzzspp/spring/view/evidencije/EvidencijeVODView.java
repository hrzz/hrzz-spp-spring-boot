/**
 * 
 */
package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.StatusEvidencija;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.repository.EvOsobljeBrOsobaRepository;
import hr.btb.hrzzspp.spring.services.EvIstrazivackaSkupinaService;
import hr.btb.hrzzspp.spring.services.EvTockeProvjereService;
import hr.btb.hrzzspp.spring.services.EvidencijaService;
import hr.btb.hrzzspp.spring.services.OsobljeService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.RezultatService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */
@Component
@Scope("session")
public class EvidencijeVODView {

	private Evidencija selectedEvidencija = new Evidencija();
	
	@Autowired
	private SessionBean session;
	
	@Autowired
	private EvidencijaService evidencijaService;
	
	@Autowired
	private ReportingPeriodsService reportingPeriodService;

	@Autowired
	private MenuController menuController;

	@Autowired
	private OsobljeView osobljeView;

	@Autowired
	private RezultatService rezultatService;
	
	@Autowired
	private EvTockeProvjereService evTockeProvjereService;
	
	@Autowired
	private OsobljeService osobljeService;
	
	@Autowired
	private EvIstrazivackaSkupinaService evIstrazivackaSkupinaService;
	
	@Autowired
	private EvOsobljeBrOsobaRepository evOsobljeBrOsobaRepository;

	public void setOsobljeView(OsobljeView osobljeView) {
		this.osobljeView = osobljeView;
	}
	
	public List<Evidencija> fillEvidencije(){
		return evidencijaService.getEvidencijaByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void navigaj() {

		if (session.getCurrentReportingPeriod() == null) {

			PfUtils.error("Razdoblje", "Nije odabrano razdoblje");

		} else {

			String pageId = selectedEvidencija.getNazivXhtml();// session.getEvidencijaSelected().getNazivXhtml();

			switch (pageId) {
			case "osoblje":
				osobljeView.init();
				break;
			default:
				break;
			}

			menuController.menuAction(pageId, "1");
		}
	}

	/*
	 * changeStatusEvidencije -funkcija za promijenu statusa svake pojedine
	 * evidencije na osnovu imena tablice i trenutnog perioda
	 */
	public void changeStatusEvidencije(String tableName) {

		for (Evidencija ev : session.getCurrentReportingPeriod()
				.getEvidencijaList()) {

			if (ev.getTitle().equals(tableName)) {

				if (ev.getStatusEvidencija().getId() == 1) {

					ev.setStatusEvidencija(new StatusEvidencija(2));
					reportingPeriodService.saveReportingPeriod(session
							.getCurrentReportingPeriod());
				}

				break;
			}
		}
	}

	/*
	 * changeStatusEvidencijeForPeriod -funkcija za promijenu statusa svakih
	 * evidencija koje su popunjene za određeni period
	 */
	public void changeStatusEvidencijeForPeriod(String akcija) {

		if (akcija != null) {

			if (akcija.equals("ZAKLJUCI")) {

				for (Evidencija ev : session.getCurrentReportingPeriod()
						.getEvidencijaList()) {

					if (ev.getStatusEvidencija().getId() == 2) {

						ev.setStatusEvidencija(new StatusEvidencija(3)); // status
																			// Zaključano
					}
				}

			} else if (akcija.equals("ISPORUCI")) {

				for (Evidencija ev : session.getCurrentReportingPeriod()
						.getEvidencijaList()) {

					if (ev.getStatusEvidencija().getId() == 3) {

						ev.setStatusEvidencija(new StatusEvidencija(4)); // status
																			// Isporučeno
					}
				}
			}
		}
	}

	public void close() {

		try {

			ReportingPeriod reportingPeriod = session
					.getCurrentReportingPeriod();
			
			int br_osoba = 0; 
			
			if (evOsobljeBrOsobaRepository.findByReportingPeriod(reportingPeriod) != null)
				br_osoba = 1;
						
			int control =   rezultatService.getRezultatiByReportingPeriod(reportingPeriod).size()
						  * evTockeProvjereService.getEvTockeProvjereByPeriod(reportingPeriod).size()
						  * br_osoba
						  * evIstrazivackaSkupinaService.getEvIstrazivackaSkupinaByPeriod(reportingPeriod).size();
						  
						  // osobljeService.findOsobljeBrOsoba(session
						  //		.getCurrentReportingPeriod());

			System.out.println("Status je:" + control);
			
			if (control != 0){
				
				reportingPeriod.setStatus("0");
				reportingPeriod.setModifiedAt(new Timestamp((new java.util.Date()).getTime()));
				
				changeStatusEvidencijeForPeriod("ZAKLJUCI");
				reportingPeriodService.saveReportingPeriod(reportingPeriod);
				
				//refresh stranice
				menuController.menuAction("evidencijeVOD", "1");
				
				PfUtils.info("Razdoblje", "Razdoblje je zatvoreno.");
				
			} else {
				PfUtils.error("Zatvaranje razdoblja", "Nisu unesene obvezne evidencije!");
			}

		} catch (Exception e) {

			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public EvidencijaService getEvidencijaService() {
		return evidencijaService;
	}

	public void setEvidencijaService(EvidencijaService evidencijaService) {
		this.evidencijaService = evidencijaService;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

	public Evidencija getSelectedEvidencija() {
		return selectedEvidencija;
	}

	public void setSelectedEvidencija(Evidencija selectedEvidencija) {
		this.selectedEvidencija = selectedEvidencija;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}
	
	public RezultatService getRezultatService() {
		return rezultatService;
	}

	public void setRezultatService(RezultatService rezultatService) {
		this.rezultatService = rezultatService;
	}

	public EvTockeProvjereService getEvTockeProvjereService() {
		return evTockeProvjereService;
	}

	public void setEvTockeProvjereService(
			EvTockeProvjereService evTockeProvjereService) {
		this.evTockeProvjereService = evTockeProvjereService;
	}

	public OsobljeService getOsobljeService() {
		return osobljeService;
	}

	public void setOsobljeService(OsobljeService osobljeService) {
		this.osobljeService = osobljeService;
	}

	public EvIstrazivackaSkupinaService getEvIstrazivackaSkupinaService() {
		return evIstrazivackaSkupinaService;
	}

	public void setEvIstrazivackaSkupinaService(
			EvIstrazivackaSkupinaService evIstrazivackaSkupinaService) {
		this.evIstrazivackaSkupinaService = evIstrazivackaSkupinaService;
	}

	public EvOsobljeBrOsobaRepository getEvOsobljeBrOsobaRepository() {
		return evOsobljeBrOsobaRepository;
	}

	public void setEvOsobljeBrOsobaRepository(
			EvOsobljeBrOsobaRepository evOsobljeBrOsobaRepository) {
		this.evOsobljeBrOsobaRepository = evOsobljeBrOsobaRepository;
	}
}
