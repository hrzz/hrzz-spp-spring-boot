package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvPopisZnRadova;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.FileZnRad;
import hr.btb.hrzzspp.spring.services.EvPopisZnRadovaService;
import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.FileUploadBean;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ZnanstveniRadoviView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = 8984011819960166355L;
	private static final Logger LOG = LoggerFactory
			.getLogger(ZnanstveniRadoviView.class);

	private static final String TABLE_NAME = "Tablica 8";
	
	private EvPopisZnRadova odabranaEvPopisZnRadova = new EvPopisZnRadova();
	private EvPopisZnRadova novaEvPopisZnRadova;
	private EvSifarnik lovObjectRecenzija;
	private EvSifarnik lovObjectKvartil;
	private EvSifarnik lovObjectOtvoreniPristup;
	
	@Autowired
	private SessionBean session;
	@Autowired
	private FileUploadBean fileUploadBean;

	@Autowired
	private EvPopisZnRadovaService evPopisZnRadovaService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public ZnanstveniRadoviView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvPopisZnRadova = new EvPopisZnRadova();
	}

	public List<EvPopisZnRadova> fillTable() {
		// setting entity for file upload - specific type for table
		// ev_popis_zn_radova
		this.fileUploadBean.clean().setFileEntityName(
				ConstantsEvidencije.FILE_ZN_RADOVI);
		return evPopisZnRadovaService.getEvPopisZnRadovaByPeriod(session
				.getCurrentReportingPeriod());
	}

	public void insert() {
		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(), novaEvPopisZnRadova.getGodinaObjavljivanja())){
				PfUtils.dialogFail("Unos", "Godina nije unutar izvještajnog razdoblja!");
				return;
			}
			novaEvPopisZnRadova.setReportingPeriod(session
					.getCurrentReportingPeriod());
			novaEvPopisZnRadova.setRecenzija(lovObjectRecenzija.getCode());
			novaEvPopisZnRadova.setKvartil(lovObjectKvartil.getCode());
			novaEvPopisZnRadova.setOtvoreniPristup(lovObjectOtvoreniPristup
					.getCode());

			if (fileUploadBean.getEvFile() != null) {
				novaEvPopisZnRadova.setFile((FileZnRad) fileUploadBean
						.getEvFile());
			} else {
				String poveznica_upis = fileUploadBean.getPoveznica();
				if (poveznica_upis != null && !"".equals(poveznica_upis)) {
					if (!poveznica_upis.startsWith("http://") && !poveznica_upis.startsWith("https://")) {
						novaEvPopisZnRadova.setPoveznica("http://"
								+ poveznica_upis);
					} else {
						novaEvPopisZnRadova.setPoveznica( poveznica_upis );
					}
				}
			}

			evPopisZnRadovaService.saveEvPopisZnRadova(novaEvPopisZnRadova);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
		clean();
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvPopisZnRadova);
			evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	
	public void update() {
		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(), odabranaEvPopisZnRadova.getGodinaObjavljivanja())){
				PfUtils.dialogFail("Promjena", "Godina nije unutar izvještajnog razdoblja!");
				return;
			}

			String poveznica_upis = fileUploadBean.getPoveznica();

			// kada je već postavljen file, i želi se editirati
			if (odabranaEvPopisZnRadova.getFile() != null) {

				if (fileUploadBean.getEvFile() != null) {
					odabranaEvPopisZnRadova.getFile().setZnRad(null);
					odabranaEvPopisZnRadova.setFile(null);
					evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);
					odabranaEvPopisZnRadova.setFile((FileZnRad) fileUploadBean
							.getEvFile());
				}
				odabranaEvPopisZnRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada nema linka, a dodaje se file
			} else if ("".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisZnRadova.getFile() != null) {
					odabranaEvPopisZnRadova.getFile().setZnRad(null);
					odabranaEvPopisZnRadova.setFile(null);
					evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);
				}
				odabranaEvPopisZnRadova.setFile((FileZnRad) fileUploadBean
						.getEvFile());
				odabranaEvPopisZnRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada je upisan link, a link se želi editirati
			} else if (poveznica_upis != null && !"".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() == null) {
				if (!poveznica_upis.startsWith("http://")
						&& !poveznica_upis.startsWith("https://")) {
					odabranaEvPopisZnRadova.setPoveznica("http://"
							+ poveznica_upis);
				} else {
					odabranaEvPopisZnRadova.setPoveznica(poveznica_upis);
				}

				// kada je upisan link, a dodaje se file
			} else if (poveznica_upis != null
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisZnRadova.getFile() != null) {
					odabranaEvPopisZnRadova.getFile().setZnRad(null);
					odabranaEvPopisZnRadova.setFile(null);
					evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);
				}
				odabranaEvPopisZnRadova.setFile((FileZnRad) fileUploadBean
						.getEvFile());
				odabranaEvPopisZnRadova.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));
			} else if (poveznica_upis.equals("")) {
				odabranaEvPopisZnRadova.setPoveznica(poveznica_upis);
			}

			odabranaEvPopisZnRadova.setRecenzija(lovObjectRecenzija.getCode());
			odabranaEvPopisZnRadova.setKvartil(lovObjectKvartil.getCode());
			odabranaEvPopisZnRadova.setOtvoreniPristup(lovObjectOtvoreniPristup
					.getCode());

			evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);

			PfUtils.dialogUpdateSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
		}
		clean();
	}

	public void deleteFileRezultat() {
		LOG.debug("... deleting file reference ... {}", odabranaEvPopisZnRadova);
		odabranaEvPopisZnRadova.setFile(null);
		try {
			evPopisZnRadovaService.saveEvPopisZnRadova(odabranaEvPopisZnRadova);
		} catch (Exception e) {
			PfUtils.error("Greška","Greška prilikom brisanja datoteke!");
			LOG.error("Error deleting fileZnRad: " + e.toString());
		}
		PfUtils.info("Brisanje datoteka","Datoteka uspješno izbrisana");
	}

	public void cancelFileDelete() {
		LOG.debug("... canceling file delete ...");
		odabranaEvPopisZnRadova = null;
	}

	public void setDelete(EvPopisZnRadova selected) {
		evPopisZnRadovaService.deleteEvPopisZnRadova(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvPopisZnRadova = new EvPopisZnRadova();
		novaEvPopisZnRadova.setGodinaObjavljivanja((new DateTime(System.currentTimeMillis())).getYear());
		fileUploadBean.clean();
	}

	public EvPopisZnRadova getOdabranaEvPopisZnRadova() {
		return odabranaEvPopisZnRadova;
	}

	public void setOdabranaEvPopisZnRadova(
			EvPopisZnRadova odabranaEvPopisZnRadova) {
		LOG.debug("... odabranaEvPopisZnRadova: {}", odabranaEvPopisZnRadova);

		this.lovObjectOtvoreniPristup = findByKey("ev_popis_zn_radova",
				"otvoreni_pristup",
				odabranaEvPopisZnRadova.getOtvoreniPristup());
		this.lovObjectKvartil = findByKey("ev_popis_zn_radova", "kvartil",
				odabranaEvPopisZnRadova.getKvartil());
		this.lovObjectRecenzija = findByKey("ev_popis_zn_radova", "recenzija",
				odabranaEvPopisZnRadova.getRecenzija());

		this.odabranaEvPopisZnRadova = odabranaEvPopisZnRadova;
		this.fileUploadBean.setSelectedRow(odabranaEvPopisZnRadova);
	}

	public void setEvPopisZnRadovaService(
			EvPopisZnRadovaService evPopisZnRadovaService) {
		this.evPopisZnRadovaService = evPopisZnRadovaService;
	}

	public EvPopisZnRadova getNovaEvPopisZnRadova() {
		return novaEvPopisZnRadova;
	}

	public void setNovaEvPopisZnRadova(EvPopisZnRadova novaEvPopisZnRadova) {
		this.novaEvPopisZnRadova = novaEvPopisZnRadova;
	}

	public EvSifarnik getLovObjectRecenzija() {
		return lovObjectRecenzija;
	}

	public void setLovObjectRecenzija(EvSifarnik lovObjectRecenzija) {
		this.lovObjectRecenzija = lovObjectRecenzija;
	}

	public EvSifarnik getLovObjectKvartil() {
		return lovObjectKvartil;
	}

	public void setLovObjectKvartil(EvSifarnik lovObjectKvartil) {
		this.lovObjectKvartil = lovObjectKvartil;
	}

	public EvSifarnik getLovObjectOtvoreniPristup() {
		return lovObjectOtvoreniPristup;
	}

	public void setLovObjectOtvoreniPristup(EvSifarnik lovObjectOtvoreniPristup) {
		this.lovObjectOtvoreniPristup = lovObjectOtvoreniPristup;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
