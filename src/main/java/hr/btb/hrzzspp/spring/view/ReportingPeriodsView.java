package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.business.ConstantsModel;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.Evaluator;
import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.RolePeriodPK;
import hr.btb.hrzzspp.jpa.StatusEvidencija;
import hr.btb.hrzzspp.jpa.StatusRazdoblja;
import hr.btb.hrzzspp.jpa.StatusRolePeriod;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.jpa.UserRole;
import hr.btb.hrzzspp.jpa.UserRolePeriodPK;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.repository.UserRoleRepository;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;
import hr.btb.hrzzspp.spring.services.EvidencijaService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.StatusRolePeriodService;
import hr.btb.hrzzspp.spring.services.UserRolePeriodStatusService;
import hr.btb.hrzzspp.spring.services.UsersService;
import hr.btb.hrzzspp.spring.view.evidencije.PopisEvidencijaView;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("request")
public class ReportingPeriodsView {

	@Autowired
	private EvidencijaService evidencijaService;

	@Autowired
	private EvNarativnoIzvjesceService evNarativnoIzvjesceService;

	@Autowired
	private ReportingPeriodsService reportingPeriodsService;

	@Autowired
	private UsersService usersService;

	@Autowired
	private UserRolePeriodStatusService userRolePeriodStatusService;

	@Autowired
	private PopisEvidencijaView popisEvidencijaView;

	@Autowired
	private ReportingPeriodsService reportingPeriodService;

	@Autowired
	private StatusRolePeriodService statusRolePeriodService;

	@Autowired
	private UserRoleRepository userRoleRepository;
	
	@Autowired
	private MenuController menuController;

	private SessionBean session;
	private List<ReportingPeriod> reportingPeriods;
	private Date dateFrom;
	private Date dateTo;
	private BigDecimal approvedResources;
	private User user = new User();
	private UserData userData = new UserData();
	private Evaluator evaluator = new Evaluator();
	private EvSifarnik lovObjectTipRazdoblja;
	private String akcijaPrka;
	private String naslovAkcijaPrk = "";

	@PostConstruct
	public void init() {

		reportingPeriods = reportingPeriodsService.getReportingPeriods(session
				.getCurrentProject());
	}

	public void postaviAll() {

		popisEvidencijaView.init();

	}

	public List<ReportingPeriod> getReportingPeriods() {
		reportingPeriods = reportingPeriodsService.getReportingPeriods(session
				.getCurrentProject());

		// System.out.println("---------> getam periods" +
		// reportingPeriods.size());

		return reportingPeriods;
	}

	public void insert() {

		try {

			java.sql.Timestamp maxTimestampDateTo = null;
			java.util.Date maxUtilDateTo = null;

			maxTimestampDateTo = reportingPeriodsService
					.findMaxDateFrom(session.getCurrentProject());

			if (maxTimestampDateTo != null)
				maxUtilDateTo = new Date(maxTimestampDateTo.getTime());

			if (dateFrom.compareTo(dateTo) > 0) {
				PfUtils.error("Unos razdoblja",
						"Datum do ne može biti manji od datuma od");
				// } else if (maxTimestampDateTo != null &&
				// dateFrom.compareTo(maxUtilDateTo) < 0){
				// PfUtils.error("Unos razdoblja",
				// "Datum od postoji u intervalima definirniranih razdoblja");
			} else {

				ReportingPeriod reportingPeriod = new ReportingPeriod();

				reportingPeriod.setDateFrom(new java.sql.Timestamp(dateFrom
						.getTime()));
				reportingPeriod.setDateTo(new java.sql.Timestamp(dateTo
						.getTime()));
				reportingPeriod.setApprovedResources(approvedResources);
				reportingPeriod
						.setTipRazdoblja(lovObjectTipRazdoblja.getCode());
				reportingPeriod.setStatus("1");
				if ("4".equals(lovObjectTipRazdoblja.getCode())) {
					reportingPeriod.setIsFinal("D");
				} else {
					reportingPeriod.setIsFinal("N");
				}

				Integer orN = reportingPeriodsService.findMaxOrdNum(session
						.getCurrentProject());

				if (orN == null)
					orN = 1;
				else
					orN = orN + 1;

				reportingPeriod.setOrdinalNumber(orN);

				reportingPeriod.setProject(session.getCurrentProject());
				// kreiranje liste svih 15 tablica
				reportingPeriod
						.setEvidencijaList(kreiranjeListeEvedencija(reportingPeriod)); 
				
				//defaultni status 
				reportingPeriod.setStatusRazdoblja(new StatusRazdoblja(31));
				
				reportingPeriodsService.saveReportingPeriod(reportingPeriod);

				UserRolePeriodStatus urps = new UserRolePeriodStatus();

				// unos novog perioda za sve članove COV u radnu tablicu
				// UserRolePeriodStatus koja prati promjene statusa za tog usera
				List<UserRole> userRoleCOV = userRoleRepository
						.findByRole(new Role(4));
				for (UserRole userRole : userRoleCOV) {

					UserRolePeriodPK urpPK = new UserRolePeriodPK();
					urpPK.setPeriodId(reportingPeriod.getId());
					urpPK.setRoleId(new Role(4).getId());
					urpPK.setUserId(userRole.getUser().getId());

					urps.setId(urpPK);
					urps.setStatusRazdoblja(new StatusRazdoblja(11));
					urps.setVrijemePromjene(new Timestamp(System
							.currentTimeMillis()));
					urps.setUser(userRole.getUser());
					urps.setPeriod(reportingPeriod);
					urps.setRole(new Role(4));

					userRolePeriodStatusService.saveUserRolePeriodStatus(urps);

				}

				// unos novog perioda za sve članove CUO u radnu tablicu
				// UserRolePeriodStatus koja prati promjene statusa za tog usera
				List<UserRole> userRoleCUO = userRoleRepository
						.findByRole(new Role(5));
				for (UserRole userRole : userRoleCUO) {

					UserRolePeriodPK urpPK = new UserRolePeriodPK();
					urpPK.setPeriodId(reportingPeriod.getId());
					urpPK.setRoleId(new Role(5).getId());
					urpPK.setUserId(userRole.getUser().getId());

					urps.setId(urpPK);
					urps.setStatusRazdoblja(new StatusRazdoblja(20));
					urps.setVrijemePromjene(new Timestamp(System
							.currentTimeMillis()));
					urps.setUser(userRole.getUser());
					urps.setPeriod(reportingPeriod);
					urps.setRole(new Role(5));

					userRolePeriodStatusService.saveUserRolePeriodStatus(urps);

				}

				// unos novog perioda u radnu tablicu StatusRolePeriod koja
				// prati promjene statusa pojedinog dijela perioda
				// (dio je, u biti, podijeljen po rolama)

				RolePeriodPK rpPK = new RolePeriodPK();
				rpPK.setPeriodId(reportingPeriod.getId());

				StatusRolePeriod srp = new StatusRolePeriod();
				srp.setPeriod(reportingPeriod);

				// unos za praćenje statusa evidencija
				rpPK.setRoleId(2);

				srp.setId(rpPK);
				srp.setRole(new Role(2));
				srp.setStatusRazdoblja(new StatusRazdoblja(31));
				srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				statusRolePeriodService.save(srp);

				// unos za praćenje statusa evaluatora
				rpPK.setRoleId(3);

				srp.setId(rpPK);
				srp.setRole(new Role(3));
				srp.setStatusRazdoblja(new StatusRazdoblja(1));
				srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				statusRolePeriodService.save(srp);

				// unos za praćenje statusa COV
				rpPK.setRoleId(4);

				srp.setId(rpPK);
				srp.setRole(new Role(4));
				srp.setStatusRazdoblja(new StatusRazdoblja(11));
				srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				statusRolePeriodService.save(srp);

				// unos za praćenje statusa CUO
				rpPK.setRoleId(5);

				srp.setId(rpPK);
				srp.setRole(new Role(5));
				srp.setStatusRazdoblja(new StatusRazdoblja(20));
				srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				statusRolePeriodService.save(srp);

				// unos za praćenje statusa financiranja
				rpPK.setRoleId(1);

				srp.setId(rpPK);
				srp.setRole(new Role(1));
				srp.setStatusRazdoblja(new StatusRazdoblja(28));
				srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				statusRolePeriodService.save(srp);

				setDateFrom(null);
				setDateTo(null);
				setApprovedResources(null);

				PfUtils.info("Unos razdoblja", "Razdoblje uspješno uneseno.");
			}
		} catch (Exception e) {

			PfUtils.info("Greška!", "Greška kod unosa razdoblja!");
			e.printStackTrace();
		}

	}

	/*
	 * kreiranjeListeEvedencija -funkcija za kreiranje liste tablica/evidencija
	 */
	private List<Evidencija> kreiranjeListeEvedencija(
			ReportingPeriod reportingPeriod) {

		List<Evidencija> list = new ArrayList<Evidencija>();

		for (int i = 0; i < ConstantsModel.titleArea.length; i++) {

			Evidencija e = new Evidencija();

			e.setTitle(ConstantsModel.titleArea[i]);
			e.setNazivXhtml(ConstantsModel.nazivXhtmlArea[i]);
			e.setOpis(ConstantsModel.opisArea[i]);

			e.setStatusPrk(0);
			e.setKomentarPrk("test");
			e.setStatusEvidencija(new StatusEvidencija(1, "Neaktivno"));
			e.setReportingPeriod(reportingPeriod);

			list.add(e);
		}

		return list;
	}

	public boolean prikazAkcijaPrka(String rola) {
		if (session.hasRoles(new String[] { "PRK" })) {
			if (rola.equals(session.getCurrentReportingPeriod().getStatusRazdoblja().getRole())) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public void potvrdiEvidencije(String daNe) {

		if (session.getCurrentReportingPeriod() == null) {
			PfUtils.error("Greška!",
					"Morate prvo odabrati izvještajno razdoblje!");
			return;
		}
		// provjera polja u reporting periodu, iznosi jesu li popunjeni
		// if(session.getCurrentReportingPeriod().getNeutroseno()==null ||
		// "".equals(session.getCurrentReportingPeriod().getNeutroseno())){
		//
		// }

		RolePeriodPK rpPK = new RolePeriodPK();
		rpPK.setPeriodId(session.getCurrentReportingPeriod().getId());

		StatusRolePeriod srp = new StatusRolePeriod();
		srp.setPeriod(session.getCurrentReportingPeriod());

		// unos za praćenje statusa evidencija
		rpPK.setRoleId(2);
		srp.setId(rpPK);
		srp.setRole(new Role(2));

		if("D".equals(daNe)){
			srp.setStatusRazdoblja(new StatusRazdoblja(35));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(35));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("VOD");
		} else{
			srp.setStatusRazdoblja(new StatusRazdoblja(36));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(36));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("VOD");
		}
		srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));

		statusRolePeriodService.save(srp);
		reportingPeriodsService.saveReportingPeriod(session.getCurrentReportingPeriod());
		
		if("D".equals(daNe)){
			PfUtils.info("Potvrda", "Evidencije su potvrđene!");
		}else{
			PfUtils.info("Potvrda", "Evidencije nisu potvrđene!");
		}		
	}

	public void potvrdiEvaluacije(String daNe) {

		if (session.getCurrentReportingPeriod() == null) {
			PfUtils.error("Greška!",
					"Morate prvo odabrati izvještajno razdoblje!");
			return;
		}

		RolePeriodPK rpPK = new RolePeriodPK();
		rpPK.setPeriodId(session.getCurrentReportingPeriod().getId());

		StatusRolePeriod srp = new StatusRolePeriod();
		srp.setPeriod(session.getCurrentReportingPeriod());

		// unos za praćenje statusa evidencija
		rpPK.setRoleId(3);
		srp.setId(rpPK);
		srp.setRole(new Role(3));

		if("D".equals(daNe)){
			srp.setStatusRazdoblja(new StatusRazdoblja(9));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(9));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("EVL");
		} else{
			srp.setStatusRazdoblja(new StatusRazdoblja(10));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(10));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("EVL");
		}
		srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));

		statusRolePeriodService.save(srp);
		reportingPeriodsService.saveReportingPeriod(session.getCurrentReportingPeriod());
		if("D".equals(daNe)){
			PfUtils.info("Potvrda", "Vrednovanje (evaluacija) je potvrđena!");
		}else{
			PfUtils.info("Potvrda", "Vrednovanje (evaluacija) nije potvrđena!");
		}		
	}

	public void potvrdiCOV(String daNe) {

		if (session.getCurrentReportingPeriod() == null) {
			PfUtils.error("Greška!",
					"Morate prvo odabrati izvještajno razdoblje!");
			return;
		}

		RolePeriodPK rpPK = new RolePeriodPK();
		rpPK.setPeriodId(session.getCurrentReportingPeriod().getId());

		StatusRolePeriod srp = new StatusRolePeriod();
		srp.setPeriod(session.getCurrentReportingPeriod());

		// unos za praćenje statusa evidencija
		rpPK.setRoleId(4);
		srp.setId(rpPK);
		srp.setRole(new Role(4));

		if("D".equals(daNe)){
			srp.setStatusRazdoblja(new StatusRazdoblja(18));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(18));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("COV");
		} else{
			srp.setStatusRazdoblja(new StatusRazdoblja(19));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(19));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("COV");
		}
		srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));

		statusRolePeriodService.save(srp);
		reportingPeriodsService.saveReportingPeriod(session.getCurrentReportingPeriod());
		if("D".equals(daNe)){
			PfUtils.info("Potvrda", "Odbor za vrednovanje je potvrdio izvješća!");
		}else{
			PfUtils.info("Potvrda", "Odbor za vrednovanje nije potvrdio izvješća!");
		}		
	}

	public void potvrdiCUO(String daNe) {

		if (session.getCurrentReportingPeriod() == null) {
			PfUtils.error("Greška!",
					"Morate prvo odabrati izvještajno razdoblje!");
			return;
		}

		RolePeriodPK rpPK = new RolePeriodPK();
		rpPK.setPeriodId(session.getCurrentReportingPeriod().getId());

		StatusRolePeriod srp = new StatusRolePeriod();
		srp.setPeriod(session.getCurrentReportingPeriod());

		// unos za praćenje statusa evidencija
		rpPK.setRoleId(5);
		srp.setId(rpPK);
		srp.setRole(new Role(5));

		if("D".equals(daNe)){
			srp.setStatusRazdoblja(new StatusRazdoblja(26));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(26));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("CUO");
		} else{
			srp.setStatusRazdoblja(new StatusRazdoblja(27));
			session.getCurrentReportingPeriod().setStatusRazdoblja(new StatusRazdoblja(27));
			session.getCurrentReportingPeriod().getStatusRazdoblja().setRole("CUO");
		}
		srp.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));

		statusRolePeriodService.save(srp);
		reportingPeriodsService.saveReportingPeriod(session.getCurrentReportingPeriod());
		if("D".equals(daNe)){
			PfUtils.info("Potvrda", "Upravni odbor je potvrdio razdoblje!");
		}else{
			PfUtils.info("Potvrda", "Upravni odbor nije potvrdio razdoblje!");
		}		
	}

	public void setAkcijaPRK(String rola) {
		setAkcijaPrka(rola);
	}

	public void izvrsiAkcijuPRKDa() {
		if ("VOD".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiEvidencije("D");
		} else if ("EVL".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiEvaluacije("D");
		} else if ("COV".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiCOV("D");
		} else if ("CUO".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiCUO("D");
		}
		RequestContext.getCurrentInstance().execute("PF('prkAkcijaDialogWidget').hide();");
		menuController.goToPopisEvidencija();
	}

	public void izvrsiAkcijuPRKNe() {
		if ("VOD".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiEvidencije("N");
		} else if ("EVL".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiEvaluacije("N");
		} else if ("COV".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiCOV("N");
		} else if ("CUO".equals(popisEvidencijaView.akcijaPrka)) {
			potvrdiCUO("N");
		}
		RequestContext.getCurrentInstance().execute("PF('prkAkcijaDialogWidget').hide();");
		menuController.goToPopisEvidencija();
	}

	public BigDecimal getApprovedResources() {
		return approvedResources;
	}

	public void setApprovedResources(BigDecimal approvedResources) {
		this.approvedResources = approvedResources;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public UsersService getUsersService() {
		return usersService;
	}

	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	public UserRolePeriodStatusService getUserRolePeriodService() {
		return userRolePeriodStatusService;
	}

	public void setUserRolePeriodService(
			UserRolePeriodStatusService userRolePeriodService) {
		this.userRolePeriodStatusService = userRolePeriodService;
	}

	public String conv2UtilDate(java.sql.Timestamp timestamp) {
		return new SimpleDateFormat(PfUtils.getOutputDateFormat())
				.format(timestamp);
	}

	public ReportingPeriodsService getReportingPeriodService() {
		return reportingPeriodService;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}

	public PopisEvidencijaView getPopisEvidencijaView() {
		return popisEvidencijaView;
	}

	public void setPopisEvidencijaView(PopisEvidencijaView popisEvidencijaView) {
		this.popisEvidencijaView = popisEvidencijaView;
	}

	public Evaluator getEvaluator() {
		return evaluator;
	}

	public void setEvaluator(Evaluator evaluator) {
		this.evaluator = evaluator;
	}

	public EvNarativnoIzvjesceService getEvNarativnoIzvjesceService() {
		return evNarativnoIzvjesceService;
	}

	public void setEvNarativnoIzvjesceService(
			EvNarativnoIzvjesceService evNarativnoIzvjesceService) {
		this.evNarativnoIzvjesceService = evNarativnoIzvjesceService;
	}

	public EvidencijaService getEvidencijaService() {
		return evidencijaService;
	}

	public void setEvidencijaService(EvidencijaService evidencijaService) {
		this.evidencijaService = evidencijaService;
	}

	public EvSifarnik getLovObjectTipRazdoblja() {
		return lovObjectTipRazdoblja;
	}

	public void setLovObjectTipRazdoblja(EvSifarnik lovObjectTipRazdoblja) {
		this.lovObjectTipRazdoblja = lovObjectTipRazdoblja;
	}

	public ReportingPeriodsService getReportingPeriodsService() {
		return reportingPeriodsService;
	}

	public void setReportingPeriodsService(
			ReportingPeriodsService reportingPeriodsService) {
		this.reportingPeriodsService = reportingPeriodsService;
	}

	public SessionBean getSession() {
		return session;
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public void setReportingPeriods(List<ReportingPeriod> reportingPeriod) {
		this.reportingPeriods = reportingPeriod;
	}

	public String getAkcijaPrka() {
		return akcijaPrka;
	}

	public void setAkcijaPrka(String akcijaPrka) {
		if ("VOD".equals(akcijaPrka)) {
			setNaslovAkcijaPrk("Želite li uputiti izvješće i evidencije vrednovateljima?");
		} else if ("EVL".equals(akcijaPrka)) {
			setNaslovAkcijaPrk("Želite li uputiti izvješće članovima odbora za vrednovanje?");
		} else if ("COV".equals(akcijaPrka)) {
			setNaslovAkcijaPrk("Želite li uputiti izvješće članovima Upravnog odbora?");
		} else if ("CUO".equals(akcijaPrka)) {
			setNaslovAkcijaPrk("Želite li prihvatiti izvješće za razdoblje?");
		}
		popisEvidencijaView.akcijaPrka = akcijaPrka;
	}

	public String getNaslovAkcijaPrk() {
		return naslovAkcijaPrk;
	}

	public void setNaslovAkcijaPrk(String naslovAkcijaPrk) {
		this.naslovAkcijaPrk = naslovAkcijaPrk;
	}

}
