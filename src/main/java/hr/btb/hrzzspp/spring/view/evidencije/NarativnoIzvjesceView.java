package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvIstrazivackaSkupina;
import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Rezultat;
import hr.btb.hrzzspp.spring.jasper.ReportBean;
import hr.btb.hrzzspp.spring.services.EvIstrazivackaSkupinaService;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;
import hr.btb.hrzzspp.spring.services.ProjectsService;
import hr.btb.hrzzspp.spring.services.RezultatService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.UploadIzvjescaView;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.event.ActionEvent;

import org.primefaces.component.commandbutton.CommandButton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class NarativnoIzvjesceView {

	private static final Logger LOG = LoggerFactory
			.getLogger(NarativnoIzvjesceView.class);

	@Autowired
	private ReportBean rpt;
	private SessionBean session;

	private Project project;
	private ReportingPeriod reportingPeriod;
	private EvNarativnoIzvjesce evNarativnoIzvjesce;

	@Autowired
	private EvNarativnoIzvjesceService evNarativnoIzvjesceService;
	@Autowired
	private ProjectsService projectsService;
	@Autowired
	private EvIstrazivackaSkupinaService evIstrazivackaSkupinaService;
	@Autowired
	private RezultatService rezultatService;
	
	@Autowired
	private UploadIzvjescaView uploadIzvjescaView;

	SimpleDateFormat datumFormat = new SimpleDateFormat("dd.MM.yyyy");
	String date = datumFormat.format(new Date());

	private List<Rezultat> listRezultati;

	private List<EvIstrazivackaSkupina> listEvIstrazivackaSkupina;

	private String evIstrazivackaGrupaClanovi;
	private String evRezultatBrIzvjesce;
	private String evRezultatOstvarenoIzvjesce;
	private String evIstrazivackaGrupaClanoviArea;
	private String evIstrazivackaGrupaUloga;

	@PostConstruct
	public void init() {
		project = session.getCurrentProject();
		reportingPeriod = session.getCurrentReportingPeriod();
		listRezultati = rezultatService
				.getRezultatiByReportingPeriod(reportingPeriod);
		listEvIstrazivackaSkupina = evIstrazivackaSkupinaService
				.getEvIstrazivackaSkupinaByPeriod(reportingPeriod);
		evNarativnoIzvjesce = evNarativnoIzvjesceService
				.getEvNarativnoIzvjesceByPeriod(reportingPeriod);

		if (evNarativnoIzvjesce == null) {
			evNarativnoIzvjesce = new EvNarativnoIzvjesce();
		}
		evNarativnoIzvjesce.setDatumMjesto(date);
	}
	

	public void executeAndView() {

		project = projectsService.getProjectById(project.getId());
		session.setCurrentProject(project);
		
		if (nisuPopunjenaSvaPolja(project, evNarativnoIzvjesce)) {
			PfUtils.warn("Sva polja moraju biti popunjena prije kreiranju PDF-a.");
			return;
		}
		
		// rpt = new ReportBean();

		rpt.setReportTitle("Opisno izvješće");

		Map<String, Object> map = new HashMap<String, Object>();
		map.put("program", project.getTenderType().getDescription());
		map.put("brojPr", project.getCode());
		map.put("sredstva", dajLepiRvackiFormatPliz(reportingPeriod.getApprovedResources()));
		map.put("voditelj", project.getLeader().getUserData().getName() + " "
				+ project.getLeader().getUserData().getSurname());
		map.put("ustanova", project.getInstitutionId().getName());
		map.put("celnik", project.getCelnikUstanove());
		map.put("naziv", project.getName());
		map.put("akronim", project.getAcronym());
		map.put("znPodrucje", project.getMajorScientificField().getName());
		map.put("znPolje", project.getScientificField().getName());
		map.put("poveznica", project.getPoveznicaMreza());
		map.put("programRok", project.getTenderDeadline());
		map.put("trajanje", project.getDuration());
		map.put("razdoblje", reportingPeriod.getOrdinalNumber());
		map.put("razdobljeDatum", session.getRpDateFromFormat() + " - "
				+ session.getRpDateToFormat());
		map.put("grupaClanovi", getEvIstrazivackaGrupaClanovi());
		map.put("evRezultatOstvarenoIzvjesce", getEvRezultatOstvarenoIzvjesce());
		map.put("evRezultatBrIzvjesce", getEvRezultatBrIzvjesce());
		map.put("evIstrazivackaGrupaClanoviArea",
				getEvIstrazivackaGrupaClanoviArea());
		map.put("evIstrazivackaGrupaUloga", getEvIstrazivackaGrupaUloga());
		map.put("summaryHR", project.getSummaryHR());
		map.put("ppOdstupanje", evNarativnoIzvjesce.getPpOdstupanje());
		map.put("ppObjasnjenje", evNarativnoIzvjesce.getPpObjasnjenje());
		map.put("ppNovaPitanja", evNarativnoIzvjesce.getPpNovaPitanja());
		map.put("ppPreporuke", evNarativnoIzvjesce.getPpPreporuke());
		map.put("ppPotpora", evNarativnoIzvjesce.getPpPotpora());
		map.put("igAktivnosti", evNarativnoIzvjesce.getIgAktivnosti());
		map.put("igPromjena", evNarativnoIzvjesce.getIgPromjena());
		map.put("osPrijetnje", evNarativnoIzvjesce.getOsPrijetnje());
		map.put("osPoduzimanja", evNarativnoIzvjesce.getOsPoduzimanja());
		map.put("datumMjesto", evNarativnoIzvjesce.getDatumMjesto());

		rpt.setReportParameters(map);

		rpt.executeAndView();
		
		evNarativnoIzvjesce.setKreiranPdf(1);
		evNarativnoIzvjesce.setVrijemeKreiranjaPdf(new Timestamp(System.currentTimeMillis()));
		evNarativnoIzvjesceService.saveEvNarativnoIzvjesce(evNarativnoIzvjesce);
	}


	public static boolean nisuPopunjenaSvaPolja(Project p_project, EvNarativnoIzvjesce p_evNarativnoIzvjesce) {
		return p_project.getCelnikUstanove() == null ? true : "".equals(p_project.getCelnikUstanove().trim())
				|| p_project.getPoveznicaMreza() == null ? true : "".equals(p_project.getPoveznicaMreza().trim())
				|| p_project.getDuration() == null ? true : "".equals(p_project.getDuration())
				|| p_project.getSummaryHR() == null ? true : "".equals(p_project.getSummaryHR().trim())		
				|| p_evNarativnoIzvjesce.getPpOdstupanje() == null ? true : "".equals(p_evNarativnoIzvjesce.getPpOdstupanje().trim())
				|| p_evNarativnoIzvjesce.getPpObjasnjenje() == null ? true : "".equals(p_evNarativnoIzvjesce.getPpObjasnjenje().trim())
				|| p_evNarativnoIzvjesce.getPpNovaPitanja() == null ? true : "".equals(p_evNarativnoIzvjesce.getPpNovaPitanja().trim())
				|| p_evNarativnoIzvjesce.getPpPotpora() == null ? true : "".equals(p_evNarativnoIzvjesce.getPpPotpora().trim())
				|| p_evNarativnoIzvjesce.getPpPreporuke() == null ? true : "".equals(p_evNarativnoIzvjesce.getPpPreporuke().trim())
				|| p_evNarativnoIzvjesce.getIgAktivnosti() == null ? true : "".equals(p_evNarativnoIzvjesce.getIgAktivnosti().trim())
				|| p_evNarativnoIzvjesce.getIgPromjena() == null ? true : "".equals(p_evNarativnoIzvjesce.getIgPromjena().trim())
				|| p_evNarativnoIzvjesce.getOsPrijetnje() == null ? true : "".equals(p_evNarativnoIzvjesce.getOsPrijetnje().trim())
				|| p_evNarativnoIzvjesce.getOsPoduzimanja() == null ? true : "".equals(p_evNarativnoIzvjesce.getOsPoduzimanja().trim());
	}

	private String dajLepiRvackiFormatPliz(BigDecimal approvedResources) {
		String ovoIdeVan1="";
		String ovoIdeVan2="";
		String aOvoIdeVanStvarno="";
		String lepFormat = approvedResources.toString();
		lepFormat = lepFormat.replace('.', ',');
		aOvoIdeVanStvarno=lepFormat;
		if(lepFormat.length()>6){
			ovoIdeVan1=lepFormat.substring(lepFormat.length()-6, lepFormat.length());
			ovoIdeVan1="."+ovoIdeVan1;
			ovoIdeVan1=lepFormat.substring(0, lepFormat.length()-6)+ovoIdeVan1;
			aOvoIdeVanStvarno=ovoIdeVan1;
			if(ovoIdeVan1.length()>10){
				ovoIdeVan2=ovoIdeVan1.substring(ovoIdeVan1.length()-10, ovoIdeVan1.length());
				ovoIdeVan2="."+ovoIdeVan2;
				ovoIdeVan2=ovoIdeVan1.substring(0, ovoIdeVan1.length()-10)+ovoIdeVan2;
				aOvoIdeVanStvarno=ovoIdeVan2;
			}
		}
		
		return aOvoIdeVanStvarno;
	}


	public void save(ActionEvent actionEvent) {
		
		try {

			EvNarativnoIzvjesce evNarIzvReload = evNarativnoIzvjesceService.getEvNarativnoIzvjesceByPeriod(reportingPeriod);
			if(evNarIzvReload!=null && evNarIzvReload.getEvNiFiles()!=null){
				List<EvNiFile> listUploadFiles=evNarIzvReload.getEvNiFiles();
				if(listUploadFiles==null || listUploadFiles.size()==0){
					if(!(evNarativnoIzvjesce.getEvNiFiles()!=null && evNarativnoIzvjesce.getEvNiFiles().size()>0)){
						evNarativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
						evNarativnoIzvjesce.getEvNiFiles().clear();
						evNarativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
					}
				}else{
					evNarativnoIzvjesce.setEvNiFiles(listUploadFiles);
				}
			}else{
				if(!(evNarativnoIzvjesce.getEvNiFiles()!=null && evNarativnoIzvjesce.getEvNiFiles().size()>0)){
					evNarativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
					evNarativnoIzvjesce.getEvNiFiles().clear();
					evNarativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
				}
			}
			evNarativnoIzvjesce.setReportingPeriod(reportingPeriod);
			evNarativnoIzvjesceService
					.saveEvNarativnoIzvjesce(evNarativnoIzvjesce);
			projectsService.saveProject(project);
			
			String im = "Podaci uspješno spremljeni";
			
			if (((CommandButton)actionEvent.getComponent()).getId().equals("callJasper"))
				PfUtils.info("Kreiranje izvješća",im + ", pričekajte kreiranje izvješća");
			else
				PfUtils.info("Unos opisnog izvješća",im);
			
			//ponovno učitavanje narativnog izvješća nakon spremanja
			uploadIzvjescaView.init();
			
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja opisnog izvješća");
		}
	}

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}

	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}

	public EvNarativnoIzvjesce getEvNarativnoIzvjesce() {
		return evNarativnoIzvjesce;
	}

	public void setEvNarativnoIzvjesce(EvNarativnoIzvjesce evNarativnoIzvjesce) {
		this.evNarativnoIzvjesce = evNarativnoIzvjesce;
	}

	@Autowired
	public void setEvNarativnoIzvjesceService(
			EvNarativnoIzvjesceService evNarativnoIzvjesceService) {
		this.evNarativnoIzvjesceService = evNarativnoIzvjesceService;
	}

	@Autowired
	public void setProjectsService(ProjectsService projectsService) {
		this.projectsService = projectsService;
	}

	public String getEvIstrazivackaGrupaClanovi() {
		StringBuffer sb = new StringBuffer();
		for (EvIstrazivackaSkupina evIstrazivacka : listEvIstrazivackaSkupina) {
			sb.append(evIstrazivacka.getImePrezime());
			sb.append(", ");
		}
		return sb.length() > 2 ? sb.toString().substring(0, sb.length() - 2)
				: "";
	}

	public void setEvIstrazivackaGrupaClanovi(String evIstrazivackaGrupaClanovi) {
		this.evIstrazivackaGrupaClanovi = evIstrazivackaGrupaClanovi;
	}

	@Autowired
	public void setEvIstrazivackaGrupaService(
			EvIstrazivackaSkupinaService evIstrazivackaSkupinaService) {
		this.evIstrazivackaSkupinaService = evIstrazivackaSkupinaService;
	}

	@Autowired
	public void setRezultatService(RezultatService rezultatService) {
		this.rezultatService = rezultatService;
	}

	public String getEvRezultatBrIzvjesce() {
		StringBuffer sb = new StringBuffer();
		for (Rezultat rezultat : listRezultati) {
			sb.append(rezultat.getBrCilja() + " " + rezultat.getBrRezultata()
					+ " " + rezultat.getNaziv());
			sb.append("\n");
		}
		return sb.length() > 1 ? sb.toString().substring(0, sb.length() - 1)
				: "";
	}

	public void setEvRezultatBrIzvjesce(String evRezultatBrIzvjesce) {
		this.evRezultatBrIzvjesce = evRezultatBrIzvjesce;
	}

	public String getEvRezultatOstvarenoIzvjesce() {
		StringBuffer sb = new StringBuffer();
		for (Rezultat rezultat : listRezultati) {
			sb.append(rezultat.getPostignuto().getDescription());
			sb.append("\n");
		}
		return sb.length() > 1 ? sb.toString().substring(0, sb.length() - 1)
				: "";
	}

	public void setEvRezultatOstvarenoIzvjesce(
			String evRezultatOstvarenoIzvjesce) {
		this.evRezultatOstvarenoIzvjesce = evRezultatOstvarenoIzvjesce;
	}

	public List<Rezultat> getListRezultati() {
		return listRezultati;
	}

	public void setListRezultati(List<Rezultat> listRezultati) {
		this.listRezultati = listRezultati;
	}

	public List<EvIstrazivackaSkupina> getListEvIstrazivackaSkupina() {
		return listEvIstrazivackaSkupina;
	}

	public void setListEvIstrazivackaSkupina(
			List<EvIstrazivackaSkupina> listEvIstrazivackaSkupina) {
		this.listEvIstrazivackaSkupina = listEvIstrazivackaSkupina;
	}

	public String getEvIstrazivackaGrupaClanoviArea() {
		StringBuffer sb = new StringBuffer();
		for (EvIstrazivackaSkupina evIstrazivacka : listEvIstrazivackaSkupina) {
			sb.append(evIstrazivacka.getImePrezime());
			sb.append("\n");
		}
		return sb.length() > 1 ? sb.toString().substring(0, sb.length() - 1)
				: "";
	}

	public void setEvIstrazivackaGrupaClanoviArea(
			String evIstrazivackaGrupaClanoviArea) {
		this.evIstrazivackaGrupaClanoviArea = evIstrazivackaGrupaClanoviArea;
	}

	public String getEvIstrazivackaGrupaUloga() {
		StringBuffer sb = new StringBuffer();
		for (EvIstrazivackaSkupina evIstrazivacka : listEvIstrazivackaSkupina) {
			sb.append(evIstrazivacka.getUloga());
			sb.append("\n");
		}
		return sb.length() > 1 ? sb.toString().substring(0, sb.length() - 1)
				: "";
	}

	public void setEvIstrazivackaGrupaUloga(String evIstrazivackaGrupaUloga) {
		this.evIstrazivackaGrupaUloga = evIstrazivackaGrupaUloga;
	}


	public UploadIzvjescaView getUploadIzvjescaView() {
		return uploadIzvjescaView;
	}


	public void setUploadIzvjescaView(UploadIzvjescaView uploadIzvjescaView) {
		this.uploadIzvjescaView = uploadIzvjescaView;
	}
}
	
