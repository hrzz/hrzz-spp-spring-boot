package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvOstalo;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.spring.services.EvOstaloService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("session")
public class OstaloView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -575782517760332151L;

	private static final Logger LOG = LoggerFactory
			.getLogger(OstaloView.class);
	
	private static final String TABLE_NAME = "Tablica 14";
	
	private EvOstalo odabranaEvOstalo = new EvOstalo();
	private EvOstalo novaEvOstalo;
	private EvSifarnik lovObject;
	
	SessionBean session;
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}
	
	@Autowired
	private EvOstaloService evOstaloService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public OstaloView() {
		
	}
	
	@PostConstruct
	public void init(){
		this.novaEvOstalo = new EvOstalo();
	}
	
	public List<EvOstalo> fillTable(){
		return evOstaloService.getEvOstaloByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvOstalo);
			evOstaloService.saveOstalo(odabranaEvOstalo);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	
	
	public void insert(){
		
		try {

			novaEvOstalo.setVrsta(lovObject.getCode());
			novaEvOstalo.setReportingPeriod(session.getCurrentReportingPeriod());
			evOstaloService.saveOstalo(novaEvOstalo);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void update() {

		try {

			odabranaEvOstalo.setVrsta(lovObject.getCode());
			evOstaloService.saveOstalo(odabranaEvOstalo);
			PfUtils.dialogUpdateSuccessful();

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	public void setDelete(EvOstalo selected) {
		evOstaloService.deleteOstalo(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	
	
	public void clean(){
		novaEvOstalo = new EvOstalo();
	}
	
	public void onRowSelect(SelectEvent event){
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvOstalo = (EvOstalo) event.getObject();
		LOG.debug("... odabranaEvOstalo: {}", odabranaEvOstalo);	
	}
	
	public void setCurrentRow(EvOstalo selected) {

		this.lovObject = findByKey("ev_ostalo","vrsta",selected.getVrsta());
		
		odabranaEvOstalo = selected;
	}


	public EvOstalo getOdabranaEvOstalo() {
		return odabranaEvOstalo;
	}

	public void setOdabranaEvOstalo(EvOstalo odabranaEvOstalo) {
		this.odabranaEvOstalo = odabranaEvOstalo;
	}

	@Autowired
	public void setEvOstaloService(EvOstaloService evOstaloService) {
		this.evOstaloService = evOstaloService;
	}

	public EvOstalo getNovaEvOstalo() {
		return novaEvOstalo;
	}

	public void setNovaEvOstalo(EvOstalo novaEvOstalo) {
		this.novaEvOstalo = novaEvOstalo;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

}