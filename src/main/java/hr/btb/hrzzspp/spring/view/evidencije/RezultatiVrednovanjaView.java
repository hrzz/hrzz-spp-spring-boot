package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.VrednovanjeRezIzvjesce;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.services.VrednovanjeRezIzvjesceService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class RezultatiVrednovanjaView implements Serializable{

	private static final long serialVersionUID = 3740630683279263379L;
	
	private static final Logger LOG = LoggerFactory
			.getLogger(RezultatiVrednovanjaView.class);
	
	@Autowired
	SessionBean sessionBean;
	
	private Project project;
	
	private ReportingPeriod reportingPeriod;
	private VrednovanjeRezIzvjesce selectedRezVrednovanjaIzv;
	
	@Autowired
	private VrednovanjeRezIzvjesceService rezultatiVrednovanjaIzvjService;

	@Autowired
	private PopisEvidencijaView popisEvidencijaView;
	
	@PostConstruct
	public void init(){
		selectedRezVrednovanjaIzv = popisEvidencijaView.getVrednovanjeRezIzvjesce();
		setProject(sessionBean.getCurrentProject());
//		reportingPeriod = sessionBean.setCurrentReportingPeriod();
	}
	
	public void save(){
		try{
			selectedRezVrednovanjaIzv.setPeriod(sessionBean.getCurrentReportingPeriod());
			selectedRezVrednovanjaIzv.setUser(sessionBean.getCurrentUserData().getUser());
			selectedRezVrednovanjaIzv.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
			
			rezultatiVrednovanjaIzvjService.saveRezultatiVrednovanjaIzvj(selectedRezVrednovanjaIzv);
			PfUtils.dialogInsertSuccessful();
		}catch(Exception e){
			e.printStackTrace();
			PfUtils.error("Greška", "Greška prilikom spremanja izvješća");
		}
		clean();
	}
	
	public void clean() {
		selectedRezVrednovanjaIzv = new VrednovanjeRezIzvjesce();
	}

	public ReportingPeriod getReportingPeriod() {
		return reportingPeriod;
	}
	public void setReportingPeriod(ReportingPeriod reportingPeriod) {
		this.reportingPeriod = reportingPeriod;
	}
	public VrednovanjeRezIzvjesce getSelectedEvRezVrednovanjaIzv() {
		return selectedRezVrednovanjaIzv;
	}
	public void setSelectedEvRezVrednovanjaIzv(
			VrednovanjeRezIzvjesce selectedEvRezVrednovanjaIzv) {
		this.selectedRezVrednovanjaIzv = selectedEvRezVrednovanjaIzv;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public PopisEvidencijaView getPopisEvidencijaView() {
		return popisEvidencijaView;
	}

	public void setPopisEvidencijaView(PopisEvidencijaView popisEvidencijaView) {
		this.popisEvidencijaView = popisEvidencijaView;
	}
	
}
