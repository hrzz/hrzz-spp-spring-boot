package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.EvSifarnik;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

@ManagedBean
@RequestScoped
public class SifarnikConverter implements Converter {

	@ManagedProperty(value = "#{selectOneMenu}")
	private SelectOneMenu menu;

	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {

		if (value != null && value.trim().length() > 0) {
			try {

				EvSifarnik ret = menu.getMapper().get(Integer.parseInt(value));

				return ret;
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "Conversion Error",
						"Not a valid theme."));
			}
		} else {
			return null;
		}
	}

	public String getAsString(FacesContext fc, UIComponent uic, Object object) {

		if (object != null) {
			return String.valueOf(((EvSifarnik) object).getId());
		} else {
			return null;
		}

	}

	public SelectOneMenu getMenu() {
		return menu;
	}

	public void setMenu(SelectOneMenu menu) {
		this.menu = menu;
	}
}
