package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvPopisDisAktivnosti;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.spring.services.EvPopisDisAktivnostiService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;


@Component
@Scope("session")
public class DiseminacijskeAktivnostiView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = 3549873376833191922L;
	private static final Logger LOG = LoggerFactory
			.getLogger(TockeProvjereView.class);
	
	private static final String TABLE_NAME = "Tablica 10";
	
	private EvPopisDisAktivnosti odabranaEvPopisDisAktivnosti = new EvPopisDisAktivnosti();
	private EvPopisDisAktivnosti novaEvPopisDisAktivnosti;
	private EvSifarnik lovObjectVrsta;
	private EvSifarnik lovObjectUloga;
	private EvSifarnik lovObjectVrstaPublike;
	
	private SessionBean session;
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}
	
	@Autowired
	private EvPopisDisAktivnostiService evPopisDisAktivnostiService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public DiseminacijskeAktivnostiView() {
		
	}
	
	@PostConstruct
	public void init(){
		this.novaEvPopisDisAktivnosti = new EvPopisDisAktivnosti();
	}
	
	public List<EvPopisDisAktivnosti> fillTable(){
		return evPopisDisAktivnostiService.getEvPopisDisAktivnostiByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void insert(){
		try{
			
		novaEvPopisDisAktivnosti.setReportingPeriod(session.getCurrentReportingPeriod());
		novaEvPopisDisAktivnosti.setVrsta(lovObjectVrsta.getCode());
		novaEvPopisDisAktivnosti.setUloga(lovObjectUloga.getCode());
		novaEvPopisDisAktivnosti.setVrstaPublike(lovObjectVrstaPublike.getCode());
	
		evPopisDisAktivnostiService.savePopisDisAktivnosti(novaEvPopisDisAktivnosti);
		
		//promijena statusa evidencije
		evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

		PfUtils.dialogInsertSuccessful();
		
		}catch(Exception e){
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}
	
	public void spremiPRK(){
		try{
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvPopisDisAktivnosti);
			evPopisDisAktivnostiService.savePopisDisAktivnosti(odabranaEvPopisDisAktivnosti);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	public void update() {

		try {
			odabranaEvPopisDisAktivnosti.setVrsta(lovObjectVrsta.getCode());
			odabranaEvPopisDisAktivnosti.setUloga(lovObjectUloga.getCode());
			odabranaEvPopisDisAktivnosti.setVrstaPublike(lovObjectVrstaPublike.getCode());
			
			evPopisDisAktivnostiService.savePopisDisAktivnosti(odabranaEvPopisDisAktivnosti);

			PfUtils.dialogUpdateSuccessful();
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	public void setDelete(EvPopisDisAktivnosti selected) {
		evPopisDisAktivnostiService.deletePopisDisAktivnosti(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	public void clean(){
		novaEvPopisDisAktivnosti = new EvPopisDisAktivnosti();
	}
	
	public void onRowSelect(SelectEvent event){
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass().getName());
		odabranaEvPopisDisAktivnosti = (EvPopisDisAktivnosti) event.getObject();
		LOG.debug("... odabranaEvTockaProvjere: {}", odabranaEvPopisDisAktivnosti);
	}
	
	public void setCurrentRow(EvPopisDisAktivnosti selected) {

		this.lovObjectVrsta = findByKey("ev_popis_dis_aktivnosti","vrsta",selected.getVrsta());
		this.lovObjectVrstaPublike = findByKey("ev_popis_dis_aktivnosti","vrsta_publike",selected.getVrstaPublike());
		this.lovObjectUloga= findByKey("ev_popis_dis_aktivnosti","uloga",selected.getUloga());
		
		odabranaEvPopisDisAktivnosti = selected;
	}


	public EvPopisDisAktivnosti getOdabranaEvPopisDisAktivnosti() {
		return odabranaEvPopisDisAktivnosti;
	}

	public void setOdabranaEvPopisDisAktivnosti(EvPopisDisAktivnosti odabranaEvPopisDisAktivnosti) {
		this.odabranaEvPopisDisAktivnosti = odabranaEvPopisDisAktivnosti;
	}

	public void setEvPopisDisAktivnostiService(EvPopisDisAktivnostiService evPopisDisAktivnostiService) {
		this.evPopisDisAktivnostiService = evPopisDisAktivnostiService;
	}

	public EvPopisDisAktivnosti getNovaEvPopisDisAktivnosti() {
		return novaEvPopisDisAktivnosti;
	}

	public void setNovaEvPopisDisAktivnosti(EvPopisDisAktivnosti novaEvPopisDisAktivnosti) {
		this.novaEvPopisDisAktivnosti = novaEvPopisDisAktivnosti;
	}

	public EvSifarnik getLovObjectVrsta() {
		return lovObjectVrsta;
	}

	public void setLovObjectVrsta(EvSifarnik lovObjectVrsta) {
		this.lovObjectVrsta = lovObjectVrsta;
	}

	public EvSifarnik getLovObjectUloga() {
		return lovObjectUloga;
	}

	public void setLovObjectUloga(EvSifarnik lovObjectUloga) {
		this.lovObjectUloga = lovObjectUloga;
	}

	public EvSifarnik getLovObjectVrstaPublike() {
		return lovObjectVrstaPublike;
	}

	public void setLovObjectVrstaPublike(EvSifarnik lovObjectVrstaPublike) {
		this.lovObjectVrstaPublike = lovObjectVrstaPublike;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}