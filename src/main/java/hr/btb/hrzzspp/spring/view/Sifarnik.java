package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.EvSifarnik;

import org.springframework.beans.factory.annotation.Autowired;

public abstract class Sifarnik {

	@Autowired
	SelectOneMenu menu;

	public EvSifarnik findByKey(String refTable, String refColumn, String code) {
		return menu.findByKey(refTable, refColumn, code);
	}

	public String getDescription(String refTable, String refColumn, String code) {
				
		EvSifarnik ev = findByKey(refTable, refColumn, code);
		
		if (ev != null)
			return ev.getDescription();
		else
			return code;
	}
}
