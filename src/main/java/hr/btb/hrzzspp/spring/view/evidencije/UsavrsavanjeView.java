package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.EvUsavrsavanje;
import hr.btb.hrzzspp.spring.services.EvUsavrsavanjeService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class UsavrsavanjeView extends Sifarnik implements Serializable {

	
	private static final long serialVersionUID = -8376572534557629644L;
	private static final Logger LOG = LoggerFactory
			.getLogger(BrDocDisertacijaView.class);
	
	private static final String TABLE_NAME = "Tablica 5";

	private EvUsavrsavanje odabranaEvUsavrsavanje = new EvUsavrsavanje();
	private EvUsavrsavanje novaEvUsavrsavanje;
	private EvSifarnik lovObject;
	
	SessionBean session;
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	private EvUsavrsavanjeService evUsavrsavanjeService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public UsavrsavanjeView() {
		
	}
	
	@PostConstruct
	public void init(){
		this.novaEvUsavrsavanje = new EvUsavrsavanje();
	}
	
	public List<EvUsavrsavanje> fillTable(){
		return evUsavrsavanjeService.getEvUsavrsavanjeByPeriod(session.getCurrentReportingPeriod());
	}
	
	public void insert(){
		
		try {
			
			novaEvUsavrsavanje.setVrstaAktivnosti(lovObject.getCode());
			novaEvUsavrsavanje.setReportingPeriod(session.getCurrentReportingPeriod());
			evUsavrsavanjeService.saveUsavrsavanje(novaEvUsavrsavanje);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
		
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvUsavrsavanje);
			evUsavrsavanjeService.saveUsavrsavanje(odabranaEvUsavrsavanje);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	
	
	public void update() {

		try {

			odabranaEvUsavrsavanje.setVrstaAktivnosti(lovObject.getCode());
			evUsavrsavanjeService.saveUsavrsavanje(odabranaEvUsavrsavanje);
			PfUtils.dialogUpdateSuccessful();
			
		} catch (Exception e) {
			// TO DO: handle exception
			e.printStackTrace();
		}

	}
	public void setDelete(EvUsavrsavanje selected) {
		evUsavrsavanjeService.deleteUsavrsavanje(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}
	
	
	public void clean(){
		novaEvUsavrsavanje = new EvUsavrsavanje();
	}
	
	public void onRowSelect(SelectEvent event){
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvUsavrsavanje = (EvUsavrsavanje) event.getObject();
		LOG.debug("... odabranaEvUsavrsavanje: {}", odabranaEvUsavrsavanje);	
	}
	
	public void setCurrentRow(EvUsavrsavanje selected) {
		this.lovObject = findByKey("ev_usavrsavanje","vrsta_aktivnosti",selected.getVrstaAktivnosti());
		odabranaEvUsavrsavanje = selected;
	}

	public EvUsavrsavanje getOdabranaEvUsavrsavanje() {
		return odabranaEvUsavrsavanje;
	}

	public void setOdabranaEvUsavrsavanje(EvUsavrsavanje odabranaEvUsavrsavanje) {
		this.odabranaEvUsavrsavanje = odabranaEvUsavrsavanje;
	}

	@Autowired
	public void setEvUsavrsavanjeService(EvUsavrsavanjeService evUsavrsavanjeService) {
		this.evUsavrsavanjeService = evUsavrsavanjeService;
	}

	public EvUsavrsavanje getNovaEvUsavrsavanje() {
		return novaEvUsavrsavanje;
	}

	public void setNovaEvUsavrsavanje(EvUsavrsavanje novaEvUsavrsavanje) {
		this.novaEvUsavrsavanje = novaEvUsavrsavanje;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

}
