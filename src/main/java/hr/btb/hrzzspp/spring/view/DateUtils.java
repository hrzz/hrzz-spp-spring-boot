package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.ReportingPeriod;

import java.util.Calendar;

import org.joda.time.DateTime;
import org.joda.time.Interval;

public class DateUtils {

	public static java.util.Date toDate(java.sql.Timestamp timestamp) {
	    long milliseconds = timestamp.getTime();
	    return new java.util.Date(milliseconds);
	}
	
	public static java.sql.Timestamp toTimestamp(java.util.Date date) {
	    return new java.sql.Timestamp(date.getTime());
	    
	}

	public static boolean isMMGGGGInPeriod( ReportingPeriod currentReportingPeriod, String dateRealizacije ) {
		String substringMonth = dateRealizacije.substring(0, 2);
		int numberOfMonth = Integer.parseInt(substringMonth);
		String substringYear = dateRealizacije.substring(3, 7);
		int numberOfYear = Integer.parseInt(substringYear);
		
		Calendar calUnos = Calendar.getInstance();
		calUnos.clear();
		calUnos.set(Calendar.MONTH, numberOfMonth - 1);
		calUnos.set(Calendar.YEAR, numberOfYear);
		
		Calendar calPeriodFrom = Calendar.getInstance();
		calPeriodFrom.setTime(currentReportingPeriod.getDateFrom());
		int numberOfMonthRPFrom = calPeriodFrom.get(Calendar.MONTH);
		int numberOfYearRPFrom = calPeriodFrom.get(Calendar.YEAR);
		calPeriodFrom.clear();
		calPeriodFrom.set(Calendar.MONTH, numberOfMonthRPFrom);
		calPeriodFrom.set(Calendar.YEAR, numberOfYearRPFrom);
		
		Calendar calPeriodTo = Calendar.getInstance();
		calPeriodTo.setTime(currentReportingPeriod.getDateTo());
		int numberOfMonthRPTo = calPeriodTo.get(Calendar.MONTH);
		int numberOfYearRPTo = calPeriodTo.get(Calendar.YEAR);
		calPeriodTo.clear();
		calPeriodTo.set(Calendar.MONTH, numberOfMonthRPTo);
		calPeriodTo.set(Calendar.YEAR, numberOfYearRPTo);
		
		//datum unosa je poslije početnog dana perioda
		boolean datumFromOK = calPeriodFrom.compareTo(calUnos) <= 0 ? true : false; 
		//datum unosa je prije zadnjeg dana perioda
		boolean datumToOK = calPeriodTo.compareTo(calUnos) >= 0 ? true : false;
		
		return datumFromOK && datumToOK;
	}

	public static boolean isYearInPeriod(ReportingPeriod currentReportingPeriod, int godina) {
		Calendar calPeriodFrom = Calendar.getInstance();
		calPeriodFrom.setTime(currentReportingPeriod.getDateFrom());
		int numberOfYearRPFrom = calPeriodFrom.get(Calendar.YEAR);
		
		Calendar calPeriodTo = Calendar.getInstance();
		calPeriodTo.setTime(currentReportingPeriod.getDateTo());
		int numberOfYearRPTo = calPeriodTo.get(Calendar.YEAR);
		
		return numberOfYearRPFrom <= godina && numberOfYearRPTo >= godina;
	}
	
	public static boolean isDateInPeriod(long startDate, long endDate, long checkDate) {
		Interval interval = new Interval(new DateTime(startDate), new DateTime(
				endDate));
		return interval.contains(new DateTime(checkDate));
	}
	
}
