package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.UsersService;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ProjectsView implements Serializable {

	private static final long serialVersionUID = 2265411416397010974L;
	private static final Logger LOG = LoggerFactory
			.getLogger(ProjectsView.class);

	private SessionBean session;
//	private ProjectsService service;
	private UsersService usersService;
	
	@Autowired
	private ReportingPeriodsService reportingPeriodsService;
	private String dateFrom;
	private String dateTo;
	private BigDecimal approvedResources;
	
	private List<Project> filteredProjects;
	

	
	@PostConstruct
	public void init() {
		LOG.debug("... ProjectsView init ...");
	}

	public List<Project> getFilteredProjects() {
		LOG.debug("... dohvaćam filtered projects ... length: {}",
				(filteredProjects == null) ? "null" : filteredProjects.size());
		return filteredProjects;
	}

	public List<Project> getProjects() {
		LOG.debug("... dohvaćam session projects ... length: {}",
				(session.getProjects() == null) ? "null" : session.getProjects().size());
		
		/*kod liste projekata ispis tekućih financija i praćenja od-do aktivnog perioda za projekt*/
		List<Project> projekti = session.getProjects();
		for(Project p : projekti){
			for(ReportingPeriod rp : p.getReportingPeriods()){
				if(rp.getStatus()!="3"){
					rp.getApprovedResources();
					setApprovedResources(rp.getApprovedResources());
					
					rp.getDateFrom();
					String dateFormatFrom = new SimpleDateFormat("dd.MM.yyyy." ).format(rp.getDateFrom());
					setDateFrom(dateFormatFrom);
					
					rp.getDateTo();
					String dateFormatTo = new SimpleDateFormat("dd.MM.yyyy.").format(rp.getDateTo());
					setDateTo(dateFormatTo);
					
				}
			}
		}
		
		return projekti;
	}

	public void setFilteredProjects(List<Project> filteredProjects) {
		LOG.debug("... setiram filtered projects ..., length: {}",
				(filteredProjects == null) ? "null" : filteredProjects.size());
		this.filteredProjects = filteredProjects;
	}
	
	

	public UserData getProjectLeader(String id) {
		return usersService.getUserDataByUserId(Long.valueOf(id));
	}

//	@Autowired
//	public void setService(ProjectsService service) {
//		LOG.debug("... setiram service ...");
//		this.service = service;
//	}

	@Autowired
	public void setUsersService(UsersService service) {
		LOG.debug("... setiram usersService ...");
		this.usersService = service;
	}
	
	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	public ReportingPeriodsService getReportingPeriodsService() {
		return reportingPeriodsService;
	}

	public void setReportingPeriodsService(
			ReportingPeriodsService reportingPeriodsService) {
		this.reportingPeriodsService = reportingPeriodsService;
	}

	

	public BigDecimal getApprovedResources() {
		return approvedResources;
	}

	public void setApprovedResources(BigDecimal approvedResources) {
		this.approvedResources = approvedResources;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public SessionBean getSession() {
		return session;
	}

	public UsersService getUsersService() {
		return usersService;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	

	



	
}
