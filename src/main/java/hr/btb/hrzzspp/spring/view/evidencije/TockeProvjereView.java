package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.EvTockeProvjere;
import hr.btb.hrzzspp.spring.services.EvTockeProvjereService;
import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */
@Component
@Scope("session")
public class TockeProvjereView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -9031750779744379218L;
	private static final Logger LOG = LoggerFactory
			.getLogger(TockeProvjereView.class);
	
	private static final String TABLE_NAME = "Tablica 2";

	private EvTockeProvjere odabranaEvTockaProvjere = new EvTockeProvjere();
	private EvTockeProvjere novaEvTockaProvjere;
	private EvSifarnik lovObject;

	private SessionBean session;

	@Autowired
	public void setSession(SessionBean session) {
		this.session = session;
	}

	@Autowired
	private EvTockeProvjereService evTockeProvjereService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public TockeProvjereView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvTockaProvjere = new EvTockeProvjere();
	}

	public List<EvTockeProvjere> fillTable() {
		return evTockeProvjereService.getEvTockeProvjereByPeriod(session
				.getCurrentReportingPeriod());
	}

	public void insert() {
		try {

			if (novaEvTockaProvjere.getDatumRealizacije() != null
					&& !"".equals(novaEvTockaProvjere.getDatumRealizacije())
					&& !DateUtils.isMMGGGGInPeriod(
							session.getCurrentReportingPeriod(),
							novaEvTockaProvjere.getDatumRealizacije())) {
				PfUtils.dialogFail("Unos",
						"Datum realizacije nije unutar izvještajnog razdoblja!");
				return;
			}
			novaEvTockaProvjere.setReportingPeriod(session
					.getCurrentReportingPeriod());
			novaEvTockaProvjere.setPostignuto(lovObject.getCode());
			evTockeProvjereService.saveTockeProvjere(novaEvTockaProvjere);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);

			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			PfUtils.error("Greška","Greška prilikom spremanja podataka");
		}
	}

	public void update() {

		try {
			if (!DateUtils.isMMGGGGInPeriod(
					session.getCurrentReportingPeriod(),
					odabranaEvTockaProvjere.getDatumRealizacije())) {
				PfUtils.dialogFail("Promjena",
						"Datum realizacije nije unutar izvještajnog razdoblja!");
				return;
			}
			odabranaEvTockaProvjere.setPostignuto(lovObject.getCode());
			evTockeProvjereService.saveTockeProvjere(odabranaEvTockaProvjere);

			PfUtils.dialogUpdateSuccessful();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvTockaProvjere);
			evTockeProvjereService.saveTockeProvjere(odabranaEvTockaProvjere);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	// Check if DatumRealizacije iz greater than 12. e.g. MM/YYYY => 07/2015
	// (valid), 14/2015 (not valid)
	// private boolean isDateValid(EvTockeProvjere evTockeProvjere) {
	//
	// if (evTockeProvjere.getDatumRealizacije() != null) {
	//
	// int indexofForwardSlash = evTockeProvjere.getDatumRealizacije()
	// .indexOf('/');
	// String substringOfDatumRealizacije = evTockeProvjere
	// .getDatumRealizacije().substring(0, indexofForwardSlash);
	// int numberOfMonth = Integer.parseInt(substringOfDatumRealizacije);
	//
	// if (numberOfMonth <= 12 && numberOfMonth > 0) {
	// return true;
	// }
	// return false;
	// }
	// return false;
	// }

	public void setDelete(EvTockeProvjere selected) {
		evTockeProvjereService.deleteTockeProvjere(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvTockaProvjere = new EvTockeProvjere();
	}

	public void onRowSelect(SelectEvent event) {
		LOG.debug("... SelectedEvent object: {}", event.getObject().getClass()
				.getName());
		odabranaEvTockaProvjere = (EvTockeProvjere) event.getObject();
		LOG.debug("... odabranaEvTockaProvjere: {}", odabranaEvTockaProvjere);
	}

	public void setCurrentRow(EvTockeProvjere selected) {

		this.lovObject = findByKey("ev_tocke_provjere", "postignuto",
				selected.getPostignuto());

		odabranaEvTockaProvjere = selected;
	}

	public EvTockeProvjere getOdabranaEvTockaProvjere() {
		return odabranaEvTockaProvjere;
	}

	public void setOdabranaEvTockaProvjere(
			EvTockeProvjere odabranaEvTockaProvjere) {
		this.odabranaEvTockaProvjere = odabranaEvTockaProvjere;
	}

	@Autowired
	public void setEvTockeProvjereService(
			EvTockeProvjereService evTockeProvjereService) {
		this.evTockeProvjereService = evTockeProvjereService;
	}

	public EvTockeProvjere getNovaEvTockaProvjere() {
		return novaEvTockaProvjere;
	}

	public void setNovaEvTockaProvjere(EvTockeProvjere novaEvTockaProvjere) {
		this.novaEvTockaProvjere = novaEvTockaProvjere;
	}

	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}
}
