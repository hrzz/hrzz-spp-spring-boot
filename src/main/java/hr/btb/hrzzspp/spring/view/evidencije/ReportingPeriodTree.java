package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.ReportingPeriod;

import org.primefaces.model.TreeNode;

public class ReportingPeriodTree {

	private ReportingPeriod period;
	private TreeNode tree;

	public ReportingPeriodTree() {
	}

	public ReportingPeriod getPeriod() {
		return period;
	}

	public void setPeriod(ReportingPeriod period) {
		this.period = period;
	}

	public TreeNode getTree() {
		return tree;
	}

	public void setTree(TreeNode tree) {
		this.tree = tree;
	}

}
