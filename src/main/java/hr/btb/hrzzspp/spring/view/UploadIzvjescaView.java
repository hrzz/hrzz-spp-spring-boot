package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.FileFIzvjesce;
import hr.btb.hrzzspp.jpa.FileKKonta;
import hr.btb.hrzzspp.jpa.FileOIzvjesce;
import hr.btb.hrzzspp.jpa.FileOstalo1;
import hr.btb.hrzzspp.jpa.FileOstalo2;
import hr.btb.hrzzspp.jpa.FileOstalo3;
import hr.btb.hrzzspp.jpa.FileOstalo4;
import hr.btb.hrzzspp.jpa.FileOstalo5;
import hr.btb.hrzzspp.jpa.FilePFPlana;
import hr.btb.hrzzspp.jpa.FilePRPlana;
import hr.btb.hrzzspp.jpa.FileROpreme;
import hr.btb.hrzzspp.jpa.FileRacuni;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.scope.SpringViewScoped;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author mmesnjak
 *
 */
@Component
// @Scope("view")
@SpringViewScoped
@ManagedBean
public class UploadIzvjescaView implements Serializable {

	
	private static final long serialVersionUID = 1900241312456260786L;

	private static final Logger LOG = LoggerFactory
			.getLogger(UploadIzvjescaView.class);

	private UploadedFile newFile;
	private EvNarativnoIzvjesce narativnoIzvjesce;

	@Autowired
	SessionBean sessionBean;
	@Autowired
	EvNarativnoIzvjesceService evNarativnoIzvjesceService;
	@Autowired
	MenuController menuController;
	boolean oIzvjesceDisabled = false;
	boolean fIzvjesceDisabled = false;
	boolean rOpremeDisabled = false;
	boolean racuniDisabled = false;
	boolean kKontaDisabled = false;
	boolean ostalo1Disabled = false;
	boolean ostalo2Disabled = false;
	boolean ostalo3Disabled = false;
	boolean ostalo4Disabled = false;
	boolean ostalo5Disabled = false;
	boolean pFPlanaDisabled = false;
	boolean pRPlanaDisabled = false;

	@PostConstruct
	public void init() {
		LOG.debug("... initializing ...");
		this.narativnoIzvjesce = evNarativnoIzvjesceService
				.getEvNarativnoIzvjesceByPeriod(sessionBean
						.getCurrentReportingPeriod());
		if (this.narativnoIzvjesce != null
				&& this.narativnoIzvjesce.getEvNiFiles() != null
				&& this.narativnoIzvjesce.getEvNiFiles().size() > 0) {
			setDisabledUploadButton(this.narativnoIzvjesce.getEvNiFiles());
		}

	}

	private void setDisabledUploadButton(List<EvNiFile> listEvNiFiles) {
		for (EvNiFile evNiFile : listEvNiFiles) {
			if (evNiFile instanceof FileOIzvjesce) {
				oIzvjesceDisabled = true;
			}else if (evNiFile instanceof FileFIzvjesce) {
				fIzvjesceDisabled = true;
			}else if (evNiFile instanceof FileROpreme) {
				rOpremeDisabled = true;
			}else if (evNiFile instanceof FileRacuni) {
				racuniDisabled = true;
			}else if (evNiFile instanceof FileKKonta) {
				kKontaDisabled = true;
			}else if (evNiFile instanceof FileOstalo1) {
				ostalo1Disabled = true;
			}else if (evNiFile instanceof FileOstalo2) {
				ostalo2Disabled = true;
			}else if (evNiFile instanceof FileOstalo3) {
				ostalo3Disabled = true;
			}else if (evNiFile instanceof FileOstalo4) {
				ostalo4Disabled = true;
			}else if (evNiFile instanceof FileOstalo5) {
				ostalo5Disabled = true;
			}else if (evNiFile instanceof FilePFPlana) {
				pFPlanaDisabled = true;
			}else if (evNiFile instanceof FilePRPlana) {
				pRPlanaDisabled = true;
			}
		}
	}

	public void saveUploadFiles() {
		if (this.narativnoIzvjesce != null) {
			if (this.narativnoIzvjesce.getReportingPeriod() == null) {
				this.narativnoIzvjesce.setReportingPeriod(sessionBean
						.getCurrentReportingPeriod());
			}
			this.narativnoIzvjesce = evNarativnoIzvjesceService
					.saveEvNarativnoIzvjesce(this.narativnoIzvjesce);
		}
		
		//ponovno učitavanje izvješća nakon spremanja
		init();
		
		PfUtils.info("Unos", "Dokumenti uspješno spremljeni");
		;
	}

	public EvNiFile handleFileUpload(FileUploadEvent fileUploadEvent) {

		String evNiType = (String) fileUploadEvent.getComponent()
				.getAttributes().get("id");

		LOG.debug("... handling upload: {} ...", evNiType);

		// problem hibernate greške orphants EvNiFiles
		if (this.narativnoIzvjesce == null) {
			this.narativnoIzvjesce = evNarativnoIzvjesceService
					.getEvNarativnoIzvjesceByPeriod(sessionBean
							.getCurrentReportingPeriod());
			if (this.narativnoIzvjesce == null) {
				this.narativnoIzvjesce = new EvNarativnoIzvjesce();
				this.narativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
				this.narativnoIzvjesce.getEvNiFiles().clear();
				this.narativnoIzvjesce.setEvNiFiles(new ArrayList<EvNiFile>());
			}
		}

		newFile = fileUploadEvent.getFile();

		EvNiFile evNiFile = null;
		try {
			switch (evNiType) {
			case ConstantsEvidencije.NI_O_IZVJESCE:
				// removeIfExists(ConstantsEvidencije.NI_O_IZVJESCE);
				evNiFile = new FileOIzvjesce();
				this.oIzvjesceDisabled = true;
				break;
			case ConstantsEvidencije.NI_F_IZVJESCE:
				// removeIfExists(ConstantsEvidencije.NI_F_IZVJESCE);
				evNiFile = new FileFIzvjesce();
				this.fIzvjesceDisabled = true;
				break;
			case ConstantsEvidencije.NI_R_OPREME:
				// removeIfExists(ConstantsEvidencije.NI_R_OPREME);
				evNiFile = new FileROpreme();
				this.rOpremeDisabled = true;
				break;
			case ConstantsEvidencije.NI_RACUNI:
				// removeIfExists(ConstantsEvidencije.NI_RACUNI);
				evNiFile = new FileRacuni();
				this.racuniDisabled = true;
				break;
			case ConstantsEvidencije.NI_K_KONTA:
				// removeIfExists(ConstantsEvidencije.NI_K_KONTA);
				evNiFile = new FileKKonta();
				this.kKontaDisabled = true;
				break;
			case ConstantsEvidencije.NI_OSTALO1:
				// removeIfExists(ConstantsEvidencije.NI_OSTALO1);
				evNiFile = new FileOstalo1();
				this.ostalo1Disabled = true;
				break;
			case ConstantsEvidencije.NI_OSTALO2:
				// removeIfExists(ConstantsEvidencije.NI_OSTALO2);
				evNiFile = new FileOstalo2();
				this.ostalo2Disabled = true;
				break;
			case ConstantsEvidencije.NI_OSTALO3:
				// removeIfExists(ConstantsEvidencije.NI_OSTALO3);
				evNiFile = new FileOstalo3();
				this.ostalo3Disabled = true;
				break;
			case ConstantsEvidencije.NI_OSTALO4:
				// removeIfExists(ConstantsEvidencije.NI_OSTALO4);
				evNiFile = new FileOstalo4();
				this.ostalo4Disabled = true;
				break;
			case ConstantsEvidencije.NI_OSTALO5:
				// removeIfExists(ConstantsEvidencije.NI_OSTALO5);
				evNiFile = new FileOstalo5();
				this.ostalo5Disabled = true;
				break;
			case ConstantsEvidencije.NI_P_F_PLANA:
				// removeIfExists(ConstantsEvidencije.NI_P_F_PLANA);
				evNiFile = new FilePFPlana();
				this.pFPlanaDisabled = true;
				break;
			case ConstantsEvidencije.NI_P_R_PLANA:
				// removeIfExists(ConstantsEvidencije.NI_P_R_PLANA);
				evNiFile = new FilePRPlana();
				this.pRPlanaDisabled = true;
				break;
			default:
				return evNiFile;
			}
			evNiFile.setFileName(newFile.getFileName());
			evNiFile.setFileType(newFile.getContentType());
			evNiFile.setFileSize(newFile.getSize());
			evNiFile.setFileBlob(newFile.getContents());

			this.narativnoIzvjesce.addEvNiFile(evNiFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return evNiFile;
	}

//	private void removeIfExists(String evNiType) {
//
//		EvNiFile evNiFile = this.evNarativnoIzvjesceService.getFileByEvNiType(
//				evNiType, this.narativnoIzvjesce);
//
//		if (evNiFile != null) {
//			LOG.debug("... removing evNiFile: {} ...", evNiFile);
//			LOG.debug("... BEFORE narativnoIzvjesce.length =  {} ...",
//					this.narativnoIzvjesce.getEvNiFiles().size());
//			evNiFile = this.narativnoIzvjesce.removeEvNiFile(evNiFile);
//			LOG.debug("... removed evNiFile : {} ...",
//					evNiFile.getNarativnoIzvjesce());
//			LOG.debug("... AFTER narativnoIzvjesce.length =  {} ...",
//					this.narativnoIzvjesce.getEvNiFiles().size());
//		}
//	}

	public void markForDeleteOIzvjesce() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOIzvjesce) {
				lisEvNiFiles.remove(evNiFile);
				this.oIzvjesceDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteFIzvjesce() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileFIzvjesce) {
				lisEvNiFiles.remove(evNiFile);
				this.fIzvjesceDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteROpreme() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileROpreme) {
				lisEvNiFiles.remove(evNiFile);
				this.rOpremeDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteRacuni() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileRacuni) {
				lisEvNiFiles.remove(evNiFile);
				this.racuniDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteKKonta() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileKKonta) {
				lisEvNiFiles.remove(evNiFile);
				this.kKontaDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteOstalo1() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOstalo1) {
				lisEvNiFiles.remove(evNiFile);
				this.ostalo1Disabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteOstalo2() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOstalo2) {
				lisEvNiFiles.remove(evNiFile);
				this.ostalo2Disabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteOstalo3() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOstalo3) {
				lisEvNiFiles.remove(evNiFile);
				this.ostalo3Disabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteOstalo4() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOstalo4) {
				lisEvNiFiles.remove(evNiFile);
				this.ostalo4Disabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeleteOstalo5() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FileOstalo5) {
				lisEvNiFiles.remove(evNiFile);
				this.ostalo5Disabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeletePFPlana() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FilePFPlana) {
				lisEvNiFiles.remove(evNiFile);
				this.pFPlanaDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public void markForDeletePRPlana() {
		List<EvNiFile> lisEvNiFiles = new ArrayList<EvNiFile>();
		lisEvNiFiles.addAll(this.narativnoIzvjesce.getEvNiFiles());
		for (EvNiFile evNiFile : lisEvNiFiles) {
			if (evNiFile instanceof FilePRPlana) {
				lisEvNiFiles.remove(evNiFile);
				this.pRPlanaDisabled = false;
				break;
			}
		}
		this.narativnoIzvjesce.getEvNiFiles().clear();
		this.narativnoIzvjesce.setEvNiFiles(lisEvNiFiles);
	}

	public UploadedFile getNewFile() {
		return newFile;
	}

	public void setNewFile(UploadedFile newFile) {
		this.newFile = newFile;
	}

	public FileOIzvjesce getOpisnoIzvjesce() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOIzvjesce)
					return (FileOIzvjesce) evNiFile;
			}
		}
		return null;
	}

	public FileFIzvjesce getFinancijskoIzvjesce() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileFIzvjesce)
					return (FileFIzvjesce) evNiFile;
			}
		}
		return null;
	}

	public FileROpreme getRegistarOpreme() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileROpreme)
					return (FileROpreme) evNiFile;
			}
		}
		return null;
	}

	public FileRacuni getRacuni() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileRacuni)
					return (FileRacuni) evNiFile;
			}
		}
		return null;
	}

	public FileKKonta getKarticaKonta() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileKKonta)
					return (FileKKonta) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo1 getOstalo1() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo1)
					return (FileOstalo1) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo2 getOstalo2() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo2)
					return (FileOstalo2) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo3 getOstalo3() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo3)
					return (FileOstalo3) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo4 getOstalo4() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo4)
					return (FileOstalo4) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo5 getOstalo5() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo5)
					return (FileOstalo5) evNiFile;
			}
		}
		return null;
	}

	public FilePFPlana getFinancijskiPlan() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FilePFPlana)
					return (FilePFPlana) evNiFile;
			}
		}
		return null;
	}

	public FilePRPlana getRadniPlan() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FilePRPlana)
					return (FilePRPlana) evNiFile;
			}
		}
		return null;
	}

	@Autowired
	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

	public boolean isoIzvjesceDisabled() {
		return oIzvjesceDisabled;
	}

	public void setoIzvjesceDisabled(boolean oIzvjesceDisabled) {
		this.oIzvjesceDisabled = oIzvjesceDisabled;
	}

	public boolean isfIzvjesceDisabled() {
		return fIzvjesceDisabled;
	}

	public void setfIzvjesceDisabled(boolean fIzvjesceDisabled) {
		this.fIzvjesceDisabled = fIzvjesceDisabled;
	}

	public boolean isrOpremeDisabled() {
		return rOpremeDisabled;
	}

	public void setrOpremeDisabled(boolean rOpremeDisabled) {
		this.rOpremeDisabled = rOpremeDisabled;
	}

	public boolean isRacuniDisabled() {
		return racuniDisabled;
	}

	public void setRacuniDisabled(boolean racuniDisabled) {
		this.racuniDisabled = racuniDisabled;
	}

	public boolean iskKontaDisabled() {
		return kKontaDisabled;
	}

	public void setkKontaDisabled(boolean kKontaDisabled) {
		this.kKontaDisabled = kKontaDisabled;
	}

	public boolean isOstalo1Disabled() {
		return ostalo1Disabled;
	}

	public void setOstalo1Disabled(boolean ostalo1Disabled) {
		this.ostalo1Disabled = ostalo1Disabled;
	}

	public boolean isOstalo2Disabled() {
		return ostalo2Disabled;
	}

	public void setOstalo2Disabled(boolean ostalo2Disabled) {
		this.ostalo2Disabled = ostalo2Disabled;
	}

	public boolean isOstalo3Disabled() {
		return ostalo3Disabled;
	}

	public void setOstalo3Disabled(boolean ostalo3Disabled) {
		this.ostalo3Disabled = ostalo3Disabled;
	}

	public boolean isOstalo4Disabled() {
		return ostalo4Disabled;
	}

	public void setOstalo4Disabled(boolean ostalo4Disabled) {
		this.ostalo4Disabled = ostalo4Disabled;
	}

	public boolean isOstalo5Disabled() {
		return ostalo5Disabled;
	}

	public void setOstalo5Disabled(boolean ostalo5Disabled) {
		this.ostalo5Disabled = ostalo5Disabled;
	}

	public boolean ispFPlanaDisabled() {
		return pFPlanaDisabled;
	}

	public void setpFPlanaDisabled(boolean pFPlanaDisabled) {
		this.pFPlanaDisabled = pFPlanaDisabled;
	}

	public boolean ispRPlanaDisabled() {
		return pRPlanaDisabled;
	}

	public void setpRPlanaDisabled(boolean pRPlanaDisabled) {
		this.pRPlanaDisabled = pRPlanaDisabled;
	}

}
