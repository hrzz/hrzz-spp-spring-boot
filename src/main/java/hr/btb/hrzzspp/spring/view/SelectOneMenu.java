package hr.btb.hrzzspp.spring.view;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import hr.btb.hrzzspp.spring.services.EvSifarnikService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import hr.btb.hrzzspp.jpa.EvSifarnik;

@Component
@Scope("session")
public class SelectOneMenu {
	
	@Autowired
	private EvSifarnikService evSifarnikService;
	
	private List<EvSifarnik> lovObjects;
	private EvSifarnik lovObject;
	private Map<Integer,EvSifarnik> mapper =  new HashMap<Integer,EvSifarnik>();
	private Map<String,EvSifarnik> cacheMapper =  new HashMap<String,EvSifarnik>();
	
	public Map<Integer, EvSifarnik> getMapper() {
		return mapper;
	}

	public void setMapper(Map<Integer, EvSifarnik> mapper) {
		this.mapper = mapper;
	}

	public List<EvSifarnik> getLovObjects() {
		return lovObjects;
	}

	public List<EvSifarnik> getLovObjects(String param) {		
		return lovObjects;

	}

	public List<EvSifarnik> getObjects(String table, String column) {
		
		List <EvSifarnik> tmpList = new ArrayList<>();

		for (EvSifarnik tmp : lovObjects){
			
			if (tmp.getRefTable().equals(table) && tmp.getRefColumn().equals(column))
				tmpList.add(tmp);
		}
		
		return tmpList;
	}
	
	@PostConstruct
	public void init(){

		this.lovObjects = evSifarnikService.getEvSifarnik();
		
		for (EvSifarnik tmp : this.lovObjects){
			mapper.put(tmp.getId(), tmp);
			cacheMapper.put(tmp.getRefTable() + "#" + tmp.getRefColumn() + "#" + tmp.getCode(), tmp);
		}
		
	}

	public EvSifarnik findByKey(String refTable, String refColumn, String code){
		return cacheMapper.get(refTable + "#" + refColumn + "#" + code);
	}
	
	public EvSifarnik getLovObject() {
		return lovObject;
	}

	public void setLovObject(EvSifarnik lovObject) {
		this.lovObject = lovObject;
	}

	public Map<String,EvSifarnik> getCacheMapper() {
		return cacheMapper;
	}

	public void setCacheMapper(Map<String,EvSifarnik> cacheMapper) {
		this.cacheMapper = cacheMapper;
	}	
	
}
