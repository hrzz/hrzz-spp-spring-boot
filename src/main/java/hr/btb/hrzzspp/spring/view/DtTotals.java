package hr.btb.hrzzspp.spring.view;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class DtTotals {

	public static BigDecimal getBigDecimal(Object value) {
		BigDecimal ret = null;
		if (value != null) {
			if (value instanceof BigDecimal) {
				ret = (BigDecimal) value;
			} else if (value instanceof String) {
				ret = new BigDecimal((String) value);
			} else if (value instanceof BigInteger) {
				ret = new BigDecimal((BigInteger) value);
			} else if (value instanceof Number) {
				ret = new BigDecimal(((Number) value).doubleValue());
			} else {
				throw new ClassCastException("Not possible to coerce [" + value
						+ "] from class " + value.getClass()
						+ " into a BigDecimal.");
			}
		}
		return ret;
	}

	public static BigDecimal getTotal(List<?> items, String property) {

		double sum = 0;
		Method method = null;
		String refAttr = "get" + property.substring(0, 1).toUpperCase()
				+ property.substring(1);

		for (Object o : items) {

			try {

				method = o.getClass().getMethod(refAttr, new Class[] {});
				sum += getBigDecimal((Object) method.invoke(o, new Object[] {}))
						.doubleValue();

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return new BigDecimal(sum);
	}

}
