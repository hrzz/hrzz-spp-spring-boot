package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsModel;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.jpa.UserRole;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.services.ProjectsService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.UserRoleService;
import hr.btb.hrzzspp.spring.services.UsersService;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class UserView implements Serializable {

	private static final long serialVersionUID = 2349774785720920098L;

	private static final Logger LOG = LoggerFactory.getLogger(UserView.class);

	@Autowired
	private UsersService usersService;

	@Autowired
	private MenuController menuController;

	private User user = new User();

	private UserData userData = new UserData();

	private Project project = new Project();

	private List<Project> projektiKorisnikaList = new ArrayList<Project>();

	@Autowired
	private ProjectsService projectsService;

	@Autowired
	private ReportingPeriodsService reportingPeriodService;

	@Autowired
	private SessionBean sessionBean;

	@Autowired
	private UserRoleService userRoleService;

	private List<UserRole> usersRoles;

	private List<User> listAllUsers;
	
	
	public UserView() {

	}

	@PostConstruct
	public void init() {

		listAllUsers = usersService.getAllUsers();
				
		for (User u : listAllUsers) {
			resetRole(u);
			for (UserRole rola : u.getUserRoles()) {
				if (rola.getRole().getId() == ConstantsModel.PRK) {
					u.setRolaPRK(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.VOD) {
					u.setRolaVOD(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.EVL) {
					u.setRolaEVL(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.COV) {
					u.setRolaCOV(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.CUO) {
					u.setRolaCUO(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.ADM) {
					u.setRolaADM(sessionBean.cssIkona(10));
				}
			}
		}
		
	}

	public List<User> fillTable() {
		
		/*
		List<User> listAllUsers = usersService.getAllUsers();
		for (User u : listAllUsers) {
			resetRole(u);
			for (UserRole rola : u.getUserRoles()) {
				if (rola.getRole().getId() == ConstantsModel.PRK) {
					u.setRolaPRK(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.VOD) {
					u.setRolaVOD(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.EVL) {
					u.setRolaEVL(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.COV) {
					u.setRolaCOV(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.CUO) {
					u.setRolaCUO(sessionBean.cssIkona(10));
				}else if (rola.getRole().getId() == ConstantsModel.ADM) {
					u.setRolaADM(sessionBean.cssIkona(10));
				}
			}
		}
		*/
		return this.listAllUsers;

	}

	public void pregledKorisnika() {

		// popuni listu ako je user evl, cov, ili cuo
		projektiKorisnikaList = projectsService.findProjectsByUserId(user
				.getId());

		// popuni listu ako je user vod
		List<Project> projektiVodLista = projectsService.findByLeaderId(user
				.getId());
		projektiKorisnikaList.addAll(projektiVodLista);

		menuController.menuAction("pregledKorisnika", "1");
	}

	private void resetRole(User u) {
		u.setRolaPRK("display-none");
		u.setRolaVOD("display-none");
		u.setRolaEVL("display-none");
		u.setRolaCOV("display-none");
		u.setRolaCUO("display-none");
		u.setRolaADM("display-none");
	}

	public void openSelectedProject() {

		menuController.goToPopisEvidencija();
	}

	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public List<Project> getProjektiKorisnikaList() {
		return projektiKorisnikaList;
	}

	public void setProjektiKorisnikaList(List<Project> projektiKorisnikaList) {
		this.projektiKorisnikaList = projektiKorisnikaList;
	}

	public ProjectsService getProjectsService() {
		return projectsService;
	}

	public void setProjectsService(ProjectsService projectsService) {
		this.projectsService = projectsService;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public MenuController getMenuController() {
		return menuController;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

	public List<UserRole> getUsersRoles() {
		return usersRoles;
	}

	public void setUsersRoles(List<UserRole> usersRoles) {
		this.usersRoles = usersRoles;
	}

}
