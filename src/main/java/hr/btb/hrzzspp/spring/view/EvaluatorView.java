package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.business.CustomSystemData;
import hr.btb.hrzzspp.jpa.EvObrazacEvaluacije;
import hr.btb.hrzzspp.jpa.Evaluator;
import hr.btb.hrzzspp.jpa.EvaluatorStatus;
import hr.btb.hrzzspp.jpa.Institution;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.StatusRazdoblja;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.jpa.UserRole;
import hr.btb.hrzzspp.jpa.UserRolePK;
import hr.btb.hrzzspp.jpa.UserRolePeriodPK;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.services.EvObrazacEvaluacijeService;
import hr.btb.hrzzspp.spring.services.EvaluatorService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.UserRolePeriodStatusService;
import hr.btb.hrzzspp.spring.services.UserRoleService;
import hr.btb.hrzzspp.spring.services.UsersService;
import hr.btb.hrzzspp.spring.view.evidencije.PopisEvidencijaView;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class EvaluatorView {
	
	@Autowired
	private ReportingPeriodsService reportingPeriodService;
	
	@Autowired
	private EvaluatorService evaluatorService;
	
	@Autowired
	private UsersService usersService;
	
	@Autowired
	private UserRolePeriodStatusService userRolePeriodStatusService;
	
	@Autowired
	private UserRoleService userRoleService;
	
	@Autowired
	private PopisEvidencijaView popisEvidencijaView;
	
	@Autowired
	private EvObrazacEvaluacijeService evObrazacEvaluacijeService;
	
	@Autowired
	private SessionBean session;
	
	@Autowired
	private MenuController menuController;
	
	private User user = new User();
	private UserData userData = new UserData();
	private Evaluator evaluator = new Evaluator();
	
	List<EvObrazacEvaluacije> evalIzvjescaList;
	
	
	@PostConstruct
	public void init(){
		
	}
	
	/*
	 * insertIntoEvaluator
	 * -funkcija za spremanje u tablicu Evaluator
	 * prilikom pozivanja evaluatora(slanja mail-a)
	 */
	public void insertIntoEvaluator(){
		
		List<Evaluator> evalList = evaluatorService
				.getEvaluatorByPeriodAndEmail(
						session.getCurrentReportingPeriod(),
						this.evaluator.getEmail());
		
		if (evalList.size() > 0) { //postojeći evaluator
			
			PfUtils.info("Evaluator već postoji!");
			
		} else { //novi evaluator
			
			this.evaluator.setPassword(generatePassword(this.evaluator));
			this.evaluator.setPeriod(session.getCurrentReportingPeriod());
			this.evaluator.setStatus(new EvaluatorStatus(5));
			
			try {
				
				evaluatorService.saveEvaluator(this.evaluator);
				
			} catch (Exception ex) {
				ex.printStackTrace();
				
				PfUtils.error("Došlo je do greške!");

				return;
			}
			
//			slanjeMaila(this.evaluator);
			
			PfUtils.info("Unos","Evaluator je uspješno dodan!");
			RequestContext.getCurrentInstance().execute("PF('evaluatoriDialogWidget').hide();"); //zatvaranje dialoga
			popisEvidencijaView.setEvaluatorList(evaluatorService.getEvaluatorByPeriod(session.getCurrentReportingPeriod()));

	        menuController.goToPopisEvidencija();
	        
		}
	}
	
	public void clean() {
		this.evaluator = new Evaluator();
	}
	
	public void slanjeMaila(Evaluator evaluator) {
		
		InternetAddress[] addresses = null;
		try {
			//addresses = InternetAddress.parse(evaluator.getEmail());
			addresses = InternetAddress.parse("dalibor.harmina@btb.hr");
		} catch (AddressException e) {
			e.printStackTrace();
		}

		// Sender's email ID needs to be mentioned
		String from = CustomSystemData.mailFrom;

		// Assuming you are sending email from localhost
		String host = "webmail.hrzz.hr";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.put("mail.smtp.host", host);
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.port", "25"); 

		properties.put("mail.smtp.auth", "true");
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(CustomSystemData.userSMTP,
						CustomSystemData.passSMTP);
			}
		};
		
		// Get the default Session object.
		Session mailSession = Session.getDefaultInstance(properties,
				authenticator);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, addresses);
		//	message.addRecipient(Message.RecipientType.BCC, new InternetAddress("dalibor.harmina@btb.hr"));
			
			// Set Subject: header field
			message.setSubject("Poziv za sudjelovanje u postupku vrednovanja HRZZ projekata");
			
			StringBuffer sb=new StringBuffer();
			sb.append("Poštovani, ");
			sb.append("\n");
			sb.append("Hrvatska zaklada za znanost (dalje, Zaklada) poziva Vas na sudjelovanje u postupku vrednovanja ");
			sb.append("periodičnih izvješća projekt financiranih u sklopu natječaja " + session.getCurrentProject().getTenderType().getDescription());
			sb.append("\n" +session.getCurrentProject().getName() + session.getCurrentProject().getCode());
			sb.append(" s natječajnog roka u ");
			sb.append(session.getCurrentProject().getTenderType().getDescription()+".");
			
			sb.append("\nZaklada organizira sustavno praćenje i nadgledanje projekata koje financira.");
			sb.append("\nOsnovni kriterij odabira vrednovatelja odgovarajuće su kompetencije za vrednovanje projekata "
					+ "(utvrđuju se na temelju dosadašnjega znanstveno-istraživačkog rada i postignuća) i kompetencije "
					+ "u području programa u okviru kojega je natječaj raspisan, neovisnost i nepostojanje sukoba interesa.\n");
			sb.append("Vrednovanje periodičnih izvješća provodi se prema kriterijima definiranima u Obrascu za vrednovanje koji Vam dostavljamo u prilogu.");
			sb.append("\n\nZadaće su vrednovatelja sljedeće:\n");
			sb.append("-samostalno vrednovati opisni dio periodičnih izvješća putem Obrasca za vrednovanje – rok je 5 radnih dana po izvješću\n");
			sb.append("-uvidom u obavljeno vrednovanje drugoga vrednovatelja dodatno komentirati izvješće i unijeti izmjene u svoje obrasce te uskladiti komentare – rok je 2 radna dana po izvješću");
			sb.append("\n\nUpravni odbor Zaklade na temelju mišljenja nezavisnih stručnjaka donosi odluku o daljnjem financiranju, dodatnim mjerama, odnosno prestanku financiranja projekta.");
			sb.append("\n\nLjubazno Vas molim da mi najkasnije do  godine na adresu elektroničke pošte javite prihvaćate li poziv za sudjelovanje u vrednovanju projekata.");
			sb.append("\n\nZa sva pitanja stojim Vam na raspolaganju.");
			sb.append("\n\nS poštovanjem,");
			sb.append("\n");
			sb.append("HRVATSKA ZAKLADA ZA ZNANOST");
			
			// Now set the actual message
			message.setText(sb.toString()); 

			// Send message
			Transport.send(message);
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
	
	public String generatePassword(Evaluator evaluator) {
		
		String password = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		password += (evaluator.getIme() != null ? evaluator.getIme()
				.substring(0, 1).toLowerCase() : "_");
		password += (evaluator.getPrezime() != null ? evaluator.getPrezime()
				.substring(0, 1).toLowerCase() : "_");
		password += cal.get(Calendar.HOUR);
		password += cal.get(Calendar.MINUTE);
		password += cal.get(Calendar.SECOND);
		password += cal.get(Calendar.MILLISECOND);
		
		return password;
	}

	//ova metoda se poziva nakon što EVL prihvati biti EVL na projektu, kada PRK povoce akciju 'Confirm' na tablici evaluatora
	//EVL je prihvaćen i unosi se u sustav kao novi user
	//TODO pokupiti username pass za tog EVL da se unese u UserData
	public void insertEvaluator(){
		
		String poruka = "";
		
		try {
			
			UserRolePeriodStatus urps = new UserRolePeriodStatus();
			UserRolePeriodPK urpPK = new UserRolePeriodPK();
			
			urpPK.setPeriodId(session.getCurrentReportingPeriod().getId());
			urpPK.setRoleId(new Role(3).getId());
			
			List<User> userList = usersService.getUsers(user.getUsername());
			
			if (userList.size() > 0) { //postojeći user
				
				//provjera dali uneseni evaluator postoji na istom projektu
				boolean validProject = isValidPeriod(session.getCurrentReportingPeriod());
				
				if (!validProject)
					poruka = "Odabrani evaluator već postoji na projektu!";
				else
					urpPK.setUserId(userList.get(0).getId());
				
			} else { //novi user
				
				//postavljanje usera
				
				user.setUsername(this.evaluator.getEmail());
				user.setPassword(this.evaluator.getPassword());
				userData.setName(this.evaluator.getIme());
				userData.setSurname(this.evaluator.getPrezime());
				user.setUserData(userData);
				user.setIsAccountExpired("N");
				user.setIsAccountLocked("N");
				user.setIsCredentialsExpired("N");
				user.setIsEnabled("Y");
				user.getUserData().setInstitutionId(new Institution(1));
				user.getUserData().setTitle("prof. dr. sc.");
				user.getUserData().setDegree("doktor/ica znanosti");
				user.getUserData().setPhoneNumber("01-231-123");
				user.getUserData().setEmail(user.getUsername());
				user.getUserData().setOib("09182831241");
				user.getUserData().setSex("m");
				user.getUserData().setDateOfBirth("20441");
				user.getUserData().setTelephoneNumber("01-213-124");
				user.getUserData().setAddressOfResidence("Srebrnjak 13b");
				user.getUserData().setZipCode("10000");
				user.getUserData().setCity("Zagreb");
				user.getUserData().setNationality("Croatia (Hrvatska)");
				user.getUserData().setInstitutionTmp("Sveučilište u Zagrebu, Tekstilno-tehnološki fakultet");
				user.getUserData().setCreatedAt(new Date());
				user.getUserData().setUser(user);
				
				usersService.saveUser(user); //insert usera
				
				UserRole userRole = new UserRole();
				userRole.setRole(new Role(3));
				userRole.setUser(user);
				userRole.setCreatedAt(new Timestamp(new Date().getTime()));
				
				urpPK.setUserId(user.getId());
				
				UserRolePK urPK = new UserRolePK();
				urPK.setRoleId(new Role(3).getId());
				urPK.setUserId(user.getId());
				userRole.setId(urPK);
				userRoleService.saveUserRole(userRole);//insert user role
				
				urps.setId(urpPK);
				urps.setStatusRazdoblja(new StatusRazdoblja(1));
				urps.setVrijemePromjene(new Timestamp(System.currentTimeMillis()));
				urps.setUser(user);
				urps.setPeriod(session.getCurrentReportingPeriod());
				urps.setRole(new Role(3));
				
				userRolePeriodStatusService.saveUserRolePeriodStatus(urps); 
			}
			
			if (poruka.equals("")) {
				
				FacesMessage msg = new FacesMessage("Akcija je uspješno obavljena!");
		        FacesContext.getCurrentInstance().addMessage(null, msg);
		        
		        RequestContext.getCurrentInstance().execute("PF('evaluatoriDialogWidget').hide();"); //zatvaranje dialoga
		        
		        menuController.goToPopisEvidencija();
//		        evaluatoriList = serviceUserRolePeriod.findByPeriodAndRole(selectedReportingPeriod, new Role(3));
		        
		        
			} else {
			
				FacesMessage msg = new FacesMessage(poruka);
		        FacesContext.getCurrentInstance().addMessage(null, msg);
			}
			
		} catch (Exception e) {
	
			PfUtils.info("Greška","Greška prilikom spremanja podataka!");
			e.printStackTrace();
		}
	}
	//za evaluatore
		public static void refreshStranice(){
			ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
	        try {
				context.redirect(((HttpServletRequest) context.getRequest()).getRequestURI());
			} catch (IOException e) {
				PfUtils.info("Greška","Greška pri spremanju podataka!");
				e.printStackTrace();
			}
		}

	private boolean isValidNOEvaluator(ReportingPeriod selectedReportingPeriod) {
		
		boolean valid = false;
		
		int noEvaluator = userRolePeriodStatusService.findByPeriodAndRole(
				selectedReportingPeriod, new Role(3)).size();
		
		if (noEvaluator < 3)
			valid = true;
		
		return valid;
	}

	private boolean isValidPeriod(ReportingPeriod selectedReportingPeriod) {
		
		boolean valid = true;
		
		List<UserRolePeriodStatus> urpsList = userRolePeriodStatusService.findByPeriod(selectedReportingPeriod);
		
		Iterator<UserRolePeriodStatus> iter = urpsList.iterator();
		
		while (iter.hasNext()) {
		
			UserRolePeriodStatus temp = iter.next();
			
			if (temp.getUser().getUsername() != null)
				if (temp.getUser().getUsername().equals(user.getUsername())) {
					valid = false;
					break;
			}
		}
		
		return valid;
	}
	
	/*SEND button za email za evaluatore*/
	public void evlSend(){
		if (this.evaluator.getStatus().getId() == 2
				|| this.evaluator.getStatus().getId() == 3
				|| this.evaluator.getStatus().getId() == 4) {
			return;
		} else {
			this.evaluator.setStatus(new EvaluatorStatus(1));
			this.evaluator.getStatus().setIkona("ikona-send");
			evaluatorService.saveEvaluator(this.evaluator);
			slanjeMaila(this.evaluator);
			menuController.goToPopisEvidencija();
		}
	}
	
	/*RESEND button za za email evaluatore*/
	public void evlResend() {
		if (this.evaluator.getStatus().getId() == 3
				|| this.evaluator.getStatus().getId() == 4) {
			return;
		} else if(this.evaluator.getStatus().getId() == 1){
			this.evaluator.setStatus(new EvaluatorStatus(2));
			this.evaluator.getStatus().setIkona("ikona-resend");
			evaluatorService.saveEvaluator(this.evaluator);
			slanjeMaila(this.evaluator);
			menuController.goToPopisEvidencija();
		}

	}
	
	/*CONFIRM button za evaluatore(prk)*/
	public void evlConfirm(){
		if(this.evaluator.getStatus().getId()==4 || this.evaluator.getStatus().getId()==5){
			return;
		}else{
			this.evaluator.setStatus(new EvaluatorStatus(3));
			this.evaluator.getStatus().setIkona("ikona-confirm");
			evaluatorService.saveEvaluator(this.evaluator);
			insertEvaluator();
			menuController.goToPopisEvidencija();
		}
			
	}
	
	/*REFUSE button za evaluatore(prk)*/
	public void evlRefuse(){
		this.evaluator.setStatus(new EvaluatorStatus(4));
		this.evaluator.getStatus().setIkona("ikona-refuse");
		evaluatorService.saveEvaluator(this.evaluator);
		menuController.goToPopisEvidencija();
	}
	
	
	String zavOcjenaA;
	String zavOcjenaB;
	String zavOcjenaC;
	String zavOcjenaD;
	
	
	//postavljanje kvačica na A:B:C:D ovisno o odgovoru evaluatora u evaluator_izvješće
	public List<EvObrazacEvaluacije> getEvalIzvjescaList() {
		for(EvObrazacEvaluacije oe : popisEvidencijaView.getEvalIzvjescaList()){
			
//			if(oe.getZavOcjenaA()!=false){
//				zavOcjenaA = "&#x2713;";
//			}
//			if(oe.getZavOcjenaB()!=false){
//				zavOcjenaB = "&#x2713;";
//			}
//			if(oe.getZavOcjenaC()!=false){
//				zavOcjenaC = "&#x2713;";
//			}
//			if(oe.getZavOcjenaD()!=false){
//				zavOcjenaD = "&#x2713;";
//			}
			
		}
		return evalIzvjescaList;
	}

	public void setEvalIzvjescaList(List<EvObrazacEvaluacije> evalIzvjescaList) {
		this.evalIzvjescaList = evalIzvjescaList;
	}


	
	
	public ReportingPeriodsService getReportingPeriodService() {
		return reportingPeriodService;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}

	public EvaluatorService getEvaluatorService() {
		return evaluatorService;
	}

	public void setEvaluatorService(EvaluatorService evaluatorService) {
		this.evaluatorService = evaluatorService;
	}

	public UsersService getUsersService() {
		return usersService;
	}

	public EvObrazacEvaluacijeService getEvObrazacEvaluacijeService() {
		return evObrazacEvaluacijeService;
	}

	public void setEvObrazacEvaluacijeService(
			EvObrazacEvaluacijeService evObrazacEvaluacijeService) {
		this.evObrazacEvaluacijeService = evObrazacEvaluacijeService;
	}

	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	public UserRolePeriodStatusService getUserRolePeriodStatusService() {
		return userRolePeriodStatusService;
	}

	public void setUserRolePeriodStatusService(
			UserRolePeriodStatusService userRolePeriodStatusService) {
		this.userRolePeriodStatusService = userRolePeriodStatusService;
	}

	public UserRoleService getUserRoleService() {
		return userRoleService;
	}

	public void setUserRoleService(UserRoleService userRoleService) {
		this.userRoleService = userRoleService;
	}

	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public MenuController getMenuController() {
		return menuController;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}


	public UserData getUserData() {
		return userData;
	}

	public void setUserData(UserData userData) {
		this.userData = userData;
	}

	public Evaluator getEvaluator() {
		return evaluator;
	}

	public void setEvaluator(Evaluator evaluator) {
		this.evaluator = evaluator;
	}

	public PopisEvidencijaView getPopisEvidencijaView() {
		return popisEvidencijaView;
	}

	public void setPopisEvidencijaView(PopisEvidencijaView popisEvidencijaView) {
		this.popisEvidencijaView = popisEvidencijaView;
	}

	public String getZavOcjenaA() {
		return zavOcjenaA;
	}

	public void setZavOcjenaA(String zavOcjenaA) {
		this.zavOcjenaA = zavOcjenaA;
	}

	public String getZavOcjenaB() {
		return zavOcjenaB;
	}

	public void setZavOcjenaB(String zavOcjenaB) {
		this.zavOcjenaB = zavOcjenaB;
	}

	public String getZavOcjenaC() {
		return zavOcjenaC;
	}


	public void setZavOcjenaC(String zavOcjenaC) {
		this.zavOcjenaC = zavOcjenaC;
	}

	public String getZavOcjenaD() {
		return zavOcjenaD;
	}

	public void setZavOcjenaD(String zavOcjenaD) {
		this.zavOcjenaD = zavOcjenaD;
	}

}
