package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvPopisPublikacija;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.FilePublikacija;
import hr.btb.hrzzspp.spring.services.EvPopisPublikacijaService;
import hr.btb.hrzzspp.spring.view.DateUtils;
import hr.btb.hrzzspp.spring.view.FileUploadBean;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.Sifarnik;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Tablica 9. : Popis ostalih objavljenih publikacija prozašlih iz akitvnosti na
 * projektu
 */
@Component
@Scope("session")
public class OstalePublikacijeView extends Sifarnik implements Serializable {

	private static final long serialVersionUID = -3523125767220089068L;
	private static final Logger LOG = LoggerFactory
			.getLogger(OstalePublikacijeView.class);
	
	private static final String TABLE_NAME = "Tablica 9";

	private EvPopisPublikacija odabranaEvPopisPublikacija = new EvPopisPublikacija();
	private EvPopisPublikacija novaEvPopisPublikacija;
	private EvSifarnik lovObjectVrsta;
	private EvSifarnik lovObjectPristup;

	@Autowired
	private SessionBean session;
	@Autowired
	private FileUploadBean fileUploadBean;
	@Autowired
	private EvPopisPublikacijaService evPopisPublikacijaService;
	@Autowired
	private EvidencijeVODView evidencijeVODView;

	public OstalePublikacijeView() {

	}

	@PostConstruct
	public void init() {
		this.novaEvPopisPublikacija = new EvPopisPublikacija();
	}

	public List<EvPopisPublikacija> fillTable() {
		// setting entity for file upload - specific type for table
		// ev_popis_publikacija
		this.fileUploadBean.clean().setFileEntityName(
				ConstantsEvidencije.FILE_PUBLIKACIJA);
		return evPopisPublikacijaService.getEvPopisPublikacijaByPeriod(session
				.getCurrentReportingPeriod());
	}

	public void insert() {

		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(), novaEvPopisPublikacija.getGodinaObjavljivanja())){
				PfUtils.dialogFail("Unos", "Godina nije unutar izvještajnog razdoblja!");
				return;
			}
			novaEvPopisPublikacija.setReportingPeriod(session
					.getCurrentReportingPeriod());
			novaEvPopisPublikacija.setVrsta(lovObjectVrsta.getCode());
			novaEvPopisPublikacija.setOtvoreniPristup(lovObjectPristup
					.getCode());

			if (fileUploadBean.getEvFile() != null) {
				novaEvPopisPublikacija.setFile((FilePublikacija) fileUploadBean
						.getEvFile());
			} else {

				String poveznica_upis = fileUploadBean.getPoveznica();
				if (poveznica_upis != null && !"".equals(poveznica_upis)) {
					if (!poveznica_upis.startsWith("http://") && !poveznica_upis.startsWith("https://")) {
						novaEvPopisPublikacija.setPoveznica("http://"
								+ poveznica_upis);
					} else {
						novaEvPopisPublikacija.setPoveznica(poveznica_upis);
					}
				}
			}

			evPopisPublikacijaService
					.saveEvPopisPublikacija(novaEvPopisPublikacija);
			
			//promijena statusa evidencije
			evidencijeVODView.changeStatusEvidencije(TABLE_NAME);
			
			PfUtils.dialogInsertSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.dialogInsertFail();
		}
		clean();
	}
	
	public void spremiPRK(){
		try{
			
			
			LOG.debug("... updating selectedRezultat: {} ...", odabranaEvPopisPublikacija);
			evPopisPublikacijaService.saveEvPopisPublikacija(odabranaEvPopisPublikacija);

			PfUtils.dialogUpdateSuccessful();
		
		}catch(Exception e){
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}

	public void update() {
		try {
			if (!DateUtils.isYearInPeriod(session.getCurrentReportingPeriod(), odabranaEvPopisPublikacija.getGodinaObjavljivanja())){
				PfUtils.dialogFail("Promjena", "Godina nije unutar izvještajnog razdoblja!");
				return;
			}
			String poveznica_upis = fileUploadBean.getPoveznica();

			// kada je već postavljen file, i želi se editirati
			if (odabranaEvPopisPublikacija.getFile() != null) {

				if (fileUploadBean.getEvFile() != null) {
					odabranaEvPopisPublikacija.getFile().setPublikacija(null);
					odabranaEvPopisPublikacija.setFile(null);
					evPopisPublikacijaService.saveEvPopisPublikacija(odabranaEvPopisPublikacija);
					odabranaEvPopisPublikacija.setFile((FilePublikacija) fileUploadBean
							.getEvFile());
				}
				odabranaEvPopisPublikacija.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada nema linka, a dodaje se file
			} else if ("".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisPublikacija.getFile() != null) {
					odabranaEvPopisPublikacija.getFile().setPublikacija(null);
					odabranaEvPopisPublikacija.setFile(null);
					evPopisPublikacijaService.saveEvPopisPublikacija(odabranaEvPopisPublikacija);
				}
				odabranaEvPopisPublikacija.setFile((FilePublikacija) fileUploadBean
						.getEvFile());
				odabranaEvPopisPublikacija.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));

				// kada je upisan link, a link se želi editirati
			} else if (poveznica_upis != null && !"".equals(poveznica_upis)
					&& fileUploadBean.getEvFile() == null) {
				if (!poveznica_upis.startsWith("http://")
						&& !poveznica_upis.startsWith("https://")) {
					odabranaEvPopisPublikacija.setPoveznica("http://"
							+ poveznica_upis);
				} else {
					odabranaEvPopisPublikacija.setPoveznica(poveznica_upis);
				}

				// kada je upisan link, a dodaje se file
			} else if (poveznica_upis != null
					&& fileUploadBean.getEvFile() != null) {

				if (odabranaEvPopisPublikacija.getFile() != null) {
					odabranaEvPopisPublikacija.getFile().setPublikacija(null);
					odabranaEvPopisPublikacija.setFile(null);
					evPopisPublikacijaService.saveEvPopisPublikacija(odabranaEvPopisPublikacija);
				}
				odabranaEvPopisPublikacija.setFile((FilePublikacija) fileUploadBean
						.getEvFile());
				odabranaEvPopisPublikacija.setPoveznica(poveznica_upis.replace(
						poveznica_upis, ""));
			} else if (poveznica_upis.equals("")) {
				odabranaEvPopisPublikacija.setPoveznica(poveznica_upis);
			}
			odabranaEvPopisPublikacija.setVrsta(lovObjectVrsta.getCode());
			odabranaEvPopisPublikacija.setOtvoreniPristup(lovObjectPristup
					.getCode());

			evPopisPublikacijaService
					.saveEvPopisPublikacija(odabranaEvPopisPublikacija);

			PfUtils.dialogUpdateSuccessful();
		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.dialogUpdateFail();
		}
		clean();
	}

	public void deleteFileRezultat() {
		LOG.debug("... deleting file reference ... {}",
				odabranaEvPopisPublikacija);
		odabranaEvPopisPublikacija.setFile(null);
		try {
			evPopisPublikacijaService
					.saveEvPopisPublikacija(odabranaEvPopisPublikacija);
		} catch (Exception e) {
			PfUtils.error("Greška","Greška prilikom brisanja datoteke!");
			LOG.error("Error deleting fileRad: " + e.toString());
		}
		PfUtils.info("Brisanje datoteke","Datoteka uspješno izbrisana");
	}

	public void cancelFileDelete() {
		LOG.debug("... canceling file delete ...");
		odabranaEvPopisPublikacija = null;
	}

	public void setDelete(EvPopisPublikacija selected) {
		evPopisPublikacijaService.deleteEvPopisPublikacija(selected);
		PfUtils.info("Brisanje podataka","Podatak uspješno izbrisan!!");
	}

	public void clean() {
		novaEvPopisPublikacija = new EvPopisPublikacija();
		novaEvPopisPublikacija.setGodinaObjavljivanja((new DateTime(System.currentTimeMillis())).getYear());
		fileUploadBean.clean();
	}

	public EvPopisPublikacija getOdabranaEvPopisPublikacija() {
		return odabranaEvPopisPublikacija;
	}

	public void setOdabranaEvPopisPublikacija(
			EvPopisPublikacija odabranaEvPopisPublikacija) {
		this.odabranaEvPopisPublikacija = odabranaEvPopisPublikacija;
		this.fileUploadBean.setSelectedRow(this.odabranaEvPopisPublikacija);
		LOG.debug("... odabranaEvPopisPublikacija: {}",
				odabranaEvPopisPublikacija);

		// setting values for edit dialog
		this.lovObjectPristup = findByKey(
				ConstantsEvidencije.TBL_EV_POPIS_PUBLIKACIJA,
				ConstantsEvidencije.EV_PUBLIKACIJA_PRISTUP,
				odabranaEvPopisPublikacija.getOtvoreniPristup());

		this.lovObjectVrsta = findByKey(
				ConstantsEvidencije.TBL_EV_POPIS_PUBLIKACIJA,
				ConstantsEvidencije.EV_PUBLIKACIJA_VRSTA,
				odabranaEvPopisPublikacija.getVrsta());
	}

	public void setEvPopisPublikacijaService(
			EvPopisPublikacijaService evPopisPublikacijaService) {
		this.evPopisPublikacijaService = evPopisPublikacijaService;
	}

	public EvPopisPublikacija getNovaEvPopisPublikacija() {
		return novaEvPopisPublikacija;
	}

	public void setNovaEvPopisPublikacija(
			EvPopisPublikacija novaEvPopisPublikacija) {
		this.novaEvPopisPublikacija = novaEvPopisPublikacija;
	}

	public EvSifarnik getLovObjectPristup() {
		return lovObjectPristup;
	}

	public void setLovObjectPristup(EvSifarnik lovObjectPristup) {
		this.lovObjectPristup = lovObjectPristup;
	}

	public EvSifarnik getLovObjectVrsta() {
		return lovObjectVrsta;
	}

	public void setLovObjectVrsta(EvSifarnik lovObjectVrsta) {
		this.lovObjectVrsta = lovObjectVrsta;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

}
