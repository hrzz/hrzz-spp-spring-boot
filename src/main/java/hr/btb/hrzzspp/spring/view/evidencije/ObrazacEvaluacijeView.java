package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvObrazacEvaluacije;
import hr.btb.hrzzspp.spring.services.EvObrazacEvaluacijeService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class ObrazacEvaluacijeView implements Serializable {

	private static final long serialVersionUID = -1154629882301837029L;

	private static final Logger LOG = LoggerFactory
			.getLogger(ObrazacEvaluacijeView.class);

	@Autowired
	private SessionBean session;
	
	@Autowired
	private EvObrazacEvaluacijeService evObrazacEvaluacijeService;

	private EvObrazacEvaluacije oe = new EvObrazacEvaluacije();

	@PostConstruct
	public void init() {
	}

	public void save() {
		try {

			oe.setPeriod((session.getCurrentReportingPeriod()));
			oe.setUser(session.getCurrentUserData().getUser());

			evObrazacEvaluacijeService.saveObrazacEvaluacije(oe);

			PfUtils.dialogInsertSuccessful();

		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška", "Greška prilikom spremanja izvješća");
		}
		clean();
	}

	public void clean() {
		oe = new EvObrazacEvaluacije();
	}

	public SessionBean getSession() {
		return session;
	}

	public void setSession(SessionBean session) {
		this.session = session;
	}

	public EvObrazacEvaluacijeService getEvObrazacEvaluacijeService() {
		return evObrazacEvaluacijeService;
	}

	public void setEvObrazacEvaluacijeService(
			EvObrazacEvaluacijeService evObrazacEvaluacijeService) {
		this.evObrazacEvaluacijeService = evObrazacEvaluacijeService;
	}

	public EvObrazacEvaluacije getOe() {
		return oe;
	}

	public void setOe(EvObrazacEvaluacije oe) {
		this.oe = oe;
	}
	
}
