package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.business.CustomSystemData;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.SukobInteresa;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.security.ForceLogout;
import hr.btb.hrzzspp.spring.services.SukobInteresaService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.primefaces.context.RequestContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class SukobInteresaView implements Serializable {

	private static final long serialVersionUID = 1241611372979204356L;

	@Autowired
	SessionBean sessionBean;

	private Project project;
	private User user;
	private SukobInteresa selectedSukobInteresa = new SukobInteresa();
	private MenuController menuController; 
	
	@Autowired
	private SukobInteresaService sukobInteresaService;

	public SukobInteresaView() {

	}

	@PostConstruct
	public void init() {
		project = sessionBean.getCurrentProject();
		user = sessionBean.getCurrentUserData().getUser();
	}

	public void save() {
		try {
			selectedSukobInteresa.setPeriod(sessionBean
					.getCurrentReportingPeriod());
			selectedSukobInteresa.setUser(sessionBean.getCurrentUserData()
					.getUser());
			selectedSukobInteresa.setVrijemePromjene(new Timestamp(System
					.currentTimeMillis()));

			sukobInteresaService.saveSukobInteresa(selectedSukobInteresa);

			PfUtils.dialogInsertSuccessful();

		} catch (Exception e) {
			e.printStackTrace();
			PfUtils.error("Greška", "Greška prilikom spremanja izvješća");
		}
		clean();
	}

	public void clean() {
		selectedSukobInteresa = new SukobInteresa();
	}
	
	

	public void odgovorNE() {
		slanjeMailaOdgNE();
		selectedSukobInteresa.setUvjetPovjerljivostiNe(true);
		selectedSukobInteresa.setPeriod(sessionBean
				.getCurrentReportingPeriod());
		selectedSukobInteresa.setUser(sessionBean.getCurrentUserData()
				.getUser());
		selectedSukobInteresa.setVrijemePromjene(new Timestamp(System
				.currentTimeMillis()));
		sukobInteresaService.saveSukobInteresa(selectedSukobInteresa);
	}
	
	public void odgovor1(){
		selectedSukobInteresa.setUvjetPovjerljivostiDa(true);
		selectedSukobInteresa.setSudjelovanjeUtvrdjeniSukob(true);
		selectedSukobInteresa.setPeriod(sessionBean
				.getCurrentReportingPeriod());
		selectedSukobInteresa.setUser(sessionBean.getCurrentUserData()
				.getUser());
		selectedSukobInteresa.setVrijemePromjene(new Timestamp(System
				.currentTimeMillis()));
		sukobInteresaService.saveSukobInteresa(selectedSukobInteresa);
		slanjeMailaOdg1();
		//TODO odjava korisnika
		//i postavljanje statusa za tog korisnika
	}
	
	public void odgovor2(){
		selectedSukobInteresa.setUvjetPovjerljivostiDa(true);
		selectedSukobInteresa.setSudjelovanjeMoguciSukob(true);
		selectedSukobInteresa.setPeriod(sessionBean
				.getCurrentReportingPeriod());
		selectedSukobInteresa.setUser(sessionBean.getCurrentUserData()
				.getUser());
		selectedSukobInteresa.setVrijemePromjene(new Timestamp(System
				.currentTimeMillis()));
		sukobInteresaService.saveSukobInteresa(selectedSukobInteresa);
		slanjeMailaOdg2();
		//TODO odjava korisnika
		//i postavljanje statusa za tog korisnika
	}
	
	public void odgovor3(){
		selectedSukobInteresa.setUvjetPovjerljivostiDa(true);
		selectedSukobInteresa.setUser(user);
		selectedSukobInteresa.setPeriod(sessionBean.getCurrentReportingPeriod());
		selectedSukobInteresa.setVrijemePromjene(new Timestamp(System
				.currentTimeMillis()));
		sukobInteresaService.saveSukobInteresa(selectedSukobInteresa);
		//TODO odjava korisnika
		//i postavljanje statusa za tog korisnika
	}
	
	private void slanjeMailaOdg1() {
		InternetAddress[] addresses = null;
		try {
			// addresses = InternetAddress.parse(evaluator.getEmail());
			addresses = InternetAddress.parse("iris.zuza@btb.hr");
		} catch (AddressException e) {
			e.printStackTrace();
		}

		// Sender's email ID needs to be mentioned
		String from = CustomSystemData.mailFrom;

		// Assuming you are sending email from localhost
		String host = "webmail.hrzz.hr";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.put("mail.smtp.host", host);
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.port", "25");

		properties.put("mail.smtp.auth", "true");
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(CustomSystemData.userSMTP,
						CustomSystemData.passSMTP);
			}
		};

		// Get the default Session object.
		Session mailSession = Session.getDefaultInstance(properties,
				authenticator);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, addresses);
			message.addRecipient(Message.RecipientType.BCC,
					new InternetAddress("dalibor.harmina@btb.hr"));

			// Set Subject: header field
			message.setSubject("Info_SPP");

			StringBuffer sb = new StringBuffer();
			
//			 List<UserRole> role =
//			 sessionBean.getCurrentUserData().getUser().getUserRoles();
//			 for(UserRole ur : role){
//			 if(ur.getRole().getDescription().equals("Evaluator")){
//			 sb.append(ur.getRole().getDescription());
//			 }
//			 
			if (sessionBean.hasRoles(new String[] { "EVL" }))
				sb.append("Evaluator ");
			if (sessionBean.hasRoles(new String[] { "COV" }))
				sb.append("Član odbora za vrednovanje ");
			if (sessionBean.hasRoles(new String[] { "CUO" }))
				sb.append("Član upravnog odbora ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getName()
					+ " ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getSurname()
					+ " je u direktnom sukobu interesa ");
			sb.append(sessionBean.getCurrentProject().getCode() + " "
					+ sessionBean.getCurrentProject().getName() + " !");

			// Now set the actual message
			message.setText(sb.toString());

			// Send message
			Transport.send(message);

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
	
	private void slanjeMailaOdg2() {
		InternetAddress[] addresses = null;
		try {
			// addresses = InternetAddress.parse(evaluator.getEmail());
			addresses = InternetAddress.parse("iris.zuza@btb.hr");
		} catch (AddressException e) {
			e.printStackTrace();
		}

		// Sender's email ID needs to be mentioned
		String from = CustomSystemData.mailFrom;

		// Assuming you are sending email from localhost
		String host = "webmail.hrzz.hr";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.put("mail.smtp.host", host);
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.port", "25");

		properties.put("mail.smtp.auth", "true");
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(CustomSystemData.userSMTP,
						CustomSystemData.passSMTP);
			}
		};

		// Get the default Session object.
		Session mailSession = Session.getDefaultInstance(properties,
				authenticator);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, addresses);
			message.addRecipient(Message.RecipientType.BCC,
					new InternetAddress("dalibor.harmina@btb.hr"));

			// Set Subject: header field
			message.setSubject("Info_SPP");

			StringBuffer sb = new StringBuffer();

			// List<UserRole> role =
			// sessionBean.getCurrentUserData().getUser().getUserRoles();
			// for(UserRole ur : role){
			// if(ur.getRole().getDescription()=="Evaluator")
			// sb.append(ur.getRole().getDescription());
			// }
			if (sessionBean.hasRoles(new String[] { "EVL" }))
				sb.append("Evaluator ");
			if (sessionBean.hasRoles(new String[] { "COV" }))
				sb.append("Član odbora za vrednovanje ");
			if (sessionBean.hasRoles(new String[] { "CUO" }))
				sb.append("Član upravnog odbora ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getName()
					+ " ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getSurname()
					+ " je u mogućem sukobu interesa ");
			sb.append(sessionBean.getCurrentProject().getCode() + " "
					+ sessionBean.getCurrentProject().getName() + " !");

			// Now set the actual message
			message.setText(sb.toString());

			// Send message
			Transport.send(message);

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}
	

	public void slanjeMailaOdgNE() {

		InternetAddress[] addresses = null;
		try {
			// addresses = InternetAddress.parse(evaluator.getEmail());
			addresses = InternetAddress.parse("iris.zuza@btb.hr");
		} catch (AddressException e) {
			e.printStackTrace();
		}

		// Sender's email ID needs to be mentioned
		String from = CustomSystemData.mailFrom;

		// Assuming you are sending email from localhost
		String host = "webmail.hrzz.hr";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.put("mail.smtp.host", host);
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.port", "25");

		properties.put("mail.smtp.auth", "true");
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(CustomSystemData.userSMTP,
						CustomSystemData.passSMTP);
			}
		};

		// Get the default Session object.
		Session mailSession = Session.getDefaultInstance(properties,
				authenticator);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, addresses);
			message.addRecipient(Message.RecipientType.BCC,
					new InternetAddress("dalibor.harmina@btb.hr"));

			// Set Subject: header field
			message.setSubject("Info_SPP");

			StringBuffer sb = new StringBuffer();

			// List<UserRole> role =
			// sessionBean.getCurrentUserData().getUser().getUserRoles();
			// for(UserRole ur : role){
			// if(ur.getRole().getDescription()=="Evaluator")
			// sb.append(ur.getRole().getDescription());
			// }
			if (sessionBean.hasRoles(new String[] { "EVL" }))
				sb.append("Evaluator ");
			if (sessionBean.hasRoles(new String[] { "COV" }))
				sb.append("Član odbora za vrednovanje ");
			if (sessionBean.hasRoles(new String[] { "CUO" }))
				sb.append("Član upravnog odbora ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getName()
					+ " ");
			sb.append(sessionBean.getCurrentUserData().getUser().getUserData()
					.getSurname()
					+ " ne prihvaća uvijete o povjerljivosti podataka na projektu ");
			sb.append(sessionBean.getCurrentProject().getCode() + " "
					+ sessionBean.getCurrentProject().getName() + " !");

			// Now set the actual message
			message.setText(sb.toString());

			// Send message
			Transport.send(message);

		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	public SessionBean getSessionBean() {
		return sessionBean;
	}

	@Autowired
	public void setSessionBean(SessionBean sessionBean) {
		this.sessionBean = sessionBean;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public SukobInteresaService getSukobInteresaService() {
		return sukobInteresaService;
	}

	public void setSukobInteresaService(
			SukobInteresaService sukobInteresaService) {
		this.sukobInteresaService = sukobInteresaService;
	}

	public SukobInteresa getSelectedSukobInteresa() {
		return selectedSukobInteresa;
	}

	public void setSelectedSukobInteresa(SukobInteresa selectedSukobInteresa) {
		this.selectedSukobInteresa = selectedSukobInteresa;
	}

	public MenuController getMenuController() {
		return menuController;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

}
