package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvFile;
import hr.btb.hrzzspp.jpa.FilePublikacija;
import hr.btb.hrzzspp.jpa.FileRad;
import hr.btb.hrzzspp.jpa.FileRezultat;
import hr.btb.hrzzspp.jpa.FileZnRad;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("session")
public class FileUploadBean {
	
	private static final Logger LOG = LoggerFactory.getLogger(FileUploadBean.class);

	protected UploadedFile newFile;
	protected EvFile evFile;
	protected String fileEntityName = "EvFile";
	protected FileRelatedObjectInterface selectedRow;
	protected String poveznica;
	
	public FileUploadBean clean() {
		this.evFile = null;
		this.selectedRow = null;
		this.newFile = null;
		this.poveznica = null;
		
		return this;
	}

	public void handleFileUpload(FileUploadEvent fileUploadEvent) {
		this.newFile = fileUploadEvent.getFile();
		LOG.debug("... fileUploadEvent: {} ...", fileUploadEvent.getSource());

		if (newFile != null) {
//			PfUtils.info("Datoteka uspješno podignuta.", "Veličina datoteke: "
//					+ newFile.getSize() + " Byte");

			try {
				EvFile evFile = null;
				switch(fileEntityName) {				
				case ConstantsEvidencije.FILE_REZULTAT:
					evFile = new FileRezultat();
					break;
				case ConstantsEvidencije.FILE_RADOVI:
					evFile = new FileRad();
					break;
				case ConstantsEvidencije.FILE_ZN_RADOVI:
					evFile = new FileZnRad();
					break;
				case ConstantsEvidencije.FILE_PUBLIKACIJA:
					evFile = new FilePublikacija();
					break;
				default:
					PfUtils.error("Greška kod kreiranja objekta datoteke!", "Nisam uspio pronaći pravu tablicu za datoteku");
				}
				
				if (evFile != null) {
					evFile.setFileName(newFile.getFileName());
					evFile.setFileSize(newFile.getSize());
					evFile.setFileType(newFile.getContentType());
					evFile.setFileBlob(newFile.getContents());
	
					this.evFile = evFile;
	
					LOG.debug("FILE SIZE: {} | BLOB length: {}", newFile.getSize(),
							newFile.getContents().length);
				}
			} catch (Exception e) {
				PfUtils.error("Greška kod kreiranja novog objekta datoteke!",
						e.toString());
			}
		} else {
			PfUtils.warn("Datoteka nije uspješno učitana!",
					"Veličina datoteke je 0 Byte. Je li to u redu?");
		}
	}
	
	public void markForDelete() {
		selectedRow.setFile(null);
	}

	public UploadedFile getNewFile() {
		return newFile;
	}

	public void setNewFile(UploadedFile newFile) {
		this.newFile = newFile;
	}

	public EvFile getEvFile() {
		return evFile;
	}

	public void setEvFile(EvFile evFile) {
		this.evFile = evFile;
	}

	public String getFileEntityName() {
		return fileEntityName;
	}

	public void setFileEntityName(String fileEntityName) {
		this.fileEntityName = fileEntityName;
	}	
	
	public Object getSelectedRow() {
		return selectedRow;
	}

	public void setSelectedRow(FileRelatedObjectInterface selectedRow) {
		this.selectedRow = selectedRow;
		this.poveznica = selectedRow.getPoveznica();
	}
	
	public String getCurrentFileName() {
		LOG.debug("... getting relevant file name ...");
		if (newFile != null) {
			return newFile.getFileName();

		} else {
			return selectedRow.getFile().getFileName();
		}
	}
	

	public String getPoveznica() {
		return poveznica;
	}

	public void setPoveznica(String poveznica) {
		this.poveznica = poveznica;
	}

	@Override
	public String toString() {
		return String
				.format("FileUploadBean [newFile=%s, evFile=%s, fileEntityName=%s, selectedRow=%s]",
						newFile, evFile, fileEntityName, selectedRow);
	}
	
	
}
