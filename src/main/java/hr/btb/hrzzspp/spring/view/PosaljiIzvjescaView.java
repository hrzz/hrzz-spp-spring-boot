/**
 * 
 */
package hr.btb.hrzzspp.spring.view;

import hr.btb.hrzzspp.business.CustomSystemData;
import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.FileFIzvjesce;
import hr.btb.hrzzspp.jpa.FileKKonta;
import hr.btb.hrzzspp.jpa.FileOIzvjesce;
import hr.btb.hrzzspp.jpa.FilePFPlana;
import hr.btb.hrzzspp.jpa.FilePRPlana;
import hr.btb.hrzzspp.jpa.FileROpreme;
import hr.btb.hrzzspp.jpa.FileRacuni;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;
import hr.btb.hrzzspp.spring.services.EvidencijaService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.view.evidencije.EvidencijeVODView;
import hr.btb.hrzzspp.spring.view.evidencije.NarativnoIzvjesceView;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */

@Component
@Scope("session")
public class PosaljiIzvjescaView {

	private SessionBean session;
	private EvNarativnoIzvjesce evNarativnoIzvjesce;
	
	@Autowired
	private ReportingPeriodsService reportingPeriodsService;
	@Autowired
	private EvNarativnoIzvjesceService evNarativnoIzvjesceService;
	@Autowired
	private EvidencijaService evidencijaService;
	
	@Autowired
	private EvidencijeVODView evidencijeVODView;
	
	@Autowired
	private MenuController menuController;

	public void sendReports() {
		
		evNarativnoIzvjesce = evNarativnoIzvjesceService
				.getEvNarativnoIzvjesceByPeriod(session.getCurrentReportingPeriod());
		if(evNarativnoIzvjesce !=null && evNarativnoIzvjesce.getEvNiFiles().size() > 6){
			int koliko=0;
			for(EvNiFile evNiFile: evNarativnoIzvjesce.getEvNiFiles()){
				if(evNiFile instanceof FileOIzvjesce || evNiFile instanceof FileRacuni || evNiFile instanceof FileFIzvjesce || evNiFile instanceof FileROpreme 
						|| evNiFile instanceof FileKKonta || evNiFile instanceof FilePFPlana || evNiFile instanceof FilePRPlana){
					if(evNiFile != null){
						koliko++;
					}
				}
			}
			if(koliko<7){
				PfUtils.error("Greška","Niste uploadali sve dokumente");
				return;
			}
		}else{
			PfUtils.error("Greška","Niste uploadali sve dokumente");
			return;
			
		}
		if(NarativnoIzvjesceView.nisuPopunjenaSvaPolja(session.getCurrentProject(), evNarativnoIzvjesce)){
			PfUtils.error("Greška","Niste popunili sva polja u opisnom izvješću!");
			return;
		}
			
		PfUtils.info("Slanje izvješća","Provjera uspješna. Molimo pričekajte slanje izvješća...");
		// Recipient's email ID needs to be mentioned.
	//	String to = "koordinatori@hrzz.hr,dalibor.harmina@btb.hr"; // Koji email????
		
		// Multiple Recipient Address[] addresses
		InternetAddress[] addresses = null;
		try {
			addresses = InternetAddress.parse(session.getCurrentProject().getMajorScientificField().getMail());
		} catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Sender's email ID needs to be mentioned
		String from = CustomSystemData.mailFrom;

		// Assuming you are sending email from localhost
		String host = "webmail.hrzz.hr";

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.put("mail.smtp.host", host);
		properties.put("mail.transport.protocol", "smtp");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.port", "25"); 

		properties.put("mail.smtp.auth", "true");
		Authenticator authenticator = new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(CustomSystemData.userSMTP,
						CustomSystemData.passSMTP);
			}
		};
		
		// Get the default Session object.
		Session mailSession = Session.getDefaultInstance(properties,
				authenticator);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
		/*	message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to)); */
			
			// Set To: header field of the header.
			message.addRecipients(Message.RecipientType.TO, addresses);
			message.addRecipient(Message.RecipientType.BCC, new InternetAddress("dalibor.harmina@btb.hr"));
			
			// Set Subject: header field
			message.setSubject("Opisno izvješće");

			StringBuffer sb=new StringBuffer();
			sb.append("Voditelj " + session.getNameAndSurname() + " je isporučio opisno izvješće za " + 
					session.getRpDateFromFormat() + "-" + session.getRpDateToFormat() + " razdoblje na evaluaciji.");
			sb.append("\n");
			sb.append("Šifra projekta: " + session.getCurrentProject().getCode());
			sb.append("\n");
			sb.append("Naziv projekta: " + session.getCurrentProject().getName());
			sb.append("\n");
			sb.append(" ");
			sb.append("\n");
			sb.append("Znanstveno područje: "+ session.getCurrentProject().getMajorScientificField().getName());
			
			// Now set the actual message
			message.setText(sb.toString()); 

			// Send message
			Transport.send(message);
			//Status perioda u 2 - Poslano izvješće
			ReportingPeriod rp = session.getCurrentReportingPeriod();
			rp.setStatus("2");
			
			evidencijeVODView.changeStatusEvidencijeForPeriod("ISPORUCI");
			reportingPeriodsService.saveReportingPeriod(rp);
			
			//refresh stranice
			menuController.menuAction("evidencijeVOD", "1");
			
			PfUtils.info("Slanje izvješća","Izvješće uspješno poslano.");
			
		} catch (Exception mex) {
			mex.printStackTrace();
		}
	}
	
	@Autowired
	public void setSessionBean(SessionBean session) {
		this.session = session;
	}

	public ReportingPeriodsService getReportingPeriodsService() {
		return reportingPeriodsService;
	}

	public void setReportingPeriodsService(
			ReportingPeriodsService reportingPeriodsService) {
		this.reportingPeriodsService = reportingPeriodsService;
	}

	public EvNarativnoIzvjesceService getEvNarativnoIzvjesceService() {
		return evNarativnoIzvjesceService;
	}

	public void setEvNarativnoIzvjesceService(
			EvNarativnoIzvjesceService evNarativnoIzvjesceService) {
		this.evNarativnoIzvjesceService = evNarativnoIzvjesceService;
	}

	public EvNarativnoIzvjesce getEvNarativnoIzvjesce() {
		return evNarativnoIzvjesce;
	}

	public void setEvNarativnoIzvjesce(EvNarativnoIzvjesce evNarativnoIzvjesce) {
		this.evNarativnoIzvjesce = evNarativnoIzvjesce;
	}

	public EvidencijeVODView getEvidencijeVODView() {
		return evidencijeVODView;
	}

	public void setEvidencijeVODView(EvidencijeVODView evidencijeVODView) {
		this.evidencijeVODView = evidencijeVODView;
	}

	public MenuController getMenuController() {
		return menuController;
	}

	public void setMenuController(MenuController menuController) {
		this.menuController = menuController;
	}

	
}
