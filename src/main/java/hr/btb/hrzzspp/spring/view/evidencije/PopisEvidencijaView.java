package hr.btb.hrzzspp.spring.view.evidencije;

import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.EvObrazacEvaluacije;
import hr.btb.hrzzspp.jpa.Evaluator;
import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.FileFIzvjesce;
import hr.btb.hrzzspp.jpa.FileKKonta;
import hr.btb.hrzzspp.jpa.FileOIzvjesce;
import hr.btb.hrzzspp.jpa.FileOstalo1;
import hr.btb.hrzzspp.jpa.FileOstalo2;
import hr.btb.hrzzspp.jpa.FileOstalo3;
import hr.btb.hrzzspp.jpa.FileOstalo4;
import hr.btb.hrzzspp.jpa.FileOstalo5;
import hr.btb.hrzzspp.jpa.FilePFPlana;
import hr.btb.hrzzspp.jpa.FilePRPlana;
import hr.btb.hrzzspp.jpa.FileROpreme;
import hr.btb.hrzzspp.jpa.FileRacuni;
import hr.btb.hrzzspp.jpa.PeriodIzvjestaj;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.jpa.VrednovanjeRezIzvjesce;
import hr.btb.hrzzspp.spring.controller.MenuController;
import hr.btb.hrzzspp.spring.security.SecurityChecker;
import hr.btb.hrzzspp.spring.services.EvNarativnoIzvjesceService;
import hr.btb.hrzzspp.spring.services.EvObrazacEvaluacijeService;
import hr.btb.hrzzspp.spring.services.EvaluatorService;
import hr.btb.hrzzspp.spring.services.EvidencijaService;
import hr.btb.hrzzspp.spring.services.ReportingPeriodsService;
import hr.btb.hrzzspp.spring.services.StatusRolePeriodService;
import hr.btb.hrzzspp.spring.services.UserRolePeriodStatusService;
import hr.btb.hrzzspp.spring.services.VrednovanjeRezIzvjesceService;
import hr.btb.hrzzspp.spring.view.PfUtils;
import hr.btb.hrzzspp.spring.view.SessionBean;
import hr.btb.hrzzspp.spring.view.TreeNodeUtil;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.TreeNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * @author dalibor.harmina
 *
 */
@Component
@Scope("session")
public class PopisEvidencijaView {

	// private static final long serialVersionUID = 2377724969174882856L;

	private List<Evidencija> evidencije = new ArrayList<>();
	private ReportingPeriod noviPeriod;
	private String activeIndex;
	private final MenuController menuController;
	private final SessionBean session;
	private final EvidencijaService evidencijaService;
	private Evidencija selectedEvidencija = new Evidencija();
	private ReportingPeriod selectedReportingPeriod;
	private List<ReportingPeriod> reportingPeriods;
	private EvNarativnoIzvjesce narativnoIzvjesce;
	private List<Evaluator> evaluatorList = new ArrayList<Evaluator>();
	List<UserRolePeriodStatus> urpsEVLList = new ArrayList<UserRolePeriodStatus>();
	List<UserRolePeriodStatus> urpsCOVList = new ArrayList<UserRolePeriodStatus>();
	List<UserRolePeriodStatus> urpsCUOList = new ArrayList<UserRolePeriodStatus>();
	List<EvObrazacEvaluacije> evalIzvjescaList = new ArrayList<EvObrazacEvaluacije>();
	private String msg;

	private List<Evidencija> evidencijeList = new ArrayList<Evidencija>();

	private List<VrednovanjeRezIzvjesce> vrednovanjeRezIzvjesceList = new ArrayList<VrednovanjeRezIzvjesce>();

	private VrednovanjeRezIzvjesce vrednovanjeRezIzvjesce = new VrednovanjeRezIzvjesce();

	@Autowired
	private VrednovanjeRezIzvjesceService rezultatiVrednovanjaIzvjService;
	
	@Autowired
	private ObrazacEvaluacijeView obrazacEvaluacijeView;

	@Autowired
	private ReportingPeriodsService reportingPeriodService;
	
	@Autowired
	private UserRolePeriodStatusService userRolePeriodStatusService;

	@Autowired
	private ArhivaView arhivaView;
	
	@Autowired
	private TreeNodeUtil treeNodeUtil;

	@Autowired
	private EvNarativnoIzvjesceService evNarativnoIzvjesceService;

	@Autowired
	private EvaluatorService evaluatorService;
	
	@Autowired
	private EvObrazacEvaluacijeService evObrazacEvaluacijeService;
	
	@Autowired
	private StatusRolePeriodService statusRolePeriodService;
	
	private String nazivMarkeri;
	private String datumMarkeri;
	private String satiMarkeri;
	
	TreeNode selectedNode;

	@Autowired
	public PopisEvidencijaView(MenuController menuController,
			SessionBean session, EvidencijaService evidencijaService) {
		this.menuController = menuController;
		this.session = session;
		this.evidencijaService = evidencijaService;
	}

	@Autowired
	private OsobljeView osobljeView;

	public void setOsobljeView(OsobljeView osobljeView) {
		this.osobljeView = osobljeView;
	}
	
//	@Autowired
//	private RezultatiVrednovanjaView rezultatiVrednovanjaView;

	@Autowired
	ReportingPeriodsService reportingPeriodsService;
	public String akcijaPrka;

	@PostConstruct
	public void init() {
		evidencijeList = evidencijaService
				.getEvidencijaByPeriod(session.getCurrentReportingPeriod());
		narativnoIzvjesce = evNarativnoIzvjesceService
				.getEvNarativnoIzvjesceByPeriod(session.getCurrentReportingPeriod());
		evaluatorList = evaluatorService
				.getEvaluatorByPeriod(session.getCurrentReportingPeriod());
		//popuni listu evaluatora u statusna polja
//		urpsEVLList = userRolePeriodStatusService.findByPeriodAndRole(session.getCurrentReportingPeriod(), new Role(3));	
		//popuni listu vrednovatelja u statusna polja
//		urpsCOVList = userRolePeriodStatusService.findByPeriodAndRole(session.getCurrentReportingPeriod(), new Role(4));
		//popuni listu upravni odbor u statusna polja
//		urpsCUOList = userRolePeriodStatusService.findByPeriodAndRole(session.getCurrentReportingPeriod(), new Role(5));	
		
		evalIzvjescaList = evObrazacEvaluacijeService.getEvalIzvjesca(session.getCurrentReportingPeriod());
		
		if( session.getCurrentReportingPeriod() !=null ){
			vrednovanjeRezIzvjesceList = rezultatiVrednovanjaIzvjService
					.findRezVredByPeriodUserLastModified(session.getCurrentReportingPeriod().getId());
			if(vrednovanjeRezIzvjesceList.size()>0){
				vrednovanjeRezIzvjesce = vrednovanjeRezIzvjesceList.get(0);
			}
		}
		
		fillEvidencije();
	}

	public void goToArhiva() {

		arhivaView.init();
		menuController.menuAction("arhiva", "1");
	}
	
	public void fillEvidencije() {
		evidencije=evidencijaService.getEvidencijaByPeriod(session.getCurrentReportingPeriod());
	}

	public void clear() {
		this.noviPeriod = new ReportingPeriod();
	}

	public void navigaj(String periodId) {

		if (periodId != null) {

			session.setCurrentReportingPeriod(reportingPeriodsService
					.getReportingPeriodById(Integer.valueOf(periodId)
							.intValue()));
			navigaj();
		}
	}

	public void navigaj() {

		if (session.getCurrentReportingPeriod() == null) {

			PfUtils.error("Razdoblje", "Nije odabrano razdoblje");

		} else {

			String pageId = selectedEvidencija.getNazivXhtml();// session.getEvidencijaSelected().getNazivXhtml();

			switch (pageId) {
			case "osoblje":
				osobljeView.init();
				break;
			default:
				break;
			}

			menuController.menuAction(pageId, "1");
		}
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getMsg() {
		StringBuilder sb = new StringBuilder();
		sb.append("Odabirom opcije Pošalji izvješća zaključavate unose za cijelo razdoblje.Više nećete ");
		sb.append(System.getProperty("line.separator"));
		sb.append("\n");
		sb.append("\n moći mijenjati niti dodavati podatke i dokumente,a izvješće će biti isporučeno");
		sb.append(" na daljnju obradu. Želite li nastaviti s radom?");

		return sb.toString();
	}

	public void insert() {
		Project project = new Project();
		project.setId(1329L);
		noviPeriod.setProject(project);

		noviPeriod.setIsFinal("N");
		noviPeriod.setOrdinalNumber(2);
	}

	public List<Evidencija> getEvidencije() {
		return evidencije;
	}

	public void spremiPRK() {

		try {
			selectedEvidencija.setReportingPeriod(session.getCurrentReportingPeriod());
			evidencijaService.saveEvidencija(selectedEvidencija);

			menuController.goToPopisEvidencija();
			PfUtils.dialogUpdateSuccessful();

		} catch (Exception e) {
			PfUtils.info("Error!!");
			e.printStackTrace();
		}
	}
	
	public String postaviIkonuOpcija(Evidencija evidencija) {
		
		String result = "";
		
		int status = evidencija.getStatusPrk();
		
		if (SecurityChecker.hasRoles(new String[] { "PRK" })) {
			
			if (status == 1)
				result = "ikona-kvacica button-prk";
			else if (status == 2)
				result = "ikona-usklicnik button-prk";
			else if (status == 3)
				result = "ikona-upitnik button-prk";
			else if (status == 4)
				result = "ikona-upitnik button-prk";
			else
				result = "ikona-prk button-prk";
		}
		else if (SecurityChecker.hasRoles(new String[] { "EVL" })) {
			
			if (evidencija.getKomentarPrk() != null ? !evidencija.getKomentarPrk().equals("") : false)
				result = "ikona-oblak-crna button-evl";
			else
				result = "visibility-hidden";
		}
		
		return result;
	}
	
	public void onNodeSelect(NodeSelectEvent event) {
		
        PeriodIzvjestaj selectedPI = (PeriodIzvjestaj) selectedNode.getData();
        String tip = selectedPI.getTip();
        
        if (tip != null) {
        
        	if (tip.equals(TreeNodeUtil.STAT_EVID)) {
        		
        		if (selectedPI.getUserId() == 0) {
        			
        			// TODO klik na evidencija root
        		}
        		else {
        			
        		}
        	}
        	else if (tip.equals(TreeNodeUtil.STAT_EVL)) {
        		
        		if (selectedPI.getUserId() == 0) {
        			
        			// TODO klik na vrednovanje root
        		}
        		else {
        			
					EvObrazacEvaluacije oe = evObrazacEvaluacijeService
							.getEvalIzvjesceByUserAndPerid(
									new User(selectedPI.getUserId()),
									new ReportingPeriod(selectedPI
											.getPeriodId()));
        			
					if (oe != null)
						obrazacEvaluacijeView.setOe(oe);
					else
						obrazacEvaluacijeView.setOe(new EvObrazacEvaluacije());
					
					RequestContext.getCurrentInstance().update("evaluacijskoIzvjesceForm");
        		}
        	}
        	else if (tip.equals(TreeNodeUtil.STAT_OEVL)) {
        		
        		if (selectedPI.getUserId() == 0) {
        			
        			// TODO klik na odbor za vrednovanje root
        		}
        		else {
        			
        		}
        	}
        	else if (tip.equals(TreeNodeUtil.STAT_UO)) {
        		
        		if (selectedPI.getUserId() == 0) {
        			
        			// TODO klik na upravni odbor root
        		}
        		else {
        			
        		}
        	}
        	else if (tip.equals(TreeNodeUtil.STAT_FIN)) {
        		
        		if (selectedPI.getUserId() == 0) {
        			
        			// TODO klik na financiranje root
        		}
        		else {
        			
        		}
        	}
        }
    }

	public void setCurrentReportingPeriodOpisnoIzvj() {

		session.setCurrentReportingPeriod(session.getCurrentReportingPeriod());
		this.narativnoIzvjesce = evNarativnoIzvjesceService
				.getEvNarativnoIzvjesceByPeriod(session.getCurrentReportingPeriod());

		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('uploadFileDialog').show();");

		// this.narativnoIzvjesce = evNarativnoIzvjesceService
		// .getEvNarativnoIzvjesceByPeriod(selectedReportingPeriod);

	}

	public void setCurrentReportingPeriod() {

//		session.setCurrentReportingPeriod(selectedReportingPeriod);
	}

	public void setEvidencije(List<Evidencija> p_evidencije) {
		evidencije = p_evidencije;
	}

	public Evidencija getEvidencijaSelected() {
		return session.getEvidencijaSelected();
	}

	public void setEvidencijaSelected(Evidencija evidencijaSelected) {
		this.session.setEvidencijaSelected(evidencijaSelected);
	}

	public ReportingPeriod getNoviPeriod() {
		return noviPeriod;
	}

	public void setNoviPeriod(ReportingPeriod noviPeriod) {
		this.noviPeriod = noviPeriod;
	}

	public String getActiveIndex() {

		if (SecurityChecker.hasRoles(new String[] { "PRK" })) {
			activeIndex = "0";
		} else {
			activeIndex = "0,1,2";
		}

		return activeIndex;
	}

	public void setActiveIndex(String activeIndex) {
		this.activeIndex = activeIndex;
	}

	public Evidencija getSelectedEvidencija() {
		return selectedEvidencija;
	}

	public void setSelectedEvidencija(Evidencija selectedEvidencija) {
		this.selectedEvidencija = selectedEvidencija;
	}

	public ReportingPeriod getSelectedReportingPeriod() {
		return selectedReportingPeriod;
	}

	public void setSelectedReportingPeriod(
			ReportingPeriod selectedReportingPeriod) {
		this.selectedReportingPeriod = selectedReportingPeriod;
	}

	public List<ReportingPeriod> getReportingPeriods() {
		return reportingPeriods;
	}

	public void setReportingPeriods(List<ReportingPeriod> reportingPeriods) {
		this.reportingPeriods = reportingPeriods;
	}

	public ReportingPeriodsService getReportingPeriodsService() {
		return reportingPeriodsService;
	}

	public void setReportingPeriodsService(
			ReportingPeriodsService reportingPeriodsService) {
		this.reportingPeriodsService = reportingPeriodsService;
	}

	public ArhivaView getArhivaView() {
		return arhivaView;
	}

	public void setArhivaView(ArhivaView arhivaView) {
		this.arhivaView = arhivaView;
	}

	public EvNarativnoIzvjesceService getEvNarativnoIzvjesceService() {
		return evNarativnoIzvjesceService;
	}

	public void setEvNarativnoIzvjesceService(
			EvNarativnoIzvjesceService evNarativnoIzvjesceService) {
		this.evNarativnoIzvjesceService = evNarativnoIzvjesceService;
	}

	public EvNarativnoIzvjesce getNarativnoIzvjesce() {
		return narativnoIzvjesce;
	}

	public void setNarativnoIzvjesce(EvNarativnoIzvjesce narativnoIzvjesce) {
		this.narativnoIzvjesce = narativnoIzvjesce;
	}

	public FileOIzvjesce getOpisnoIzvjesce() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOIzvjesce)
					return (FileOIzvjesce) evNiFile;
			}
		}
		return null;
	}

	public FileFIzvjesce getFinancijskoIzvjesce() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileFIzvjesce)
					return (FileFIzvjesce) evNiFile;
			}
		}
		return null;
	}

	public FileROpreme getRegistarOpreme() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileROpreme)
					return (FileROpreme) evNiFile;
			}
		}
		return null;
	}

	public FileRacuni getRacuni() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileRacuni)
					return (FileRacuni) evNiFile;
			}
		}
		return null;
	}

	public FileKKonta getKarticaKonta() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileKKonta)
					return (FileKKonta) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo1 getOstalo1() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo1)
					return (FileOstalo1) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo2 getOstalo2() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo2)
					return (FileOstalo2) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo3 getOstalo3() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo3)
					return (FileOstalo3) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo4 getOstalo4() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo4)
					return (FileOstalo4) evNiFile;
			}
		}
		return null;
	}

	public FileOstalo5 getOstalo5() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FileOstalo5)
					return (FileOstalo5) evNiFile;
			}
		}
		return null;
	}

	public FilePFPlana getFinancijskiPlan() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FilePFPlana)
					return (FilePFPlana) evNiFile;
			}
		}
		return null;
	}

	public FilePRPlana getRadniPlan() {
		if (this.narativnoIzvjesce != null) {
			for (EvNiFile evNiFile : this.narativnoIzvjesce.getEvNiFiles()) {
				if (evNiFile instanceof FilePRPlana)
					return (FilePRPlana) evNiFile;
			}
		}
		return null;
	}

	public List<Evidencija> getEvidencijeList() {
		return evidencijeList;
	}

	public void setEvidencijeList(List<Evidencija> evidencijeList) {
		this.evidencijeList = evidencijeList;
	}

	public List<Evaluator> getEvaluatorList() {
		return evaluatorList;
	}

	public void setEvaluatorList(List<Evaluator> evaluatorList) {
		this.evaluatorList = evaluatorList;
	}

	public EvaluatorService getEvaluatorService() {
		return evaluatorService;
	}

	public void setEvaluatorService(EvaluatorService evaluatorService) {
		this.evaluatorService = evaluatorService;
	}

	public VrednovanjeRezIzvjesceService getRezultatiVrednovanjaIzvjService() {
		return rezultatiVrednovanjaIzvjService;
	}

	public void setRezultatiVrednovanjaIzvjService(
			VrednovanjeRezIzvjesceService rezultatiVrednovanjaIzvjService) {
		this.rezultatiVrednovanjaIzvjService = rezultatiVrednovanjaIzvjService;
	}

	public List<VrednovanjeRezIzvjesce> getVrednovanjeRezIzvjesceList() {
		return vrednovanjeRezIzvjesceList;
	}

	public void setVrednovanjeRezIzvjesceList(
			List<VrednovanjeRezIzvjesce> vrednovanjeRezIzvjesceList) {
		this.vrednovanjeRezIzvjesceList = vrednovanjeRezIzvjesceList;
	}

	public VrednovanjeRezIzvjesce getVrednovanjeRezIzvjesce() {
		return vrednovanjeRezIzvjesce;
	}

	public void setVrednovanjeRezIzvjesce(
			VrednovanjeRezIzvjesce vrednovanjeRezIzvjesce) {
		this.vrednovanjeRezIzvjesce = vrednovanjeRezIzvjesce;
	}

	public UserRolePeriodStatusService getUserRolePeriodStatusService() {
		return userRolePeriodStatusService;
	}

	public void setUserRolePeriodStatusService(
			UserRolePeriodStatusService userRolePeriodStatusService) {
		this.userRolePeriodStatusService = userRolePeriodStatusService;
	}

	public List<UserRolePeriodStatus> getUrpsEVLList() {
		return urpsEVLList;
	}

	public void setUrpsEVLList(List<UserRolePeriodStatus> urpsEVLList) {
		this.urpsEVLList = urpsEVLList;
	}

	public List<UserRolePeriodStatus> getUrpsCOVList() {
		return urpsCOVList;
	}

	public void setUrpsCOVList(List<UserRolePeriodStatus> urpsCOVList) {
		this.urpsCOVList = urpsCOVList;
	}

	public List<UserRolePeriodStatus> getUrpsCUOList() {
		return urpsCUOList;
	}

	public void setUrpsCUOList(List<UserRolePeriodStatus> urpsCUOList) {
		this.urpsCUOList = urpsCUOList;
	}

	public List<EvObrazacEvaluacije> getEvalIzvjescaList() {
		return evalIzvjescaList;
	}

	public void setEvalIzvjescaList(List<EvObrazacEvaluacije> evalIzvjescaList) {
		this.evalIzvjescaList = evalIzvjescaList;
	}

	public ReportingPeriodsService getReportingPeriodService() {
		return reportingPeriodService;
	}

	public void setReportingPeriodService(
			ReportingPeriodsService reportingPeriodService) {
		this.reportingPeriodService = reportingPeriodService;
	}

	public EvObrazacEvaluacijeService getEvObrazacEvaluacijeService() {
		return evObrazacEvaluacijeService;
	}

	public void setEvObrazacEvaluacijeService(
			EvObrazacEvaluacijeService evObrazacEvaluacijeService) {
		this.evObrazacEvaluacijeService = evObrazacEvaluacijeService;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public MenuController getMenuController() {
		return menuController;
	}

	public SessionBean getSession() {
		return session;
	}

	public EvidencijaService getEvidencijaService() {
		return evidencijaService;
	}

	public OsobljeView getOsobljeView() {
		return osobljeView;
	}

	public StatusRolePeriodService getStatusRolePeriodService() {
		return statusRolePeriodService;
	}

	public void setStatusRolePeriodService(
			StatusRolePeriodService statusRolePeriodService) {
		this.statusRolePeriodService = statusRolePeriodService;
	}

	public String getNazivMarkeri() {
		return nazivMarkeri;
	}

	public String getDatumMarkeri() {
		return datumMarkeri;
	}

	public String getSatiMarkeri() {
		return satiMarkeri;
	}

	public ObrazacEvaluacijeView getObrazacEvaluacijeView() {
		return obrazacEvaluacijeView;
	}

	public void setObrazacEvaluacijeView(ObrazacEvaluacijeView obrazacEvaluacijeView) {
		this.obrazacEvaluacijeView = obrazacEvaluacijeView;
	}

	public void setNazivMarkeri(String nazivMarkeri) {
		this.nazivMarkeri = nazivMarkeri;
	}

	public void setDatumMarkeri(String datumMarkeri) {
		this.datumMarkeri = datumMarkeri;
	}

	public void setSatiMarkeri(String satiMarkeri) {
		this.satiMarkeri = satiMarkeri;
	}

	public TreeNodeUtil getTreeNodeUtil() {
		return treeNodeUtil;
	}

	public void setTreeNodeUtil(TreeNodeUtil treeNodeUtil) {
		this.treeNodeUtil = treeNodeUtil;
	}

	public String getAkcijaPrka() {
		return akcijaPrka;
	}

	public void setAkcijaPrka(String akcijaPrka) {
		this.akcijaPrka = akcijaPrka;
	}
	
	
//	@Autowired
//	public void setRezultatiVrednovanjaView(
//			RezultatiVrednovanjaView rezultatiVrednovanjaView) {
//		this.rezultatiVrednovanjaView = rezultatiVrednovanjaView;
//	}

}
