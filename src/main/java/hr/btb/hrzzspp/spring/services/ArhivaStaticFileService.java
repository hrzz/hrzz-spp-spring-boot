package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.ArhivaStaticFile;
import hr.btb.hrzzspp.spring.repository.ArhivaStaticFileRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ArhivaStaticFileService {

	private static final Logger LOG = LoggerFactory
			.getLogger(ArhivaStaticFileService.class);
	private final ArhivaStaticFileRepository arhivaStaticFileRepository;

	@Autowired
	public ArhivaStaticFileService(
			final ArhivaStaticFileRepository arhivaStaticFileRepository) {
		this.arhivaStaticFileRepository = arhivaStaticFileRepository;
	}

	public List<ArhivaStaticFile> getArhivaStaticFiles() {
		return arhivaStaticFileRepository.findAll();
	}

	public ArhivaStaticFileRepository getArhivaStaticFileRepository() {
		return arhivaStaticFileRepository;
	}

}
