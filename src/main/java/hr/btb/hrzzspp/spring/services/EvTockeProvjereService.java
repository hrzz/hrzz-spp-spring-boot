/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import java.util.List;

import hr.btb.hrzzspp.jpa.EvTockeProvjere;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvTockeProvjereRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dalibor.harmina
 *
 */
@Service
public class EvTockeProvjereService {

	private final EvTockeProvjereRepository evTockeProvjereRepository;
	
	@Autowired
	public EvTockeProvjereService(final EvTockeProvjereRepository evTockeProvjereRepository) {
		this.evTockeProvjereRepository = evTockeProvjereRepository;
	}
	
	public List<EvTockeProvjere> getEvTockeProvjere(){
		return evTockeProvjereRepository.findAll();
	}
	
	public List<EvTockeProvjere> getEvTockeProvjereByPeriod(ReportingPeriod reportingPeriod){
		return evTockeProvjereRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveTockeProvjere(EvTockeProvjere evTockeProvjere){
		evTockeProvjereRepository.save(evTockeProvjere);
	}
	
	public void deleteTockeProvjere(EvTockeProvjere evTockeProvjere){
		evTockeProvjereRepository.delete(evTockeProvjere);
	}
}
