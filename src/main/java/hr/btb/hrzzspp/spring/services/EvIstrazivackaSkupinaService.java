/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvIstrazivackaSkupina;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvIstrazivackaSkupinaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class EvIstrazivackaSkupinaService {

	/**
	 * 
	 */
	private final EvIstrazivackaSkupinaRepository evIstrazivackaSkupinaRepository;
	
	@Autowired
	public EvIstrazivackaSkupinaService(final EvIstrazivackaSkupinaRepository evIstrazivackaSkupinaRepository) {
		this.evIstrazivackaSkupinaRepository = evIstrazivackaSkupinaRepository;
	}
	
	public List<EvIstrazivackaSkupina> getEvIstrazivackaSkupina(){
		return evIstrazivackaSkupinaRepository.findAll();
	}
	
	public List<EvIstrazivackaSkupina> getEvIstrazivackaSkupinaByPeriod(ReportingPeriod reportingPeriod){
		return evIstrazivackaSkupinaRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveIstrazivackaSkupina(EvIstrazivackaSkupina evIstrazivackaSkupina){
		evIstrazivackaSkupinaRepository.save(evIstrazivackaSkupina);
	}
	
	public void deleteIstrazivackaSkupina(EvIstrazivackaSkupina evIstrazivackaSkupina){
		evIstrazivackaSkupinaRepository.delete(evIstrazivackaSkupina);
	}

}
