package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvObrazacEvaluacije;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.spring.repository.EvObrazacEvaluacijeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvObrazacEvaluacijeService {
	
	private final EvObrazacEvaluacijeRepository evObrazacEvaluacijeRepository;
	
	@Autowired
	public EvObrazacEvaluacijeService(final EvObrazacEvaluacijeRepository evObrazacEvaluacijeRepository){
		this.evObrazacEvaluacijeRepository = evObrazacEvaluacijeRepository;
	}
	
	public void saveObrazacEvaluacije(EvObrazacEvaluacije evObrazacEvaluacije){
		evObrazacEvaluacijeRepository.save(evObrazacEvaluacije);
	}
	
	public List<EvObrazacEvaluacije> getEvalIzvjesca(ReportingPeriod period){
		return evObrazacEvaluacijeRepository.findByPeriod(period);
	}
	
	public EvObrazacEvaluacije getEvalIzvjesceByUserAndPerid(User user, ReportingPeriod period){
		return evObrazacEvaluacijeRepository.findByUserAndPeriod(user, period);
	}
	
}
