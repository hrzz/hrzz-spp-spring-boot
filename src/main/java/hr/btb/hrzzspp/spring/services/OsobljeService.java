package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvOsoblje;
import hr.btb.hrzzspp.jpa.EvOsobljeBrOsoba;
import hr.btb.hrzzspp.jpa.EvSifarnik;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvOsobljeBrOsobaRepository;
import hr.btb.hrzzspp.spring.repository.EvOsobljeRepository;
import hr.btb.hrzzspp.spring.repository.EvSifarnikRepository;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OsobljeService {

	private static final Logger LOG = LoggerFactory
			.getLogger(OsobljeService.class);
	private static final String OSOBLJE_TABLE = "ev_osoblje";
	private static final String RADNO_MJESTO_COLUMN = "radno_mjesto";

	@Autowired
	private EvOsobljeRepository evOsobljeRepository;
	@Autowired
	private EvSifarnikRepository evSifarnikRepository;
	@Autowired
	private EvOsobljeBrOsobaRepository evOsobljeBrOsobaRepository;

	public List<EvOsoblje> getOsobljeForReportingPeriod(
			final ReportingPeriod reportingPeriod) {
		return evOsobljeRepository.findByReportingPeriod(reportingPeriod);
	}

	public List<EvOsoblje> getFreshOsobljeList(
			final ReportingPeriod reportingPeriod) {
		LOG.debug("... getting fresh list for osoblje on project: {} ...",
				reportingPeriod);
		List<EvSifarnik> radnaMjesta = evSifarnikRepository
				.findByRefTableAndRefColumn(OSOBLJE_TABLE, RADNO_MJESTO_COLUMN);

		List<EvOsoblje> osoblje = new ArrayList<>();
		for (EvSifarnik evSifarnik : radnaMjesta) {
			EvOsoblje row = new EvOsoblje(evSifarnik, reportingPeriod);
			// LOG.debug("... adding row for radno_mjesto: {}",
			// evSifarnik.getDescription());
			osoblje.add(row);
		}

		return osoblje;
	}

	public List<EvOsoblje> saveOsobljeOnProject(List<EvOsoblje> osoblje) {
		return evOsobljeRepository.save(osoblje);
	}
	
	public EvOsoblje saveOsobljeOnProject(EvOsoblje osoblje) {
		return evOsobljeRepository.save(osoblje);
	}
	
	public EvOsobljeBrOsoba findOsobljeBrOsoba(final ReportingPeriod reportingPeriod) {
		return evOsobljeBrOsobaRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public EvOsobljeBrOsoba saveOsobljeBrOsoba(final EvOsobljeBrOsoba evOsobljeBrOsoba){
		return evOsobljeBrOsobaRepository.save(evOsobljeBrOsoba);
	}
}
