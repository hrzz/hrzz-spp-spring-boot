/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvUsavrsavanje;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvUsavrsavanjeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Service
public class EvUsavrsavanjeService {

	private final EvUsavrsavanjeRepository evUsavrsavanjeRepository;
	
	@Autowired
	public EvUsavrsavanjeService(final EvUsavrsavanjeRepository evUsavrsavanjeRepository) {
		this.evUsavrsavanjeRepository = evUsavrsavanjeRepository;
	}
	
	public List<EvUsavrsavanje> getEvUsavrsavanje(){
		return evUsavrsavanjeRepository.findAll();
	}
	
	public List<EvUsavrsavanje> getEvUsavrsavanjeByPeriod(ReportingPeriod reportingPeriod){
		return evUsavrsavanjeRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveUsavrsavanje(EvUsavrsavanje evUsavrsavanje){
		evUsavrsavanjeRepository.save(evUsavrsavanje);
	}
	public void deleteUsavrsavanje(EvUsavrsavanje evUsavrsavanje){
		evUsavrsavanjeRepository.delete(evUsavrsavanje);
	}
}
