package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.business.SifarnikEnum;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Rezultat;
import hr.btb.hrzzspp.jpa.SifarnikInterface;
import hr.btb.hrzzspp.spring.repository.DiseminacijeRezultatSifarnikRepository;
import hr.btb.hrzzspp.spring.repository.FileRezultatRepository;
import hr.btb.hrzzspp.spring.repository.PostignutoRezultatSifarnikRepository;
import hr.btb.hrzzspp.spring.repository.RezultatRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RezultatService {

	private final RezultatRepository rezultatRepository;
	private final FileRezultatRepository fileRezultatRepository;
	private final DiseminacijeRezultatSifarnikRepository diseminacijeRezultatSifarnikRepository;
	private final PostignutoRezultatSifarnikRepository postignutoRezultatSifarnikRepository;

	@Autowired
	public RezultatService(
			final RezultatRepository rezultatRepository,
			final FileRezultatRepository fileRezultatRepository,
			final DiseminacijeRezultatSifarnikRepository diseminacijeRezultatSifarnikRepository,
			final PostignutoRezultatSifarnikRepository postignutoRezultatSifarnikRepository) {
		this.rezultatRepository = rezultatRepository;
		this.fileRezultatRepository = fileRezultatRepository;
		this.diseminacijeRezultatSifarnikRepository = diseminacijeRezultatSifarnikRepository;
		this.postignutoRezultatSifarnikRepository = postignutoRezultatSifarnikRepository;
	}

	public List<Rezultat> getAllRezultat() {
		return rezultatRepository.findAll();
	}

	public List<Rezultat> getRezultatiByReportingPeriod(
			ReportingPeriod reportingPeriod) {
		return rezultatRepository.findByReportingPeriod(reportingPeriod);
	}

	public void saveRezultat(Rezultat rezultat) {
		rezultatRepository.save(rezultat);
	}

	public SifarnikInterface getSifarnikForRefAndCode(String ref, String code) {
		switch (ref) {
		case SifarnikEnum.Values.REZULTAT_DISEMINACIJA:
			return diseminacijeRezultatSifarnikRepository.findByCode(code);
		case SifarnikEnum.Values.REZULTAT_POSTIGNUTO:
			return postignutoRezultatSifarnikRepository.findByCode(code);
		}

		return null;
	}

	public void deleteFileRezultat(Integer id) {
		fileRezultatRepository.delete(id);
	}

	public void deleteRezultat(Rezultat selected) {
		rezultatRepository.delete(selected);
	}
}
