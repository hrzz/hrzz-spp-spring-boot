package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.business.ConstantsModel;
import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.spring.repository.MajorScientificFieldsRepository;
import hr.btb.hrzzspp.spring.repository.ProjectsRepository;
import hr.btb.hrzzspp.spring.repository.UserRolePeriodStatusRepository;
import hr.btb.hrzzspp.spring.security.SecurityChecker;
import hr.btb.hrzzspp.spring.view.SessionBean;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProjectsService {

	private static final Logger LOG = LoggerFactory
			.getLogger(ProjectsService.class);

	private final ProjectsRepository projectsRepository;
	private final MajorScientificFieldsRepository msfRepository;
	private final UserRolePeriodStatusRepository userRolePeriodStatusRepository;

	@Autowired
	public ProjectsService(final ProjectsRepository projectsRepository,
			final MajorScientificFieldsRepository msfRepository,
			final UserRolePeriodStatusRepository userRolePeriodStatusRepository) {
		this.projectsRepository = projectsRepository;
		this.msfRepository = msfRepository;
		this.userRolePeriodStatusRepository = userRolePeriodStatusRepository;
		LOG.debug("... ProjectsService construct ...");
	}

	public List<Project> getProjects() {
		return projectsRepository.findAll();
	}
	
	public Project getProjectById(long id){
		return projectsRepository.findOne(id);
	}

	public List<Project> getMsfFilteredProjects(SessionBean session) {

		MajorScientificField msf = msfRepository.findOne(session.getFilteredProjectByScientificField());
		
		if (SecurityChecker.hasRoles(new String[] { "PRK" })) {

			if (msf.getCode().equals("ALL"))
				return projectsRepository.findAll();
			else
				return projectsRepository.findByMajorScientificField(msf);

		} else if (SecurityChecker.hasRoles(new String[] { "VOD" })) {

			if (msf.getCode().equals("ALL")) {
				return projectsRepository.findByLeaderId(session
						.getCurrentUserData().getUser().getId());
			} else {
				return projectsRepository
						.findByMajorScientificFieldAndLeaderId(msf, session
								.getCurrentUserData().getUser().getId());
			}
		} else {
			Role role = new Role();
			if (SecurityChecker.hasRoles(new String[] { "EVL" })) {
		
				role.setId(ConstantsModel.EVL);
			
			} else if (SecurityChecker.hasRoles(new String[] { "COV" })) {
				
				role.setId(ConstantsModel.COV);
			
			} else if (SecurityChecker.hasRoles(new String[] { "CUO" })) {
				
				role.setId(ConstantsModel.CUO);
			
			} else {
				return new ArrayList<Project>();
			}

			List<UserRolePeriodStatus> listUserRolePeriodStatus = userRolePeriodStatusRepository.findByUserAndRole(session.getCurrentUserData().getUser() , role);
			List<Project> listProject = new ArrayList<Project>();
			int i = 0;
			for(UserRolePeriodStatus userRolePeriodStatus : listUserRolePeriodStatus){
				i++;
				listProject.add(userRolePeriodStatus.getPeriod().getProject());
				for(Project pIzListe : listProject){
					if(i > 1 && pIzListe.getId().equals(userRolePeriodStatus.getPeriod().getProject().getId())){
						listProject.remove(userRolePeriodStatus.getPeriod().getProject());
					}
				}
			}
			return listProject;
		}
			
	}
	
	public void saveProject(Project project){
		projectsRepository.save(project);
	}
	
	public List<Project> findProjectsByUserId(final int userId){
		return projectsRepository.findProjectsByUserId(userId);
	}
	
	public List<Project> findByLeaderId(final int userId){
		return projectsRepository.findByLeaderId(userId);
	}
	
}