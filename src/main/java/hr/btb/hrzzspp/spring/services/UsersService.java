package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.jpa.UserData;
import hr.btb.hrzzspp.spring.repository.UserDataRepository;
import hr.btb.hrzzspp.spring.repository.UsersRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UsersService {

	private static final Logger LOG = LoggerFactory
			.getLogger(ProjectsService.class);

	private final UsersRepository usersRepository;
	private final UserDataRepository userDataRepository;

	@Autowired
	public UsersService(final UsersRepository usersRepository,
			final UserDataRepository userDataRepository) {
		LOG.debug("... Users Service contruct ...");
		this.usersRepository = usersRepository;
		this.userDataRepository = userDataRepository;
	}

	public List<User> getUsers(String username) {
		return usersRepository.findByUsername(username);
	}

	public User getUserById(int id) {
		return usersRepository.findById(id);
	}

	public List<User> getAllUsers() {
		return usersRepository.findAll();
	}

	public UserData getUserDataByUserId(long id) {
		return userDataRepository.findByUser(usersRepository.findOne(id));
	}
	
	public UserData getUserDataByUser(User user){
		return userDataRepository.findByUser(user);
	}
	
	public void saveUser(User user) {
		usersRepository.save(user);
	}
}
