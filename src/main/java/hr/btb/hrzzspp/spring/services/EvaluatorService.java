package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.Evaluator;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvaluatorRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvaluatorService {

	private static final Logger LOG = LoggerFactory
			.getLogger(EvaluatorService.class);
	private final EvaluatorRepository evaluatorRepository;

	@Autowired
	public EvaluatorService(final EvaluatorRepository evaluatorRepository) {
		this.evaluatorRepository = evaluatorRepository;
	}

	public List<Evaluator> getEvaluator() {
		return evaluatorRepository.findAll();
	}

	public void saveEvaluator(Evaluator evaluator) {
		evaluatorRepository.save(evaluator);
	}

	public List<Evaluator> getEvaluatorByPeriod(ReportingPeriod period) {
		return evaluatorRepository.findByPeriod(period);
	}
	
	public List<Evaluator> getEvaluatorByPeriodAndEmail(ReportingPeriod period, String email) {
		return evaluatorRepository.findByPeriodAndEmail(period, email);
	}
	
	public List<Evaluator> getEvaluatorByEmail(String email) {
		return evaluatorRepository.findByEmail(email);
	}

	public EvaluatorRepository getEvaluatorRepository() {
		return evaluatorRepository;
	}
}
