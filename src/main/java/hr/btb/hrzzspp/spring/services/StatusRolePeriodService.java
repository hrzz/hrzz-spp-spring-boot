package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.StatusRolePeriod;
import hr.btb.hrzzspp.spring.repository.StatusRolePeriodRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class StatusRolePeriodService {

	private final StatusRolePeriodRepository statusRolePeriodRepository;
	
	@Autowired
	public StatusRolePeriodService(final StatusRolePeriodRepository statusRolePeriodRepository) {
		this.statusRolePeriodRepository = statusRolePeriodRepository;
	}
	
	public StatusRolePeriod findByPeriodAndRole(final ReportingPeriod reportingPeriod, final Role role){
		return statusRolePeriodRepository.findByPeriodAndRole(reportingPeriod, role);
	}
	
	public void save(StatusRolePeriod statusRolePeriod){
		statusRolePeriodRepository.save(statusRolePeriod);
	}
}
