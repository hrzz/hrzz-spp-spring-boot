package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.UserRolePeriodStatus;
import hr.btb.hrzzspp.spring.repository.UserRolePeriodStatusRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class UserRolePeriodStatusService {

	final static private Logger LOG = LoggerFactory
			.getLogger(UserRolePeriodStatusService.class);

	private UserRolePeriodStatusRepository userRolePeriodRepository;

	public UserRolePeriodStatusService() {
	}

	@Autowired
	public UserRolePeriodStatusService(
			UserRolePeriodStatusRepository userRolePeriodRepository) {
		this.setUserRolePeriodRepository(userRolePeriodRepository);
		LOG.debug("... UserRolePeriodRepository construct ...");
	}
	
	public List<UserRolePeriodStatus> findByPeriodAndRole(ReportingPeriod period, Role role) {
		return userRolePeriodRepository.findByPeriodAndRole(period, role);
	}
	
	public List<UserRolePeriodStatus> findByPeriod(ReportingPeriod period) {
		return userRolePeriodRepository.findByPeriod(period);
	}
	
	public void saveUserRolePeriodStatus(UserRolePeriodStatus userRolePeriod) {
		userRolePeriodRepository.save(userRolePeriod);
	}

	public UserRolePeriodStatusRepository getUserRolePeriodRepository() {
		return userRolePeriodRepository;
	}

	public void setUserRolePeriodRepository(UserRolePeriodStatusRepository userRolePeriodRepository) {
		this.userRolePeriodRepository = userRolePeriodRepository;
	}
}
