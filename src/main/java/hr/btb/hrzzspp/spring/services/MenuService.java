package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.MajorScientificField;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MenuService {

	final private MajorScientificFieldsService msfService;

	@Autowired
	public MenuService(MajorScientificFieldsService msfService) {
		this.msfService = msfService;
	}
	
	public List<MajorScientificField> getAllMajorScientificFields() {
		return msfService.getAllMajorScientificFields();
	}
}
