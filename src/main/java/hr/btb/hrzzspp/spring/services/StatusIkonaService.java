/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.StatusIkona;
import hr.btb.hrzzspp.spring.repository.StatusIkonaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dalibor.harmina
 *
 */
@Service
public class StatusIkonaService {
	
	@Autowired
	private StatusIkonaRepository statusIkonaRepository;

	public List<StatusIkona> getAllStatusIkona() {
		return statusIkonaRepository.findAll();
	}
}
