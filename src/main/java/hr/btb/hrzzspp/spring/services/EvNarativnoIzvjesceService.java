package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.business.ConstantsEvidencije;
import hr.btb.hrzzspp.jpa.EvNarativnoIzvjesce;
import hr.btb.hrzzspp.jpa.EvNiFile;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvNarativnoIzvjesceRepository;
import hr.btb.hrzzspp.spring.repository.FileOIzvjesceRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvNarativnoIzvjesceService {

	private static final Logger LOG = LoggerFactory
			.getLogger(EvNarativnoIzvjesceService.class);
	private final EvNarativnoIzvjesceRepository evNarativnoIzvjesceRepository;
	private final FileOIzvjesceRepository fileOIzvjesceRepository;

	@Autowired
	public EvNarativnoIzvjesceService(
			final EvNarativnoIzvjesceRepository evNarativnoIzvjesceRepository,
			final FileOIzvjesceRepository fileOIzvjesceRepository) {
		this.evNarativnoIzvjesceRepository = evNarativnoIzvjesceRepository;
		this.fileOIzvjesceRepository = fileOIzvjesceRepository;
	}

	public List<EvNarativnoIzvjesce> getEvNarativnoIzvjesce() {
		return evNarativnoIzvjesceRepository.findAll();
	}

	public EvNarativnoIzvjesce getEvNarativnoIzvjesceByPeriod(
			ReportingPeriod reportingPeriod) {
		return evNarativnoIzvjesceRepository
				.findByReportingPeriod(reportingPeriod);
	}

	public EvNarativnoIzvjesce saveEvNarativnoIzvjesce(EvNarativnoIzvjesce evNarativnoIzvjesce) {
		return evNarativnoIzvjesceRepository.save(evNarativnoIzvjesce);
	}

	public void deleteEvNarativnoIzvjesce(
			EvNarativnoIzvjesce evNarativnoIzvjesce) {
		evNarativnoIzvjesceRepository.delete(evNarativnoIzvjesce);
	}

	public EvNiFile getFileByEvNiType(String evNiType,
			EvNarativnoIzvjesce evNarativnoIzvjesce) {

		LOG.debug("... getting fileByEvNiType: {} | {}...", evNiType,
				evNarativnoIzvjesce);

		EvNiFile evNiFile = null;
		switch (evNiType) {
		case ConstantsEvidencije.NI_O_IZVJESCE:
			evNiFile = fileOIzvjesceRepository
					.findByNarativnoIzvjesce(evNarativnoIzvjesce);
		}

		return evNiFile;
	}
}
