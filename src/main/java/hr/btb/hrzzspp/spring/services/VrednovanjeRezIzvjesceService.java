package hr.btb.hrzzspp.spring.services;

import java.util.List;

import hr.btb.hrzzspp.jpa.VrednovanjeRezIzvjesce;
import hr.btb.hrzzspp.spring.repository.VrednovanjeRezIzvjesceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class VrednovanjeRezIzvjesceService {

	private final VrednovanjeRezIzvjesceRepository vrednovanjeRezIzvjesceRepository;

	@Autowired
	public VrednovanjeRezIzvjesceService(
			final VrednovanjeRezIzvjesceRepository vrednovanjeRezIzvjesceRepository) {
		this.vrednovanjeRezIzvjesceRepository = vrednovanjeRezIzvjesceRepository;
	}

	public void saveRezultatiVrednovanjaIzvj(
			VrednovanjeRezIzvjesce vrednovanjeRezIzvjesce) {
		vrednovanjeRezIzvjesceRepository.save(vrednovanjeRezIzvjesce);
	}
	
	public List<VrednovanjeRezIzvjesce> findRezVredByPeriodUserLastModified(final int periodId){
		PageRequest pageR = new PageRequest(1,1);
		return vrednovanjeRezIzvjesceRepository.findRezVredByPeriodUserLastModified(periodId, pageR);
	}

}
