package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.ReportingPeriodsRepository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportingPeriodsService {

	private final ReportingPeriodsRepository reportingPeriodsRepository;

	@Autowired
	public ReportingPeriodsService(
			final ReportingPeriodsRepository reportingPeriodsRepository) {
		this.reportingPeriodsRepository = reportingPeriodsRepository;
	}

    public ReportingPeriod getReportingPeriodById(int id) {
        return reportingPeriodsRepository.findById(id);
    }
	
	public List<ReportingPeriod> getReportingPeriods() {
		return reportingPeriodsRepository.findAll();
	}

	public List<ReportingPeriod> getReportingPeriods(Project project) {
		return reportingPeriodsRepository.findByProject(project);
	}

	public void saveReportingPeriod(ReportingPeriod reportingPeriod) {
		reportingPeriodsRepository.save(reportingPeriod);
	}

	public Integer findMaxOrdNum(Project project) {
		return reportingPeriodsRepository.findMaxOrdNum(project);
	}

	public List<ReportingPeriod> findByPeriodInterval(Project pr,
			Timestamp refDate) {
		return reportingPeriodsRepository.findByPeriodInterval(pr, refDate);
	}
	
	public Timestamp findMaxDateFrom(Project pr){
		return reportingPeriodsRepository.findMaxDateTo(pr);
	}
	
	public Timestamp findMaxDateToExcludeCurr(Project pr, int currId){
		return reportingPeriodsRepository.findMaxDateToExcludeCurr(pr,currId);
	}
	
	public Timestamp findMinDateFromExcludeCurr(Project pr, int currId){
		return reportingPeriodsRepository.findMinDateFromExcludeCurr(pr,currId);
	}
	
	public Timestamp findMinReportingPeriodDateFrom(Project pr){
		return reportingPeriodsRepository.findMinReportingPeriodDateFrom(pr);
	}
}