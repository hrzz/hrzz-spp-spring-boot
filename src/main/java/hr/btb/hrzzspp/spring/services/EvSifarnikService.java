package hr.btb.hrzzspp.spring.services;

import java.util.List;

import hr.btb.hrzzspp.spring.repository.EvSifarnikRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hr.btb.hrzzspp.jpa.EvSifarnik;

@Service
public class EvSifarnikService {

	private final EvSifarnikRepository evSifarnikRepository;

	@Autowired
	public EvSifarnikService(final EvSifarnikRepository evSifarnikRepository) {
		this.evSifarnikRepository = evSifarnikRepository;
	}

	public List<EvSifarnik> getEvSifarnik(String table, String column) {
		return evSifarnikRepository.findByRefTableAndRefColumn(table, column);
	}

	public EvSifarnik getEvSifarnikRow(String table, String column, String code) {
		return evSifarnikRepository.findByRefTableAndRefColumnAndCode(table,
				column, code);
	}

	public List<EvSifarnik> getEvSifarnik() {
		return evSifarnikRepository.findAll();
	}
}
