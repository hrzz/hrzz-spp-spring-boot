package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.MajorScientificField;
import hr.btb.hrzzspp.spring.repository.MajorScientificFieldsRepository;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class MajorScientificFieldsService {

	final static private Logger LOG = LoggerFactory
			.getLogger(MajorScientificFieldsService.class);

	private MajorScientificFieldsRepository msfRepository;

	public MajorScientificFieldsService() {
	}

	@Autowired
	public MajorScientificFieldsService(
			MajorScientificFieldsRepository msfRepository) {
		this.msfRepository = msfRepository;
		LOG.debug("... MajorScientificFieldsService construct ...");
	}

	public List<MajorScientificField> getAllMajorScientificFields() {
		return msfRepository.findAll();
	}

	public String getMSFName(Integer id) {
		return getMSFById(id).getName();
	}
	
	public MajorScientificField getMSFById(Integer id) {
		return msfRepository.findOne(id);
	}
	
	public MajorScientificField getMDFByCode(String code){
		return msfRepository.findByCode(code);
	}
}
