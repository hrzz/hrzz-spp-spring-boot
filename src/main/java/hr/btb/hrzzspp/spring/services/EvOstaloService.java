package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvOstalo;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvOstaloRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvOstaloService {

	private final EvOstaloRepository evOstaloRepository;

	@Autowired
	public EvOstaloService(final EvOstaloRepository evOstaloRepository) {
		this.evOstaloRepository = evOstaloRepository;
	}

	public List<EvOstalo> getEvOstalo() {
		return evOstaloRepository.findAll();
	}

	public List<EvOstalo> getEvOstalo(ReportingPeriod rp) {
		return evOstaloRepository.findByReportingPeriod(rp);
	}
	public List<EvOstalo> getEvOstaloByPeriod(ReportingPeriod reportingPeriod){
		return evOstaloRepository.findByReportingPeriod(reportingPeriod);
	}

	public void saveOstalo(EvOstalo evOstalo) {

		evOstaloRepository.save(evOstalo);
	}
	public void deleteOstalo(EvOstalo evOstalo){
		evOstaloRepository.delete(evOstalo);
	}

}
