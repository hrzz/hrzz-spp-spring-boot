package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvPopisRadova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvPopisRadovaRepository;
import hr.btb.hrzzspp.spring.repository.FileRadRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvPopisRadovaService {

	private final EvPopisRadovaRepository evPopisRadovaRepository;
	private final FileRadRepository fileRadRepository;

	@Autowired
	public EvPopisRadovaService(
			final EvPopisRadovaRepository evPopisRadovaRepository,
			final FileRadRepository fileRadRepository) {
		this.evPopisRadovaRepository = evPopisRadovaRepository;
		this.fileRadRepository = fileRadRepository;
	}

	public List<EvPopisRadova> getEvPopisRadova() {
		return evPopisRadovaRepository.findAll();
	}

	public List<EvPopisRadova> getEvPopisRadovaByPeriod(
			ReportingPeriod reportingPeriod) {
		return evPopisRadovaRepository.findByReportingPeriod(reportingPeriod);
	}

	public void savePopisRadova(EvPopisRadova evPopisRadova) {
		evPopisRadovaRepository.save(evPopisRadova);
	}

	public void deleteEvPopisRadova(EvPopisRadova evPopisRadova) {
		evPopisRadovaRepository.delete(evPopisRadova);
	}
	
	public void deleteFileRad(int id) {
		fileRadRepository.delete(id);
	}
}
