package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvPatenata;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvPatenataRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author ivan.valetic
 *
 */

@Service
public class EvPatenataService {

	private final EvPatenataRepository evPatenataRepository;

	@Autowired
	public EvPatenataService(final EvPatenataRepository evPatenataRepository) {
		this.evPatenataRepository = evPatenataRepository;
	}

	public List<EvPatenata> getEvPatenata() {
		return evPatenataRepository.findAll();
	}

	public List<EvPatenata> getEvPatenataByPeriod(
			ReportingPeriod reportingPeriod) {
		return evPatenataRepository.findByReportingPeriod(reportingPeriod);
	}

	public void saveEvPatenata(EvPatenata evPatenata) {
		evPatenataRepository.save(evPatenata);
	}

	public void deleteEvPatenata(EvPatenata evPatenata) {
		evPatenataRepository.delete(evPatenata);
	}

}
