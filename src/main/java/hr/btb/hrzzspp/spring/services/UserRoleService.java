package hr.btb.hrzzspp.spring.services;

import java.util.List;

import hr.btb.hrzzspp.jpa.Project;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.SukobInteresa;
import hr.btb.hrzzspp.jpa.User;

import java.util.List;

import hr.btb.hrzzspp.jpa.Role;
import hr.btb.hrzzspp.jpa.UserRole;
import hr.btb.hrzzspp.spring.repository.UserRoleRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class UserRoleService {

	final static private Logger LOG = LoggerFactory
			.getLogger(UserRoleService.class);

	private UserRoleRepository userRoleRepository;

	public UserRoleService() {
	}

	@Autowired
	public UserRoleService(
			UserRoleRepository userRoleRepository) {
		this.setUserRoleRepository(userRoleRepository);
		LOG.debug("... UserRoleRepository construct ...");
	}
	
	public void saveUserRole(UserRole userRole) {
		userRoleRepository.save(userRole);
	}

	public UserRoleRepository getUserRoleRepository() {
		return userRoleRepository;
	}

	public void setUserRoleRepository(UserRoleRepository userRoleRepository) {
		this.userRoleRepository = userRoleRepository;
	}
	
	public List<UserRole> findByRole(Role role){
		return userRoleRepository.findByRole(role);
	}
	
	
	public List<UserRole> findRolesByUserId(final int userId){
		return userRoleRepository.findRoleByUserId(userId);
	}
	
}
