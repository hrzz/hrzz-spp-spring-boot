package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvDokDisertacije;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvDokDisertacijeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class EvDokDisertacijeService {

	private final EvDokDisertacijeRepository evDokDisertacijeRepository;
	
	@Autowired
	public EvDokDisertacijeService(final EvDokDisertacijeRepository evDokDisertacijeRepository) {
		this.evDokDisertacijeRepository = evDokDisertacijeRepository;
	}
	
	public List<EvDokDisertacije> getEvDokDisertacije(){
		return evDokDisertacijeRepository.findAll();
	}
	
	public List<EvDokDisertacije> getEvDokDisertacijeByPeriod(ReportingPeriod reportingPeriod){
		return evDokDisertacijeRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveDokDisertacije(EvDokDisertacije evDokDisertacije){
		evDokDisertacijeRepository.save(evDokDisertacije);
	}
	
	public void deleteDokDisertacije(EvDokDisertacije evDokDisertacije){
		evDokDisertacijeRepository.delete(evDokDisertacije);
	}
}
