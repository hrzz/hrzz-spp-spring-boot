package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvPopisZnRadova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvPopisZnRadovaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvPopisZnRadovaService {
	private final EvPopisZnRadovaRepository evPopisZnRadovaRepository;

	@Autowired
	public EvPopisZnRadovaService(final EvPopisZnRadovaRepository evPopisZnRadovaRepository){
		this.evPopisZnRadovaRepository = evPopisZnRadovaRepository;
	}
	
	public List<EvPopisZnRadova> getEvPopisZnRadova(){
		return evPopisZnRadovaRepository.findAll();
	}
	
	public List<EvPopisZnRadova> getEvPopisZnRadovaByPeriod(ReportingPeriod reportingPeriod){
		return evPopisZnRadovaRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveEvPopisZnRadova(EvPopisZnRadova evPopisZnRadova){
		evPopisZnRadovaRepository.save(evPopisZnRadova);
	}
	
	public void deleteEvPopisZnRadova(EvPopisZnRadova evPopisZnRadova){
		evPopisZnRadovaRepository.delete(evPopisZnRadova);
	}
}
