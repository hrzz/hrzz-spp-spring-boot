package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvSuradnje;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvSuradnjeRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class EvSuradnjeService {
	
	/**
	 * 
	 */
	private final EvSuradnjeRepository evSuradnjeRepository;
	
	@Autowired
	public EvSuradnjeService(final EvSuradnjeRepository evSuradnjeRepository){
		this.evSuradnjeRepository = evSuradnjeRepository;
	}
	
	public List<EvSuradnje> getEvSuradnje(){
		return evSuradnjeRepository.findAll();
	}
	
	public List<EvSuradnje> getEvSuradnjeByPeriod(ReportingPeriod reportingPeriod){
		return evSuradnjeRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveEvSuradnje(EvSuradnje evSuradnje){
		evSuradnjeRepository.save(evSuradnje);
	}
	
	public void deleteEvSuradnje(EvSuradnje evSuradnje){
		evSuradnjeRepository.delete(evSuradnje);
	}
}
