/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvPopisDisAktivnosti;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvPopisDisAktivnostiRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ivan.valetic
 */

@Service
public class EvPopisDisAktivnostiService {
	
	private final EvPopisDisAktivnostiRepository evPopisDisAktivnostiRepository;
	
	@Autowired
	public EvPopisDisAktivnostiService(final EvPopisDisAktivnostiRepository evPopisDisAktivnostiRepository) {
		this.evPopisDisAktivnostiRepository = evPopisDisAktivnostiRepository;
	}
	
	public List<EvPopisDisAktivnosti> getEvPopisDisAktivnosti(){
		return evPopisDisAktivnostiRepository.findAll();
	}
	
	public List<EvPopisDisAktivnosti> getEvPopisDisAktivnostiByPeriod(ReportingPeriod reportingPeriod){
		return evPopisDisAktivnostiRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void savePopisDisAktivnosti(EvPopisDisAktivnosti evPopisDisAktivnosti){
		evPopisDisAktivnostiRepository.save(evPopisDisAktivnosti);
	}

	public void deletePopisDisAktivnosti(EvPopisDisAktivnosti evPopisDisAktivnosti){
		evPopisDisAktivnostiRepository.delete(evPopisDisAktivnosti);
	}
}
