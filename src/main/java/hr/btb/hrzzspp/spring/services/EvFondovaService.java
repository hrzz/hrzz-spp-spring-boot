package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvFondova;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvFondovaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvFondovaService {

	/**
	 * 
	 */
	private final EvFondovaRepository evFondovaRepository;
	
	@Autowired
	public EvFondovaService(final EvFondovaRepository evFondovaRepository) {
		this.evFondovaRepository = evFondovaRepository;
	}
	
	public List<EvFondova> getEvFondova(){
		return evFondovaRepository.findAll();
	}
	
	public List<EvFondova> getEvFondovaByPeriod(ReportingPeriod reportingPeriod){
		return evFondovaRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveFondova(EvFondova evFondova){
		evFondovaRepository.save(evFondova);
	}
	public void deleteFondova(EvFondova evFondova){
		evFondovaRepository.delete(evFondova);
	}
}
