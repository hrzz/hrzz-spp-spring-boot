package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.jpa.SukobInteresa;
import hr.btb.hrzzspp.jpa.User;
import hr.btb.hrzzspp.spring.repository.SukobInteresaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SukobInteresaService {
	
	private final SukobInteresaRepository sukobInteresaRepository;
	
	@Autowired
	public SukobInteresaService(final SukobInteresaRepository sukobInteresaRepository){
		this.sukobInteresaRepository = sukobInteresaRepository;
	}
	
	public void saveSukobInteresa(SukobInteresa sukobInteresa){
		sukobInteresaRepository.save(sukobInteresa);
	}
	
	public SukobInteresa findSukobInterByPeriodLastModified(final int periodId){
		return sukobInteresaRepository.findSukobInterByPeriodLastModified(periodId);
	}
	
	public List<SukobInteresa> getObrazacSukobInteresaByUserAndPeriod(User user, ReportingPeriod reportingPeriod){
		return sukobInteresaRepository.findByUserAndPeriod(user, reportingPeriod);
	}
	
}
