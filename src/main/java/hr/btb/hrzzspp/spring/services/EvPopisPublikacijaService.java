package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.EvPopisPublikacija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvPopisPublikacijaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EvPopisPublikacijaService {

	private final EvPopisPublikacijaRepository evPopisPublikacijaRepository ;

	@Autowired
	public EvPopisPublikacijaService(final EvPopisPublikacijaRepository evPopisPublikacijaRepository) {
		this.evPopisPublikacijaRepository = evPopisPublikacijaRepository;
	}

	public List<EvPopisPublikacija> getEvPopisPublikacija() {
		return evPopisPublikacijaRepository.findAll();
	}
	
	public List<EvPopisPublikacija> getEvPopisPublikacijaByPeriod(ReportingPeriod reportingPeriod){
		return evPopisPublikacijaRepository.findByReportingPeriod(reportingPeriod);
	}

	public void saveEvPopisPublikacija(EvPopisPublikacija evPopisPublikacija) {
		evPopisPublikacijaRepository.save(evPopisPublikacija);
	}
	
	public void deleteEvPopisPublikacija(EvPopisPublikacija evPopisPublikacija){
		evPopisPublikacijaRepository.delete(evPopisPublikacija);
	}
}
