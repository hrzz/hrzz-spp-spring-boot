/**
 * 
 */
package hr.btb.hrzzspp.spring.services;

import hr.btb.hrzzspp.jpa.Evidencija;
import hr.btb.hrzzspp.jpa.ReportingPeriod;
import hr.btb.hrzzspp.spring.repository.EvidencijaRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author dalibor.harmina
 *
 */
@Service
public class EvidencijaService {

	private final EvidencijaRepository evidencijaRepository;

	@Autowired
	public EvidencijaService(final EvidencijaRepository evidencijaRepository) {
		this.evidencijaRepository = evidencijaRepository;
	}
	
	public List<Evidencija> getEvidencija(){
		return evidencijaRepository.findAll();
	}
	
	public List<Evidencija> getEvidencijaByPeriod(ReportingPeriod reportingPeriod){
		return evidencijaRepository.findByReportingPeriod(reportingPeriod);
	}
	
	public void saveEvidencija(Evidencija evidencija){
		evidencijaRepository.save(evidencija);
	}
}
